# MIGRATION GUIDES
This part describes the most frictionless way to migrate between pod versions

## 0.1 -> 0.1.1
#### Pods
At pods you can remove *!use_frameworks* directive. Now it can be linked directly.

after that *pod update Briefly* will checkout new version.

#### App delegate
At initialization you should replace
```objective-c
@import Briefly;

...

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[Briefly sharedInstance] configureWithApiKey:@"your_api_key" widgetsOn:NO];
    // Other initialization code
    return YES;
}
```

to

```objective-c
#import <Briefly/Briefly.h>

...

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Briefly configureWithApiKey:@"api_key" widgetsOn:NO];
    // Other initialization code
    return YES;
}
```

#### Widgets global properties
Were:
```
BOOL areWidgetsShown = [[Briefly sharedInstance] visualizingStatisticOn];
[[Briefly sharedInstance] setVisualizingStatisticOn:YES];
```
Now:
```
BOOL areWidgetsShown = [Briefly widgetsOn];
[Briefly setWidgetsOn:YES];
```

#### Widgets

Change instances' classes from **BFPurchasesStatisticLabel** to **BFStatisticsLabel** and modify properties in this way:

* **eventId** property now became **analyticsObjectId**. So move all values in storyboards and code to new field
* set string property **analyticsDefaultProperty** to "*sold.total(month)*"
* and finally setup property **widgetGroup** with some meaningful ios-client specific value (like 'main_view_cell_widget'). Be aware of using special symbols in this id. Allowed: underscore(_), capital letters ( A – Z ), small letters ( a – z ), digits ( 0 – 9 )


