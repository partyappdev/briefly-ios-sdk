# Heatmaps intergation guide

## Начало работы
Для начала работы с тепловыми картами необходимо интегрировать библиотеку в приложение. Прочесть об этом можно в README.md.

## Режимы работы Briefly
Библиотека умеет работать в двух основных режимах:

- Виджеты администратора (плашки + тепловые карты) включены / сбор нажатий не происходит
- Виджеты администратора выключены / нажатия собираются и отправляются на сервер

Соответственно простейший вариант реализации - это иметь две сборки приложения:

- С выключенными виджетами для конецного пользователя. Код инициализации имеет такой вид:
```
[Briefly configureWithApiKey:@"api_key" widgetsOn:NO];
```

- С включенными виджетами для администраторов:
```
[Briefly configureWithApiKey:@"api_key" widgetsOn:YES];
```

Можно реализовать и в виде одной сборки, но на уровне приложения проверять, нужно ли включать виджеты исходя из бизнес логики (напрмиер для захардкоженных user_id). В этом случае библиотека изначально инициализируется с флагом **widgetsOn** == NO, и уже после в рантайме, если обнарудивается, что текущий пользователь администратор - вызывается
```
[Briefly setWidgetsOn:YES];
```

## Настройка сбора данных
### Идентификаторы экранов
Для большинства экранов дополнительная настройка не обязательно. Окна идентифицируются по классу текущего контроллера. Если для каждого окна в системе у вас используется отдельный класс - то статистика будет собираться без настроек. Но есть случаи в которых лучше айди окна установить самостоятельно:

- Если используется UIKit'овский класс без сабклассинга
- Один и тот же класс контроллера используется в несколкьих местах
- Очень нетривиальная иерархия вложенных в контейнеры контроллеров

Сделать это можно задав в IB или коде контроллеру параметр:
```
[controller setTrackingId:@"main_screen_03"];
```
### Идентификаторы для динамического контента
Помимо этого для каждого отдельного обьекта UIView можно настроить отдельный от основного экрана сбор нажатий. Это особенно необходимо для recycling views таких, как **UITableViewCell**, **UICollectionViewCell**: для них айди трекинга генерится автоматически на базе класса этой ячейки (если этот класс был определен). Но вы можете задать этот айди и вручную (причем сделать это можно для любого потомка UIView), выставив проперти:
```
[view setTrackingId:@"cell_item_2039289"];
```

### Пример

![picture](img/heatmap-handlings.png)

В данном примере для каждой из ячеек выставлен уникальный trackingId - а значит нажатия на них отправляются на отдельные тепловые карты. Когда как для хидеров trackingId не выставлены, следовательно эти нажатия отправляются на одну и ту же карту. Те нажатия, которые не попали ни в один UIView с trackingId - привязываются к экрану.


## Отладка
Отлаживать настройку всех trackingId удобнее всего на сборке с включенным интерфейсом администратора (widgetsOn == YES). В нем можно включить локлаьный сбор тепловых карт, который эмулирует сбор и обработку карт на сервере локально, что позволяет сразу видеть результаты нажатий. Чтобы включить этот режим необходимо:

- В сборке с включенным интерфейсом администрирования произвести одновременное нажатие тремя пальцами на экран (это откроет настройки)
- В открышвемся меню переключатель **debug touch heatmaps mode** перевести в положение **debug**. **Select widgets to be shown** же должен быть в состоянии **Widgets+touches**
- Теперь все нажатия будут отображаться в интерфейсе в реальном времени, как если бы они уходили на сервер, и возвращались в виде результирующей тепловой карты
- На следущем запуске библиотека выйдет из **debug** режима (а надо ли?)

## Совместимость с другими библиотеками

Библиотека активно ипользует Method Swizzling, что означает, что если кто-то еще им активно пользуется может возникнуть конфликт (напрмер Firebase в хвост и гриву его гоняет для детекта переходов между экранами). По идее с Firebase мы перехватываем разные методы, но с другими сервисами аналитики эта либа не тестировалась.

