//
//  Briefly.h
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    BFStatisticsPeriodPreviousDay,
    BFStatisticsPeriodPreviousWeek,
    BFStatisticsPeriodPreviousMonth
} BFStatisticsPeriod;

typedef enum : NSUInteger {
    BFMetricMissingDataTreatingPolicyNoData,
    BFMetricMissingDataTreatingPolicyNoChanges
} BFMetricMissingDataTreatingPolicy;

typedef enum : NSUInteger {
    BFHeatmapsModeNormal, // with widgets ON: show/not_handle, OFF: don't-show/handle
    BFHeatmapsModeDebug, // with widgets ON (show/handle), OFF: don't-show/handle
    BFHeatmapsModeDebugLocal, // with widgets ON (show/handle), OFF: don't-show/handle
} BFHeatmapsMode;

typedef enum : NSUInteger {
    BFStageInternalDevelopment,
    BFStageTest,
    BFStageLive
} BFStage;

@interface Briefly : NSObject


/*
    This call initializes Briefly library for tracking touches + widgets visualization.
    It should be executed on app initialization
 
    apiKey - your public api key
    accessToken - optional JWT access token of administrator
 */
+(void)configureWithApiKey:(NSString* _Nonnull)apiKey;
+(void)configureWithApiKey:(NSString* _Nonnull)apiKey accessToken:(NSString* _Nonnull)accessToken;

/*
    Set current app information
 
    version - version of this client. The statistics is not shared between
              different versions
    clientId - identificator of clinet ('ios-client' by default)

 */
+(void)setClientId:(NSString* _Nonnull)clientId version:(NSString* _Nonnull)version;

/*
    Set current user's id for more dateiled tracking
 */
+(void)setUserId:(NSString* _Nullable)userId;

/*
    Per screen tracking option tells briefly, if you want to distignuish recorded touches made for view with the same trackingId on different screens.
 
    By default it's set to YES
 */
+(void)setPerScreenTrackingIdOn:(BOOL)perScreenTrackingId;


/*
    Try to handle access tokens, send to this user
    Returns YES, if successfuly recognized the access token URL
 */
+(BOOL)handleOpeningUrl:(NSURL* _Nullable)url withOptions:(NSDictionary<UIApplicationOpenURLOptionsKey,id> * _Nullable)options;

/*
    Add metric data
 
 */
-(void)addMetricData:(NSNumber* _Nonnull)data
         forObjectId:(NSString* _Nonnull)objectId
         forMetricId:(NSString* _Nonnull)metricId;

-(void)addMetricData:(NSNumber* _Nonnull)data
              atTime:(NSDate* _Nullable)date
         forObjectId:(NSString* _Nonnull)objectId
          objectName:(NSString* _Nullable)objectName
         objectGroup:(NSString* _Nullable)objectGroup
     objectGroupName:(NSString* _Nullable)objectGroupName
         forMetricId:(NSString* _Nonnull)metricId
          metricName:(NSString* _Nullable)metricName
          metricUnit:(NSString* _Nullable)metricUnit
defaultClientRequest:(NSString* _Nullable)defaultRequest
   missingDataPolicy:(BFMetricMissingDataTreatingPolicy)missingDataPolicy;

// debugging
+(void)turnOnDebugLogging;
+(void)turnOffDebugLogging;
+(void)setHeatmapsDebugMode:(BFHeatmapsMode)mode;
+(void)setStage:(BFStage)stage;


//
// Access tokens management
//
+(NSString* _Nullable)currentAccessToken;
+(void)setAccessToken:(NSString* _Nullable)accessToken;


// depricated
+(void)configureWithApiKey:(NSString* _Nonnull)apiKey widgetsOn:(BOOL)widgetsOn;
+(BOOL)widgetsOn;

@end
