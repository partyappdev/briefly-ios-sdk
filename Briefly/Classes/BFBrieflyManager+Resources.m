//
//  BFBrieflyManager+Resources.m
//  Pods
//
//  Created by Artem Bondar on 14.12.2017.
//
//

#import "BFBrieflyManager+Resources.h"
#import "BFBrieflyManager+Private.h"

@implementation BFBrieflyManager (Resources)

+(NSBundle*)locateBundle
{
    /**
     Bundle location is inspired by Stripe
     Places to check:
     1. Briefly.bundle (for manual static installations, Fabric, and framework-less Cocoapods)
     2. Briefly.framework/Briefly.bundle (for framework-based Cocoapods)
     3. Briefly.framework (for Carthage, manual dynamic installations)
     4. main bundle (for people dragging all our files into their project)
    **/
    
    static NSBundle *ourBundle;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ourBundle = [NSBundle bundleWithPath:@"Briefly.bundle"];
        if (ourBundle == nil) {
            // This might be the same as the previous check if not using a dynamic framework
            NSString *path = [[NSBundle bundleForClass:[BFBrieflyManager class]] pathForResource:@"Briefly" ofType:@"bundle"];
            ourBundle = [NSBundle bundleWithPath:path];
        }
        
        if (ourBundle == nil) {
            // This will be the same as mainBundle if not using a dynamic framework
            ourBundle = [NSBundle bundleForClass:[BFBrieflyManager class]];
        }
        
        if (ourBundle == nil) {
            ourBundle = [NSBundle mainBundle];
        }
    });
    
    return ourBundle;
}

+(UIStoryboard*)baseStoryboard
{
    return [UIStoryboard storyboardWithName:@"ManagementUI" bundle:[BFBrieflyManager locateBundle]];
}

+(UIImage*)imageNamed:(NSString*)imageName
{
    NSBundle * bundle = [BFBrieflyManager locateBundle];
    UIImage *image = nil;
    if ([UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        image = [UIImage imageNamed:imageName inBundle:bundle compatibleWithTraitCollection:nil];
    }
    if (image == nil) {
        image = [UIImage imageNamed:imageName];
    }
    return image;
}

@end
