//
//  BFBrieflyHacks.h
//  Base64
//
//  Created by Artem Bondar on 15.02.2018.
//

#import <Foundation/Foundation.h>

@interface BFBrieflyHacks : NSObject

+(BOOL)hackIsBlacklistedHeatmapViewId:(NSString*)viewId;

@end
