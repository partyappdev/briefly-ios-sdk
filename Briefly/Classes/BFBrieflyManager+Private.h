//
//  BFBrieflyManager+Private.h
//  Base64
//
//  Created by Artem Bondar on 19.02.2018.
//

#import "BFBrieflyManager.h"
#import "CRQueue.h"

@interface BFBrieflyManager ()

-(void)makeApiCallToEndpoint:(NSString*)endpoint
                   parametrs:(NSDictionary*)parametrs
                    priority:(NSInteger)priority
                    logLevel:(BFLogLevel)logLevel
             needAccessToken:(BOOL)needAccessToken
                  andHandler:(void(^)(BOOL succeed, NSObject* params, NSObject* response))handler;

@property (strong, nonatomic) NSString * clientId;
@property (strong, nonatomic) NSString * version;

@end

@interface BFBrieflyManager (ApiWrapperPrivate)

-(CRQueueSquashingLogic)squashingLogic;

-(NSString*)currentAccessToken;
-(void)setCurrentAccessToken:(NSString*)accessToken;

@end
