//
//  BFSortableDataSource.h
//  Briefly
//
//  Created by Artem Bondar on 23.03.2018.
//

#import <UIKit/UIKit.h>

@class BFSorter;

@protocol BFSortableDataSource <NSObject>

-(void)sortableDataSourceEnumerateAllSortableObjects:(void(^)(NSIndexPath *indexPath, NSString *objectId))enumerationHandler;
-(NSString*)sortableDataSourceWidgetGroupId;
-(void)sortableDataSourceSortOrderChanged;

@optional
-(NSString*)sortableDataSourceAnyObjectId;

@end

@protocol BFSortableController

-(BFSorter*)sortableControllerSorter;

@end
