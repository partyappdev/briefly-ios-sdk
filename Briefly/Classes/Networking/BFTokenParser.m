//
//  BFTokenParser.m
//  Briefly
//
//  Created by Artem Bondar on 06/02/2018.
//

#import "BFTokenParser.h"
#import "BFBrieflyManager+Resources.h"
#import <JWT/JWT.h>

@implementation BFTokenParser

+(NSDictionary*)keysFromToken:(NSString*)token
{
    return ([JWTBuilder decodeMessage:token].options(@(1)).decode)[@"payload"];
}

@end
