//
//  BFBrieflyManager+ApiWrapper.m
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "BFBrieflyManager+ApiWrapper.h"
#import "BFBrieflyManager+Private.h"
#import "BFViewModelMetricRequest.h"
#import "BFModelChartData.h"
#import "BFModelHeatMap.h"
#import "BFBrieflyHacks.h"

const NSString * kBFMissingPointsTreatingPolicyNoData = @"no_data";
const NSString * kBFMissingPointsTreatingPolicyNoChanges = @"no_cahnges";

//
// Priority levels: (90 - 1, 99 - 1, 100 - 1, 102 - 1)
// - 9 add touches (basic 1 channel)
// - 10 add metric data (basic 1 channel)
// - 90 - get heatmap (2 channels)
// - 99 - calculate widget metric number (3 channels)
// - 100 - calculate sort-order info (4 channels)
// - 102 - get metrics  chart (5 channels)
// - 103 - get object meta (for widget window)
//
@implementation BFBrieflyManager (ApiWrapper)

-(void)addToCardItemId:(NSString*)itemId
              itemName:(NSString*)itemName
             itemPrice:(NSDecimalNumber*)itemPrice
              currency:(NSString*)currency
{
    NSDictionary * params = @{@"item_id": itemId,
                              @"item_name": itemName,
                              @"one_item_cost": itemPrice,
                              @"count": @1,
                              @"item_currency": currency,
                              @"timestamp": @(self.currentTimestamp),
                              @"user_id": self.currentUserId};
    [self makeApiCallToEndpoint:@"shop/add_to_cart"
                      parametrs:params
                       priority:10
                       logLevel:BFLogLevelFatal
                needAccessToken:NO
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                           
                           // do nothing, because all logging will be done
                           // in make-api-call method
                           
                       }];
}

-(void)addMetricData:(NSNumber*)data
              atTime:(NSDate*)date
         forObjectId:(NSString*)objectId
          objectName:(NSString*)objectName
         objectGroup:(NSString*)objectGroup
     objectGroupName:(NSString*)objectGroupName
         forMetricId:(NSString*)metricId
          metricName:(NSString*)metricName
          metricUnit:(NSString*)metricUnit
defaultClientRequest:(NSString*)defaultRequest
   missingDataPolicy:(const NSString*)missingDataPolicy
         withHandler:(void(^)(BOOL))handler
{
    if (objectId == nil || metricId == nil || data == nil) {
        handler(NO);
        [self logRecord:@"Skipped add metric request, because of nil data" logLevel:BFLogLevelWarning];
        return;
    }
    date = date == nil ? [NSDate date] : date;
    id objectGroupObj = objectGroup == nil ? NSNull.null : objectGroup;
    id objectGroupNameObj = objectGroupName == nil ? NSNull.null : objectGroupName;
    id metricNameObj = metricName == nil ? NSNull.null : metricName;
    id metricUnitObj = metricUnit == nil ? NSNull.null : metricUnit;
    id defaultRequestObj = defaultRequest == nil ? NSNull.null : defaultRequest;
    id objectNameObj = objectName == nil ? NSNull.null : objectName;
    id missingDataPolicyObj = missingDataPolicy == nil ? kBFMissingPointsTreatingPolicyNoData : missingDataPolicy;
    NSInteger ts = [date timeIntervalSince1970] * 1000000;
    NSDictionary * params = @{@"object_id": objectId,
                              @"object_name": objectNameObj,
                              @"object_group": objectGroupObj,
                              @"object_group_name": objectGroupNameObj,
                              @"metric_id": metricId,
                              @"metric_name": metricNameObj,
                              @"unit": metricUnitObj,
                              @"ts": @(ts),
                              @"conflicted_ts_resolution_logic": @"rewrite",
                              @"missing_points_treating_policy": missingDataPolicyObj,
                              @"default_request": defaultRequestObj,
                              @"value": data,
                              };
    [self makeApiCallToEndpoint:@"events/add_metric_data"
                      parametrs:params
                       priority:10
                       logLevel:BFLogLevelFatal
                needAccessToken:NO
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                         
                         // do nothing, because all logging will be done
                         // in make-api-call method
                         
                     }];
}

-(void)removeFromCardItemId:(NSString*)itemId
              itemName:(NSString*)itemName
             itemPrice:(NSDecimalNumber*)itemPrice
              currency:(NSString*)currency
{
    NSDictionary * params = @{@"item_id": itemId,
                              @"item_name": itemName,
                              @"one_item_cost": itemPrice,
                              @"count": @1,
                              @"item_currency": currency,
                              @"timestamp": @(self.currentTimestamp),
                              @"user_id": self.currentUserId};
    [self makeApiCallToEndpoint:@"shop/remove_from_cart"
                      parametrs:params
                       priority:10
                       logLevel:BFLogLevelFatal
                needAccessToken:NO
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                           
                           // do nothing, because all logging will be done
                           // in make-api-call method
                           
                       }];
}

-(void)purchaseItemId:(NSString*)itemId
             itemName:(NSString*)itemName
            itemPrice:(NSDecimalNumber*)itemPrice
             currency:(NSString*)currency
           purchaseId:(NSString*)purchaseId
{
    // TODO: make a call, which do all this stuff at once,
    // without hacky adding to cart
    __weak __typeof__(self) weakSelf = self;
    NSDictionary * params = @{@"item_id": itemId,
                              @"item_name": itemName,
                              @"one_item_cost": itemPrice,
                              @"count": @1,
                              @"item_currency": currency,
                              @"timestamp": @(self.currentTimestamp),
                              @"user_id": self.currentUserId};
    [self makeApiCallToEndpoint:@"shop/add_to_cart"
                      parametrs:params
                       priority:10
                       logLevel:BFLogLevelFatal
                needAccessToken:NO
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                           
                           // and now make a purchase
                           if (succeed) {
                               [weakSelf purchaseWithPurchaseId:purchaseId];
                           }
                       }];
}

-(void)purchaseWithPurchaseId:(NSString*)purchaseId
{
    NSDictionary * params = @{@"purchase_id": purchaseId,
                              @"timestamp": @(self.currentTimestamp),
                              @"user_id": self.currentUserId};
    [self makeApiCallToEndpoint:@"shop/make_purchase"
                      parametrs:params
                       priority:10
                       logLevel:BFLogLevelFatal
                needAccessToken:NO
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                           
                           // do nothing, because all logging will be done
                           // in make-api-call method
                           
                       }];
}

-(void)purchaseCancelForPurchaseId:(NSString*)purchaseId
{
    NSDictionary * params = @{@"purchase_id": purchaseId,
                              @"timestamp": @(self.currentTimestamp),
                              @"user_id": self.currentUserId};
    [self makeApiCallToEndpoint:@"shop/cancel_purchase"
                      parametrs:params
                       priority:10
                       logLevel:BFLogLevelFatal
                needAccessToken:NO
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                           
                           // do nothing, because all logging will be done
                           // in make-api-call method
                           
                       }];
}

-(void)purchaseStatisticsForItemId:(NSString*)itemId
                            period:(BFStatisticsPeriod)period
                       withHandler:(void(^)(BOOL, NSNumber*))handler {
    
    if (period != BFStatisticsPeriodPreviousMonth) {
        [self logRecord:@"only month period is supported now :(" logLevel:BFLogLevelFatal];
        handler(NO, 0);
        return;
    }
    
    NSString * cahceKey = [NSString stringWithFormat:@"shop_items_statistics_last_month_total_%@", itemId];
    
    __weak __typeof__(self) weakSelf = self;
    [self.cache nonoutdatedCacheValueForKey:cahceKey withHandler:^(NSObject * cachedValue) {
        
        if (cachedValue != nil) {
            handler(YES, (NSNumber*)cachedValue);
            return;
        }

        // if we can't find value in cache - then ask service
        NSDictionary * params = @{@"item_id": itemId,
                                  @"values": @[@"last_month_total"]};
        [self makeApiCallToEndpoint:@"shop/items/statistics"
                          parametrs:params
                           priority:100
                           logLevel:BFLogLevelError
                    needAccessToken:YES
                         andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                             
                             NSDictionary * resultsDict = (NSDictionary*)results;
                             if ([resultsDict.allKeys containsObject:@"last_month_total"]) {
                                 
                                 NSNumber * total = resultsDict[@"last_month_total"];
                                 if (total != nil) {
                                     [weakSelf.cache setCacheValue:total forKey:cahceKey];
                                     handler(YES, total);
                                     return;
                                 }
                             }
                             
                             // Fatal - because WTF???
                             [weakSelf logRecord:[NSString stringWithFormat:@"unknown response came at purchaseStatisticsForItemId: %@", results] logLevel:BFLogLevelFatal];
                             handler(NO, 0);
                         }];
    }];
}

-(void)calculateMetricRequests:(NSArray<NSString*>*)requests
                  withPriority:(BFBrieflyApiPriority)priority
               withHandler:(void(^)(BOOL, NSArray<BFModelCalculateMetricResponseItem*>*))handler
{
    NSString * cacheKey = @"calculateMetricRequests";
    NSMutableArray<NSString*>* filteredArray = [NSMutableArray array];
    NSMutableArray * singleResultCaches = [NSMutableArray array];
    for (NSString * req in requests) {
        cacheKey = [cacheKey stringByAppendingString:@"+"];
        cacheKey = [cacheKey stringByAppendingString:req];
        [filteredArray addObject:req];
        [singleResultCaches addObject:[NSString stringWithFormat:@"cmr_s_%@", req]];
    }

    __weak __typeof__(self) weakSelf = self;
    [self.cache nonoutdatedCacheValueForKey:cacheKey withHandler:^(NSObject * cachedValue) {

        if (cachedValue != nil) {
            handler(YES, (NSArray<BFModelCalculateMetricResponseItem*>*)cachedValue);
            return;
        }
        
        [self.cache nonoutdatedCacheValuesAtOnceForKeys:singleResultCaches withHandler:^(NSArray *cachedResults) {
            if (cachedResults != nil) {
                handler(YES, (NSArray<BFModelCalculateMetricResponseItem*>*)cachedResults);
                return;
            }
            
            [self internalCalculateMetricRequests:filteredArray withPriority:priority withHandler:^(BOOL succeed, NSArray<BFModelCalculateMetricResponseItem *> * results) {
                
                if (succeed && results != nil) {
                    //
                    // add results to cache
                    //
                    [weakSelf.cache setCacheValue:results forKey:cacheKey validityTime:BFValididtyTimeIntervalHalfHour];
                    [results enumerateObjectsUsingBlock:^(BFModelCalculateMetricResponseItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSString * key = [NSString stringWithFormat:@"cmr_s_%@", obj.request];
                        [weakSelf.cache setCacheValue:obj forKey:key validityTime:BFValididtyTimeIntervalHalfHour];
                    }];
                }
                handler(succeed, results);
            }];
        }];
    }];
}

-(void)internalCalculateMetricRequests:(NSArray<NSString*>*)requests
                          withPriority:(BFBrieflyApiPriority)priority
                           withHandler:(void(^)(BOOL, NSArray<BFModelCalculateMetricResponseItem*>*))handler
{
    // if we can't find value in cache - then ask service
    NSInteger priorityInt = 0;
    switch (priority) {
        case BFBrieflyApiPriorityLow:
            priorityInt = 91;
            break;
        case BFBrieflyApiPriorityNormal:
            priorityInt = 99;
            break;
        case BFBrieflyApiPriorityHigh:
            priorityInt = 100;
            break;
    }
    __weak __typeof__(self) weakSelf = self;
    NSDictionary * params = @{@"requests": requests};
    [self makeApiCallToEndpoint:@"events/calculate_metric_requests"
                      parametrs:params
                       priority:priorityInt
                       logLevel:BFLogLevelError
                needAccessToken:YES
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                         
                         NSDictionary * resultsDict = (NSDictionary*)results;
                         if ([resultsDict.allKeys containsObject:@"results"]) {
                             NSArray<BFModelCalculateMetricResponseItem*>* res = [BFModelCalculateMetricResponseItem objectsFromJson:resultsDict[@"results"] byLeavingOnlyRequests:requests];
                             
                             if (res != nil) {
                                 [weakSelf logRecord:[NSString stringWithFormat:@"calculateMetricRequests: %@", res] logLevel:BFLogLevelDebug];
                                 handler(YES, res);
                                 return;
                             }
                         }
                         
                         // Fatal - because WTF???
                         [weakSelf logRecord:[NSString stringWithFormat:@"unknown response came at calculateMetricRequests: %@", results] logLevel:BFLogLevelFatal];
                         handler(NO, 0);
                     }];
}

-(void)getMetaForObjectWithId:(NSString*)objectId
                  withHandler:(void(^)(BOOL, BFModelObjectMeta*))handler
{
    NSString * cacheKey = [NSString stringWithFormat:@"getMetaForObjectWithId-%@", objectId];

    __weak __typeof__(self) weakSelf = self;
    [self.cache nonoutdatedCacheValueForKey:cacheKey
                                withHandler:^(NSObject * cachedValue) {
        
        if (cachedValue != nil) {
            handler(YES, (BFModelObjectMeta*)cachedValue);
            return;
        }
        
        // if we can't find value in cache - then ask service
        NSDictionary * params = @{@"object_id": objectId};
        [self makeApiCallToEndpoint:@"events/get_object_meta"
                          parametrs:params
                           priority:103
                           logLevel:BFLogLevelError
                    needAccessToken:YES
                         andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                             
                             NSDictionary * resultsDict = (NSDictionary*)results;
                             if ([resultsDict.allKeys containsObject:@"properties"] && [resultsDict.allKeys containsObject:@"events"]) {
                                 BFModelObjectMeta * ret = [BFModelObjectMeta objectFromJson:resultsDict];
                                 ret.objectId = objectId;
                                 
                                 [weakSelf.cache setCacheValue:ret forKey:cacheKey];
                                 handler(YES, ret);
                                 return;
                             }
                             
                             // Fatal - because WTF???
                             [weakSelf logRecord:[NSString stringWithFormat:@"unknown response came at getMetaForObjectWithId: %@", results] logLevel:BFLogLevelFatal];
                             handler(NO, 0);
                         }];
    }];
}


-(void)chartForRequests:(NSArray<NSString*>*)requests
            withHandler:(void(^)(BOOL, BFModelChartData*))handler
{
    __weak __typeof__(self) weakSelf = self;
    [self makeApiCallToEndpoint:@"events/metric_chart"
                      parametrs:@{@"requests": requests}
                       priority:102
                       logLevel:BFLogLevelError
                needAccessToken:YES
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                         
                         if (succeed) {
                             NSDictionary * resultsDict = (NSDictionary*)results;
                             BFModelChartData * obj = [BFModelChartData objectFromJson:resultsDict];
                             if (obj != nil) {
                                 handler(YES, obj);
                                 return;
                             }
                         }
                         
                         // Fatal - because WTF???
                         [weakSelf logRecord:[NSString stringWithFormat:@"unknown response came at chartForRequests: %@", results] logLevel:BFLogLevelFatal];
                         handler(NO, nil);
                     }];
}

-(void)addTouches:(NSArray<NSValue*>*)points
      orientation:(NSString*)orientation
           viewId:(NSString*)viewId
          groupId:(NSString*)groupId
            width:(NSInteger)width
           height:(NSInteger)height
      withHandler:(void(^)(BOOL))handler
{
    NSMutableArray * pointsFormatted = [NSMutableArray array];
    for (NSValue * value in points) {
        [pointsFormatted addObject:@{@"x": @([value CGPointValue].x),
                                     @"y": @([value CGPointValue].y),
                                     @"t": @"tap"}];
    }
    NSDateFormatter * formetter = [[NSDateFormatter alloc] init];
    formetter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    NSString * dateString = [NSString stringWithFormat:@"%@.000Z", [formetter stringFromDate:[NSDate date]]];
    NSDictionary * params = @{@"user_id": self.currentUserId != nil ? self.currentUserId : [NSNull null],
                              @"client_id": self.clientId,
                              @"client_version": self.version,
                              @"device_id": @"iphone",
                              @"date": dateString,
                              @"touches": @[@{
                                                @"orientation": orientation != nil ? orientation : NSNull.null,
                                                @"view_group_id": groupId != nil ? groupId : NSNull.null,
                                                @"view_id": viewId != nil ? viewId : NSNull.null,
                                  @"w": @(width),
                                  @"h": @(height),
                                  @"points": pointsFormatted
                              }]};
    
    __weak __typeof__(self) weakSelf = self;
    [self makeApiCallToEndpoint:@"heatmaps/add_touches"
                      parametrs:params
                       priority:9
                       logLevel:BFLogLevelError
                needAccessToken:NO
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                         
                         if (succeed) {
                             handler(YES);
                             return;
                         }

                         // Fatal - because WTF???
                         handler(NO);
                         [weakSelf logRecord:[NSString stringWithFormat:@"addTouches failed : %@", results] logLevel:BFLogLevelFatal];
                     }];
}


-(void)getHeatmapForViewId:(NSString*)viewId
               orientation:(NSString*)orientation
                     width:(NSInteger)width
                    height:(NSInteger)height
               withHandler:(void(^)(BOOL, BFModelHeatMap*))handler
{
    // HACK 1.0
    //
    // To speed up development process, we blacklist view,
    // which sends trashy data.
    // At the next release
    //if ([BFBrieflyHacks hackIsBlacklistedHeatmapViewId:viewId]) {
    //    handler(YES, nil);
    //    return;
    //}

    // if we can't find value in cache - then ask service
    NSDictionary * params = @{@"view_id": viewId,
                              @"orientation": orientation,
                              @"view_width": @(width),
                              @"view_height": @(height),
                              @"touch_type": @"tap"
                              };
    [self getHeatmapsWithRequests:@[params] withHandler:^(BOOL succeed, NSArray<BFModelHeatMap *> *res) {
        handler(succeed, res.firstObject);
    }];
}

-(void)getHeatmapsWithRequests:(NSArray<NSDictionary*>*)requests
                   withHandler:(void(^)(BOOL, NSArray<BFModelHeatMap*>*))handler
{
    __weak __typeof__(self) weakSelf = self;
    NSDictionary * params = @{@"client_id": self.clientId,
                              @"client_version": self.version,
                              @"format": @"base64",
                              @"requests" : requests
                              };
    [self makeApiCallToEndpoint:@"heatmaps/get_heatmap"
                      parametrs:params
                       priority:90
                       logLevel:BFLogLevelError
                needAccessToken:YES
                     andHandler:^(BOOL succeed, NSObject * parametrs, NSObject * results) {
                         
                         if (succeed && results) {
                             NSDictionary * resultsDict = (NSDictionary*)results;
                             NSArray * heatmaps = [resultsDict objectForKey:@"heatmaps"];
                             
                             //
                             // Handle squashing logic: here can
                             // come results for different requests
                             // so we should find only ours
                             //
                             NSArray<BFModelHeatMap*>* res = [weakSelf squashingLogicHeatmapsFromResults:heatmaps withRequests:requests];
                             handler(YES, res);
                             return;
                         }
                         
                         // Fatal - because WTF???
                         [weakSelf logRecord:[NSString stringWithFormat:@"unknown response came at getHeatmapForViewId: %@", results] logLevel:BFLogLevelFatal];
                         handler(NO, nil);
                     }];
}

-(NSArray*)squashingLogicHeatmapsFromResults:(NSArray<NSDictionary*>*)results withRequests:(NSArray<NSDictionary*>*)requests
{
    //
    // Handle squashing logic: here can
    // come results for different requests
    // so we should find only ours
    //
    NSMutableArray * indexes = [NSMutableArray array];
    for (NSDictionary<NSString*, id>* req in requests) {
        __block NSNumber * index;
        [results enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull heatmap, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString * reqViewId = [req objectForKey:@"view_id"];
            NSString * reqOrient = [req objectForKey:@"orientation"];
            NSNumber * reqWidth = [req objectForKey:@"view_width"];
            NSNumber * reqHeight = [req objectForKey:@"view_height"];
            
            if ([reqViewId isEqualToString:heatmap[@"view_id"]] &&
                [reqOrient isEqualToString:heatmap[@"orientation"]] &&
                [reqWidth isEqualToNumber:heatmap[@"view_width"]] &&
                [reqHeight isEqualToNumber:heatmap[@"view_height"]]) {
                index = @(idx);
                return;
            }
        }];
        
        if (index) {
            [indexes addObject:index];
        } else {
            [indexes addObject:@(-1)];
        }
    }
    
    // Now build the response
    NSMutableArray * responses = [NSMutableArray array];
    for (NSNumber * idx in indexes) {
        BFModelHeatMap * obj;
        
        if (idx.integerValue != -1) {
            obj = [BFModelHeatMap objectFromJson:results[idx.integerValue]];
            obj.base64Encoded = YES;
        }
        if (obj == nil) {
            obj = [BFModelHeatMap new];
        }
        [responses addObject:obj];
    }
    return responses;
}

@end

@implementation BFBrieflyManager (ApiWrapperPrivate)

-(CRQueueSquashingLogic)squashingLogic
{
    return ^NSObject*(NSObject* params1, NSString *taskTypeId1, NSObject* params2, NSString *taskTypeId2) {
        
        NSDictionary *pDict1 = (NSDictionary*)params1;
        NSDictionary *pDict2 = (NSDictionary*)params2;
        
        if (![taskTypeId1 isEqualToString:taskTypeId2]) {
            return nil;
        }
        
        // events/get_object_properties supports batch requests
        if ([taskTypeId1 isEqualToString:@"events/get_object_properties"]) {
            NSSet<NSString*> * requests1 = [[NSSet alloc] initWithArray:pDict1[@"requests"]];
            NSSet<NSString*> * requests2 = [[NSSet alloc] initWithArray:pDict2[@"requests"]];;
            
            if (![requests1 isEqualToSet:requests2]) {
                return nil;
            } else {
                return pDict1;
            }

            /*// Unite the requests
            NSSet * unitedProperties = [requests1 setByAddingObjectsFromSet:requests2];
            
            // Check, if not too much
            if (unitedProperties.count > 50) {
                return nil;
            }
            
            // Everything seems OK. Return the squashed parametrs set
            return @{@"requests": unitedProperties.allObjects};*/
        } else if ([taskTypeId1 isEqualToString:@"heatmaps/get_heatmap"]) {
            
            NSArray<NSDictionary*> *pDict1 = [(NSDictionary*)params1 objectForKey:@"requests"];
            NSArray<NSDictionary*> *pDict2 = [(NSDictionary*)params2 objectForKey:@"requests"];

            NSMutableArray * squashedDictionary = [NSMutableArray arrayWithArray:pDict1];
            BOOL haveChanges = NO;
            for (NSDictionary * dict2 in pDict2) {
                BOOL found = NO;
                for (NSDictionary * dict1 in pDict1) {
                    NSString * reqViewId = [dict1 objectForKey:@"view_id"];
                    NSString * reqOrient = [dict1 objectForKey:@"orientation"];
                    NSNumber * reqWidth = [dict1 objectForKey:@"view_width"];
                    NSNumber * reqHeight = [dict1 objectForKey:@"view_height"];
                    
                    if ([reqViewId isEqualToString:dict2[@"view_id"]] &&
                        [reqOrient isEqualToString:dict2[@"orientation"]] &&
                        [reqWidth isEqualToNumber:dict2[@"view_width"]] &&
                        [reqHeight isEqualToNumber:dict2[@"view_height"]]) {
                        found = YES;
                        break;
                    }
                }
                
                if (!found) {
                    haveChanges = YES;
                    [squashedDictionary addObject:dict2];
                }
            }
            if (haveChanges) {
                NSMutableDictionary * finalResults = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)params1];
                finalResults[@"requests"] = squashedDictionary;
                return finalResults;
            }
            return params1;
        }
        
        return nil;
    };
}

@end
