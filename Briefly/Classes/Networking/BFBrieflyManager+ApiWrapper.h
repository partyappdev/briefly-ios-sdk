//
//  BFBrieflyManager+ApiWrapper.h
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "BFBrieflyManager.h"
#import "BrieflyFacade.h"
#import "BFPublicModels.h"
#import "BFModelChartData.h"
#import "BFModelHeatMap.h"

@class BFModelTimePeriod;

extern const NSString * kBFMissingPointsTreatingPolicyNoData;
extern const NSString * kBFMissingPointsTreatingPolicyNoChanges;

typedef NS_ENUM(NSUInteger, BFBrieflyApiPriority) {
    BFBrieflyApiPriorityLow,
    BFBrieflyApiPriorityNormal,
    BFBrieflyApiPriorityHigh,
};

@interface BFBrieflyManager (ApiWrapper)

-(void)addToCardItemId:(NSString*)itemId
              itemName:(NSString*)itemName
             itemPrice:(NSDecimalNumber*)itemPrice
              currency:(NSString*)currency;

-(void)removeFromCardItemId:(NSString*)itemId
                   itemName:(NSString*)itemName
                  itemPrice:(NSDecimalNumber*)itemPrice
                   currency:(NSString*)currency;

-(void)purchaseItemId:(NSString*)itemId
             itemName:(NSString*)itemName
            itemPrice:(NSDecimalNumber*)itemPrice
             currency:(NSString*)currency
           purchaseId:(NSString*)purchaseId;

-(void)purchaseWithPurchaseId:(NSString*)purchaseId;
-(void)purchaseCancelForPurchaseId:(NSString*)purchaseId;
-(void)purchaseStatisticsForItemId:(NSString*)itemId
                            period:(BFStatisticsPeriod)period
                       withHandler:(void(^)(BOOL, NSNumber*))handler;

-(void)calculateMetricRequests:(NSArray<NSString*>*)requests
                  withPriority:(BFBrieflyApiPriority)priority
                   withHandler:(void(^)(BOOL, NSArray<BFModelCalculateMetricResponseItem*>*))handler;

-(void)getMetaForObjectWithId:(NSString*)objectId
                  withHandler:(void(^)(BOOL, BFModelObjectMeta*))handler;

-(void)chartForRequests:(NSArray<NSString*>*)requests
            withHandler:(void(^)(BOOL, BFModelChartData*))handler;

-(void)addTouches:(NSArray<NSValue*>*)points
      orientation:(NSString*)orientation
           viewId:(NSString*)viewId
          groupId:(NSString*)groupId
            width:(NSInteger)width
           height:(NSInteger)height
      withHandler:(void(^)(BOOL))handler;

-(void)addMetricData:(NSNumber*)data
              atTime:(NSDate*)date
         forObjectId:(NSString*)objectId
          objectName:(NSString*)objectName
         objectGroup:(NSString*)objectGroup
     objectGroupName:(NSString*)objectGroupName
         forMetricId:(NSString*)metricId
          metricName:(NSString*)metricName
          metricUnit:(NSString*)metricUnit
defaultClientRequest:(NSString*)defaultRequest
   missingDataPolicy:(const NSString*)missingDataPolicy
      withHandler:(void(^)(BOOL))handler;

-(void)getHeatmapForViewId:(NSString*)viewId
               orientation:(NSString*)orientation
                     width:(NSInteger)width
                    height:(NSInteger)height
               withHandler:(void(^)(BOOL, BFModelHeatMap*))handler;

@end
