//
//  BFTokenParser.h
//  Briefly
//
//  Created by Artem Bondar on 06/02/2018.
//

#import <Foundation/Foundation.h>

@interface BFTokenParser : NSObject

+(NSDictionary*)keysFromToken:(NSString*)token;

@end
