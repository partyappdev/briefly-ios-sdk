//
//  BFApiRequestEntity.h
//  Pods
//
//  Created by Artem Bondar on 16.11.17.
//
//

#import <Foundation/Foundation.h>
#import "BFBrieflyManager.h"

@interface BFApiRequestEntity : NSObject

@property (strong, nonatomic) NSString * endpoint;
@property (strong, nonatomic) NSDictionary * params;
@property (nonatomic) BFLogLevel logLevel;
@property (strong, nonatomic) void (^handler)(BOOL, NSDictionary*);

+(instancetype)requestWithEndpoint:(NSString*)endpoint
                         parametrs:(NSDictionary*)parametrs
                          logLevel:(BFLogLevel)logLevel
                        andHandler:(void(^)(BOOL, NSDictionary*))handler;

-(NSTimeInterval)timeBeforeNextAttempt;
-(BOOL)canMakeAnotherAttempt;
-(void)markAsFailed;
-(NSString*)debugDescription;

@end
