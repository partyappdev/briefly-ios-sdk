//
//  BFApiRequestEntity.m
//  Pods
//
//  Created by Artem Bondar on 16.11.17.
//
//

#import "BFApiRequestEntity.h"

@interface BFApiRequestEntity ()

@property (nonatomic) NSInteger failedAttampts;

@end

@implementation BFApiRequestEntity

+(instancetype)requestWithEndpoint:(NSString*)endpoint
                         parametrs:(NSDictionary*)parametrs
                          logLevel:(BFLogLevel)logLevel
                        andHandler:(void(^)(BOOL, NSDictionary*))handler;
{
    BFApiRequestEntity * newObj = [BFApiRequestEntity new];
    newObj.endpoint = endpoint;
    newObj.params = parametrs;
    newObj.logLevel = logLevel;
    newObj.handler = handler;
    return newObj;
}

-(NSTimeInterval)timeBeforeNextAttempt
{
    NSInteger maxAttempts = [[BFBrieflyManager sharedManager] maxRequestAttempts];
    if (self.failedAttampts == 0) {
        return 0;
    } else if (self.failedAttampts < maxAttempts){
        return pow(2, self.failedAttampts);
    }
    return 0;
}

-(BOOL)canMakeAnotherAttempt
{
    return self.failedAttampts < [[BFBrieflyManager sharedManager] maxRequestAttempts];
}

-(void)markAsFailed
{
    self.failedAttampts += 1;
}

-(NSString*)debugDescription
{
    return [NSString stringWithFormat:@"ApiRequest to %@ with params: %@", self.endpoint, self.params];
}

@end
