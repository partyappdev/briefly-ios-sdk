//
//  BFUIManager+Storage.h
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "BFUIManager.h"

@class BFViewModelPreset;

@interface BFUIManager (Storage)

-(BOOL)isSortingOnForWidgetGroup:(NSString*)widgetGroup;
-(void)setSortingOn:(BOOL)sortingOn forWidgetGroup:(NSString*)widgetGroup;

-(NSArray<BFViewModelPreset*>*)presetsForSingleValueWidgetWithObjectId:(NSString*)objectId;
-(void)setPresets:(NSArray*)presets forSingleValueWidgetWithObjectId:(NSString*)objectId;

-(NSNumber*)selectedPresetsIdxForSingleValueWidgetWithObjectId:(NSString*)objectId;
-(void)setSelectedPresetIdx:(NSNumber*)selected forSingleValueWidgetWithObjectId:(NSString*)objectId;

-(BOOL)onceCreatedPresets;
-(void)setOnceCreatedPreset;

-(void)addEventViewShown:(NSString*)objectId;
-(void)addEventViewTouched:(NSString*)objectId;
-(void)addEventViewLeaved:(NSString*)objectId withTimeInSeconds:(NSInteger)seconds;

@end
