//
//  UIViewController+Tracking.h
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (Tracking)

@property (strong, nonatomic) IBInspectable NSString* trackingId;

-(void)setTrackingIdWithClassNameAndObjectId:(NSString*)objectId;

-(void)setBfTrackingEnabled:(BOOL)bfTrackingEnabled;
-(BOOL)bfTrackingEnabled;

@end
