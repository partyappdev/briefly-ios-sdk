//
//  UIView+Tracking.m
//  Pods
//
//  Created by Artem Bondar on 04.12.17.
//
//

#import "UIView+Tracking.h"
#import <objc/runtime.h>
#import "BFUIManager.h"
#import "BFHeatmapImageView.h"
#import "BFStatisticsLabelScreen.h"
#import "BFBrieflyManager.h"
#import "UIViewController+Utils.h"
#import "BFUIManager.h"
#import "BFUIManager+Overlay.h"

@interface UIView (Heatmap)
@end

@interface UIView (TrackingInternal)
@property (strong, nonatomic) NSString* bf_internal_trackingId;

@end

@implementation UIView (Heatmap)

-(void)bf_updateOverlayViews
{
    BFUIManager * uiManager = [BFBrieflyManager sharedManager].uiManager;
    NSString * trackingId = self.trackingId;
    BOOL foundTrackingView = NO;
    BOOL foundStaisticsView = NO;
    BOOL needHeatmap = uiManager.shouldShowHeatmaps;
    BOOL needStatisticsOverlay = [uiManager shouldShowUIActionsStatistic] && [uiManager shouldRecordUIActionsForView:self];
    for (UIView * view in self.subviews) {
        if ([view isKindOfClass:[BFHeatmapImageView class]]) {
            if (trackingId && needHeatmap) {
                ((BFHeatmapImageView*)view).bf_trackingId = trackingId;
            } else {
                [view removeFromSuperview];
            }
            foundTrackingView = YES;
        } else if ([view isKindOfClass:[BFStatisticsLabelScreen class]]) {
            if (needStatisticsOverlay) {
                //((BFStatisticsLabelScreen*)view).analyticsObjectId = statisticsObjectId;
                // update object id at screenCheckTimer
                // to have consistent screenId+trackingId
            } else {
                [view removeFromSuperview];
            }
            foundStaisticsView = YES;
        }
        
        if (foundStaisticsView && foundTrackingView) {
            break;
        }
    }
    
    if (!foundTrackingView && trackingId && needHeatmap) {
        BFHeatmapImageView * imageView = [[BFHeatmapImageView alloc] initWithFrame:self.bounds];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:imageView];
        [self addConstraints:@[
                               [imageView.topAnchor constraintEqualToAnchor:self.topAnchor],
                               [imageView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor],
                               [imageView.leftAnchor constraintEqualToAnchor:self.leftAnchor],
                               [imageView.rightAnchor constraintEqualToAnchor:self.rightAnchor]]];
        imageView.bf_trackingId = trackingId;
        // also subscribe for changes here
        // do nothing now
    }
    
    if (!foundStaisticsView && needStatisticsOverlay) {
        BFStatisticsLabelScreen * statisticsLabel = [[BFStatisticsLabelScreen alloc] initWithFrame:CGRectMake(4, 20, 120, 32)];
        [statisticsLabel addToView:self topInset:0 leftInset:0];
        // update object id at screenCheckTimer
        // to have consistent screenId+trackingId
        //statisticsLabel.analyticsObjectId = statisticsObjectId;
    }
}

@end

@implementation UIView (TrackingInternal)

-(void)bf_trackingIdChangedFrom:(NSString*)oldId to:(NSString*)newId
{
    if (self.window == nil) {
        // in that case we don't care: we are definetly not subscribed
    } else {
        if (oldId != nil && newId != nil) {
            // not interesting - must be already subscribed
        } else if (oldId == nil && newId != nil) {
            // need subscribe
            [[BFBrieflyManager sharedManager].uiManager subscribeViewForChanges:self];
        } else if (oldId != nil && newId == nil) {
            // need unsubscribe
            [[BFBrieflyManager sharedManager].uiManager subscribeViewForChanges:self];
        } else if (oldId == nil && newId == nil) {
            // already unsubscribed
        }
    }
}

-(void)setBf_internal_trackingId:(NSString *)bf_internal_trackingId
{
    NSString * oldValue = self.trackingId;
    objc_setAssociatedObject(self, @selector(bf_internal_trackingId), bf_internal_trackingId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self bf_updateOverlayViews];
    [self bf_trackingIdChangedFrom:oldValue to:self.trackingId];
}

-(NSString *)bf_internal_trackingId
{
    return objc_getAssociatedObject(self, @selector(bf_internal_trackingId));
}

@end

@implementation UIView (Tracking)

+ (void)load {
    [super load];
    Method didMoveToWindow = class_getInstanceMethod(self, @selector(didMoveToWindow));
    Method bfDidMoveToWindow = class_getInstanceMethod(self, @selector(bfDidMoveToWindow));
    method_exchangeImplementations(didMoveToWindow, bfDidMoveToWindow);
    
    Method layoutSubviews = class_getInstanceMethod(self, @selector(layoutSubviews));
    Method bfLayoutSubviews = class_getInstanceMethod(self, @selector(bfLayoutSubviews));
    method_exchangeImplementations(layoutSubviews, bfLayoutSubviews);
}

-(void)bfLayoutSubviews
{
    [self bfLayoutSubviews];
    for (UIView * subview in self.subviews) {
        if ([subview isKindOfClass:[BFHeatmapImageView class]]) {
            if (!CGRectEqualToRect(subview.frame, self.bounds)) {
                subview.frame = self.bounds;
                [(BFHeatmapImageView*)subview bf_onNeedReloadData];
            }
            // don't have more than one instance of BFHeatmapImageView
            break;
        }
    }
}

static BOOL bf_KindButNotMember(UIView * view, Class forClass) {
    return [view isKindOfClass:[forClass self]] && [view class] != forClass;
}

-(void)setTrackingIdWithClassNameAndObjectId:(NSString*)objectId
{
    self.trackingId = [NSString stringWithFormat:@"%@/%@", NSStringFromClass(self.class), objectId];
}

-(void)setTrackingIdWithClassNameAndIndexPath:(NSIndexPath*)indexPath
{
    self.trackingId = [NSString stringWithFormat:@"%@/%d-%d", NSStringFromClass(self.class), (int)indexPath.section, (int)indexPath.item];
}

-(void)bfDidMoveToWindow
{
    // HOHO! You must be really crasy, if you really want to intercept this call!!
    // Performance here really matters, so do all the stuff as quickly as possible
    //
    // Who are candidates to have tracking id?
    // - views with programmically set tracking id (done)
    // - Reusable views (cells/headers) is they have the particular class
    // - (NOT SUPPORTED NOW) UIScrollView subclasses (because their coordinates not mapping on base coords)
    // For scroll views idea is quite simple:
    // we try to move down to first view controller, and by the way, calculating the way down
    if (bf_KindButNotMember(self, [UICollectionViewCell class]) ||
        bf_KindButNotMember(self, [UICollectionReusableView class]) ||
        bf_KindButNotMember(self, [UITableViewCell class]) ||
        bf_KindButNotMember(self, [UITableViewHeaderFooterView class])) {
        
        [self setBf_internal_trackingId:NSStringFromClass(self.class)];
    }

    /*if ([self isKindOfClass:[UIScrollView class]] && [self trackingId] == nil) {
        // Start traversing!
        // now we do it without any check, that we have a scroll view with the same id
        // (but it can simply happen). We'll do it later
        UIView * currentView = self;
        __block NSMutableArray * viewsChain = [NSMutableArray arrayWithObject:currentView];
        while(![[currentView nextResponder] isKindOfClass:[UIViewController class]] && [currentView nextResponder] != nil) {
            
            currentView = currentView.superview;
            [viewsChain addObject:currentView];
        }
        UIResponder * ctrl = [currentView nextResponder];
        if ([ctrl isKindOfClass:[UIViewController class]]) {
            [viewsChain addObject:ctrl];
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
                
                // well it's not good idea to access UIKit stuff from background queue,
                // but we do it:
                // - in readonly
                // - with taking strong references to any object
                // so god help us!
                
                // Firstly check, is controller generic class?
                NSString * controllerUID = [(UIViewController*)viewsChain.lastObject uid];
                if (controllerUID != nil) {
                    __block NSString * resultId = controllerUID;
                    [viewsChain removeLastObject];
                    [viewsChain enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(UIView * view, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSString * viewId = [view trackingId];
                        if (viewId == nil) {
                            viewId = NSStringFromClass(view.class);
                        }
                        resultId = [resultId stringByAppendingString:@"/"];
                        resultId = [resultId stringByAppendingString:viewId];
                    }];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setBf_internal_trackingId:resultId];
                    });
                }
            });
        }
    }*/
    // subscribe/unsubscribe according to tracking id state
    if ([self trackingId] != nil) {
        [self bf_updateOverlayViews];
        if (self.window != nil) {
            [[BFBrieflyManager sharedManager].uiManager subscribeViewForChanges:self];
        } else {
            [[BFBrieflyManager sharedManager].uiManager unsubscribeViewFromChanges:self];
        }
    }
    [self bfDidMoveToWindow];
}

-(void)setTrackingId:(NSString *)trackingId
{
    NSString * oldValue = self.trackingId;
    objc_setAssociatedObject(self, @selector(trackingId), trackingId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self bf_updateOverlayViews];
    [self bf_trackingIdChangedFrom:oldValue to:self.trackingId];
}

-(NSString *)trackingId
{
    NSString * trackingId = objc_getAssociatedObject(self, @selector(trackingId));
    if (trackingId == nil)
        return self.bf_internal_trackingId;
    else
        return trackingId;
}

-(BOOL)bfTrackingDisabled
{
    return [(NSNumber*)objc_getAssociatedObject(self, @selector(bfTrackingDisabled)) boolValue];
}

-(NSString*)bfExcplicitTrackingId
{
    return objc_getAssociatedObject(self, @selector(trackingId));
}

-(BOOL)bfTrackingEnabled
{
    return !((NSNumber*)objc_getAssociatedObject(self, @selector(bfTrackingDisabled))).boolValue;
}

-(void)setBfTrackingEnabled:(BOOL)bfTrackingEnabled
{
    objc_setAssociatedObject(self, @selector(bfTrackingDisabled), @(!bfTrackingEnabled), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)bf_onChangeWidgetsOnState
{
    // TODO: show/hide heatmaps according widgets state
    [self bf_updateOverlayViews];
}

-(void)bf_onNeedReloadData
{
    // TODO: update heatmaps
    [self bf_updateOverlayViews];
}

@end
