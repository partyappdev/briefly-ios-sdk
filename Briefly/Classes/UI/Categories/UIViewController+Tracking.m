//
//  UIViewController+Tracking.m
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "UIViewController+Tracking.h"
#import <objc/runtime.h>

@implementation UIViewController (Tracking)

-(void)setTrackingId:(NSString *)trackingId
{
    objc_setAssociatedObject(self, @selector(trackingId), trackingId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSString *)trackingId
{
    return objc_getAssociatedObject(self, @selector(trackingId));
}

-(void)setTrackingIdWithClassNameAndObjectId:(NSString*)objectId
{
    self.trackingId = [NSString stringWithFormat:@"%@/%@", NSStringFromClass(self.class), objectId];
}

-(BOOL)bfTrackingDisabled
{
    return [(NSNumber*)objc_getAssociatedObject(self, @selector(bfTrackingDisabled)) boolValue];
}

-(BOOL)bfTrackingEnabled
{
    return !self.bfTrackingDisabled;
}

-(void)setBfTrackingEnabled:(BOOL)bfTrackingEnabled
{
    objc_setAssociatedObject(self, @selector(bfTrackingDisabled), @(!bfTrackingEnabled), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
