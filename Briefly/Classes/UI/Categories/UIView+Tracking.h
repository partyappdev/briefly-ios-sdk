//
//  UIView+Tracking.h
//  Pods
//
//  Created by Artem Bondar on 04.12.17.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Tracking)

@property (strong, nonatomic) IBInspectable NSString* trackingId;

-(void)bf_onChangeWidgetsOnState;
-(void)bf_onNeedReloadData;
-(void)setTrackingIdWithClassNameAndObjectId:(NSString*)objectId;
-(void)setTrackingIdWithClassNameAndIndexPath:(NSIndexPath*)indexPath;

-(void)setBfTrackingEnabled:(BOOL)bfTrackingEnabled;
-(BOOL)bfTrackingEnabled;
-(NSString*)bfExcplicitTrackingId;

@end
