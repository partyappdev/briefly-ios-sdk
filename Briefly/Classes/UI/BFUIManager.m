//
//  BFUIManager.m
//  Pods
//
//  Created by Artem Bondar on 30.11.17.
//
//

#import <UIKit/UIKit.h>
#import "BFUIManager.h"
#import "BFUIManager_Private.h"
#import "BFUIManager+Storage.h"
#import "BFUIManager+Overlay.h"

#import "BFGestureRecognizer.h"
#import "BFBrieflyManager.h"
#import "UIViewController+Utils.h"
#import "UIViewController+Tracking.h"
#import "UIView+Tracking.h"
#import "BFPreferencesController.h"
#import "BFSorter+Private.h"
#import "BFOnScreenTimeController.h"

#import "BFModelAccessToken.h"


@interface BFUIManager () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) NSTimer * observingTimer;
@property (strong, nonatomic) NSString * oldContrallerTreeDescription;
@property (strong, nonatomic) BFOnScreenTimeController * timeOnScreenController;
@end


@implementation BFUIManager

+(instancetype)instanceWithHost:(BFBrieflyManager*)host
{
    BFUIManager * newObj = [BFUIManager new];
    newObj.host = host;

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"bf_menu_show_widgets"]) {
        newObj.showWidgetsByUserPreference = [[NSUserDefaults standardUserDefaults] boolForKey:@"bf_menu_show_widgets"];
    } else {
        newObj.showWidgetsByUserPreference = YES;
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"bf_menu_show_heatmaps"]) {
        newObj.showHeatmapsByUserPreference = [[NSUserDefaults standardUserDefaults] boolForKey:@"bf_menu_show_heatmaps"];
    } else {
        newObj.showHeatmapsByUserPreference = NO;
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"bf_menu_show_ui_stats"]) {
        newObj.showUIActionsStatsByUserPreference = [[NSUserDefaults standardUserDefaults] boolForKey:@"bf_menu_show_ui_stats"];
    } else {
        newObj.showUIActionsStatsByUserPreference = NO;
    }
    newObj.timeOnScreenController = [BFOnScreenTimeController new];
    newObj.cachcedPresets = [NSMutableDictionary dictionary];
    newObj.heatmapMode = BFHeatmapsModeDebug;
    newObj.heatmapRequests = [NSMutableSet set];
    newObj.localHeatmaps = [NSMutableDictionary dictionary];
    newObj.analyticsViews = (NSHashTable<UIView*>*)[NSHashTable hashTableWithOptions:NSPointerFunctionsWeakMemory];
    newObj.sorters = (NSHashTable<BFSorter*>*)[NSHashTable hashTableWithOptions:NSPointerFunctionsWeakMemory];
    [newObj updateOrientationIdentifier];
    [[NSNotificationCenter defaultCenter] addObserver:newObj selector:@selector(windowChanged) name:UIWindowDidBecomeKeyNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:newObj selector:@selector(orientationChanged) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    newObj.currentWindow = [UIApplication sharedApplication].keyWindow;
    return newObj;
}

-(BOOL)shouldShowHeatmaps
{
    return self.host.accessShowsHeatmaps && self.showHeatmapsByUserPreference;
}

-(BOOL)shouldShowUIActionsStatistic
{
    return self.host.accessShowsHeatmaps && self.showUIActionsStatsByUserPreference;
}

-(BOOL)shouldShowWidgetsForObjectId:(NSString*)objectId
                        widgetGroup:(NSString*)widgetGroup
{
    return [self.host accessHaveToWidgetGroupWithId:widgetGroup andObjectId:objectId] && self.showWidgetsByUserPreference;
}

-(BOOL)shouldShowSorterControlsForWidgetGroup:(NSString*)widgetGroup
{
    if (widgetGroup == nil) {
        return NO;
    }
    return [self.host accessHaveToWidgetGroupWithId:widgetGroup andObjectId:nil] && self.showWidgetsByUserPreference;
}

-(BOOL)shouldRecordUIActions
{
    switch (self.heatmapMode) {
        case BFHeatmapsModeNormal:
            return self.host.accessIsUser;
        case BFHeatmapsModeDebug:
            // in debug mode we record touches only if the heatmaps are hidden
            // (no realtime touches)
            return !([self shouldShowHeatmaps] || [self shouldShowUIActionsStatistic] || [self shouldShowWidgetsForObjectId:nil widgetGroup:nil]);
        case BFHeatmapsModeDebugLocal:
            return YES;
    }
}

-(BOOL)shouldRecordUIActionsForView:(UIView*)view
{
    return [view bfExcplicitTrackingId];
}

-(void)startInteractionRecordings
{
    self.recording = YES;
    [self.observingTimer invalidate];
    self.observingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkCurrentScreenTimerTick) userInfo:nil repeats:YES];
    [self recreateOverlayView];
}

-(void)stopInteractionRecordings
{
    self.recording = NO;
    [self.observingTimer invalidate];
}

-(void)checkCurrentScreenTimerTick
{
    // Debug print of stack (heavy shit - do it only on debug)
    if ([self.host currentLogLevel] == BFLogLevelDebug) {
        NSString * stackDescription = [self.currentWindow.rootViewController treeDescriptionOfThisController];
        if (![self.oldContrallerTreeDescription isEqualToString:stackDescription]) {
            [self.host logRecord:stackDescription logLevel:BFLogLevelDebug];
            self.oldContrallerTreeDescription = stackDescription;
        }
    }
    // find the topmost view controller
    NSString * oldId = self.currentControllerUid;
    UIViewController * current = [self currentTopViewController];
    NSString * newControllerId = [current uid];
    
    if (!(oldId == nil && newControllerId == nil) && ![oldId isEqualToString:newControllerId]) {
        
        self.currentControllerUid = newControllerId;
        self.currentController = current;
        
        [self.host logRecord:[NSString stringWithFormat:@"Detected new screen: %@", newControllerId] logLevel:BFLogLevelDebug];

        if (newControllerId == nil) {
            // TODO: make a screenshot and send to server, that can't
            // recognize it
        }
        [self needUpdateOverlayAfterScreenChange];
    }
    [self updateOverlayOnTimer];
    
    //
    // Check the screen-presence stuff for traking views
    //
    if ([self shouldRecordUIActions]) {
        [self.timeOnScreenController startRollCall];
        //if (self.currentControllerUid) {
        //    NSString * objectId = [self uiEventsTrackingIdForView:nil];
        //    [self.timeOnScreenController rollCallObjectIdCalled:objectId];
        //}
        for (UIView * trackingView in self.analyticsViews) {
            NSString * objectId = [self uiEventsTrackingIdForView:trackingView];
            [self.timeOnScreenController rollCallObjectIdCalled:objectId];
        }
        [self.timeOnScreenController finishRollCall];
    }
}

-(UIViewController *)topViewControllerForController:(UIViewController*)baseVC {
    
    if ([baseVC isKindOfClass:[UINavigationController class]]) {
        return [self topViewControllerForController:((UINavigationController *)baseVC).visibleViewController];
    }
    
    if ([baseVC isKindOfClass:[UITabBarController class]]) {
        UIViewController *selectedTVC = ((UITabBarController*)baseVC).selectedViewController;
        if (selectedTVC) {
            return [self topViewControllerForController:selectedTVC];
        } else {
            return baseVC;
        }
    }
    
    if ([baseVC isKindOfClass:[UIPageViewController class]]) {
        UIViewController *selectedVC = ((UIPageViewController*)baseVC).viewControllers.firstObject;
        if (selectedVC) {
            return [self topViewControllerForController:selectedVC];
        } else {
            return baseVC;
        }
    }

    if (baseVC.presentedViewController) {
        return [self topViewControllerForController:baseVC.presentedViewController];
    }
    
    if (baseVC.childViewControllers.count > 0) {
        // we prefer controller with the trackingId
        // because we have nothing more to know about it
        //
        for (UIViewController *ctrl in baseVC.childViewControllers) {
            if ([ctrl trackingId]) {
                return [self topViewControllerForController:ctrl];
            }
        }
        return [self topViewControllerForController:baseVC.childViewControllers.firstObject];
    }
    return baseVC;
}

-(UIViewController*)currentTopViewController
{
    UIViewController *baseVC = self.currentWindow.rootViewController;
    return [self topViewControllerForController:baseVC];
}

-(void)orientationChanged
{
    [self updateOrientationIdentifier];
    //[self forceWidgetsReloadData];
}

-(void)updateOrientationIdentifier
{
    NSString * newOrientation = @"port";
    /*switch ([UIDevice currentDevice].orientation) {
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            newOrientation = @"land";
            break;
        default:
            break;
    }*/
    self.currentOrientation = newOrientation;
}

-(void)windowChanged
{
    // Firstly cleanup previous window
    UIGestureRecognizer * toDelete;
    for (UIGestureRecognizer * recognizer in self.currentWindow.gestureRecognizers) {
        if ([recognizer isKindOfClass:[BFTapGestureRecognizer class]] ||
            [recognizer isKindOfClass:[BFPanGestureRecognizer class]]) {
            toDelete = recognizer;
        }
    }
    if (toDelete != nil) {
        [self.currentWindow removeGestureRecognizer:toDelete];
    }
    
    // now configure tracking on new window
    self.currentWindow = [UIApplication sharedApplication].keyWindow;
    BFTapGestureRecognizer * newRecognizer = [[BFTapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.currentWindow addGestureRecognizer:newRecognizer];
    
    UILongPressGestureRecognizer * threeFingersTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleThreeFingersTapGesture:)];
    threeFingersTap.numberOfTouchesRequired = 3;
    threeFingersTap.delaysTouchesBegan = NO;
    threeFingersTap.delaysTouchesEnded = NO;
    [self.currentWindow addGestureRecognizer:threeFingersTap];
    
    BFPanGestureRecognizer * panRecognizer = [[BFPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.currentWindow addGestureRecognizer:panRecognizer];
    
    // and recreate overlay
    [self recreateOverlayView];
}

-(void)findTrackingViewForTouchView:(UIView*)view
                        withHandler:(void(^)(NSString*, UIView*, UIView*))handler
{
    //
    // Tracking_id
    //
    UIView * trackingIdView = nil;
    UIView * uiActionRecordingView = nil;
    NSString * trackingId = nil;
    UIScrollView * firstParentScrollView = nil;
    // TODO: handle scrolling stuff
    // TODO: handle paging controllers
    //NSString * sublimeTrackingId = nil;
    UIView * currentView = view;
    while (currentView.superview != nil) {

        if (![currentView bfTrackingEnabled]) {
            break;
        }

        if ([self uiEventsTrackingIdForView:currentView]) {
            uiActionRecordingView = currentView;
            trackingIdView = currentView;
            trackingId = currentView.trackingId;
            break;
        } else if (currentView.trackingId != nil) {
            trackingIdView = currentView;
            trackingId = currentView.trackingId;
            if (uiActionRecordingView) {
                break;
            }
        } else if ([currentView isKindOfClass:[UIScrollView class]]) {
            if (firstParentScrollView == nil) {
                firstParentScrollView = (UIScrollView*)currentView;
            }
        }
        currentView = currentView.superview;
    }
    handler(trackingId, trackingIdView, uiActionRecordingView);
}

-(void)handleThreeFingersTapGesture:(BFTapGestureRecognizer*)sender
{
    if (sender.state != UIGestureRecognizerStateBegan) {
        return;
    }
    if (![self.host accessIsUser]) {
        UIViewController * ctrl = [BFPreferencesController instantiateController];
        [[self currentTopViewController] presentViewController:ctrl animated:YES completion:nil];
    }
}

-(void)handleTapGesture:(BFTapGestureRecognizer*)sender
{
    if (self.recording) {
        for (UITouch * touch in sender.currentTouches) {
            @try {
                [self handleTouch:touch logInfo:@"Handle tap"];
            }
            @catch(NSException * exception) {
                [self.host logRecord:exception.debugDescription logLevel:BFLogLevelFatal];
            }
        }
    }
}

-(void)handlePanGesture:(BFPanGestureRecognizer*)sender
{
    // don't record pan gestures now
    //
    /*if (self.recording && [sender state] == UIGestureRecognizerStateBegan) {
        for (UITouch * touch in sender.currentTouches) {
            [self handleTouch:touch logInfo:@"Handle pan"];
        }
    }*/
}

-(NSString*)webTrackingIdForRootView:(BOOL)rootView withId:(NSString*)trackingId
{
    if (self.host.perScreenTrackingId && self.currentControllerUid && !rootView) {
        return [NSString stringWithFormat:@"%@/%@", self.currentControllerUid, trackingId];
    } else {
        return trackingId;
    }
}

-(void)handleTouch:(UITouch*)touch
           logInfo:(NSString*)logInfo
{
    if (![self shouldRecordUIActions]) {
        return;
    }
    
    // check, if handling is disabled for this screen
    if (![self.currentController bfTrackingEnabled]) {
        return;
    }

    // Firstly obtain a view
    UIView * viewBase = [self.currentWindow hitTest:[touch locationInView:self.currentWindow] withEvent:nil];
    if ([viewBase isEqual:self.overlayButton]) {
        return;
    }
    __weak __typeof__(self) weakSelf = self;
    [self findTrackingViewForTouchView:viewBase withHandler:^(NSString * trackingId, UIView *view, UIView * uiTrackingView) {
        
        //
        // Handle touch for heatmaps
        //
        CGPoint pt = [touch locationInView:view];
        CGSize dimensions = trackingId ? view.bounds.size : weakSelf.currentWindow.bounds.size;
        if (trackingId == nil) {
            // send this touch directly to screen
            if (weakSelf.currentControllerUid == nil) {
                return;
            } else {
                trackingId = [weakSelf webTrackingIdForRootView:YES withId:weakSelf.currentControllerUid];
            }
        } else {
            trackingId = [weakSelf webTrackingIdForRootView:NO withId:trackingId];
        }
        [weakSelf.host logRecord:[NSString stringWithFormat:@"%@: %lf, %lf (%@ - %@)", logInfo, pt.x, pt.y, trackingId, [view class]] logLevel:BFLogLevelDebug];

        [weakSelf storeTapGesture:pt
               atViewWithWebId:trackingId
               withDimensions:dimensions
                  orientation:weakSelf.currentOrientation handler:^(BOOL succeed) {
                      // only on local debugging we allow debug touches in realtime
                      if (weakSelf.heatmapMode == BFHeatmapsModeDebugLocal) {
                          [weakSelf reloadOnlyHeatmaps];
                      }
                  }];
        
        //
        // Handle tap for UI actions recordings
        //
        NSString * uiActionTrackingId = [self uiEventsTrackingIdForView:uiTrackingView];
        if (uiActionTrackingId) {
            [self addEventViewTouched:uiActionTrackingId];
        }
        //[self addEventViewTouched:[self uiEventsTrackingIdForView:nil]];
    }];
}

-(void)heatmapForViewWithId:(NSString*)viewId
                 isRootView:(BOOL)isRootView
             withDimensions:(CGSize)dimensions
                    handler:(void(^)(UIImage*))handler
          checkAliveHandler:(BOOL(^)(void))checkAliveHandler
{
    [self checkCurrentScreenTimerTick];
    NSString * webViewId = [self webTrackingIdForRootView:isRootView withId:viewId];
    [self internalHeatmapForViewWithWebId:webViewId withDimensions:dimensions handler:handler checkAliveHandler:checkAliveHandler];
}

-(void)storeWidgetPreferences
{
    [[NSUserDefaults standardUserDefaults] setBool:self.showWidgetsByUserPreference forKey:@"bf_menu_show_widgets"];
    [[NSUserDefaults standardUserDefaults] setBool:self.showHeatmapsByUserPreference forKey:@"bf_menu_show_heatmaps"];
    [[NSUserDefaults standardUserDefaults] setBool:self.showUIActionsStatsByUserPreference forKey:@"bf_menu_show_ui_stats"];
}

-(void)setShowHeatmapsByUserPreference:(BOOL)showHeatmapsByUserPreference
{
    _showHeatmapsByUserPreference = showHeatmapsByUserPreference;
    [self widgetsStateChanged];
}

-(void)setShowWidgetsByUserPreference:(BOOL)showWidgetsByUserPreference
{
    _showWidgetsByUserPreference = showWidgetsByUserPreference;
    [self widgetsStateChanged];
}

-(void)widgetsStateChanged
{
    [self dropHeatmapRequestsQueue];
    [self recreateOverlayView];
    [self forceWidgetsReloadData];
}

-(NSString*)uiEventsTrackingIdForView:(UIView*)view
{
    NSString * identifier;
    if (view == nil) {
        identifier = [self webTrackingIdForRootView:YES withId:self.currentControllerUid];
    } else {
        if (view.bfExcplicitTrackingId) {
            identifier = [self webTrackingIdForRootView:NO withId:view.bfExcplicitTrackingId];
        }
    }
    if (identifier) {
        if (identifier.length > 256) {
            identifier = [identifier substringToIndex:250];
        }
        return [NSString stringWithFormat:@"bfpv_%@", identifier];
    }
    return nil;
}

-(void)setCurrentControllerUid:(NSString *)currentControllerUid
{
    _currentControllerUid = currentControllerUid;
}

-(void)appBecameActive
{
    [self.timeOnScreenController appOpened];
}

-(void)appBecameInactive
{
    [self.timeOnScreenController appClosed];
}

@end
