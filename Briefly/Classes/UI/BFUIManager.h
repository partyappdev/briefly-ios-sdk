//
//  BFUIManager.h
//  Pods
//
//  Created by Artem Bondar on 30.11.17.
//
//

#import <Foundation/Foundation.h>
#import "BrieflyFacade.h"

@interface BFUIManager : NSObject

-(void)appBecameActive;
-(void)appBecameInactive;

-(void)startInteractionRecordings;
-(void)stopInteractionRecordings;
-(void)widgetsStateChanged;
-(void)storeWidgetPreferences;
-(UIViewController*)currentTopViewController;

-(BOOL)shouldShowHeatmaps;
-(BOOL)shouldRecordUIActions;
-(BOOL)shouldRecordUIActionsForView:(UIView*)view;
-(BOOL)shouldShowUIActionsStatistic;
-(BOOL)shouldShowWidgetsForObjectId:(NSString*)objectId
                        widgetGroup:(NSString*)widgetGroup;

-(BOOL)shouldShowSorterControlsForWidgetGroup:(NSString*)widgetGroup;

-(void)heatmapForViewWithId:(NSString*)viewId
                 isRootView:(BOOL)isRootView
             withDimensions:(CGSize)dimensions
                    handler:(void(^)(UIImage*))handler
          checkAliveHandler:(BOOL(^)(void))checkAliveHandler;

@end
