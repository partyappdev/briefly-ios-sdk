//
//  BFViewModelMetricRequestSingle+Constructor.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFViewModelMetricRequestSingle.h"

@class BFModelObjectMeta;

@interface BFViewModelMetricRequestSingle (Builder)

-(NSString*)descriptionMetricShortWithMeta:(BFModelObjectMeta*)meta;
-(NSString*)descriptionMetricLongWithMeta:(BFModelObjectMeta*)meta;

-(NSString*)descriptionGroupByShort;
-(NSString*)descriptionGroupByLong;

-(NSString*)descriptionFunctionShort;
-(NSString*)descriptionFunctionLong;

-(NSString*)descriptionTimeAggregationShort;
-(NSString*)descriptionTimeAggregationLong;

-(NSString*)descriptionObjectGroupAggrShort;
-(NSString*)descriptionObjectGroupAggrLong;

-(NSArray*)acessableMetricsListWithMeta:(BFModelObjectMeta*)meta;
-(NSInteger)indexForCurrentMetricInAccessableListWithMeta:(BFModelObjectMeta*)meta;

-(NSArray*)acessableObjectGroupAggregationList;
-(NSInteger)indexForCurrentObjectGroupAggregation;

-(NSArray*)acessableGroupByList;
-(NSInteger)indexForCurrentGroupBy;

-(NSArray*)acessableFunctionList;
-(NSInteger)indexForCurrentFunction;

-(NSArray*)acessableTimeRangeAggregations;
-(NSInteger)indexForCurrentTimeRangeAggregation;

-(NSArray*)functionArgumentsDescriptions;
-(NSArray*)functionArgumentsDefaultValuesForFunction:(BFModelAdditionalFunction)function;

-(instancetype)requestBySelectingMetricIdx:(NSInteger)idx withMeta:(BFModelObjectMeta*)meta;
-(instancetype)requestBySelectingObjectGroupAggregationIdx:(NSInteger)idx;
-(instancetype)requestBySelectingGroupByIdx:(NSInteger)idx;
-(instancetype)requestBySelectingFunctionIdx:(NSInteger)idx;
-(instancetype)requestByUpdatingFunctionArgs:(NSArray*)args;
-(instancetype)requestBySelectingTimeRangeAggrIdx:(NSInteger)idx andCurrentTimeRange:(BFModelTimePeriod*)timeRange;


@end
