//
//  BFViewModelMetricRequestSingle+Serialization.m
//  Briefly
//
//  Created by Artem Bondar on 07.03.2018.
//

#import "BFViewModelMetricRequestSingle+Serialization.h"
#import "BFFSCalendar.h"

@interface NSString (BfNullablaSubscting)
-(NSString*)bf_substring:(NSRange)range;
@end

@implementation NSString (BfNullablaSubscting)
-(NSString*)bf_substring:(NSRange)range
{
    if (range.location == NSNotFound) {
        return nil;
    } else {
        return [self substringWithRange:range];
    }
}
@end

@implementation BFViewModelMetricRequestSingle (Serialization)

+(BFModelObjectGroupAggregation)enumForObjectGroupAggregation:(NSString*)str
{
    if ([str isEqualToString:@"avg"]) {
        return BFModelObjectGroupAggregationAvg;
    } else if ([str isEqualToString:@"max"]) {
        return BFModelObjectGroupAggregationMax;
    } else if ([str isEqualToString:@"min"]) {
        return BFModelObjectGroupAggregationMin;
    } else {
        return BFModelObjectGroupAggregationSum;
    }
}

+(BFModelAggreagationFunction)enumForAggreagtionFucntion:(NSString*)str
{
    if ([str isEqualToString:@"last"]) {
        return BFModelAggreagationFunctionLast;
    } else if ([str isEqualToString:@"min"]) {
        return BFModelAggreagationFunctionMin;
    } else if ([str isEqualToString:@"max"]) {
        return BFModelAggreagationFunctionMax;
    } else if ([str isEqualToString:@"avg"]) {
        return BFModelAggreagationFunctionAvg;
    } else {
        return BFModelAggreagationFunctionSum;
    }
}

+(BFModelAdditionalFunction)enumForAdditionalFucntion:(NSString*)str
{
    if ([str isEqualToString:@"lag"]) {
        return BFModelAdditionalFunctionLag;
    } else if ([str isEqualToString:@"expmovavg"]) {
        return BFModelAdditionalFunctionExpMovAvg;
    } else {
        return BFModelAdditionalFunctionNone;
    }
}

+(BFModelTimeRangeAggregation)enumForTimeRangeAggregation:(NSString*)str
{
    if ([str isEqualToString:@"at"]) {
        return BFModelTimeRangeAggregationAt;
    } else if ([str isEqualToString:@"sum"]) {
        return BFModelTimeRangeAggregationSum;
    } else if ([str isEqualToString:@"min"]) {
        return BFModelTimeRangeAggregationMin;
    } else if ([str isEqualToString:@"max"]) {
        return BFModelTimeRangeAggregationMax;
    } else if ([str isEqualToString:@"avg"]) {
        return BFModelTimeRangeAggregationAvg;
    } else {
        return BFModelTimeRangeAggregationRange;
    }
}

+(NSString*)objectIdPlaceholder
{
    return @"bf_priv_object_id";
}

+(NSDateFormatter*)datesFormatter
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    formatter.calendar = [BFFSCalendar utcCalendar];
    formatter.timeZone = formatter.calendar.timeZone;
    return formatter;
}

-(NSString*)serializedString
{
    return [self serializedStringWithTRAggrFunction:self.timeRangeAggregation andTRAggrArgs:self.timeRangeAggregationArgs andNeedObjectGrouping:YES];
}

-(NSString*)serializedStringWithTRAggrFunction:(BFModelTimeRangeAggregation)trAggrFunction andTRAggrArgs:(NSObject*)trAggrFunctionArgs andNeedObjectGrouping:(BOOL)needObjectGrouping
{
    NSString *agrFunc, *agrFuncArgs, *additionalFuncStr, *timeRangeArgFunc, *timeRangeArgFuncArgs, *argObjectGroupingAggr;
    
    switch (self.objectGroupAggregation) {
        case BFModelObjectGroupAggregationSum:
            argObjectGroupingAggr = @"sum";
            break;
        case BFModelObjectGroupAggregationMin:
            argObjectGroupingAggr = @"min";
            break;
        case BFModelObjectGroupAggregationMax:
            argObjectGroupingAggr = @"max";
            break;
        case BFModelObjectGroupAggregationAvg:
            argObjectGroupingAggr = @"avg";
            break;
    }

    switch (self.aggregationFucntion) {
        case BFModelAggreagationFunctionLast:
            agrFunc = @"last";
            agrFuncArgs = @"";
            break;
        case BFModelAggreagationFunctionSum:
            agrFunc = @"sum";
            agrFuncArgs = @"";
            break;
        case BFModelAggreagationFunctionMax:
            agrFunc = @"max";
            agrFuncArgs = @"";
            break;
        case BFModelAggreagationFunctionMin:
            agrFunc = @"min";
            agrFuncArgs = @"";
            break;
        case BFModelAggreagationFunctionAvg:
            agrFunc = @"avg";
            agrFuncArgs = @"";
            break;
    }
    
    switch (self.additionalFunction) {
        case BFModelAdditionalFunctionNone:
            additionalFuncStr = @"";
            break;
        case BFModelAdditionalFunctionLag: {
            NSString * additionalFuncArg = self.argAdditionalFunctionLag != nil ? self.argAdditionalFunctionLag.stringValue : @"0";
            additionalFuncStr = [NSString stringWithFormat:@".lag(%@)", additionalFuncArg];
            break;
        }
        case BFModelAdditionalFunctionExpMovAvg: {
            NSString * additionalFuncArg1 = self.argAdditionalFunctionExpMovAvg.count > 1 ? self.argAdditionalFunctionExpMovAvg[0].stringValue : @"0";
            NSString * additionalFuncArg2 = self.argAdditionalFunctionExpMovAvg.count > 1 ? self.argAdditionalFunctionExpMovAvg[1].stringValue : @"0";
            additionalFuncStr = [NSString stringWithFormat:@".expmovavg(%@,%@)", additionalFuncArg1, additionalFuncArg2];
            break;
        }
    }
    
    switch (trAggrFunction) {
        case BFModelTimeRangeAggregationAt: {
            timeRangeArgFunc = @"at";
            if (trAggrFunctionArgs) {
                timeRangeArgFuncArgs = [[BFViewModelMetricRequestSingle datesFormatter] stringFromDate:(NSDate*)trAggrFunctionArgs];
            } else {
                timeRangeArgFuncArgs = @"";
            }
            break;
        }
        case BFModelTimeRangeAggregationMin:
            timeRangeArgFunc = @"min";
            timeRangeArgFuncArgs = ((BFModelTimePeriod*)trAggrFunctionArgs).serializedString;
            break;
        case BFModelTimeRangeAggregationMax:
            timeRangeArgFunc = @"max";
            timeRangeArgFuncArgs = ((BFModelTimePeriod*)trAggrFunctionArgs).serializedString;
            break;
        case BFModelTimeRangeAggregationAvg:
            timeRangeArgFunc = @"avg";
            timeRangeArgFuncArgs = ((BFModelTimePeriod*)trAggrFunctionArgs).serializedString;
            break;
        case BFModelTimeRangeAggregationSum:
            timeRangeArgFunc = @"sum";
            timeRangeArgFuncArgs = ((BFModelTimePeriod*)trAggrFunctionArgs).serializedString;
            break;
        case BFModelTimeRangeAggregationRange: {
            timeRangeArgFunc = @"range";
            timeRangeArgFuncArgs = ((BFModelTimePeriod*)trAggrFunctionArgs).serializedString;
            break;
        }
    }
    NSString * groupingPart =  needObjectGrouping ? [NSString stringWithFormat:@".group(%@)", argObjectGroupingAggr] : @"";

    return [NSString stringWithFormat:@"%@%@.%@.group_by(day).%@(%@)%@.%@(%@)", [BFViewModelMetricRequestSingle objectIdPlaceholder], groupingPart, self.metricId, agrFunc, agrFuncArgs, additionalFuncStr, timeRangeArgFunc, timeRangeArgFuncArgs];
}

+(BFViewModelMetricRequestSingle*)privFromSerializedString:(NSString*)serializedString
{
    if (serializedString == nil) {
        return nil;
    }
    
    NSNumberFormatter * fmt = [[NSNumberFormatter alloc] init];
    NSString* pattern = [BFViewModelMetricRequestSingle regexpForParse];
    NSError* error = NULL;
    NSRange fullStringRange = (NSRange){0, serializedString.length};
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    if (error){
        return nil;
    }
    NSTextCheckingResult * match = [regex firstMatchInString:serializedString options:0 range:fullStringRange];
    
    if ([match numberOfRanges] < 18 || fullStringRange.length != [match rangeAtIndex:0].length) {
        return nil;
    }
    // Skip object_id (2) and .group() (3) because we handle it in a different
    NSString * metricId = [serializedString bf_substring:[match rangeAtIndex:6]];
    // Skip group by time range - now always day (7)
    NSString * objectGroupAggregation = [serializedString bf_substring:[match rangeAtIndex:4]];
    NSString * aggregationFunc = [serializedString bf_substring:[match rangeAtIndex:10]];
    
    //NSString * aggregationFuncArgs = [serializedString bf_substring:[match rangeAtIndex:11]];
    NSString * additionalFunction = [serializedString bf_substring:[match rangeAtIndex:13]];
    NSString * additionalFunctionArgs = [serializedString bf_substring:[match rangeAtIndex:14]];
    NSString * timeRangeAggregation = [serializedString bf_substring:[match rangeAtIndex:16]];
    NSString * timeRangeAggregationArgs = [serializedString bf_substring:[match rangeAtIndex:17]];
    
    if (metricId == nil || aggregationFunc == nil || timeRangeAggregation == nil) {
        return nil;
    }
    
    BFViewModelMetricRequestSingle * newObject = [BFViewModelMetricRequestSingle new];
    newObject.metricId = metricId;
    newObject.aggregationFucntion = [BFViewModelMetricRequestSingle enumForAggreagtionFucntion:aggregationFunc];
    newObject.additionalFunction = [BFViewModelMetricRequestSingle enumForAdditionalFucntion:additionalFunction];
    newObject.timeRangeAggregation = [BFViewModelMetricRequestSingle enumForTimeRangeAggregation:timeRangeAggregation];
    newObject.objectGroupAggregation = [BFViewModelMetricRequestSingle enumForObjectGroupAggregation:objectGroupAggregation];

    switch(newObject.additionalFunction) {
        case BFModelAdditionalFunctionLag: {
            NSNumber * num = [fmt numberFromString:additionalFunctionArgs];
            newObject.additionalFunctionArgs = num != nil ? @[num] : nil;
            break;
        }
        case BFModelAdditionalFunctionExpMovAvg: {
            NSArray * items = [additionalFunctionArgs componentsSeparatedByString:@","];
            if (items.count == 2) {
                NSNumber * num1 = [fmt numberFromString:items[0]];
                NSNumber * num2 = [fmt numberFromString:items[1]];
                if (num1 && num2) {
                    newObject.additionalFunctionArgs = @[num1, num2];
                } else {
                    // Invalid arguments, silently not set
                    newObject.additionalFunction = BFModelAdditionalFunctionNone;
                }
            } else {
                // Invalid arguments, silently not set
                newObject.additionalFunction = BFModelAdditionalFunctionNone;
            }
            break;
        }
        case BFModelAdditionalFunctionNone: {
            newObject.additionalFunctionArgs = nil;
            break;
        }
    }
    
    switch (newObject.timeRangeAggregation) {
        case BFModelTimeRangeAggregationRange:
        case BFModelTimeRangeAggregationSum:
        case BFModelTimeRangeAggregationMin:
        case BFModelTimeRangeAggregationMax:
        case BFModelTimeRangeAggregationAvg:{
            newObject.timeRangeAggregationArgs = [BFModelTimePeriod periodWithSerializedString:timeRangeAggregationArgs];
            break;
        }
        case BFModelTimeRangeAggregationAt: {
            newObject.timeRangeAggregationArgs = [[BFViewModelMetricRequestSingle datesFormatter] dateFromString:timeRangeAggregationArgs];
            break;
        }
    }
    return newObject;
}

+(NSString*)regexpForParse
{
    // This regexp is copied from backend (with a little modification),
    // all descriptions can be found there
    //
    // IMPORTANT: At backend the very last group is optional (to read graph requests - but on the client it's a mandatory!! Be very careful)
    //
    return @"(([a-zA-Z0-9_/]{1,63})(\\.group\\(([^\\(\\)]*)?\\))?)(\\.([a-zA-Z0-9_/]{1,63}))(\\.group_by\\(([^\\(\\)]*)\\))(\\.(sum|last|max|min|avg)\\(([^\\(\\)]*)\\))(\\.(expmovavg|lag)\\(([^\\(\\)]*)\\))?(\\.(sum|range|at|max|min|avg)\\(([^\\(\\)]*)\\))";
}

@end
