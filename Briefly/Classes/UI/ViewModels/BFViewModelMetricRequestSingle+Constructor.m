//
//  BFViewModelMetricRequestSingle+Constructor.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFViewModelMetricRequestSingle+Constructor.h"
#import "BFViewModelMetricRequestSingle+Serialization.h"
#import "NSString+Utils.h"
#import "BFPublicModels.h"

@implementation BFViewModelMetricRequestSingle (Builder)

-(NSString*)descriptionMetricShortWithMeta:(BFModelObjectMeta*)meta
{
    return [self uiNameWithObjectsMeta:meta];
}

-(NSString*)descriptionMetricLongWithMeta:(BFModelObjectMeta*)meta
{
    return [self uiNameWithObjectsMeta:meta];
}

-(NSString*)descriptionGroupByShort
{
    switch (self.aggregationFucntion) {
        case BFModelAggreagationFunctionLast:
            return @"BfMetricBuilder.GroupByAggregation.Last.Short".bf_localized;
        case BFModelAggreagationFunctionSum:
            return @"BfMetricBuilder.GroupByAggregation.Sum.Short".bf_localized;
        case BFModelAggreagationFunctionMax:
            return @"BfMetricBuilder.GroupByAggregation.Max.Short".bf_localized;
        case BFModelAggreagationFunctionMin:
            return @"BfMetricBuilder.GroupByAggregation.Min.Short".bf_localized;
        case BFModelAggreagationFunctionAvg:
            return @"BfMetricBuilder.GroupByAggregation.Avg.Short".bf_localized;
    }
}
-(NSString*)descriptionGroupByLong
{
    switch (self.aggregationFucntion) {
        case BFModelAggreagationFunctionLast:
            return @"BfMetricBuilder.GroupByAggregation.Last.Long".bf_localized;
        case BFModelAggreagationFunctionSum:
            return @"BfMetricBuilder.GroupByAggregation.Sum.Long".bf_localized;
        case BFModelAggreagationFunctionMax:
            return @"BfMetricBuilder.GroupByAggregation.Max.Long".bf_localized;
        case BFModelAggreagationFunctionMin:
            return @"BfMetricBuilder.GroupByAggregation.Min.Long".bf_localized;
        case BFModelAggreagationFunctionAvg:
            return @"BfMetricBuilder.GroupByAggregation.Avg.Long".bf_localized;
    }
}

-(NSString*)descriptionFunctionShort
{
    switch (self.additionalFunction) {
        case BFModelAdditionalFunctionNone:
            return @"BfMetricBuilder.Function.None.Short".bf_localized;
        case BFModelAdditionalFunctionLag:
            return @"BfMetricBuilder.Function.Lag.Short".bf_localized;
        case BFModelAdditionalFunctionExpMovAvg:
            return @"BfMetricBuilder.Function.ExpMovAvg.Short".bf_localized;
    }
}

-(NSString*)descriptionFunctionLong
{
    // TODO: implement this
    switch (self.additionalFunction) {
        case BFModelAdditionalFunctionNone:
            return @"";
        case BFModelAdditionalFunctionLag:
            return @"";
        case BFModelAdditionalFunctionExpMovAvg:
            return @"";
    }
}

-(NSString*)descriptionTimeAggregationShort
{
    switch (self.timeRangeAggregation) {
        case BFModelTimeRangeAggregationAt:
            return @"BfMetricBuilder.TimeRangeAggregation.Last.Short".bf_localized;
        case BFModelTimeRangeAggregationSum:
            return @"BfMetricBuilder.TimeRangeAggregation.Sum.Short".bf_localized;
        case BFModelTimeRangeAggregationMin:
            return @"BfMetricBuilder.TimeRangeAggregation.Min.Short".bf_localized;
        case BFModelTimeRangeAggregationMax:
            return @"BfMetricBuilder.TimeRangeAggregation.Max.Short".bf_localized;
        case BFModelTimeRangeAggregationAvg:
            return @"BfMetricBuilder.TimeRangeAggregation.Avg.Short".bf_localized;
        case BFModelTimeRangeAggregationRange:
            return @"";
    }
}

-(NSString*)descriptionTimeAggregationLong
{
    switch (self.timeRangeAggregation) {
        case BFModelTimeRangeAggregationAt:
            return @"BfMetricBuilder.TimeRangeAggregation.Last.Long".bf_localized;
        case BFModelTimeRangeAggregationSum:
            return @"BfMetricBuilder.TimeRangeAggregation.Sum.Long".bf_localized;
        case BFModelTimeRangeAggregationMin:
            return @"BfMetricBuilder.TimeRangeAggregation.Min.Long".bf_localized;
        case BFModelTimeRangeAggregationMax:
            return @"BfMetricBuilder.TimeRangeAggregation.Max.Long".bf_localized;
        case BFModelTimeRangeAggregationAvg:
            return @"BfMetricBuilder.TimeRangeAggregation.Avg.Long".bf_localized;
        case BFModelTimeRangeAggregationRange:
            return @"";
    }
}

-(NSString*)descriptionObjectGroupAggrShort
{
    switch (self.objectGroupAggregation) {
        case BFModelObjectGroupAggregationSum:
            return @"BfMetricBuilder.ObjectGroupAggr.Sum.Short".bf_localized;
        case BFModelObjectGroupAggregationMin:
            return @"BfMetricBuilder.ObjectGroupAggr.Min.Short".bf_localized;
        case BFModelObjectGroupAggregationMax:
            return @"BfMetricBuilder.ObjectGroupAggr.Max.Short".bf_localized;
        case BFModelObjectGroupAggregationAvg:
            return @"BfMetricBuilder.ObjectGroupAggr.Avg.Short".bf_localized;
    }
}

-(NSString*)descriptionObjectGroupAggrLong
{
    switch (self.objectGroupAggregation) {
        case BFModelObjectGroupAggregationSum:
            return @"BfMetricBuilder.ObjectGroupAggr.Sum.Long".bf_localized;
        case BFModelObjectGroupAggregationMin:
            return @"BfMetricBuilder.ObjectGroupAggr.Min.Long".bf_localized;
        case BFModelObjectGroupAggregationMax:
            return @"BfMetricBuilder.ObjectGroupAggr.Max.Long".bf_localized;
        case BFModelObjectGroupAggregationAvg:
            return @"BfMetricBuilder.ObjectGroupAggr.Avg.Long".bf_localized;
    }
}

-(NSArray*)acessableMetricsListWithMeta:(BFModelObjectMeta*)meta
{
    NSMutableArray * ret = [NSMutableArray array];
    for (BFModelObjectMetricMeta* metricMeta in meta.metrics) {
        [ret addObject:metricMeta.nameForUI];
    }
    return ret;
}

-(NSInteger)indexForCurrentMetricInAccessableListWithMeta:(BFModelObjectMeta*)meta
{
    __block NSInteger ret = NSNotFound;
    [meta.metrics enumerateObjectsUsingBlock:^(BFModelObjectMetricMeta * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.metricId isEqualToString:self.metricId]) {
            *stop = YES;
            ret = idx;
        }
    }];
    return ret;
}

-(NSArray*)acessableObjectGroupAggregationList
{
    return @[
             @"BfMetricBuilder.ObjectGroupAggr.Sum.Long".bf_localized,
             @"BfMetricBuilder.ObjectGroupAggr.Min.Long".bf_localized,
             @"BfMetricBuilder.ObjectGroupAggr.Max.Long".bf_localized,
             @"BfMetricBuilder.ObjectGroupAggr.Avg.Long".bf_localized
             ];
}

-(NSInteger)indexForCurrentObjectGroupAggregation
{
    switch (self.objectGroupAggregation) {
        case BFModelObjectGroupAggregationSum:
            return 0;
        case BFModelObjectGroupAggregationMin:
            return 1;
        case BFModelObjectGroupAggregationMax:
            return 2;
        case BFModelObjectGroupAggregationAvg:
            return 3;
    }
}

-(NSArray*)acessableGroupByList
{
    return @[
             @"BfMetricBuilder.GroupByAggregation.Last.Long".bf_localized,
             @"BfMetricBuilder.GroupByAggregation.Sum.Long".bf_localized,
             @"BfMetricBuilder.GroupByAggregation.Min.Long".bf_localized,
             @"BfMetricBuilder.GroupByAggregation.Max.Long".bf_localized,
             @"BfMetricBuilder.GroupByAggregation.Avg.Long".bf_localized
             ];
}

-(NSInteger)indexForCurrentGroupBy
{
    switch (self.aggregationFucntion) {
        case BFModelAggreagationFunctionLast:
            return 0;
        case BFModelAggreagationFunctionSum:
            return 1;
        case BFModelAggreagationFunctionMin:
            return 2;
        case BFModelAggreagationFunctionMax:
            return 3;
        case BFModelAggreagationFunctionAvg:
            return 4;
    }
}

-(NSArray*)acessableFunctionList
{
    NSInteger selected = [self indexForCurrentFunction];
    NSArray * argumentlessList = @[
                                   @"BfMetricBuilder.Function.Lag.Long".bf_localized,
                                   @"BfMetricBuilder.Function.ExpMovAvg.Long".bf_localized,
                                   @"BfMetricBuilder.Function.None.Long".bf_localized
                                   ];
    NSMutableArray * resulting = [NSMutableArray array];
    [argumentlessList enumerateObjectsUsingBlock:^(NSString* _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (idx == selected) {
            // add arguments
            if (idx == 0) {
                // add lag argument here
                NSString * additionalFuncArg = self.argAdditionalFunctionLag != nil ? self.argAdditionalFunctionLag.stringValue : @"0";
                NSString * args = [NSString stringWithFormat:@"BfMetricBuilder.Function.Lag.Args".bf_localized, additionalFuncArg];
                [resulting addObject:[NSString stringWithFormat:@"%@ %@", obj, args]];
            } else if (idx == 1) {
                // add moving average args here
                NSString * additionalFuncArg1 = self.argAdditionalFunctionExpMovAvg.count > 1 ? self.argAdditionalFunctionExpMovAvg[0].stringValue : @"0";
                NSString * additionalFuncArg2 = self.argAdditionalFunctionExpMovAvg.count > 1 ? self.argAdditionalFunctionExpMovAvg[1].stringValue : @"0";
                NSString * args = [NSString stringWithFormat:@"BfMetricBuilder.Function.ExpMovAvg.Args".bf_localized, additionalFuncArg1, additionalFuncArg2];
                [resulting addObject:[NSString stringWithFormat:@"%@ %@", obj, args]];
            } else {
                // no args to add
                [resulting addObject:obj];
            }
        } else {
            [resulting addObject:obj];
        }
    }];
    return resulting;
}

-(NSInteger)indexForCurrentFunction
{
    switch (self.additionalFunction) {
        case BFModelAdditionalFunctionNone:
            return 2;
        case BFModelAdditionalFunctionLag: {
            return 0;
        }
        case BFModelAdditionalFunctionExpMovAvg: {
            return 1;
        }
    }
}

-(NSArray*)acessableTimeRangeAggregations
{
    return @[
             @"BfMetricBuilder.TimeRangeAggregation.Last.Long".bf_localized,
             @"BfMetricBuilder.TimeRangeAggregation.Sum.Long".bf_localized,
             @"BfMetricBuilder.TimeRangeAggregation.Min.Long".bf_localized,
             @"BfMetricBuilder.TimeRangeAggregation.Max.Long".bf_localized,
             @"BfMetricBuilder.TimeRangeAggregation.Avg.Long".bf_localized

             ];
}

-(NSInteger)indexForCurrentTimeRangeAggregation
{
    switch (self.timeRangeAggregation) {
        case BFModelTimeRangeAggregationAt:
            return 0;
        case BFModelTimeRangeAggregationSum:
            return 1;
        case BFModelTimeRangeAggregationMin:
            return 2;
        case BFModelTimeRangeAggregationMax:
            return 3;
        case BFModelTimeRangeAggregationAvg:
            return 4;
        case BFModelTimeRangeAggregationRange:
            return NSNotFound;
    }
}

-(NSArray*)functionArgumentsDescriptions
{
    switch (self.additionalFunction) {
        case BFModelAdditionalFunctionNone: {
            return nil;
        }
        case BFModelAdditionalFunctionLag: {
            return @[@"BfMetricBuilder.FunctionArgs.Lag.Shift".bf_localized];
        }
        case BFModelAdditionalFunctionExpMovAvg: {
            return @[@"BfMetricBuilder.FunctionArgs.ExpMovAvg.Window".bf_localized, @"BfMetricBuilder.FunctionArgs.ExpMovAvg.Historical".bf_localized];
        }
    }
}

-(NSArray*)functionArgumentsDefaultValuesForFunction:(BFModelAdditionalFunction)function
{
    switch (function) {
        case BFModelAdditionalFunctionNone: {
            return nil;
        }
        case BFModelAdditionalFunctionLag: {
            return @[@7];
        }
        case BFModelAdditionalFunctionExpMovAvg: {
            return @[@5, @30];
        }
    }
}

-(instancetype)requestBySelectingMetricIdx:(NSInteger)idx withMeta:(BFModelObjectMeta*)meta
{
    NSString * metricId = meta.metrics[idx].metricId;
    
    // Check, if we have a default params for this metric
    NSString * defaultRequest = meta.metrics[idx].defaultRequest;
    if (defaultRequest) {
        NSString * requestToBuild = [NSString stringWithFormat:@"%@.%@",[BFViewModelMetricRequestSingle objectIdPlaceholder], defaultRequest];
        BFViewModelMetricRequestSingle * defaultReqObj = (BFViewModelMetricRequestSingle*)[BFViewModelMetricRequest fromSerializedString:requestToBuild];
        if (defaultReqObj) {
            return defaultReqObj;
        }
    }
    
    BFViewModelMetricRequestSingle * req = [BFViewModelMetricRequestSingle withMetricId:metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:self.aggregationFucntion additionalFunction:self.additionalFunction additionalFunctionArgs:self.additionalFunctionArgs timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:self.timeRangeAggregationArgs];
    return req;
}

-(instancetype)requestBySelectingObjectGroupAggregationIdx:(NSInteger)idx
{
    BFModelObjectGroupAggregation func = BFModelObjectGroupAggregationSum;
    if (idx == 0) {
        func = BFModelObjectGroupAggregationSum;
    } else if (idx == 1) {
        func = BFModelObjectGroupAggregationMin;
    } else if (idx == 2) {
        func = BFModelObjectGroupAggregationMax;
    } else if (idx == 3) {
        func = BFModelObjectGroupAggregationAvg;
    }
    BFViewModelMetricRequestSingle * req = [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:func aggregationFucntion:self.aggregationFucntion additionalFunction:self.additionalFunction additionalFunctionArgs:self.additionalFunctionArgs timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:self.timeRangeAggregationArgs];
    return req;
}

-(instancetype)requestBySelectingGroupByIdx:(NSInteger)idx
{
    BFModelAggreagationFunction func = BFModelAggreagationFunctionLast;
    if (idx == 0) {
        func = BFModelAggreagationFunctionLast;
    } else if (idx == 1) {
        func = BFModelAggreagationFunctionSum;
    } else if (idx == 2) {
        func = BFModelAggreagationFunctionMin;
    } else if (idx == 3) {
        func = BFModelAggreagationFunctionMax;
    } else if (idx == 4) {
        func = BFModelAggreagationFunctionAvg;
    }
    BFViewModelMetricRequestSingle * req = [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:func additionalFunction:self.additionalFunction additionalFunctionArgs:self.additionalFunctionArgs timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:self.timeRangeAggregationArgs];
    return req;
}

-(instancetype)requestBySelectingFunctionIdx:(NSInteger)idx
{
    BFModelAdditionalFunction func = BFModelAdditionalFunctionNone;
    if (idx == 0) {
        func = BFModelAdditionalFunctionLag;
    } else if (idx == 1) {
        func = BFModelAdditionalFunctionExpMovAvg;
    } else if (idx == 2) {
        func = BFModelAdditionalFunctionNone;
    }
    if (func == self.additionalFunction) {
        return self;
    }
    NSArray * args = [self functionArgumentsDefaultValuesForFunction:func];
    BFViewModelMetricRequestSingle * req = [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:self.aggregationFucntion additionalFunction:func additionalFunctionArgs:args timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:self.timeRangeAggregationArgs];
    return req;
}

-(instancetype)requestByUpdatingFunctionArgs:(NSArray*)args
{
    BFViewModelMetricRequestSingle * req = [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:self.aggregationFucntion additionalFunction:self.additionalFunction additionalFunctionArgs:args timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:self.timeRangeAggregationArgs];
    return req;
}

-(instancetype)requestBySelectingTimeRangeAggrIdx:(NSInteger)idx    andCurrentTimeRange:(BFModelTimePeriod*)timeRange
{
    BFModelTimeRangeAggregation func = BFModelTimeRangeAggregationAt;
    NSObject * timeRangeAggregationArgs;
    if (idx == 0) {
        func = BFModelTimeRangeAggregationAt;
        timeRangeAggregationArgs = [timeRange datesRange].lastObject;
    } else if (idx == 1) {
        func = BFModelTimeRangeAggregationSum;
        timeRangeAggregationArgs = timeRange;
    } else if (idx == 2) {
        func = BFModelTimeRangeAggregationMin;
        timeRangeAggregationArgs = timeRange;
    } else if (idx == 3) {
        func = BFModelTimeRangeAggregationMax;
        timeRangeAggregationArgs = timeRange;
    } else if (idx == 4) {
        func = BFModelTimeRangeAggregationAvg;
        timeRangeAggregationArgs = timeRange;
    }
    
    BFViewModelMetricRequestSingle * req = [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:self.aggregationFucntion additionalFunction:self.additionalFunction additionalFunctionArgs:self.additionalFunctionArgs timeRangeAggregation:func timeRangeAggregationArgs:timeRangeAggregationArgs];
    return req;
}

@end
