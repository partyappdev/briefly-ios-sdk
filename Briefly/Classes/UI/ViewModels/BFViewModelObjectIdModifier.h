//
//  BFViewModelObjectIdModifier.h
//  Base64
//
//  Created by Artem Bondar on 14.02.2018.
//

#import <Foundation/Foundation.h>
#import "BFPublicModels.h"

@interface BFViewModelObjectIdModifier : NSObject

+(BFViewModelObjectIdModifier*)modifyerWithSerializedString:(NSString*)string;
-(NSString*)seiralizedString;

-(NSString*)objectIdByApplyingTo:(NSString*)objectId;
-(NSString*)modifierDescriptionWithObjectMeta:(BFModelObjectMeta*)meta;

@end

@interface BFViewModelObjectIdModifierEqual : BFViewModelObjectIdModifier

+(BFViewModelObjectIdModifierEqual*)instance;

@end

@interface BFViewModelObjectIdModifierGroup : BFViewModelObjectIdModifier

+(BFViewModelObjectIdModifierGroup*)instance;

@end
