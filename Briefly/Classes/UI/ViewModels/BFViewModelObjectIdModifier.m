//
//  BFViewModelObjectIdModifier.m
//  Base64
//
//  Created by Artem Bondar on 14.02.2018.
//

#import "BFViewModelObjectIdModifier.h"
#import "NSString+Utils.h"

@implementation BFViewModelObjectIdModifier

+(BFViewModelObjectIdModifier*)modifyerWithSerializedString:(NSString*)string
{
    BFViewModelObjectIdModifier * obj = [BFViewModelObjectIdModifierGroup modifyerWithSerializedString:string];
    if (obj != nil) {
        return obj;
    }
    return [BFViewModelObjectIdModifierEqual instance];
}

-(NSString*)seiralizedString
{
    return nil;
}

-(NSString*)objectIdByApplyingTo:(NSString*)objectId
{
    return nil;
}

-(NSString*)modifierDescriptionWithObjectMeta:(BFModelObjectMeta*)meta
{
    return nil;
}

@end

@implementation BFViewModelObjectIdModifierEqual

+(BFViewModelObjectIdModifierEqual*)instance
{
    return [BFViewModelObjectIdModifierEqual new];
}

+(BFViewModelObjectIdModifier*)modifyerWithSerializedString:(NSString*)string
{
    if (string == nil) {
        return [BFViewModelObjectIdModifierEqual instance];
    }
    return nil;
}

-(NSString*)seiralizedString
{
    return @"";
}

-(NSString*)objectIdByApplyingTo:(NSString*)objectId
{
    return objectId;
}

-(NSString*)modifierDescriptionWithObjectMeta:(BFModelObjectMeta*)meta
{
    // just a UI name
    return meta.objectNameForUI;
}

-(BOOL)isEqual:(NSObject*)object
{
    return [object isKindOfClass:[BFViewModelObjectIdModifierEqual class]];
}


@end

@implementation BFViewModelObjectIdModifierGroup

+(BFViewModelObjectIdModifierGroup*)instance
{
    return [BFViewModelObjectIdModifierGroup new];
}

+(BFViewModelObjectIdModifier*)modifyerWithSerializedString:(NSString*)string
{
    if ([string isEqualToString:@".group()"]) {
        return [BFViewModelObjectIdModifierGroup instance];
    }
    return nil;
}

-(NSString*)seiralizedString
{
    return @".group()";
}

-(NSString*)objectIdByApplyingTo:(NSString*)objectId
{
    return [objectId stringByAppendingString:@".group()"];
}

-(NSString*)modifierDescriptionWithObjectMeta:(BFModelObjectMeta*)meta
{
    // just a UI name
    if (meta.groupMeta.objectNameForUI != nil) {
        return meta.groupMeta.objectNameForUI;
    }
    return [NSString stringWithFormat:@"%@ %@", meta.objectNameForUI, @"BfWidgetProperties.Group".bf_localized];
}

-(BOOL)isEqual:(NSObject*)object
{
    return [object isKindOfClass:[BFViewModelObjectIdModifierGroup class]];
}

@end
