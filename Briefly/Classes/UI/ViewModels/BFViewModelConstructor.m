//
//  BFViewModelConstructor.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFViewModelConstructor.h"
#import "BFControllerMetricRequestConstructor.h"
#import "NSString+Utils.h"
#import "BFModelTimePeriod.h"

@implementation BFViewModelConstructor

-(NSString*)ststicHeaderText
{
    return nil;
}

-(NSInteger)itemsCount
{
    return 0;
}

-(CGFloat)heightForItemAtIndex:(NSInteger)idx
{
    return 0;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellAtIndex:(NSInteger)idx
{
    return nil;
}

-(instancetype)initWithRequest:(BFViewModelMetricRequestSingle*)request
                     timeRange:(BFModelTimePeriod*)timeRange
                          meta:(BFModelObjectMeta*)meta
                           idx:(NSInteger)idx
{
    self = [super init];
    self.request = request;
    self.meta = meta;
    self.idx = idx;
    self.timeRange = timeRange;
    return self;
}

-(void)updateRequestObject:(BFViewModelMetricRequestSingle*)request
{
    self.request = request;
    [self.presenter modelChanged];
}


-(NSString*)backButtonText
{
    return @"BfWidgetProperties.Back".bf_localized;
}

-(void)selectedItemAtIdx:(NSInteger)idx
{
    
}

-(void)willBeDismissed
{
    
}

-(CGFloat)contentHeight
{
    return 0;
}

@end
