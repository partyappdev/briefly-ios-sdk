//
//  BFViewModelConstructor.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import <UIKit/UIKit.h>

@class BFControllerMetricRequestConstructor;
@class BFViewModelMetricRequestSingle;
@class BFModelObjectMeta;
@class BFModelTimePeriod;

@interface BFViewModelConstructor : NSObject

@property (nonatomic) NSInteger idx;
@property (strong, nonatomic) BFModelTimePeriod * timeRange;
@property (strong, nonatomic) BFViewModelMetricRequestSingle * request;
@property (strong, nonatomic) BFModelObjectMeta * meta;
@property (weak, nonatomic) BFControllerMetricRequestConstructor * presenter;

-(NSString*)ststicHeaderText;
-(CGFloat)contentHeight;
-(NSString*)backButtonText;
-(NSInteger)itemsCount;
-(CGFloat)heightForItemAtIndex:(NSInteger)idx;
-(UITableViewCell*)tableView:(UITableView*)tableView cellAtIndex:(NSInteger)idx;

-(instancetype)initWithRequest:(BFViewModelMetricRequestSingle*)request
                     timeRange:(BFModelTimePeriod*)timeRange
                          meta:(BFModelObjectMeta*)meta
                           idx:(NSInteger)idx;

-(void)willBeDismissed;
-(void)selectedItemAtIdx:(NSInteger)idx;
-(void)updateRequestObject:(BFViewModelMetricRequestSingle*)request;

@end
