//
//  BFViewModelPreset.m
//  Briefly
//
//  Created by Artem Bondar on 14.03.2018.
//

#import "BFViewModelPreset.h"
#import "BFViewModelObjectIdModifier.h"
#import "BFViewModelMetricRequest.h"
#import "BFViewModelMetricRequestMulti.h"
#import "BFModelTimePeriod.h"

@implementation BFViewModelPreset

-(NSDictionary*)serializedDescription
{
    NSMutableDictionary * serialized = [NSMutableDictionary dictionary];
    id name = self.name;
    id metric = self.metricRequest.serializedString;
    id time = self.timeRange.serializedString;
    id obj = self.objIdModifier.seiralizedString;
    
    serialized[@"default"] = @(self.defaultPreset);
    if (obj != nil) {
        serialized[@"obj"] = obj;
    }
    if (name != nil) {
        serialized[@"name"] = name;
    }
    if (time != nil) {
        serialized[@"time"] = time;
    }
    if (metric != nil) {
        serialized[@"metric"] = metric;
    }
    return serialized;
}

+(instancetype)fromSerializedDescription:(NSDictionary*)dict
{
    BFViewModelPreset * newObj = [BFViewModelPreset new];
    if (![dict.allKeys containsObject:@"name"]) {
        newObj.name = nil;
    } else {
        newObj.name = dict[@"name"];
    }
    if (![dict.allKeys containsObject:@"metric"]) {
        newObj.metricRequest = nil;
    } else {
        newObj.metricRequest = [BFViewModelMetricRequest fromSerializedString:dict[@"metric"]];
    }
    if (![dict.allKeys containsObject:@"time"]) {
        newObj.timeRange = nil;
    } else {
        newObj.timeRange = [BFModelTimePeriod periodWithSerializedString:dict[@"time"]];
    }
    if (![dict.allKeys containsObject:@"obj"]) {
        newObj.objIdModifier = nil;
    } else {
        newObj.objIdModifier = [BFViewModelObjectIdModifier modifyerWithSerializedString:dict[@"obj"]];
    }
    newObj.defaultPreset = ((NSNumber*)dict[@"default"]).boolValue;
    return newObj;
}

+(instancetype)defaultPresetWithRequest:(BFViewModelMetricRequest*)request
                              timeRange:(BFModelTimePeriod*)timeRange
                          objIdModifier:(BFViewModelObjectIdModifier*)objIdModifier
{
    BFViewModelPreset * newObj = [BFViewModelPreset new];
    newObj.name = nil;
    newObj.defaultPreset = YES;
    newObj.metricRequest = request;
    newObj.timeRange = timeRange;
    newObj.objIdModifier = objIdModifier;
    return newObj;
}

-(instancetype)copyThisWithName:(NSString*)name
{
    BFViewModelPreset * newObj = [BFViewModelPreset new];
    newObj.name = name;
    newObj.defaultPreset = NO;
    newObj.metricRequest = self.metricRequest;
    newObj.timeRange = self.timeRange;
    newObj.objIdModifier = self.objIdModifier;
    return newObj;
}

-(BOOL)isEqual:(NSObject*)object
{
    if ([object isKindOfClass:[BFViewModelPreset class]]) {
        BFViewModelPreset * typed = (BFViewModelPreset*)object;
        BFViewModelMetricRequest * effMetricMine = [self.metricRequest byApplyTimePeriod:self.timeRange];
        BFViewModelMetricRequest * effMetricOthers = [typed.metricRequest byApplyTimePeriod:typed.timeRange];
        return [self.objIdModifier isEqual:typed.objIdModifier] && [effMetricMine isEqual:effMetricOthers];
    }
    return NO;
}

-(BOOL)emptyPreset
{
    return self.metricRequest == nil;
}

-(void)remakeToDefault
{
    self.name = nil;
    self.defaultPreset = YES;
}

-(void)setTimeRange:(BFModelTimePeriod *)timeRange
{
    _timeRange = timeRange;
    if (timeRange) {
        self.metricRequest = [self.metricRequest byApplyTimePeriod:timeRange];
    }
}

-(NSArray<NSString*>*)apiMetricRequestsWithObjectId:(NSString*)objectId
{
    BFViewModelMetricRequest * effMetric = [self.metricRequest byApplyTimePeriod:self.timeRange];
    NSString * effObjId = [self.objIdModifier objectIdByApplyingTo:objectId];
    return [effMetric requestsForApiWithObjectId:effObjId];
}

-(NSString*)apiRequestAtIdx:(NSInteger)idx forObjectId:(NSString*)objectId
{
    BFViewModelMetricRequest * effMetric = self.metricRequest;
    if ([effMetric isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
        NSArray * childs = [(BFViewModelMetricRequestMulti*)effMetric childMetricRequests];
        if (idx >= 0 && idx < childs.count) {
            effMetric = childs[idx];
        } else {
            return nil;
        }
        
    } else {
        if (idx != 0) {
            return nil;
        }
    }
    effMetric = [effMetric byApplyTimePeriod:self.timeRange];
    NSString * effObjId = [self.objIdModifier objectIdByApplyingTo:objectId];
    return [effMetric requestsForApiWithObjectId:effObjId].firstObject;
}

-(NSArray<NSString*>*)apiMetricChartRequestsWithObjectId:(NSString*)objectId andTimeRange:(BFModelTimePeriod*)timeRange
{
    BFViewModelMetricRequest * effMetric = [self.metricRequest byApplyTimePeriod:self.timeRange];
    NSString * effObjId = [self.objIdModifier objectIdByApplyingTo:objectId];
    return [effMetric requestsForApiWithObjectId:effObjId];
}

@end
