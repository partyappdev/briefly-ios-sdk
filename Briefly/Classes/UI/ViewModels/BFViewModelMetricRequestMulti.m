//
//  BFViewModelMetricRequestMulti.m
//  Base64
//
//  Created by Artem Bondar on 07.03.2018.
//

#import "BFViewModelMetricRequestMulti.h"
#import "BFViewModelMetricRequestSingle+Serialization.h"

#import "NSString+Utils.h"

@implementation BFViewModelMetricRequestMulti

+(BFViewModelMetricRequest*)fromSerializedString:(NSString*)serializedString
{
    NSString* pattern = [BFViewModelMetricRequestSingle regexpForParse];
    NSRange fullStringRange = (NSRange){0, serializedString.length};
    NSError* error = NULL;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    if (error){
        return nil;
    }
    
    NSMutableArray * childParams = [NSMutableArray array];
    [regex enumerateMatchesInString:serializedString options:0 range:fullStringRange usingBlock:^(NSTextCheckingResult * _Nullable result, NSMatchingFlags flags, BOOL * _Nonnull stop) {
        
        BFViewModelMetricRequest * childParam = [BFViewModelMetricRequest fromSerializedString:[serializedString substringWithRange:[result rangeAtIndex:0]]];
        if (childParam) {
            [childParams addObject:childParam];
        }
    }];
    if (childParams.count == 0) {
        return nil;
    }
    return [BFViewModelMetricRequestMulti withArray:childParams];
}

-(NSString*)serializedString
{
    NSString * resultString = @"";
    // should be implemented by childs
    for (BFViewModelMetricRequest * childProperty in self.childMetricRequests) {
        if (resultString.length > 0) {
            resultString = [resultString stringByAppendingString:@","];
        }
        resultString = [resultString stringByAppendingString:childProperty.serializedString];
    }
    return resultString;
}

-(BOOL)isBasiclyEqualTo:(BFViewModelMetricRequest*)property
{
    if (![property isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
        return NO;
    }
    BFViewModelMetricRequestMulti * other = (BFViewModelMetricRequestMulti*)property;
    if (other.childMetricRequests.count != self.childMetricRequests.count) {
        return NO;
    }
    
    __block BOOL equal = YES;
    [self.childMetricRequests enumerateObjectsUsingBlock:^(BFViewModelMetricRequest * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (![obj isBasiclyEqualTo:other.childMetricRequests[idx]]) {
            equal = NO;
            *stop = YES;
        }
    }];
    return equal;
}

-(BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
        return NO;
    }
    BFViewModelMetricRequestMulti * other = (BFViewModelMetricRequestMulti*)object;
    if (other.childMetricRequests.count != self.childMetricRequests.count) {
        return NO;
    }
    
    __block BOOL equal = YES;
    [self.childMetricRequests enumerateObjectsUsingBlock:^(BFViewModelMetricRequest * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (![obj isEqual:other.childMetricRequests[idx]]) {
            equal = NO;
            *stop = YES;
        }
    }];
    return equal;
}

-(NSString*)uiNameWithObjectsMeta:(BFModelObjectMeta*)meta
{
    NSString * resultString = @"";
    // should be implemented by childs
    for (BFViewModelMetricRequest * childProperty in self.childMetricRequests) {
        if (resultString.length > 0) {
            resultString = [resultString stringByAppendingString:@", "];
        }
        resultString = [resultString stringByAppendingString:[childProperty uiNameWithObjectsMeta:meta]];
    }
    return resultString;
}

-(instancetype)byApplyTimePeriod:(BFModelTimePeriod*)period
{
    NSMutableArray * modifyed = [NSMutableArray array];
    // should be implemented by childs
    for (BFViewModelMetricRequest * childProperty in self.childMetricRequests) {
        [modifyed addObject:[childProperty byApplyTimePeriod:period]];
    }
    return [BFViewModelMetricRequestMulti withArray:modifyed];
}

-(NSString*)stringForValue:(NSNumber*)value withObjectsMeta:(BFModelObjectMeta*)meta
{
    return [NSString bf_humanReadableStringFromNumber:value unit:@" "];
}

-(NSArray<NSString*>*)requestsForChartWithObjectId:(NSString*)objectId andRange:(BFModelTimePeriod*)timeRange
{
    NSMutableArray * childRequests = [NSMutableArray array];
    for (BFViewModelMetricRequest * child in self.childMetricRequests) {
        [childRequests addObjectsFromArray:[child requestsForChartWithObjectId:objectId andRange:timeRange]];
    }
    return childRequests;
}

-(BFModelTimePeriod*)defaultTimeTangeArg
{
    BFModelTimePeriod * ret = self.childMetricRequests.firstObject.defaultTimeTangeArg;
    if (ret == nil) {
        ret = [BFModelTimePeriod defaultRange];
    }
    return ret;
}

-(NSArray*)requestsForApiWithObjectId:(NSString*)objectId
{
    NSMutableArray * childRequests = [NSMutableArray array];
    for (BFViewModelMetricRequest * child in self.childMetricRequests) {
        [childRequests addObjectsFromArray:[child requestsForApiWithObjectId:objectId]];
    }
    return childRequests;
}

-(BOOL)doesThisRequestIncludes:(BFViewModelMetricRequest*)included
{
    return [self containsBasiclyEqual:included];
}

+(instancetype)withArray:(NSArray<BFViewModelMetricRequest*>*)array
{
    BFViewModelMetricRequestMulti * newObj = [BFViewModelMetricRequestMulti new];
    newObj.childMetricRequests = array;
    return newObj;
}

-(BOOL)containsBasiclyEqual:(BFViewModelMetricRequest*)attribute
{
    // check, if already have:
    for (BFViewModelMetricRequest * existingAttr in self.childMetricRequests) {
        if ([existingAttr isBasiclyEqualTo:attribute]) {
            return YES;
        }
    }
    return NO;
}

-(BFViewModelMetricRequest*)byAppendingRequest:(BFViewModelMetricRequest*)request
{
    return [BFViewModelMetricRequestMulti withArray:[self.childMetricRequests arrayByAddingObject:request]];
}

-(BFViewModelMetricRequest*)byInsertingRequest:(BFViewModelMetricRequest*)request
{
    return [BFViewModelMetricRequestMulti withArray:[@[request] arrayByAddingObjectsFromArray:self.childMetricRequests]];
}

-(BFViewModelMetricRequest*)byRemovingRequestAtIdx:(NSInteger)idx
{
    if (idx < 0 || idx >= self.childMetricRequests.count) {
        return self;
    }
    NSMutableArray * arr = [NSMutableArray arrayWithArray:self.childMetricRequests];
    [arr removeObjectAtIndex:idx];
    return [BFViewModelMetricRequestMulti withArray:arr];
}

-(BFViewModelMetricRequest*)byUpdatingRequestAtIdx:(NSInteger)idx to:(BFViewModelMetricRequest*)request
{
    if (idx < 0 || idx >= self.childMetricRequests.count) {
        return self;
    }
    NSMutableArray * arr = [NSMutableArray arrayWithArray:self.childMetricRequests];
    arr[idx] = request;
    return [BFViewModelMetricRequestMulti withArray:arr];
}

-(NSArray<BFViewModelMetricRequest*>*)flatterenMetricRequests
{
    return self.childMetricRequests;
}

@end
