//
//  BFViewModelMetricRequestSingle.h
//  Base64
//
//  Created by Artem Bondar on 07.03.2018.
//

#import "BFViewModelMetricRequest.h"

typedef enum : NSUInteger {
    BFModelObjectGroupAggregationMin,
    BFModelObjectGroupAggregationSum,
    BFModelObjectGroupAggregationMax,
    BFModelObjectGroupAggregationAvg,
} BFModelObjectGroupAggregation;

typedef enum : NSUInteger {
    BFModelAggreagationFunctionLast,
    BFModelAggreagationFunctionSum,
    BFModelAggreagationFunctionMax,
    BFModelAggreagationFunctionMin,
    BFModelAggreagationFunctionAvg
} BFModelAggreagationFunction;

typedef enum : NSUInteger {
    BFModelAdditionalFunctionNone,
    BFModelAdditionalFunctionLag,
    BFModelAdditionalFunctionExpMovAvg
} BFModelAdditionalFunction;

typedef enum : NSUInteger {
    BFModelTimeRangeAggregationRange,
    BFModelTimeRangeAggregationAt,
    BFModelTimeRangeAggregationSum,
    BFModelTimeRangeAggregationMin,
    BFModelTimeRangeAggregationMax,
    BFModelTimeRangeAggregationAvg
} BFModelTimeRangeAggregation;

@class BFModelTimePeriod;

@interface BFViewModelMetricRequestSingle : BFViewModelMetricRequest

@property (strong, nonatomic) NSString * metricId;
@property (nonatomic) BFModelObjectGroupAggregation objectGroupAggregation;
//@property (strong, nonatomic) BFModelTimePeriod * groupByPeriod;
@property (nonatomic) BFModelAggreagationFunction aggregationFucntion;
//@property (strong, nonatomic) NSObject * aggregationArgs;
@property (nonatomic) BFModelAdditionalFunction additionalFunction;
@property (strong, nonatomic) NSArray * additionalFunctionArgs;
@property (nonatomic) BFModelTimeRangeAggregation timeRangeAggregation;
@property (strong, nonatomic) NSObject * timeRangeAggregationArgs;

// Arguments getters for different functions
//-(void)argAggregationFunctionLast; - last dont have args
//-(void)argAggregationFunctionSum; - sum dont have args
//-(void)argAggregationFunctionMax; - max dont have args

-(NSNumber*)argAdditionalFunctionLag;
-(NSArray<NSNumber*>*)argAdditionalFunctionExpMovAvg;

-(NSDate*)argAggregationTimeRangeAggregationAt;
-(BFModelTimePeriod*)argAggregationTimeRangeAggregationSum;
-(BFModelTimePeriod*)argAggregationTimeRangeAggregationRange;

//
// Constructors
//
+(BFViewModelMetricRequestSingle*)dummyWithAlreadyUsedMetricIds:(NSArray<NSString*>*)usedIds andMeta:(BFModelObjectMeta*)meta;
+(BFViewModelMetricRequestSingle*)dummyForMetricId:(NSString*)metricId;
+(BFViewModelMetricRequestSingle*)withMetricId:(NSString*)metricId
                           objGroupAggregation:(BFModelObjectGroupAggregation)objGroupAggregation
                           aggregationFucntion:(BFModelAggreagationFunction)aggregationFucntion
                            additionalFunction:(BFModelAdditionalFunction)additionalFunction
                        additionalFunctionArgs:(NSArray*)additionalFunctionArgs
                          timeRangeAggregation:(BFModelTimeRangeAggregation)timeRangeAggregation
                      timeRangeAggregationArgs:(NSObject*)timeRangeAggregationArgs;

@end
