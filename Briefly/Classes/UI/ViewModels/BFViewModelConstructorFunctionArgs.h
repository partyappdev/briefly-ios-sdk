//
//  BFViewModelConstructorFunctionArgs.h
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import "BFViewModelConstructorFunction.h"

@interface BFViewModelConstructorFunctionArgs : BFViewModelConstructor

@property (strong, nonatomic) BFViewModelConstructorFunction * parent;

@property (strong, nonatomic) NSArray * arguments;
@property (strong, nonatomic) NSArray * argumentsDescriptions;

+(instancetype)withFunctionPage:(BFViewModelConstructorFunction*)functionPage;

-(BOOL)havePropertiesToChange;

@end
