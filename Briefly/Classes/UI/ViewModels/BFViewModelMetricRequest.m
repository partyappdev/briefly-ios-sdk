//
//  BFViewModelMetricRequest.m
//  Base64
//
//  Created by Artem Bondar on 07.03.2018.
//

#import "BFViewModelMetricRequest.h"
#import "BFViewModelMetricRequestMulti.h"
#import "BFViewModelMetricRequestSingle.h"
#import "BFPublicModels.h"
#import "BFFSCalendar.h"
#import "BFModelChartData.h"

#import "NSString+Utils.h"

@implementation BFViewModelMetricRequest

-(NSString*)serializedString
{
    // should be implemented by childs
    return nil;
}

+(BFViewModelMetricRequest*)fromSerializedString:(NSString*)serializedString
{
    if (serializedString == nil) {
        return nil;
    }
    
    BFViewModelMetricRequest * single = [BFViewModelMetricRequestSingle fromSerializedString:serializedString];
    if (single) {
        return single;
    }
    return [BFViewModelMetricRequestMulti fromSerializedString:serializedString];
}

-(BOOL)isBasiclyEqualTo:(BFViewModelMetricRequest*)property
{
    return NO;
}

-(NSArray<NSString*>*)requestsForChartWithObjectId:(NSString*)objectId andRange:(BFModelTimePeriod*)timeRange;
{
    return @[];
}

-(NSArray*)requestsForApiWithObjectId:(NSString*)objectId
{
    return @[];
}


-(NSString*)uiNameWithObjectsMeta:(BFModelObjectMeta*)meta
{
    return @"";
}

-(instancetype)byApplyTimePeriod:(BFModelTimePeriod*)period
{
    return self;
}

-(NSString*)stringForValue:(NSNumber*)value withObjectsMeta:(BFModelObjectMeta*)meta
{
    return [NSString bf_humanReadableStringFromNumber:value unit:nil];
}

-(NSString*)stringDescriptionForMetricWithObjGroupingAggr:(BOOL)objGroupingAggr
{
    return @"";
}

-(NSString*)stringDescriptionForTimeAggregation
{
    return @"";
}

+(NSString*)regexpForParse
{
    return @"";
}

-(BFModelTimePeriod*)timeRangeAggreagationArgs
{
    return nil;
}

-(BFModelTimePeriod*)defaultTimeTangeArg
{
    return [BFModelTimePeriod defaultRange];
}

-(BOOL)doesThisRequestIncludes:(BFViewModelMetricRequest*)included
{
    if ([self isBasiclyEqualTo:included]) {
        // already selected - need deselect
        // but at the same time, we don't deselect the last selected
        // metric
        return YES;
    } else {
        return NO;
    }
}
-(NSArray<BFViewModelMetricRequest*>*)flatterenMetricRequests
{
    return @[self];
}

@end
