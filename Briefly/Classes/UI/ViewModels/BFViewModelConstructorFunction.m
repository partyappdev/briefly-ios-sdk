//
//  BFViewModelConstructorFunction.m
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import "BFViewModelConstructorFunction.h"
#import "BFViewModelConstructorFunctionArgs.h"
#import "BFViewModelMetricRequestSingle+Constructor.h"

#import "BFControllerMetricRequestConstructor.h"
#import "NSString+Utils.h"
#import "BFModelTimePeriod.h"

@implementation BFViewModelConstructorFunction

+(instancetype)withMainPage:(BFViewModelConstructorMainPage*)mainPage
{
    BFViewModelConstructorFunction * newObj = [[BFViewModelConstructorFunction alloc] initWithRequest:mainPage.request timeRange:mainPage.timeRange meta:mainPage.meta idx:mainPage.idx];
    newObj.parent = mainPage;
    newObj.itemStrings = [mainPage.request acessableFunctionList];
    return newObj;
}

-(NSInteger)itemsCount
{
    return self.itemStrings.count + 1;
}

-(CGFloat)heightForItemAtIndex:(NSInteger)idx
{
    if (idx == self.itemStrings.count) {
        return 64;
    }
    return 44;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellAtIndex:(NSInteger)idx
{
    NSInteger selectedIdx = [self.request indexForCurrentFunction];
    if (idx < self.itemStrings.count) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"checkmark" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
        [cell.textLabel setText:self.itemStrings[idx]];
        cell.accessoryType = selectedIdx == idx ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        return cell;
    }
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"description" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
    [cell.textLabel setText:@"BfMetricBuilder.Function.Description".bf_localized];
    [cell.detailTextLabel setText:[self.request descriptionMetricShortWithMeta:self.meta]];
    return cell;
}

-(void)selectedItemAtIdx:(NSInteger)idx
{
    //
    // change fucntion
    //
    [self updateRequestObject:[self.request requestBySelectingFunctionIdx:idx]];
    
    //
    // and open arguments editor
    //
    BFViewModelConstructorFunctionArgs * args = [BFViewModelConstructorFunctionArgs withFunctionPage:self];
    if (args.havePropertiesToChange) {
        BFControllerMetricRequestConstructor * ctrl = [BFControllerMetricRequestConstructor instantiateWithModel:args];
        [self.presenter presentViewController:ctrl animated:YES completion:^{
            //[self.presenter modelChanged];
        }];
    }
}

-(void)updateRequestObject:(BFViewModelMetricRequestSingle *)request
{
    self.itemStrings = [request acessableFunctionList];
    [super updateRequestObject:request];
    [self.parent updateRequestObject:self.request];
}

-(CGFloat)contentHeight
{
    return ([self itemsCount] - 1) * [self heightForItemAtIndex:0] + [self heightForItemAtIndex:([self itemsCount] - 1)];
}

-(NSString*)ststicHeaderText
{
    return @"BfMetricBuilder.Function.HeaderTitle".bf_localized;
}

@end
