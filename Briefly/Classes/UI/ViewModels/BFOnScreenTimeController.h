//
//  BFOnScreenTimeController.h
//  Briefly
//
//  Created by Artem Bondar on 21.03.2018.
//

#import <Foundation/Foundation.h>

@interface BFOnScreenTimeController : NSObject

-(void)startRollCall;
-(void)rollCallObjectIdCalled:(NSString*)objectId;
-(void)finishRollCall;

-(void)appClosed;
-(void)appOpened;

@end
