//
//  BFViewModelChart.m
//  Briefly
//
//  Created by Artem Bondar on 01/02/2018.
//

#import "BFViewModelChart.h"
#import "BFViewModelMetricRequest.h"
#import "BFBrieflyManager+ApiWrapper.h"

#import "NSDate+BfUtils.h"
#import "NSString+Utils.h"
#import "BFRange.h"

NSInteger bfChartLoadingDaysAtOnce = 30;
@interface BFViewModelChartDataPart : NSObject

+(instancetype)partWithMetric:(BFViewModelMetricRequest*)metric
                      timeRange:(BFModelTimePeriod*)timeRange
                           data:(BFModelChartData*)data
                         startX:(NSInteger)startX;

@property (nonatomic) NSInteger startX;
@property (nonatomic, readonly) NSInteger endX;
@property (strong, nonatomic) BFModelChartData * data;
@property (strong, nonatomic) BFModelTimePeriod * timeRange;
@property (strong, nonatomic) BFViewModelMetricRequest *metric;

@end


///
///
/// Interface + Implementation of chart data model
///
///
@interface BFViewModelChart ()

// Model state data data
@property (strong, nonatomic) NSArray<BFViewModelChartDataPart*> * charts;
@property (strong, nonatomic) BFModelTimePeriod * timeRange;
@property (strong, nonatomic) NSString * objectId;
@property (strong, nonatomic) BFViewModelMetricRequest * metric;
@property (strong, nonatomic) NSCalendar * calendar;
@property (strong, nonatomic) NSDate * currentLastDate;
@property (strong, nonatomic) NSDate * maximalDate;
@property (nonatomic) BFRange startRange;

// UI state data
@property (nonatomic) BFRange requestedLoadingRange;
@property (nonatomic) BOOL loadingHistorical;
@property (nonatomic) BOOL loadingFuture;
@property (nonatomic) BOOL loadingHistoricalFailed;
@property (nonatomic) BOOL loadingFutureFailed;

// Calculated model state variables
@property (strong, nonatomic) NSDate * minimalDate;
@property (nonatomic) NSInteger dataWindowWidth;

// Subscribers
@property (strong, nonatomic) NSHashTable * subscribers;

@end

@implementation BFViewModelChart

+(instancetype)viewModelChartWithMetric:(BFViewModelMetricRequest*)metric
                                timeRange:(BFModelTimePeriod*)timeRange
                                 objectId:(NSString*)objectId
                              maximalDate:(NSDate*)maxDate
                               atCalendar:(NSCalendar*)calendar
{
    if (metric == nil) {
        return nil;
    }
    BFViewModelChart * newObj = [[BFViewModelChart alloc] init];
    newObj.metric = metric;
    newObj.objectId = objectId;
    newObj.maximalDate = maxDate;
    newObj.calendar = calendar;
    newObj.timeRange = timeRange;
    newObj.charts = @[];
    newObj.requestedLoadingRange = (BFRange){0,0};
    
    // Now calculate all dependent constants
    NSTimeInterval oneYear = 60 * 60 * 24 * 365;
    NSArray * dates = [timeRange datesRange];
    NSDate * minimalDate = [maxDate dateByAddingTimeInterval:-oneYear];
    newObj.minimalDate = minimalDate;
    newObj.currentLastDate = dates.lastObject;
    newObj.dataWindowWidth = dates.count;
    
    // Calculate start range
    NSInteger pos = [dates.firstObject bfDaysFromDate:minimalDate];
    NSInteger length = [dates.lastObject bfDaysFromDate:dates.firstObject] + 1;
    newObj.startRange = (BFRange){pos, length};
    
    // Utils
    newObj.subscribers = [NSHashTable weakObjectsHashTable];

    return newObj;
}

-(BOOL)showChartForProperty:(BFViewModelMetricRequest*)property objectId:(NSString*)objectId
{
    return [self.metric isEqual:property] && [self.objectId isEqualToString:objectId];
}

-(void)setObjectMeta:(BFModelObjectMeta *)objectMeta
{
    _objectMeta = objectMeta;
    [self notifySubscribers];
}

-(BOOL)canLoadPreviousRange
{
    BFViewModelChartDataPart * firstLoaded = self.charts.firstObject;
    
    // check, is it too old past
    if (firstLoaded && [[firstLoaded.timeRange periodByAddingDaysShift:-bfChartLoadingDaysAtOnce withCurrentDate:self.maximalDate].datesRange.lastObject compare:self.minimalDate] != NSOrderedDescending) {
        return NO;
    }
    return YES;
}

-(BOOL)canLoadNextRange
{
    BFViewModelChartDataPart * lastLoaded = self.charts.lastObject;
    
    if (!lastLoaded) {
        return NO;
    }
    
    // If the perioud is in the future (and the return is Null) - then can't load more
    if ([lastLoaded.timeRange periodByAddingDaysShift:bfChartLoadingDaysAtOnce withCurrentDate:self.maximalDate] != nil) {
        return YES;
    }
    return NO;
}

-(void)fetchFirstRegionIfNeeded
{
    if (self.charts.count == 0) {
        [self fetchPreviousRange];
    }
}

-(void)loadDataRangeIfNeeded:(BFRange)range
{
    self.requestedLoadingRange = BFRangeIntersection(BFRangeUnion(self.requestedLoadingRange, range), (BFRange){0, self.lengthX});
    [self startLoadingAccordingRequestedRange];
}

-(void)startLoadingAccordingRequestedRange
{
    // the loading-more logic is not working until we load first part
    if (self.charts.count == 0) {
       return;
    }
    
    // check, if we need to load more into past
    BFRange loaded = [self loadedRange];
    if (self.requestedLoadingRange.location < loaded.location) {
        // start loading more in past
        [self fetchPreviousRange];
    }
    
    if (self.requestedLoadingRange.location + self.requestedLoadingRange.length > loaded.location + loaded.length) {
        // start loading more in future
        [self fetchNextRange];
    }
}

-(void)fetchPreviousRange
{
    if (self.loadingHistorical || !self.canLoadPreviousRange) {
        return;
    }
    
    // take last loaded graph, and continue loading, or use a base property to start
    BFViewModelMetricRequest * metricToLoad;
    BFViewModelChartDataPart * firstLoaded = self.charts.firstObject;
    BFModelTimePeriod * timeRangeToLoad = self.timeRange;
    if (firstLoaded) {
        timeRangeToLoad = [firstLoaded.timeRange previousRangeWithDaysLength:bfChartLoadingDaysAtOnce maxDate:self.maximalDate];
        metricToLoad = [firstLoaded.metric byApplyTimePeriod:timeRangeToLoad];
    } else {
        timeRangeToLoad = [BFModelTimePeriodDatesRange periodWithDateStart:[self.currentLastDate bfDateByShiftingForDays:-1*bfChartLoadingDaysAtOnce] dateEnd:self.currentLastDate];
        metricToLoad = [self.metric byApplyTimePeriod:timeRangeToLoad];
    }
    NSArray * requests = [metricToLoad requestsForChartWithObjectId:self.objectId andRange:timeRangeToLoad];
    if (!requests) {
        return;
    }
    
    self.loadingHistoricalFailed = NO;
    self.loadingHistorical = YES;
    __weak __typeof__(self) weakSelf = self;
    [[BFBrieflyManager sharedManager] chartForRequests:requests withHandler:^(BOOL succeed, BFModelChartData * data) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Now store the chart data anyway
            weakSelf.loadingHistoricalFailed = !succeed;
            weakSelf.loadingHistorical = NO;
            
            if (data) {
                NSInteger startX = [self idxForDate:timeRangeToLoad.datesRange.firstObject];
                BFViewModelChartDataPart * part = [BFViewModelChartDataPart partWithMetric:metricToLoad timeRange:timeRangeToLoad data:data startX:startX];
                weakSelf.charts = [@[part] arrayByAddingObjectsFromArray:weakSelf.charts];
            }
            [weakSelf notifySubscribers];
            [weakSelf startLoadingAccordingRequestedRange];
        });
    }];
}

-(void)fetchNextRange
{
    if (self.loadingFuture || !self.canLoadNextRange) {
        return;
    }
    
    // take last loaded graph, and continue loading, or use a base property to start
    BFViewModelChartDataPart * lastLoaded = self.charts.lastObject;
    BFModelTimePeriod * timeRangeToLoad = [lastLoaded.timeRange nextRangeWithDaysLength:bfChartLoadingDaysAtOnce maxDate:self.maximalDate];
    
    if (timeRangeToLoad == nil) {
        self.loadingFuture = NO;
        self.loadingFutureFailed = NO;
        return;
    }
    BFViewModelMetricRequest * metricToLoad = [lastLoaded.metric byApplyTimePeriod:timeRangeToLoad];
    NSArray * requests = [metricToLoad requestsForChartWithObjectId:self.objectId andRange:timeRangeToLoad];

    if (!requests) {
        return;
    }

    self.loadingFutureFailed = NO;
    self.loadingFuture = YES;
    __weak __typeof__(self) weakSelf = self;
    [[BFBrieflyManager sharedManager] chartForRequests:requests
                                           withHandler:^(BOOL succeed, BFModelChartData * data) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Now store the chart data anyway
            weakSelf.loadingFutureFailed = !succeed;
            weakSelf.loadingFuture = NO;
        
            if (data) {
                NSInteger startX = [self idxForDate:timeRangeToLoad.datesRange.firstObject];
                BFViewModelChartDataPart * part = [BFViewModelChartDataPart partWithMetric:metricToLoad timeRange:timeRangeToLoad data:data startX:startX];
                weakSelf.charts = [weakSelf.charts arrayByAddingObject:part];
            
                // and also move forward the date:
            
                weakSelf.currentLastDate = [weakSelf.currentLastDate bfDateByShiftingForDays:data.points.count];
            }
            [weakSelf notifySubscribers];
            [weakSelf startLoadingAccordingRequestedRange];
        });
    }];
}

-(NSInteger)idxForDate:(NSDate*)date
{
    if (date == nil) {
        return NSNotFound;
    }
    return [date bfDaysFromDate:self.minimalDate];
}

-(NSDate*)dateForIdx:(NSInteger)idx
{
    return [self.minimalDate bfDateByShiftingForDays:idx];
}

-(BOOL)isUnloadedState
{
    return (self.objectMeta == nil || self.charts.count == 0);
}

-(BOOL)lastRequestFailed
{
    return (self.loadingFutureFailed && !self.loadingFuture) || (self.loadingHistoricalFailed && !self.loadingHistorical);
}

-(BOOL)isLoading
{
    return (self.loadingFuture) || (!self.loadingHistorical);
}

-(NSInteger)lengthX
{
   return [self.maximalDate bfDaysFromDate:self.minimalDate] + 1;
}

-(NSNumber*)maxValueAtRange:(BFRange)range
{
    NSArray<BFViewModelChartDataPoint*>* points = [self valuesAtRange:range];
    NSNumber *max = nil;
    for (BFViewModelChartDataPoint * point in points) {
        if (!point.dataLoaded)
            continue;

        CGFloat sum = 0;
        for (NSNumber * val in point.multivalue) {
            
            if (self.cummulative) {
                sum += val.doubleValue;
            } else {
                if (val.doubleValue > sum) {
                    sum = val.doubleValue;
                }
            }
        }
        if (max == nil || sum > max.doubleValue) {
            max = @(sum);
        }
    }
    return max;
}

-(NSArray<NSString*>*)linesLabelsAtRange:(BFRange)range
                           forLinesCount:(NSInteger)count
{
    NSMutableArray * ret = [NSMutableArray array];
    CGFloat maxVal  = [self maxValueAtRange:range].doubleValue;
    for (NSInteger i = 0; i < count + 1; ++i) {
        [ret addObject:[NSString bf_humanReadableStringFromNumber:@(maxVal / count * (count - i)) unit:self.charts.firstObject.data.units]];
    }
    return ret;
}

-(NSArray<BFViewModelChartSection*>*)sectionsAtRange:(BFRange)range
{
    // Align the dates to the start/end of the month to return the full section
    NSDate * dateCurrent = [self.minimalDate bfDateByShiftingForDays:range.location];
    NSDate * dateEnd = [self.minimalDate bfDateByShiftingForDays:range.location + range.length];
    
    NSDate * dateCurrentAligned = [dateCurrent bfStartOfTheMonthWithCalendar:self.calendar];
    NSDate * dateEndAligned = [dateEnd bfEndOfTheMonthWithCalendar:self.calendar];
    
    NSMutableArray * sections = [NSMutableArray array];
    NSMutableArray * dateLabelsForCurrentSection = nil;
    NSNumber * lastMonthIdx = nil;
    
    // Don't forget: we have moved the start pointer for the start of the month
    // So we have to also shift the i value from 0
    NSArray * monthSymbols = [self.calendar standaloneMonthSymbols];
    NSInteger i = 0 - [dateCurrent bfDaysFromDate:dateCurrentAligned];
    NSInteger currentStartIdx = i;
    
    while([dateCurrentAligned compare:dateEndAligned] != NSOrderedDescending) {
        NSNumber * mothIdx = @([self.calendar component:NSCalendarUnitMonth fromDate:dateCurrentAligned]);
        NSNumber * dayIdx = @([self.calendar component:NSCalendarUnitDay fromDate:dateCurrentAligned]);
        if (![lastMonthIdx isEqualToNumber:mothIdx]) {
            // Store previously calculated data to array (id collected any)
            if (dateLabelsForCurrentSection.count > 0) {
                NSString * newSectionLabel = monthSymbols[lastMonthIdx.integerValue-1];
                BFViewModelChartSection * newSection = [BFViewModelChartSection sectionWithLabel:newSectionLabel dates:dateLabelsForCurrentSection startX:range.location + currentStartIdx];
                [sections addObject:newSection];
                currentStartIdx = i;
            }
            // and prepare for fetching a new section
            dateLabelsForCurrentSection = [NSMutableArray arrayWithCapacity:40];
            lastMonthIdx = mothIdx;
        }
        [dateLabelsForCurrentSection addObject:dayIdx.stringValue];
        i++;
        dateCurrentAligned = [dateCurrentAligned bfDateByShiftingForDays:1];
    }

    // and add the last one
    NSString * newSectionLabel = monthSymbols[lastMonthIdx.integerValue - 1];
    BFViewModelChartSection * newSection = [BFViewModelChartSection sectionWithLabel:newSectionLabel dates:dateLabelsForCurrentSection startX:range.location + currentStartIdx];
    [sections addObject:newSection];
    return sections;
}

-(BFViewModelChartDataPart*)dataPartForPosition:(NSInteger)position
{
    // Now go backwards, until we find the appropriate data part
    __block BFViewModelChartDataPart * foundPart;
    [self.charts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(BFViewModelChartDataPart * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (obj.startX <= position) {
            foundPart = obj;
            *stop = YES;
            return;
        }
    }];
    
    if (foundPart.endX > position) {
        return foundPart;
    } else {
        return nil;
    }
}

-(NSArray<BFViewModelChartDataPoint*>*)valuesAtRange:(BFRange)range
{
    NSMutableArray * results = [NSMutableArray array];
    BFViewModelChartDataPart * currentDataPart;
    for (NSInteger i = range.location; i < range.length + range.location; ++i) {
        if (!currentDataPart || currentDataPart.endX <= i) {
            // Get the next data part
            currentDataPart = [self dataPartForPosition:i];
        }
        // add a multival ata point to results
        BFViewModelChartDataPoint * pt = [BFViewModelChartDataPoint new];
        if (currentDataPart) {
            // If we found the chart part for this X - add value
            pt.multivalue = currentDataPart.data.points[i - currentDataPart.startX].values;
            pt.dataLoaded = YES;
        } else {
            // If not found - return 'this point is non loaded yet'
            pt.dataLoaded = NO;
            pt.past = self.charts.firstObject.startX > i;
        }
        [results addObject:pt];
    }
    return results;
}

-(BFRange)loadedRange
{
    NSInteger startIdx = [self.charts.firstObject startX];
    NSInteger endIdx = [self.charts.lastObject endX];
    return (BFRange){startIdx, endIdx - startIdx};
}

-(BFModelTimePeriod*)timePeriodForRange:(BFRange)range
{
    return [self.timeRange periodByAddingDaysShift:range.location - self.startRange.location withCurrentDate:self.maximalDate];
}

-(void)subscribeForChanges:(id<BFViewModelChartDelegate>)subscriber
{
    [self.subscribers addObject:subscriber];
}

-(void)notifySubscribers
{
    //
    // Delegate list is a very fragile stuff - we should always be careful
    // anc copy before enumeration to avoid chage-list-while-enum
    //
    NSArray * subs = [NSArray arrayWithArray:self.subscribers.allObjects];
    for (id <BFViewModelChartDelegate> delegate in subs) {
        [delegate chartDataChanged];
    }
}

@end

@implementation BFViewModelChartSection

+(instancetype)sectionWithLabel:(NSString*)label
                          dates:(NSArray<NSString*>*)dates
                         startX:(NSInteger)sectionStartX
{
    BFViewModelChartSection * newObj = [BFViewModelChartSection new];
    newObj.sectionLabel = label;
    newObj.sectionDatesLabel = dates;
    newObj.sectionStartX = sectionStartX;
    return newObj;
}

@end

@implementation BFViewModelChartDataPart

+(instancetype)partWithMetric:(BFViewModelMetricRequest*)metric
                      timeRange:(BFModelTimePeriod*)timeRange
                           data:(BFModelChartData*)data
                         startX:(NSInteger)startX;
{
    BFViewModelChartDataPart * newObj = [BFViewModelChartDataPart new];
    newObj.metric = metric;
    newObj.data = data;
    newObj.startX = startX;
    newObj.timeRange = timeRange;
    return newObj;
}

-(NSInteger)endX
{
    return self.startX + self.data.points.count;
}

@end

@implementation BFViewModelChartDataPoint

@end
