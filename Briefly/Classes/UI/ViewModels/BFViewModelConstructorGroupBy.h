//
//  BFViewModelConstructorGroupBy.h
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import "BFViewModelConstructor.h"
#import "BFViewModelConstructorMainPage.h"

@interface BFViewModelConstructorGroupBy : BFViewModelConstructor

@property (strong, nonatomic) BFViewModelConstructorMainPage * parent;
@property (strong, nonatomic) NSArray * itemStrings;
+(instancetype)withMainPage:(BFViewModelConstructorMainPage*)mainPage;

@end
