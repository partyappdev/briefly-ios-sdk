//
//  BFViewModelMetricRequestSingle.m
//  Base64
//
//  Created by Artem Bondar on 07.03.2018.
//

#import "BFViewModelMetricRequestSingle.h"
#import "BFViewModelMetricRequestSingle+Serialization.h"
#import "BFPublicModels.h"
#import "BFFSCalendar.h"
#import "BFModelChartData.h"

#import "NSString+Utils.h"

@implementation BFViewModelMetricRequestSingle

+(BFViewModelMetricRequestSingle*)dummyWithAlreadyUsedMetricIds:(NSArray<NSString*>*)usedIds andMeta:(BFModelObjectMeta*)meta
{
    // First of all find out, which metric_id we should add
    BFModelObjectMetricMeta * nonUsed = nil;
    for (BFModelObjectMetricMeta * metric in meta.metrics) {
        if (![usedIds containsObject:metric.metricId]) {
            nonUsed = metric;
            break;
        }
    }
    BFModelObjectMetricMeta * metric = nonUsed != nil ? nonUsed : meta.metrics.firstObject;
    
    if (metric.metricId == nil) {
        return nil;
    }
    
    // Now try to parse the default request
    if (metric.defaultRequest.length > 0) {
        NSString * fullReq = [NSString stringWithFormat:@"%@.%@", [BFViewModelMetricRequestSingle objectIdPlaceholder], metric.defaultRequest];
        BFViewModelMetricRequestSingle * req = (BFViewModelMetricRequestSingle*)[BFViewModelMetricRequest fromSerializedString:fullReq];
        if (req) {
            return req;
        }
    }
    
    // We don't have a default request: create a dummy one:
    return [BFViewModelMetricRequestSingle dummyForMetricId:metric.metricId];
}

+(BFViewModelMetricRequestSingle*)dummyForMetricId:(NSString*)metricId
{
    // Little hack for FF (will be removed soon)
    if ([metricId isEqualToString:@"residue"]) {
        return [BFViewModelMetricRequestSingle withMetricId:metricId objGroupAggregation:BFModelObjectGroupAggregationSum aggregationFucntion:BFModelAggreagationFunctionLast additionalFunction:BFModelAdditionalFunctionNone additionalFunctionArgs:nil timeRangeAggregation:BFModelTimeRangeAggregationAt timeRangeAggregationArgs:nil];
    } else {
        return [BFViewModelMetricRequestSingle withMetricId:metricId
                                        objGroupAggregation:BFModelObjectGroupAggregationSum aggregationFucntion:BFModelAggreagationFunctionLast additionalFunction:BFModelAdditionalFunctionNone additionalFunctionArgs:nil timeRangeAggregation:BFModelTimeRangeAggregationSum timeRangeAggregationArgs:[BFModelTimePeriodTodayRange periodWithRangeLengthDays:@(30)]];
    }
}

+(BFViewModelMetricRequestSingle*)withMetricId:(NSString*)metricId
                           objGroupAggregation:(BFModelObjectGroupAggregation)objGroupAggregation
                           aggregationFucntion:(BFModelAggreagationFunction)aggregationFucntion
                            additionalFunction:(BFModelAdditionalFunction)additionalFunction
                        additionalFunctionArgs:(NSArray*)additionalFunctionArgs
                          timeRangeAggregation:(BFModelTimeRangeAggregation)timeRangeAggregation
                      timeRangeAggregationArgs:(NSObject*)timeRangeAggregationArgs
{
    BFViewModelMetricRequestSingle * newObject = [BFViewModelMetricRequestSingle new];
    newObject.metricId = metricId;
    newObject.aggregationFucntion = aggregationFucntion;
    newObject.additionalFunction = additionalFunction;
    newObject.additionalFunctionArgs = additionalFunctionArgs;
    newObject.timeRangeAggregation = timeRangeAggregation;
    newObject.timeRangeAggregationArgs = timeRangeAggregationArgs;
    newObject.objectGroupAggregation = objGroupAggregation;
    return newObject;
}

+(BFViewModelMetricRequestSingle*)fromSerializedString:(NSString*)serializedString
{
    return [BFViewModelMetricRequestSingle privFromSerializedString:serializedString];
}

-(BOOL)isEqual:(id)object
{
    if ([(NSObject*)object isKindOfClass:[BFViewModelMetricRequestSingle class]]) {
        BFViewModelMetricRequestSingle * objectFormatted = (BFViewModelMetricRequestSingle*)object;
        return [[self serializedString] isEqualToString:[objectFormatted serializedString]];
    }
    return NO;
}

-(BOOL)isBasiclyEqualTo:(BFViewModelMetricRequest*)object
{
    if ([(NSObject*)object isKindOfClass:[BFViewModelMetricRequestSingle class]]) {
        BFViewModelMetricRequestSingle * objectFormatted = (BFViewModelMetricRequestSingle*)object;
        return [self.metricId isEqualToString:objectFormatted.metricId];
        //return [[self serializedStringWithTRAggrFunction:BFModelTimeRangeAggregationAt andTRAggrArgs:nil] isEqualToString:[objectFormatted serializedStringWithTRAggrFunction:BFModelTimeRangeAggregationAt andTRAggrArgs:nil]];
    }
    return NO;
}

-(NSArray<NSString*>*)requestsForChartWithObjectId:(NSString*)objectId andRange:(BFModelTimePeriod*)timeRange;
{
    //
    // TODO: working with string in such a manner is a bullshit
    // and should be done in better way, but it's OK for sometime
    //
    if ([objectId containsString:@".group()"]) {
        objectId = [objectId stringByReplacingOccurrencesOfString:@".group()" withString:@""];
        NSString * chartRequest = [self serializedStringWithTRAggrFunction:BFModelTimeRangeAggregationRange andTRAggrArgs:timeRange andNeedObjectGrouping:YES];
        return @[[chartRequest stringByReplacingOccurrencesOfString:[BFViewModelMetricRequestSingle objectIdPlaceholder] withString:objectId]];
    } else {
        NSString * chartRequest = [self serializedStringWithTRAggrFunction:BFModelTimeRangeAggregationRange andTRAggrArgs:timeRange andNeedObjectGrouping:NO];
        return @[[chartRequest stringByReplacingOccurrencesOfString:[BFViewModelMetricRequestSingle objectIdPlaceholder] withString:objectId]];
    }
}

-(NSArray*)requestsForApiWithObjectId:(NSString*)objectId
{
    //
    // TODO: working with string in such a manner is a bullshit
    // and should be done in better way, but it's OK for sometime
    //
    if ([objectId containsString:@".group()"]) {
        objectId = [objectId stringByReplacingOccurrencesOfString:@".group()" withString:@""];
        NSString * request = [self serializedStringWithTRAggrFunction:self.timeRangeAggregation andTRAggrArgs:self.timeRangeAggregationArgs andNeedObjectGrouping:YES];
        return @[[request stringByReplacingOccurrencesOfString:[BFViewModelMetricRequestSingle objectIdPlaceholder] withString:objectId]];
    } else {
        NSString * request = [self serializedStringWithTRAggrFunction:self.timeRangeAggregation andTRAggrArgs:self.timeRangeAggregationArgs andNeedObjectGrouping:NO];
        return @[[request stringByReplacingOccurrencesOfString:[BFViewModelMetricRequestSingle objectIdPlaceholder] withString:objectId]];
    }
}

-(NSString*)uiNameWithObjectsMeta:(BFModelObjectMeta*)meta
{
    for (BFModelObjectMetricMeta * metricMeta in meta.metrics) {
        if ([metricMeta.metricId isEqualToString:self.metricId]) {
            return [metricMeta nameForUI];
        }
    }
    return self.metricId;
}

-(BFModelTimePeriod*)defaultTimeTangeArg
{
    switch (self.timeRangeAggregation) {
        case BFModelTimeRangeAggregationAt:
            return [BFModelTimePeriod defaultRange];
        
        case BFModelTimeRangeAggregationSum:
        case BFModelTimeRangeAggregationMin:
        case BFModelTimeRangeAggregationMax:
        case BFModelTimeRangeAggregationAvg:
        case BFModelTimeRangeAggregationRange:
            return (BFModelTimePeriod*)self.timeRangeAggregationArgs;
    }
}

-(instancetype)byApplyTimePeriod:(BFModelTimePeriod*)period
{
    if (self.timeRangeAggregation == BFModelTimeRangeAggregationAt) {
        // change only last day
        if ([period isKindOfClass:[BFModelTimePeriodDatesRange class]]) {
            BFModelTimePeriodDatesRange * periodTyped = (BFModelTimePeriodDatesRange*)period;
            return [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:self.aggregationFucntion additionalFunction:self.additionalFunction additionalFunctionArgs:self.additionalFunctionArgs timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:periodTyped.dateEnd];
        } else {
            // BFModelTimePeriodTodayRange
            return [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:self.aggregationFucntion additionalFunction:self.additionalFunction additionalFunctionArgs:self.additionalFunctionArgs timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:nil];
        }
    } else {
        return [BFViewModelMetricRequestSingle withMetricId:self.metricId objGroupAggregation:self.objectGroupAggregation aggregationFucntion:self.aggregationFucntion additionalFunction:self.additionalFunction additionalFunctionArgs:self.additionalFunctionArgs timeRangeAggregation:self.timeRangeAggregation timeRangeAggregationArgs:period];
    }
}


-(NSString*)stringForValue:(NSNumber*)value withObjectsMeta:(BFModelObjectMeta*)meta
{
    for (BFModelObjectMetricMeta * metricMeta in meta.metrics) {
        if ([metricMeta.metricId isEqualToString:self.metricId]) {
            return [NSString bf_humanReadableStringFromNumber:value unit:metricMeta.unit];
        }
    }
    return [NSString bf_humanReadableStringFromNumber:value unit:nil];
}

-(NSString*)stringDescriptionForMetricWithObjGroupingAggr:(BOOL)objGroupingAggr
{
    // This string consists of two parts:
    // - first describes the group_by + aggregation
    // - second describes the applied additional function
    NSString * results = @"";
    
    if (objGroupingAggr) {
        switch (self.objectGroupAggregation) {
            case BFModelObjectGroupAggregationSum:
                results = [results stringByAppendingString:@"BfMetric.ObjectGroupAggr.Sum".bf_localized];
                break;
            case BFModelObjectGroupAggregationMax:
                results = [results stringByAppendingString:@"BfMetric.ObjectGroupAggr.Max".bf_localized];
                break;
            case BFModelObjectGroupAggregationMin:
                results = [results stringByAppendingString:@"BfMetric.ObjectGroupAggr.Min".bf_localized];
                break;
            case BFModelObjectGroupAggregationAvg:
                results = [results stringByAppendingString:@"BfMetric.ObjectGroupAggr.Avg".bf_localized];
                break;
        }
        results = [results stringByAppendingString:@", "];
    }
    switch (self.aggregationFucntion) {
        case BFModelAggreagationFunctionLast:
            results = [results stringByAppendingString:@"BfMetric.GroupBy.Last".bf_localized];
            break;
        case BFModelAggreagationFunctionSum:
            results = [results stringByAppendingString:@"BfMetric.GroupBy.Sum".bf_localized];
            break;
        case BFModelAggreagationFunctionMax:
            results = [results stringByAppendingString:@"BfMetric.GroupBy.Max".bf_localized];
            break;
        case BFModelAggreagationFunctionMin:
            results = [results stringByAppendingString:@"BfMetric.GroupBy.Min".bf_localized];
            break;
        case BFModelAggreagationFunctionAvg:
            results = [results stringByAppendingString:@"BfMetric.GroupBy.Min".bf_localized];
            break;
    }

    switch (self.additionalFunction) {
        case BFModelAdditionalFunctionNone:
            break;
        case BFModelAdditionalFunctionLag: {
            results = [results stringByAppendingString:@","];
            results = [results stringByAppendingString:@"BfMetric.Func.Lag".bf_localized];
            break;
        }
        case BFModelAdditionalFunctionExpMovAvg: {
            results = [results stringByAppendingString:@","];
            results = [results stringByAppendingString:@"BfMetric.Func.Smooth".bf_localized];
            break;
        }
    }
    return results;
}

-(NSString*)stringDescriptionForTimeAggregation
{
    switch (self.timeRangeAggregation) {
        case BFModelTimeRangeAggregationAt: {
            return @"BfMetric.Aggr.Last".bf_localized;
        }
        case BFModelTimeRangeAggregationSum:
            return @"BfMetric.Aggr.Sum".bf_localized;
        case BFModelTimeRangeAggregationMax:
            return @"BfMetric.Aggr.Max".bf_localized;
        case BFModelTimeRangeAggregationMin:
            return @"BfMetric.Aggr.Min".bf_localized;
        case BFModelTimeRangeAggregationAvg:
            return @"BfMetric.Aggr.Avg".bf_localized;
        case BFModelTimeRangeAggregationRange: {
             return @"";
        }
    }
}

-(BOOL)doesThisRequestIncludes:(BFViewModelMetricRequest*)included
{
    if ([self isBasiclyEqualTo:included]) {
        // already selected - need deselect
        // but at the same time, we don't deselect the last selected
        // metric
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Arguments convinience getters


-(NSNumber*)argAdditionalFunctionLag
{
    if (self.additionalFunction == BFModelAdditionalFunctionLag) {
        return [(NSArray<NSNumber*>*)self.additionalFunctionArgs firstObject];
    }
    return nil;
}

-(NSArray<NSNumber*>*)argAdditionalFunctionExpMovAvg
{
    if (self.additionalFunction == BFModelAdditionalFunctionExpMovAvg) {
        return (NSArray<NSNumber*>*)self.additionalFunctionArgs;
    }
    return nil;
}

-(NSDate*)argAggregationTimeRangeAggregationAt
{
    if (self.timeRangeAggregation == BFModelTimeRangeAggregationAt) {
        return (NSDate*)self.timeRangeAggregationArgs;
    }
    return nil;
}

-(BFModelTimePeriod*)argAggregationTimeRangeAggregationSum
{
    if (self.timeRangeAggregation == BFModelTimeRangeAggregationSum) {
        return (BFModelTimePeriod*)self.timeRangeAggregationArgs;
    }
    return nil;
}

-(BFModelTimePeriod*)argAggregationTimeRangeAggregationRange
{
    if (self.timeRangeAggregation == BFModelTimeRangeAggregationRange) {
        return (BFModelTimePeriod*)self.timeRangeAggregationArgs;
    }
    return nil;
}

@end
