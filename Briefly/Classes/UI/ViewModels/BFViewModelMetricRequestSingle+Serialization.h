//
//  BFViewModelMetricRequestSingle+Serialization.h
//  Briefly
//
//  Created by Artem Bondar on 07.03.2018.
//

#import "BFViewModelMetricRequestSingle.h"

@interface BFViewModelMetricRequestSingle (Serialization)

+(BFModelAggreagationFunction)enumForAggreagtionFucntion:(NSString*)str;
+(BFModelAdditionalFunction)enumForAdditionalFucntion:(NSString*)str;
+(BFModelTimeRangeAggregation)enumForTimeRangeAggregation:(NSString*)str;
+(BFModelObjectGroupAggregation)enumForObjectGroupAggregation:(NSString*)str;

+(NSString*)regexpForParse;
+(NSString*)objectIdPlaceholder;
+(NSDateFormatter*)datesFormatter;

-(NSString*)serializedStringWithTRAggrFunction:(BFModelTimeRangeAggregation)trAggrFunction
                                 andTRAggrArgs:(NSObject*)trAggrFunctionArgs
                         andNeedObjectGrouping:(BOOL)needObjectGrouping;

+(BFViewModelMetricRequestSingle*)privFromSerializedString:(NSString*)serializedString;

@end
