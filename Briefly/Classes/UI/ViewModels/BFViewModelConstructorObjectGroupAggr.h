//
//  BFViewModelConstructorObjectGroupAggr.h
//  Briefly
//
//  Created by Artem Bondar on 14.03.2018.
//

#import "BFViewModelConstructorMainPage.h"

@interface BFViewModelConstructorObjectGroupAggr : BFViewModelConstructor

@property (strong, nonatomic) BFViewModelConstructorMainPage * parent;
@property (strong, nonatomic) NSArray * itemStrings;

+(instancetype)withMainPage:(BFViewModelConstructorMainPage*)mainPage;

@end
