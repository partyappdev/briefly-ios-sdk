//
//  BFViewModelMetricRequest.h
//  Base64
//
//  Created by Artem Bondar on 07.03.2018.
//

#import <Foundation/Foundation.h>
#import "BFModelTimePeriod.h"

@class BFModelObjectMeta;
@class BFModelTimePeriod;
@class BFModelChartData;

@interface BFViewModelMetricRequest : NSObject


+(BFViewModelMetricRequest*)fromSerializedString:(NSString*)serializedString;
-(NSString*)serializedString;

// basicly equal, mean expressions are equel, but the
// time ranges can be different
-(BOOL)isBasiclyEqualTo:(BFViewModelMetricRequest*)property;
-(NSString*)uiNameWithObjectsMeta:(BFModelObjectMeta*)meta;
-(instancetype)byApplyTimePeriod:(BFModelTimePeriod*)period;
-(NSString*)stringForValue:(NSNumber*)value withObjectsMeta:(BFModelObjectMeta*)meta;
-(NSString*)stringDescriptionForMetricWithObjGroupingAggr:(BOOL)objGroupingAggr;
-(NSString*)stringDescriptionForTimeAggregation;

-(NSArray<NSString*>*)requestsForChartWithObjectId:(NSString*)objectId andRange:(BFModelTimePeriod*)timeRange;
-(NSArray*)requestsForApiWithObjectId:(NSString*)objectId;

-(BFModelTimePeriod*)defaultTimeTangeArg;
-(BOOL)doesThisRequestIncludes:(BFViewModelMetricRequest*)included;
-(NSArray<BFViewModelMetricRequest*>*)flatterenMetricRequests;

@end
