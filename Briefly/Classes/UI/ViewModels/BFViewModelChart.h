//
//  BFViewModelChart.h
//  Briefly
//
//  Created by Artem Bondar on 01/02/2018.
//

#import <Foundation/Foundation.h>
#import "BFRange.h"

// Outside definitions
@class BFModelObjectMeta;
@class BFModelTimePeriod;
@class BFModelChartData;
@class BFViewModelMetricRequest;

// Local definitions
@protocol BFViewModelChartDelegate;
@class BFViewModelChartSection;
@class BFViewModelChartDataPoint;


///
///
/// Chart ViewModel implementation
///
///
@interface BFViewModelChart : NSObject

// Construction
+(instancetype)viewModelChartWithMetric:(BFViewModelMetricRequest*)metric
                                timeRange:(BFModelTimePeriod*)timeRange
                                 objectId:(NSString*)objectId
                              maximalDate:(NSDate*)maxDate
                               atCalendar:(NSCalendar*)calendar;

// Settings
@property (nonatomic) BOOL cummulative;

// Add information
@property (strong, nonatomic) BFModelObjectMeta * objectMeta;

// Subscribe for changes
-(void)subscribeForChanges:(id<BFViewModelChartDelegate>)subscriber;

// Check, if show the data for metric
-(BOOL)showChartForProperty:(BFViewModelMetricRequest*)property objectId:(NSString*)objectId;

// Data fetching
-(void)fetchFirstRegionIfNeeded;
-(void)loadDataRangeIfNeeded:(BFRange)range;

// Graph generic state checkings
-(NSInteger)lengthX;
-(NSInteger)dataWindowWidth;
-(BFRange)loadedRange;
-(BFRange)startRange;
-(BOOL)isUnloadedState;
-(BOOL)lastRequestFailed;
-(BOOL)isLoading;

// Conversions between dates/coordinates
-(BFModelTimePeriod*)timePeriodForRange:(BFRange)range;
-(NSInteger)idxForDate:(NSDate*)date;
-(NSDate*)dateForIdx:(NSInteger)idx;

// Acessing chart data
-(NSNumber*)maxValueAtRange:(BFRange)range;
-(NSArray<NSString*>*)linesLabelsAtRange:(BFRange)range
                           forLinesCount:(NSInteger)count;
-(NSArray<BFViewModelChartSection*>*)sectionsAtRange:(BFRange)range;
-(NSArray<BFViewModelChartDataPoint*>*)valuesAtRange:(BFRange)range;

@end




@interface BFViewModelChartSection : NSObject

+(instancetype)sectionWithLabel:(NSString*)label
                          dates:(NSArray<NSString*>*)dates
                         startX:(NSInteger)sectionStartX;

@property (strong, nonatomic) NSString * sectionLabel;
@property (strong, nonatomic) NSArray<NSString*> *sectionDatesLabel;
@property (nonatomic) NSInteger sectionStartX;

@end




@protocol BFViewModelChartDelegate

-(void)chartDataChanged;

@end



@interface BFViewModelChartDataPoint : NSObject

@property (strong, nonatomic) NSArray<NSNumber*>* multivalue;
@property (nonatomic) BOOL dataLoaded;
@property (nonatomic) BOOL past;

@end
