//
//  BFViewModelConstructorMainPage.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFViewModelConstructor.h"

@class BFViewModelConstructorMainPage;

@protocol BFViewModelConstructorMainPageDelegate

-(void)requestChangedAt:(BFViewModelConstructorMainPage*)model;
-(void)requestDeletedAt:(BFViewModelConstructorMainPage*)model;

@end

@interface BFViewModelConstructorMainPage : BFViewModelConstructor

@property (nonatomic) BOOL justCreated;
@property (nonatomic) BOOL canDelete;

+(BFViewModelConstructorMainPage*)withRequest:(BFViewModelMetricRequestSingle*)request
                                    timeRange:(BFModelTimePeriod*)timeRange
                                      meta:(BFModelObjectMeta*)meta
                                       idx:(NSInteger)idx
                               justCreated:(BOOL)justCreated
                                    canDelete:(BOOL)canDelete;

@property (weak, nonatomic) id <BFViewModelConstructorMainPageDelegate> delegate;

@end
