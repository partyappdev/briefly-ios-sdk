//
//  BFViewModelConstructorTimeRangeAggr.m
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import "BFViewModelConstructorTimeRangeAggr.h"
#import "BFViewModelMetricRequestSingle+Constructor.h"

#import "NSString+Utils.h"
#import "BFModelTimePeriod.h"

@implementation BFViewModelConstructorTimeRangeAggr


+(instancetype)withMainPage:(BFViewModelConstructorMainPage*)mainPage
{
    BFViewModelConstructorTimeRangeAggr * newObj = [[BFViewModelConstructorTimeRangeAggr alloc] initWithRequest:mainPage.request timeRange:mainPage.timeRange meta:mainPage.meta idx:mainPage.idx];
    newObj.parent = mainPage;
    newObj.itemStrings = [mainPage.request acessableTimeRangeAggregations];
    return newObj;
}

-(NSInteger)itemsCount
{
    return self.itemStrings.count + 1;
}

-(CGFloat)heightForItemAtIndex:(NSInteger)idx
{
    if (idx == self.itemStrings.count) {
        return 64;
    }
    return 44;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellAtIndex:(NSInteger)idx
{
    NSInteger selectedIdx = [self.request indexForCurrentTimeRangeAggregation];
    if (idx < self.itemStrings.count) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"checkmark" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
        [cell.textLabel setText:self.itemStrings[idx]];
        cell.accessoryType = selectedIdx == idx ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        return cell;
    }
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"description" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
    [cell.textLabel setText:@"BfMetricBuilder.Function.Description".bf_localized];
    [cell.detailTextLabel setText:[self.request descriptionMetricShortWithMeta:self.meta]];
    return cell;
}

-(void)selectedItemAtIdx:(NSInteger)idx
{
    [self updateRequestObject:[self.request requestBySelectingTimeRangeAggrIdx:idx andCurrentTimeRange:self.timeRange]];
    [self.parent updateRequestObject:self.request];
}

-(CGFloat)contentHeight
{
    return ([self itemsCount] - 1) * [self heightForItemAtIndex:0] + [self heightForItemAtIndex:([self itemsCount] - 1)];
}

-(NSString*)ststicHeaderText
{
    return @"BfMetricBuilder.TimeAggreagation.HeaderTitle".bf_localized;
}

@end
