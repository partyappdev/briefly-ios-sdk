//
//  BFViewModelConstructorMetric.h
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import "BFViewModelConstructorMainPage.h"

@interface BFViewModelConstructorMetric : BFViewModelConstructor

@property (strong, nonatomic) BFViewModelConstructorMainPage * parent;
@property (strong, nonatomic) NSArray * itemStrings;
+(instancetype)withMainPage:(BFViewModelConstructorMainPage*)mainPage;

@end
