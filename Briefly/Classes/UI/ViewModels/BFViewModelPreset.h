//
//  BFViewModelPreset.h
//  Briefly
//
//  Created by Artem Bondar on 14.03.2018.
//

#import <Foundation/Foundation.h>

@class BFViewModelObjectIdModifier;
@class BFViewModelMetricRequest;
@class BFModelTimePeriod;

@interface BFViewModelPreset : NSObject

@property (nonatomic) BOOL defaultPreset;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) BFViewModelObjectIdModifier * objIdModifier;
@property (strong, nonatomic) BFViewModelMetricRequest * metricRequest;
@property (strong, nonatomic) BFModelTimePeriod * timeRange;

-(NSDictionary*)serializedDescription;
+(instancetype)fromSerializedDescription:(NSDictionary*)dict;
+(instancetype)defaultPresetWithRequest:(BFViewModelMetricRequest*)request
                              timeRange:(BFModelTimePeriod*)timeRange
                          objIdModifier:(BFViewModelObjectIdModifier*)objIdModifier;
-(instancetype)copyThisWithName:(NSString*)name;
-(void)remakeToDefault;

-(BOOL)emptyPreset;
-(NSArray<NSString*>*)apiMetricRequestsWithObjectId:(NSString*)objectId;
-(NSArray<NSString*>*)apiMetricChartRequestsWithObjectId:(NSString*)objectId andTimeRange:(BFModelTimePeriod*)timeRange;
-(NSString*)apiRequestAtIdx:(NSInteger)idx forObjectId:(NSString*)objectId;

@end
