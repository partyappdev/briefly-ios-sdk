//
//  BFViewModelConstructorFunctionArgs.m
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import "BFViewModelConstructorFunctionArgs.h"
#import "BFViewModelMetricRequestSingle+Constructor.h"

#import "NSString+Utils.h"
#import "BFModelTimePeriod.h"
#import "BFTableViewCellWithTextField.h"
#import "BFControllerMetricRequestConstructor.h"

@interface BFViewModelConstructorFunctionArgs () <BFTableViewCellWithTextFieldDelegate>
@end

@implementation BFViewModelConstructorFunctionArgs

+(instancetype)withFunctionPage:(BFViewModelConstructorFunction*)functionPage
{
    BFViewModelConstructorFunctionArgs * newObj = [[BFViewModelConstructorFunctionArgs alloc] initWithRequest:functionPage.request timeRange:functionPage.timeRange meta:functionPage.meta idx:functionPage.idx];
    newObj.parent = functionPage;
    newObj.arguments = functionPage.request.additionalFunctionArgs;
    newObj.argumentsDescriptions = functionPage.request.functionArgumentsDescriptions;
    return newObj;
}

-(NSInteger)itemsCount
{
    return self.argumentsDescriptions.count + 1;
}

-(CGFloat)heightForItemAtIndex:(NSInteger)idx
{
    if (idx == self.argumentsDescriptions.count) {
        return 64;
    }
    return 44;
}

-(NSNumber*)valueForArgAtIdx:(NSInteger)idx
{
    if (idx < self.arguments.count) {
        return ((NSNumber*)self.arguments[idx]);
    }
    return @(0);
}

-(void)textChangedTo:(NSString*)text atTextFieldWithId:(NSString*)textFieldId
{
    NSInteger changedIdx = [[[NSNumberFormatter alloc] init] numberFromString:textFieldId].integerValue;
    
    // Check if it a valid value
    NSNumber * value = [[[NSNumberFormatter alloc] init] numberFromString:text];
    if (value == nil) {
        return;
    }
    
    NSMutableArray * newArgs = [NSMutableArray array];
    [self.arguments enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (changedIdx == idx) {
            [newArgs addObject:value];
        } else {
            [newArgs addObject:obj];
        }
    }];
    self.arguments = newArgs;
    self.request = [self.request requestByUpdatingFunctionArgs:self.arguments];
    [self.parent updateRequestObject:self.request];
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellAtIndex:(NSInteger)idx
{
    if (idx < self.argumentsDescriptions.count) {
        BFTableViewCellWithTextField * cell = (BFTableViewCellWithTextField*)[tableView dequeueReusableCellWithIdentifier:@"text" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
        cell.labelText.text = self.argumentsDescriptions[idx];
        cell.textField.text = [self valueForArgAtIdx:idx].stringValue;
        cell.textFieldId = @(idx).stringValue;
        cell.delegate = self;
        return cell;
    }
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"description" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
    [cell.textLabel setText:@"BfMetricBuilder.Function.Description".bf_localized];
    [cell.detailTextLabel setText:[self.request descriptionMetricShortWithMeta:self.meta]];
    return cell;
}

-(void)selectedItemAtIdx:(NSInteger)idx
{
    [self.presenter deselectAllRows];
}

-(CGFloat)contentHeight
{
    return ([self itemsCount] - 1) * [self heightForItemAtIndex:0] + [self heightForItemAtIndex:([self itemsCount] - 1)];
}

-(NSString*)ststicHeaderText
{
    return @"BfMetricBuilder.FunctionArgs.Title".bf_localized;
}

-(void)willBeDismissed
{
}

-(BOOL)havePropertiesToChange
{
    return self.arguments.count > 0;
}

@end
