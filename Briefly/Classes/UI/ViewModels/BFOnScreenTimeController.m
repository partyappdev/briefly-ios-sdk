//
//  BFOnScreenTimeController.m
//  Briefly
//
//  Created by Artem Bondar on 21.03.2018.
//

#import "BFOnScreenTimeController.h"
#import "BFUIManager+Storage.h"
#import "BFBrieflyManager.h"

//
// Helper definition
//
@interface BFOnScreenTimeControllerItem : NSObject

@property (strong, nonatomic) NSString *trackingId;
@property (strong, nonatomic) NSDate * screenAppear;
@property (nonatomic) BOOL stillOnScreen;

@end

@implementation BFOnScreenTimeControllerItem
@end


//
// Class implementation
//
@interface BFOnScreenTimeController ()

@property (strong, nonatomic) NSMutableDictionary<NSString*, BFOnScreenTimeControllerItem*> * currentlyOnScreen;

@end

@implementation BFOnScreenTimeController

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.currentlyOnScreen = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void)startRollCall
{
    [self.currentlyOnScreen enumerateKeysAndObjectsUsingBlock:^(NSString *key, BFOnScreenTimeControllerItem *obj, BOOL * _Nonnull stop) {
        obj.stillOnScreen = NO;
    }];
}

-(void)rollCallObjectIdCalled:(NSString*)objectId
{
    if (objectId == nil) {
        return;
    }
    BFOnScreenTimeControllerItem * obj = self.currentlyOnScreen[objectId];
    if (obj) {
        obj.stillOnScreen = YES;
    } else {
        // New object
        BFOnScreenTimeControllerItem * newObj = [BFOnScreenTimeControllerItem new];
        newObj.screenAppear = [NSDate date];
        newObj.stillOnScreen = YES;
        newObj.trackingId = objectId;
        self.currentlyOnScreen[objectId] = newObj;
        [[BFBrieflyManager sharedManager].uiManager addEventViewShown:objectId];
    }
}

-(void)finishRollCall
{
    // find all dissapeared, and send data to server
    NSMutableArray * dissapearedKeys = [NSMutableArray array];
    [self.currentlyOnScreen enumerateKeysAndObjectsUsingBlock:^(NSString *key, BFOnScreenTimeControllerItem *obj, BOOL * _Nonnull stop) {
        if (obj.stillOnScreen == NO) {
            NSInteger seconds = obj.screenAppear ? [[NSDate date] timeIntervalSinceDate:obj.screenAppear] : 0;
            if (seconds > 0) {
                [[BFBrieflyManager sharedManager].uiManager addEventViewLeaved:obj.trackingId withTimeInSeconds:seconds];
            }
            [dissapearedKeys addObject:key];
        }
    }];
    
    // and now remove all dissapeared views
    [self.currentlyOnScreen removeObjectsForKeys:dissapearedKeys];
}

-(void)appClosed
{
    [self.currentlyOnScreen enumerateKeysAndObjectsUsingBlock:^(NSString *key, BFOnScreenTimeControllerItem *obj, BOOL * _Nonnull stop) {
        NSInteger seconds = obj.screenAppear ? [[NSDate date] timeIntervalSinceDate:obj.screenAppear] : 0;
        if (seconds > 0) {
            [[BFBrieflyManager sharedManager].uiManager addEventViewLeaved:obj.trackingId withTimeInSeconds:seconds];
        }
        obj.screenAppear = nil;
    }];
}

-(void)appOpened
{
    [self.currentlyOnScreen enumerateKeysAndObjectsUsingBlock:^(NSString *key, BFOnScreenTimeControllerItem *obj, BOOL * _Nonnull stop) {
        obj.screenAppear = [NSDate date];
    }];
}

@end
