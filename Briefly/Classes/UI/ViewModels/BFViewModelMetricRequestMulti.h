//
//  BFViewModelMetricRequestMulti.h
//  Base64
//
//  Created by Artem Bondar on 07.03.2018.
//

#import "BFViewModelMetricRequest.h"

@interface BFViewModelMetricRequestMulti : BFViewModelMetricRequest

+(instancetype)withArray:(NSArray<BFViewModelMetricRequest*>*)array;

-(BFViewModelMetricRequest*)byInsertingRequest:(BFViewModelMetricRequest*)request;
-(BFViewModelMetricRequest*)byAppendingRequest:(BFViewModelMetricRequest*)request;
-(BFViewModelMetricRequest*)byRemovingRequestAtIdx:(NSInteger)idx;
-(BFViewModelMetricRequest*)byUpdatingRequestAtIdx:(NSInteger)idx to:(BFViewModelMetricRequest*)request;

-(BOOL)containsBasiclyEqual:(BFViewModelMetricRequest*)attribute;

@property (strong, nonatomic) NSArray<BFViewModelMetricRequest*>* childMetricRequests;

@end
