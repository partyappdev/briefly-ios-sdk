//
//  BFViewModelConstructorMainPage.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFViewModelConstructorMainPage.h"
#import "NSString+Utils.h"
#import "UIColor+BfPalette.h"
#import "BFModelTimePeriod.h"

#import "BFControllerMetricRequestConstructor.h"
#import "BFViewModelMetricRequestSingle+Constructor.h"

#import "BFViewModelConstructorMetric.h"
#import "BFViewModelConstructorGroupBy.h"
#import "BFViewModelConstructorFunction.h"
#import "BFViewModelConstructorTimeRangeAggr.h"
#import "BFViewModelConstructorObjectGroupAggr.h"

@implementation BFViewModelConstructorMainPage

+(BFViewModelConstructorMainPage*)withRequest:(BFViewModelMetricRequestSingle*)request
                                    timeRange:(BFModelTimePeriod*)timeRange
                                         meta:(BFModelObjectMeta*)meta
                                          idx:(NSInteger)idx
                                  justCreated:(BOOL)justCreated
                                    canDelete:(BOOL)canDelete
{
    BFViewModelConstructorMainPage * constructor = [[BFViewModelConstructorMainPage alloc] initWithRequest:request timeRange:timeRange meta:meta idx:idx];
    constructor.justCreated = justCreated;
    constructor.canDelete = canDelete;
    return constructor;
}

-(NSInteger)itemsCount
{
    return self.canDelete ? 6 : 5;
}

-(CGFloat)heightForItemAtIndex:(NSInteger)idx
{
    if (idx == 5)
        return 54;

    if (!self.canDelete && idx == 4)
        return 45;

    return 44;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellAtIndex:(NSInteger)idx
{
    switch (idx) {
        case 0: {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"detail" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
            [cell.textLabel setText:@"BfMetricBuilder.Metric.Title".bf_localized];
            [cell.detailTextLabel setText:[self.request descriptionMetricShortWithMeta:self.meta]];
            return cell;
        }
        case 1: {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"detail" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
            [cell.textLabel setText:@"BfMetricBuilder.ObjectGroupAggr.Title".bf_localized];
            [cell.detailTextLabel setText:[self.request descriptionObjectGroupAggrShort]];
            return cell;
        }
        case 2: {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"detail" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
            [cell.textLabel setText:@"BfMetricBuilder.GroupByAggregation.Title".bf_localized];
            [cell.detailTextLabel setText:[self.request descriptionGroupByShort]];
            return cell;
        }
        case 3: {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"detail" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
            [cell.textLabel setText:@"BfMetricBuilder.Function.Title".bf_localized];
            [cell.detailTextLabel setText:[self.request descriptionFunctionShort]];
            return cell;
        }
        case 4: {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"detail" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
            [cell.textLabel setText:@"BfMetricBuilder.TimeAggreagation.Title".bf_localized];
            [cell.detailTextLabel setText:[self.request descriptionTimeAggregationShort]];
            return cell;
        }
        case 5: {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"action" forIndexPath:[NSIndexPath indexPathForItem:idx inSection:0]];
            [cell.textLabel setTextColor:[UIColor bf_BaseUIColor]];
            if (self.justCreated) {
                [cell.textLabel setText:@"BfMetricBuilder.Delete.JustCreated".bf_localized];
            } else {
                [cell.textLabel setText:@"BfMetricBuilder.Delete.Basic".bf_localized];
            }
            return cell;
        }
    }
    return nil;
}

+(BFViewModelConstructor*)modelWithRequest:(BFViewModelMetricRequestSingle*)request
                                      meta:(BFModelObjectMeta*)meta
                                       idx:(NSInteger)idx;
{
    BFViewModelConstructor * newObj = [BFViewModelConstructor new];
    newObj.request = request;
    newObj.meta = meta;
    newObj.idx = idx;
    return newObj;
}

-(void)selectedItemAtIdx:(NSInteger)idx
{
    if (idx == 5) {
        [self.delegate requestDeletedAt:self];
        [self.presenter dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    BFViewModelConstructor * modelToShow = nil;
    if (idx == 0) {
        modelToShow = [BFViewModelConstructorMetric withMainPage:self];
    } else if (idx == 1) {
        modelToShow = [BFViewModelConstructorObjectGroupAggr withMainPage:self];
    } else if (idx == 2) {
        modelToShow = [BFViewModelConstructorGroupBy withMainPage:self];
    } else if (idx == 3) {
        modelToShow = [BFViewModelConstructorFunction withMainPage:self];
    } else if (idx == 4) {
        modelToShow = [BFViewModelConstructorTimeRangeAggr withMainPage:self];
    }
    if (modelToShow != nil) {
        BFControllerMetricRequestConstructor * ctrl = [BFControllerMetricRequestConstructor instantiateWithModel:modelToShow];
        [self.presenter presentViewController:ctrl animated:YES completion:^{
            [self.presenter modelChanged];
        }];
    }
}

-(void)updateRequestObject:(BFViewModelMetricRequestSingle *)request
{
    [super updateRequestObject:request];
    [self.delegate requestChangedAt:self];
}

-(CGFloat)contentHeight
{
    return ([self itemsCount] - 1) * [self heightForItemAtIndex:0] + [self heightForItemAtIndex:([self itemsCount] - 1)];
}

@end
