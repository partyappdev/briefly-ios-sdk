//
//  BFPreferencesController.m
//  Pods
//
//  Created by Artem Bondar on 14.12.2017.
//
//

#import "BFPreferencesController.h"
#import "BFBrieflyManager.h"
#import "BFBrieflyManager+Resources.h"
#import "BFUIManager_Private.h"
#import "UIViewController+Utils.h"
#import "UIViewController+Tracking.h"
#import "NSString+Utils.h"

@interface BFPreferencesController ()

@property (weak, nonatomic) IBOutlet UISwitch * showWidgets;
@property (weak, nonatomic) IBOutlet UISwitch * showUIActionsStatistics;
@property (weak, nonatomic) IBOutlet UISwitch * showHeatmaps;
@property (weak, nonatomic) IBOutlet UISegmentedControl * heatmapsDebugMode;

@property (weak, nonatomic) IBOutlet UILabel * labelShowWidgets;
@property (weak, nonatomic) IBOutlet UILabel * labelShowUIActionsStatistics;
@property (weak, nonatomic) IBOutlet UILabel * labelShowHeatmaps;
@property (weak, nonatomic) IBOutlet UIBarButtonItem * barButtonItemClose;

@end


@implementation BFPreferencesController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBfTrackingEnabled:NO];

    // Setup localization
    self.navigationItem.title = @"BfPreferences.Title".bf_localized;
    self.labelShowWidgets.text = @"BfPreferences.LabelShowWidgets".bf_localized;
    self.labelShowHeatmaps.text = @"BfPreferences.LabelShowHeatmaps".bf_localized;
    self.labelShowUIActionsStatistics.text = @"BfPreferences.LabelShowUIActionsStatistics".bf_localized;
    [self.heatmapsDebugMode setTitle:@"BfPreferences.TurnOff".bf_localized forSegmentAtIndex:0];
    [self.heatmapsDebugMode setTitle:@"BfPreferences.TurnOn".bf_localized forSegmentAtIndex:1];
    [self.barButtonItemClose setTitle:@"BfPreferences.Close".bf_localized];
}

+(UIViewController*)instantiateController
{
    UINavigationController * nav = [[BFBrieflyManager baseStoryboard] instantiateViewControllerWithIdentifier:@"preferences"];
    return nav;
}

-(IBAction)widgetsStateChanged:(id)sender
{
    [BFBrieflyManager sharedManager].uiManager.showWidgetsByUserPreference = self.showWidgets.on;
    [BFBrieflyManager sharedManager].uiManager.showHeatmapsByUserPreference = self.showHeatmaps.on;
    [BFBrieflyManager sharedManager].uiManager.showUIActionsStatsByUserPreference = self.showUIActionsStatistics.on;

    [[BFBrieflyManager sharedManager].uiManager storeWidgetPreferences];
}

-(IBAction)touchesDebuggingChanged:(id)sender
{
    if (self.heatmapsDebugMode.selectedSegmentIndex == 1) {
        [[BFBrieflyManager sharedManager].uiManager setHeatmapMode:BFHeatmapsModeDebugLocal];
    } else {
        // TODO: return mode normal
        [[BFBrieflyManager sharedManager].uiManager setHeatmapMode:BFHeatmapsModeDebug];
    }
}

-(IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.showWidgets.on = [[BFBrieflyManager sharedManager].uiManager showWidgetsByUserPreference];
    self.showHeatmaps.on = [[BFBrieflyManager sharedManager].uiManager showHeatmapsByUserPreference];
    self.showUIActionsStatistics.on = [[BFBrieflyManager sharedManager].uiManager showUIActionsStatsByUserPreference];

    if ([[BFBrieflyManager sharedManager].uiManager heatmapMode] == BFHeatmapsModeDebugLocal) {
        self.heatmapsDebugMode.selectedSegmentIndex = 1;
    } else {
        self.heatmapsDebugMode.selectedSegmentIndex = 0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)uid
{
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"BfPreferences.SectionBase".bf_localized;
    }
    return @"BfPreferences.SectionHeatmaps".bf_localized;
}

@end
