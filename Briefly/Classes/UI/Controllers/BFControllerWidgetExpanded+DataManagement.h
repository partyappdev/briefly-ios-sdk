//
//  BFControllerWidgetExpanded+DataManagement.h
//  Briefly
//
//  Created by Artem Bondar on 12.01.2018.
//

#import "BFControllerWidgetExpanded.h"
#import "BFViewModelMetricRequest.h"
#import "BFViewModelObjectIdModifier.h"
#import "BFCellPropertyInfo.h"

@class BFModelTimePeriod;
@class BFViewModelPreset;

@interface BFControllerWidgetExpanded (DataManagement) <BFCellPropertyInfoDelegate, BFViewModelChartDelegate>

-(void)objectMetaChanged;
-(void)loadPresets;
-(BFViewModelMetricRequest*)metric;
-(BFViewModelObjectIdModifier*)objectIdModifier;

-(void)selectPresetIdx:(NSInteger)idx;
-(void)storePresets:(NSArray<BFViewModelPreset*>*)presets;
-(void)setMetricRequestAfterDelete:(BFViewModelMetricRequest *)request;
-(void)setMetricRequestAfterInsertion:(BFViewModelMetricRequest *)request;
-(void)setMetricRequestAndFetch:(BFViewModelMetricRequest*)request;
-(void)fetchData;
-(BFModelTimePeriod*)currentTimePeriod;
-(void)selectTimePeriod:(BFModelTimePeriod*)timePeriod;

-(NSArray<BFViewModelObjectIdModifier*>*)accessibleObjectIdModifiers;
-(void)selectObjectIdModifierAtIdx:(NSInteger)idx;
-(NSInteger)selectedObjectIdModifier;

@end
