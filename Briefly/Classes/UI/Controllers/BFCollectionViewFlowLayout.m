//
//  BFCollectionViewFlowLayout.m
//  Briefly
//
//  Created by Artem Bondar on 30/01/2018.
//

#import "BFCollectionViewFlowLayout.h"

@implementation BFCollectionViewFlowLayout

-(void)prepareLayout
{
    // Centering is painful for self-sizing cells...
    //
    CGFloat totalWidth = 0;
    NSInteger itemsCount = [self.collectionView numberOfItemsInSection:0];
    for (NSInteger i = 0; i < itemsCount; ++i) {
        NSIndexPath * iP = [NSIndexPath indexPathForItem:i inSection:0];
        UICollectionViewCell * cell = [self.collectionView cellForItemAtIndexPath:iP];
        
        if (cell == nil) {
            // In that case, we don't event try to continue calculations
            // just 'It's too big to center it'
            totalWidth = 9999999;
            break;
        }
        UICollectionViewLayoutAttributes * attribs = [self layoutAttributesForItemAtIndexPath:iP];
        totalWidth += [cell preferredLayoutAttributesFittingAttributes:attribs].size.width;
    }
    totalWidth += (itemsCount - 1) * self.minimumInteritemSpacing;
    
    if (totalWidth + 32 < self.collectionView.frame.size.width) {
        CGFloat space = self.collectionView.frame.size.width - totalWidth;
        self.sectionInset = UIEdgeInsetsMake(0, space / 2, 0, space / 2);
    } else {
        self.sectionInset = UIEdgeInsetsMake(0, 16, 0, 16);
    }
    [super prepareLayout];
}
@end
