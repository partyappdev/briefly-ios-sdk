//
//  BFControllerPropertyConstructor.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFControllerPushable.h"

@class BFViewModelConstructor;

@interface BFControllerMetricRequestConstructor : BFControllerPushable

+(BFControllerMetricRequestConstructor*)instantiateWithModel:(BFViewModelConstructor*)model;

-(void)modelChanged;
-(void)deselectAllRows;

@end
