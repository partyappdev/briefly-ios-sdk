//
//  BFControllerSortingSettings.m
//  Briefly
//
//  Created by Artem Bondar on 07.04.2018.
//

#import "BFControllerSortingSettings.h"

#import "BFBrieflyManager+ApiWrapper.h"
#import "BFBrieflyManager+Resources.h"
#import "BFUIManager+Storage.h"
#import "NSString+Utils.h"
#import "UIViewController+Tracking.h"
#import "UIColor+BFPalette.h"
#import "BFSorter+Private.h"

typedef enum : NSUInteger {
    BFStateSortSettingsZero,
    BFStateSortSettingsCollapsed,
    BFStateSortSettingsExapnded
} BFStateSortSettings;

@interface BFControllerSortingSettings () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

//
// State
//
@property (strong, nonatomic) BFSorter * sorter;
@property (strong, nonatomic) NSArray * options;

//
// Visual state
//
@property (nonatomic) UIColor * startColor;
@property (nonatomic) CGRect startRect;
@property (weak, nonatomic) UIView * startView;

@property (weak, nonatomic) IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet UIView * container;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintHeaderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintHeaderTopTextMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintHeaderCalendarBottomTextMargin;

@property (weak, nonatomic) IBOutlet UIView * viewHeaderFader;
@property (weak, nonatomic) IBOutlet UIView * viewHeader;
@property (weak, nonatomic) IBOutlet UILabel * labelHeader;
@property (weak, nonatomic) IBOutlet UIButton * buttonClose;

@property (weak, nonatomic) UILabel * labelWidgetValue;
@property (weak, nonatomic) UIImageView * imageViewWidgetView;

@property (nonatomic) BFStateSortSettings state;

@end

@implementation BFControllerSortingSettings


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBfTrackingEnabled:NO];
    self.container.clipsToBounds = YES;
    self.buttonClose.layer.cornerRadius = 8;
    self.buttonClose.clipsToBounds = YES;
    [self.buttonClose setTitleColor:[UIColor bf_BaseUIColor] forState:UIControlStateNormal];
    self.viewHeader.layer.cornerRadius = 8;
    self.viewHeader.clipsToBounds = YES;
    self.viewHeader.backgroundColor = [UIColor bf_BaseUIColor];
    
    // i'm too lazy to mess with CALayer stuff here,
    // so did it in a shitty way
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.buttonClose.frame.size.width, 1)];
    lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    lineView.backgroundColor = [UIColor bf_widgetPreferencesLightGray];
    [self.buttonClose addSubview:lineView];
    
    self.labelHeader.text = @"BfSortSettings.Header".bf_localized;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.sorter.objectMetaForSorter == nil) {
        [self.sorter dataChangedAndNeedReload:YES];
    } else {
        self.options = [self.sorter humanReadableMetricNames];
    }
    // start loading data
    //[self fetchData];
    [self.tableView  reloadData];
    [self setState:BFStateSortSettingsZero animated:NO handler:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view setNeedsUpdateConstraints];
    __weak __typeof__(self) weakSelf = self;
    [self setState:BFStateSortSettingsCollapsed animated:YES duration:0.2 handler:^{
        weakSelf.state = BFStateSortSettingsExapnded;
    }];
}

+(UIColor*)backgroundColor
{
    return [UIColor colorWithRed:223.0/255.0 green:48.0/255.0 blue:17.0/255.0 alpha:1.0];
}

+(UIColor*)textColor
{
    return [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
}

+(instancetype)instantiateToShowFromView:(UIView*)view
                               forSorter:(BFSorter*)sorter;
{
    BFControllerSortingSettings * ctrl = (BFControllerSortingSettings*)[[BFBrieflyManager baseStoryboard] instantiateViewControllerWithIdentifier:@"sortingSettings"];
    ctrl.sorter = sorter;
    ctrl.startView = view;
    ctrl.startRect = [view convertRect:view.bounds toView:nil];

    return ctrl;
}

-(void)setupConstraintsAccordingState {
    
    // window constraints
    switch(self.state) {
        case BFStateSortSettingsZero:
        case BFStateSortSettingsCollapsed: {
            self.constraintContainerHeight.constant = self.expandedSectionSize;
            self.constraintContainerWidth.constant = self.view.bounds.size.width;
            self.constraintContainerTop.constant = -self.view.bounds.size.height;
            self.constraintContainerLeft.constant = 0;
            self.constraintHeaderHeight.constant = [self headerHeight];
            self.constraintHeaderTopTextMargin.constant = 1;
            self.constraintHeaderCalendarBottomTextMargin.constant = 38;
            break;
        }
        case BFStateSortSettingsExapnded: {
            self.constraintContainerHeight.constant = self.expandedSectionSize;
            self.constraintContainerWidth.constant = self.view.bounds.size.width;
            self.constraintContainerTop.constant = (self.view.bounds.size.height - self.constraintContainerHeight.constant) / 2;
            self.constraintContainerLeft.constant = 0;
            self.constraintHeaderHeight.constant = [self headerHeight];
            self.constraintHeaderTopTextMargin.constant = 1;
            self.constraintHeaderCalendarBottomTextMargin.constant = 38;
            break;
        }
    }
    // internal constrations
    [self setTableViewContentTopInset:0];
}

-(void)setTableViewContentTopInset:(CGFloat)topInset
{
    switch(self.state) {
        case BFStateSortSettingsZero:
        case BFStateSortSettingsCollapsed: {
            self.tableView.contentInset = UIEdgeInsetsMake(0, topInset, 0, 0);
            break;
        }
        case BFStateSortSettingsExapnded: {
            self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 54 + 8, 0);
            break;
        }
    }
}

-(void)setupUIAccordingState {
    switch(self.state) {
        case BFStateSortSettingsZero:
        case BFStateSortSettingsCollapsed:
        case BFStateSortSettingsExapnded: {
            [self.buttonClose setTitle:@"BfWidgetProperties.Close".bf_localized forState:UIControlStateNormal];
            self.buttonClose.alpha = 1.0;
            self.labelWidgetValue.alpha = 0.0;
            self.viewHeaderFader.alpha = 1.0;
            self.viewHeader.alpha = 1.0;
            self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
            self.tableView.alpha = 1.0;
            self.container.alpha = 1.0;
            self.container.backgroundColor = [UIColor whiteColor];
            self.container.layer.cornerRadius = 16;
            self.labelHeader.alpha = 1.0;
            break;
        }
    }
}

-(void)setState:(BFStateSortSettings)state
{
    [self setState:state animated:YES handler:nil];
}

-(void)setState:(BFStateSortSettings)state
       animated:(BOOL)animated
        handler:(void(^)(void))handler {
    
    [self setState:state animated:animated duration:0.4 handler:handler];
}

-(void)setState:(BFStateSortSettings)state
       animated:(BOOL)animated
       duration:(NSTimeInterval)duration
        handler:(void(^)(void))handler
{
    _state = state;
    
    if (animated) {
        self.view.userInteractionEnabled = NO;
        [self setupConstraintsAccordingState];
        [self.view setNeedsUpdateConstraints];
        [self.tableView reloadData];
        [UIView animateWithDuration:duration animations:^{
            [self.view layoutIfNeeded];
            [self setupUIAccordingState];
        } completion:^(BOOL finished) {
        
            // HACK for title
            [self.labelHeader layoutSubviews];
        
            self.view.userInteractionEnabled = YES;
            if (finished && handler) {
                handler();
            }
        }];
    } else {
        [self setupConstraintsAccordingState];
        [self.view setNeedsUpdateConstraints];
        [self.tableView reloadData];
        [self.view layoutIfNeeded];
        if (handler) {
            handler();
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    switch(self.state) {
        case BFStateSortSettingsZero:
        case BFStateSortSettingsCollapsed: {
            return 1;
        }
        case BFStateSortSettingsExapnded: {
            return 1;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(self.state) {
        case BFStateSortSettingsZero:
        case BFStateSortSettingsCollapsed: {
            return 0;
        }
        case BFStateSortSettingsExapnded: {
            return self.options.count > 0 ? self.options.count : 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.options.count > 0) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.textLabel.text = self.options[indexPath.item];
        cell.accessoryType = (indexPath.item == [self.sorter currentMetricIdx]) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        return cell;
    } else {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"error" forIndexPath:indexPath];
        cell.textLabel.text = @"BfSortSettings.Error".bf_localized;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.options.count > 0) {
        self.sorter.currentMetricIdx = indexPath.item;
        [self.tableView reloadData];
    }
    //[self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

-(CGFloat)headerHeight
{
    return 38;
}

-(CGFloat)cellHeight
{
    return 44;
}

-(CGFloat)expandedSectionSize
{
    CGFloat oneButtonControlsHeight = [self headerHeight] + 54 + 16;
    return MAX([self cellHeight], [self cellHeight] * self.options.count) + oneButtonControlsHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 44;
}

-(IBAction)pressClose:(id)sender
{
    switch(self.state) {
        case BFStateSortSettingsZero:
        case BFStateSortSettingsCollapsed: {
            // do nothing
            return;
        }
        case BFStateSortSettingsExapnded: {
            __weak __typeof__(self) weakSelf = self;
            [self setState:BFStateSortSettingsCollapsed animated:YES handler:^{
                [weakSelf setState:BFStateSortSettingsZero animated:YES duration:0.2 handler:^{
                    [weakSelf dismissViewControllerAnimated:NO completion:nil];
                }];
            }];
            return;
        }
    }
}

@end
