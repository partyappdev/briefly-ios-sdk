//
//  BFControllerWidgetExpanded+Calendar.m
//  Briefly
//
//  Created by Artem Bondar on 15.01.2018.
//

#import "BFControllerWidgetExpanded+Calendar.h"
#import "BFControllerWidgetExpanded+DataManagement.h"
#import "BFControllerWidgetExpanded+Private.h"

#import "BFUIManager+Storage.h"
#import "BFBrieflyManager.h"
#import "BFViewModelMetricRequest.h"

#import "BFFSCalendar.h"

@interface BFControllerWidgetExpanded (CalendarDelegate) <BFFSCalendarDelegate, BFFSCalendarDataSource>

@end

@implementation BFControllerWidgetExpanded (CalendarDelegate)

-(void)selectDatePressed:(NSDate *)date atCalendar:(BFFSCalendar*)calendar
{
    // id depends on state:
    
    if (calendar.selectedDates.count == 0) {
        // select a single date
        [calendar selectDate:date];
    } else if (calendar.selectedDates.count == 1) {
        // select a second date
        NSArray * dates = [calendar datesRangeBetweenFirstDate:calendar.selectedDates.firstObject andSecondDate:date withDatesLimit:30];
        for (NSDate * dt in [NSArray arrayWithArray:calendar.selectedDates]) {
            [calendar deselectDate:dt];
        }
        for (NSDate * dt in dates) {
            [calendar selectDate:dt];
        }
        // Now select the same time period to defaults:
        if ([calendar isLastAcceptableDate:dates.lastObject]) {
            [self selectTimePeriod:[BFModelTimePeriodTodayRange periodWithRangeLengthDays:@(dates.count)]];
        } else {
            [self selectTimePeriod:[BFModelTimePeriodDatesRange periodWithDateStart:dates.firstObject dateEnd:dates.lastObject]];
        }
        [self setState:BFStateWidgetExpandedChart animated:YES handler:nil];
    } else {
        // start selection from scratch
        for (NSDate * dt in [NSArray arrayWithArray:calendar.selectedDates]) {
            [calendar deselectDate:dt];
        }
        [calendar selectDate:date];
    }
}

- (BOOL)calendar:(BFFSCalendar *)calendar shouldSelectDate:(NSDate *)date atMonthPosition:(BFFSCalendarMonthPosition)monthPosition
{
    if ([calendar.today compare:date] != NSOrderedAscending) {
        [self selectDatePressed:date atCalendar:calendar];
    }
    return NO;
}

- (void)calendar:(BFFSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(BFFSCalendarMonthPosition)monthPosition
{

}

- (BOOL)calendar:(BFFSCalendar *)calendar shouldDeselectDate:(NSDate *)date atMonthPosition:(BFFSCalendarMonthPosition)monthPosition
{
    if ([calendar.today compare:date] != NSOrderedAscending) {
        [self selectDatePressed:date atCalendar:calendar];
    }
    return NO;
}

- (void)calendar:(BFFSCalendar *)calendar didDeselectDate:(NSDate *)date atMonthPosition:(BFFSCalendarMonthPosition)monthPosition
{
    
}

- (NSDate *)maximumDateForCalendar:(BFFSCalendar *)calendar
{
    return [[NSDate date] dateByAddingTimeInterval: 0];
}

@end

@implementation BFControllerWidgetExpanded (Calendar)

-(NSArray<NSDate*>*)currentDatesRange
{
    // check it by reading user defaults
    
    return [[self currentTimePeriod] datesRange];
}

@end
