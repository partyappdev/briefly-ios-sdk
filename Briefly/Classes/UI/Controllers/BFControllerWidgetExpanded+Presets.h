//
//  BFControllerWidgetExpanded+Presets.h
//  Briefly
//
//  Created by Artem Bondar on 14.03.2018.
//

#import "BFControllerWidgetExpanded.h"
#import "BFCellWidgetPreset.h"

@interface BFControllerWidgetExpanded (Presets) <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, BFCellWidgetPresetDelegate>

-(void)initPresets;
-(NSString*)textForPresetsBarDescription;
-(void)reloadPresetsAfterMetricsChange;

@end
