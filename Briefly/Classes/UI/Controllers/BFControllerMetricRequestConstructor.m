//
//  BFControllerPropertyConstructor.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFControllerMetricRequestConstructor.h"
#import "BFBrieflyManager+Resources.h"

#import "BFViewModelObjectIdModifier.h"
#import "BFViewMetricRequest.h"
#import "BFViewModelConstructor.h"
#import "BFViewModelMetricRequestSingle.h"

#import "UIColor+BfPalette.h"
#import "UIViewController+Tracking.h"

@interface BFControllerMetricRequestConstructor () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) BFViewModelConstructor * model;

@property (weak, nonatomic) IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet UIView * containerView;
@property (weak, nonatomic) IBOutlet UIView * headerView;
@property (weak, nonatomic) IBOutlet UIButton * backButton;
@property (weak, nonatomic) BFViewMetricRequest * viewMetricRequest;

@property (weak, nonatomic) IBOutlet UIView * basicHeaderView;
@property (weak, nonatomic) IBOutlet UILabel * basicHeaderViewTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintHeight;

@end

@implementation BFControllerMetricRequestConstructor

+(BFControllerMetricRequestConstructor*)instantiateWithModel:(BFViewModelConstructor*)model
{
    BFControllerMetricRequestConstructor * constructor = [[BFBrieflyManager baseStoryboard] instantiateViewControllerWithIdentifier:@"metricConstructor"];
    constructor.modalPresentationStyle = UIModalPresentationOverFullScreen;
    constructor.model = model;
    model.presenter = constructor;
    return constructor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBfTrackingEnabled:NO];

    // Do any additional setup after loading the view.
    self.basicHeaderView.layer.cornerRadius = 8;
    self.basicHeaderView.backgroundColor = [UIColor bf_BaseUIColor];
    self.containerView.clipsToBounds = YES;
    self.containerView.layer.cornerRadius = 16;
    self.backButton.layer.cornerRadius = 8;
    self.backButton.clipsToBounds = YES;
    [self.backButton setTitleColor:[UIColor bf_BaseUIColor] forState:UIControlStateNormal];
    self.headerView.layer.cornerRadius = 8;
    self.headerView.clipsToBounds = YES;
    
    // i'm too lazy to mess with CALayer stuff here,
    // so did it in a shitty way
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.backButton.frame.size.width, 1)];
    lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    lineView.backgroundColor = [UIColor bf_widgetPreferencesLightGray];
    [self.backButton addSubview:lineView];
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView setContentInset: UIEdgeInsetsMake(52, 0, 54, 0)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self modelChanged];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.model willBeDismissed];
}

-(void)modelChanged
{
    [self.viewMetricRequest removeFromSuperview];
    if ([self.model ststicHeaderText]) {
        self.basicHeaderView.hidden = NO;
        self.basicHeaderViewTitle.text = [self.model ststicHeaderText];
    } else {
        self.basicHeaderView.hidden = YES;
        BFViewMetricRequest * view = [BFViewMetricRequest instantiate];
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [self.headerView addSubview:view];
        [self.headerView addConstraints:@[
                                           [view.topAnchor constraintEqualToAnchor:self.headerView.topAnchor],
                                           [view.leadingAnchor constraintEqualToAnchor:self.headerView.leadingAnchor],
                                           [view.trailingAnchor constraintEqualToAnchor:self.headerView.trailingAnchor],
                                           [view.bottomAnchor constraintEqualToAnchor:self.headerView.bottomAnchor]
                                           ]];
        
        BFViewModelObjectIdModifierGroup * modifier = [BFViewModelObjectIdModifierGroup instance];
        [view configureWithMetricRequest:self.model.request objectIdModifier:modifier meta:self.model.meta value:@(0) idx:self.model.idx];
        self.viewMetricRequest = view;
    }
    
    [self.backButton setTitle:self.model.backButtonText forState:UIControlStateNormal];
    self.constraintHeight.constant = [self.model contentHeight] + 52 + 54;
    [self.tableView reloadData];
}

-(void)deselectAllRows
{
    if (self.tableView.indexPathForSelectedRow != nil) {
        [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.model itemsCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.model tableView:tableView cellAtIndex:indexPath.item];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.model selectedItemAtIdx:indexPath.item];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.model heightForItemAtIndex:indexPath.item];
}

-(IBAction)pressClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
