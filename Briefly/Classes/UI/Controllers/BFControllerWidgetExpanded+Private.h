//
//  BFControllerWidgetExpanded+Private.h
//  Briefly
//
//  Created by Artem Bondar on 12.01.2018.
//

#import "BFControllerWidgetExpanded.h"
#import "BFModelChartData.h"
#import "BFPublicModels.h"
#import "BFCellPropertyInfo.h"

#import "BFViewModelObjectIdModifier.h"
#import "BFViewModelMetricRequest.h"
#import "BFViewModelChart.h"
#import "BFViewModelPreset.h"

typedef enum : NSUInteger {
    BFStateWidgetExpandedZero,
    BFStateWidgetExpandedCollapsed,
    BFStateWidgetExpandedChart,
    BFStateWidgetExpandedCalendar,
} BFStateWidgetExpanded;


@interface BFControllerWidgetExpanded ()

//
// Base configuration
//
@property (strong, nonatomic) NSString * objectId;
@property (strong, nonatomic) NSString * widgetGroupId;
@property (strong, nonatomic) NSString * defaultAttribute;
@property (nonatomic) BOOL enabled;

//
// Loaded data
//
@property (strong, nonatomic) BFModelObjectMeta * objectMeta;
@property (strong, nonatomic) BFViewModelChart * chart;
@property (nonatomic) BOOL isMetaLoading;
@property (nonatomic) BOOL isMetaLastReqFailed;

//
// Selection state
//
@property (strong, nonatomic) NSDate * selectedDate;

//
// Private+MetricSelection
//
@property (strong, nonatomic) NSArray* sortedMetasList;
@property (nonatomic) NSInteger sortedMetasListLastSelected;

//
// Private methods
//
-(void)deselectAll;
-(void)reloadMetricRequestsSelectors;
-(void)openPreferencesPane;
-(void)needUpdateUiOnDataChange;
-(void)reloadTableView;
-(void)setTableViewContentTopInset:(CGFloat)topInset;
-(void)makeTableViewManualDataChanges:(void(^)(UITableView*))dataChanges;
-(void)configureHeader;
-(void)setState:(BFStateWidgetExpanded)state
       animated:(BOOL)animated
        handler:(void(^)(void))handler;

//
// Presets  bar configuration
//
@property (weak, nonatomic) IBOutlet UICollectionView * collectionViewPresets;
@property (nonatomic) NSInteger currentlySelectedPreset;
@property (strong, nonatomic) NSArray<BFViewModelPreset*>* presets;

@end
