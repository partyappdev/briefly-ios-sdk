//
//  BFControllerWidgetExpanded+Calendar.h
//  Briefly
//
//  Created by Artem Bondar on 15.01.2018.
//

#import "BFControllerWidgetExpanded.h"

@interface BFControllerWidgetExpanded (Calendar)

-(NSArray<NSDate*>*)currentDatesRange;

@end
