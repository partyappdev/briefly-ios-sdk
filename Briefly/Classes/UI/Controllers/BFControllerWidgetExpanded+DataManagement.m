//
//  BFControllerWidgetExpanded+DataManagement.m
//  Briefly
//
//  Created by Artem Bondar on 12.01.2018.
//

#import "BFControllerWidgetExpanded+DataManagement.h"
#import "BFControllerWidgetExpanded+Private.h"
#import "BFControllerWidgetExpanded+Presets.h"

#import "BFBrieflyManager+ApiWrapper.h"
#import "BFBrieflyManager+Resources.h"
#import "BFUIManager+Storage.h"
#import "BFUIManager+Overlay.h"
#import "BFUIManager+WidgetsManager.h"

#import "BFFSCalendar.h"
#import "BFViewModelMetricRequestSingle.h"
#import "BFViewModelMetricRequestMulti.h"

@implementation BFControllerWidgetExpanded (DataManagement)

-(void)loadPresets
{
    self.presets = [[BFBrieflyManager sharedManager].uiManager presetsListForObjectId:self.objectId widgetGroupId:self.widgetGroupId andDefaultRequestString:self.defaultAttribute];
    self.currentlySelectedPreset = [[BFBrieflyManager sharedManager].uiManager currentlySelectedPresetIdxForObjectId:self.objectId widgetGroupId:self.widgetGroupId];
}

-(void)objectMetaChanged
{
    self.chart.objectMeta = self.objectMeta;
}

-(BFViewModelMetricRequest*)metric
{
    return self.presets[self.currentlySelectedPreset].metricRequest;
}

-(BFViewModelObjectIdModifier*)objectIdModifier
{
    return self.presets[self.currentlySelectedPreset].objIdModifier;
}

-(BFModelTimePeriod*)currentTimePeriod
{
    return self.presets[self.currentlySelectedPreset].timeRange;
}

-(void)selectPresetIdx:(NSInteger)idx
{
    self.currentlySelectedPreset = idx;
    [[BFBrieflyManager sharedManager].uiManager setPresets:self.presets selectedIdx:self.currentlySelectedPreset forObjectId:self.objectId widgetGroupId:self.widgetGroupId];
    
    [[BFBrieflyManager sharedManager].uiManager forceWidgetsReloadData];
    [self reloadMetricRequestsSelectors];
    [self fetchData];
    
    [self setState:BFStateWidgetExpandedChart animated:YES handler:^{
        [[BFBrieflyManager sharedManager].uiManager forceWidgetsReloadData];
    }];
}

-(void)storePresets:(NSArray<BFViewModelPreset*>*)presets
{
    self.presets = presets;
    [[BFBrieflyManager sharedManager].uiManager setPresets:self.presets selectedIdx:self.currentlySelectedPreset forObjectId:self.objectId widgetGroupId:self.widgetGroupId];
}

-(void)metricRequestsChanged
{
    [[BFBrieflyManager sharedManager].uiManager setPresets:self.presets selectedIdx:self.currentlySelectedPreset forObjectId:self.objectId widgetGroupId:self.widgetGroupId];
    [[BFBrieflyManager sharedManager].uiManager forceWidgetsReloadData];
}

-(void)setMetricRequestAfterDelete:(BFViewModelMetricRequest *)request
{
    self.presets[self.currentlySelectedPreset].metricRequest = request;
    [self metricRequestsChanged];
    [self reloadTableView];
    [self reloadPresetsAfterMetricsChange];
    [self fetchData];
    
    [self setState:BFStateWidgetExpandedChart animated:YES handler:^{
        [[BFBrieflyManager sharedManager].uiManager forceWidgetsReloadData];
    }];
}

-(void)setMetricRequestAfterInsertion:(BFViewModelMetricRequest *)request
{
    self.presets[self.currentlySelectedPreset].metricRequest = request;
    [self metricRequestsChanged];
}

-(void)setMetricRequestAndFetch:(BFViewModelMetricRequest *)request
{
    //
    // change the preset, and store it to the storage
    //
    self.presets[self.currentlySelectedPreset].metricRequest = request;
    [self metricRequestsChanged];
    [self reloadMetricRequestsSelectors];
    [self reloadPresetsAfterMetricsChange];
    [self fetchData];

    [self setState:BFStateWidgetExpandedChart animated:YES handler:^{
        [[BFBrieflyManager sharedManager].uiManager forceWidgetsReloadData];
    }];
}

-(void)fetchData
{
    __weak __typeof__(self) weakSelf = self;
    
    // update the chart viewmodel if required
    NSString * objId = [self.objectIdModifier objectIdByApplyingTo:self.objectId];
    if (!self.chart || ![self.chart showChartForProperty:self.metric objectId:objId]) {
        self.chart = [BFViewModelChart viewModelChartWithMetric:self.metric timeRange:self.currentTimePeriod objectId:objId maximalDate:[BFFSCalendar maximalDate] atCalendar:[BFFSCalendar utcCalendar]];
        [self.chart subscribeForChanges:self];
        self.chart.objectMeta = self.objectMeta;
    }

    // We fetch meta data, only if we don't have it at all
    if (!self.objectMeta) {
        self.isMetaLoading = YES;
        [[BFBrieflyManager sharedManager] getMetaForObjectWithId:self.objectId withHandler:^(BOOL succeed, BFModelObjectMeta * objectMeta) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (succeed) {
                    weakSelf.objectMeta = objectMeta;
                    weakSelf.isMetaLastReqFailed = NO;
                } else {
                    weakSelf.isMetaLastReqFailed = YES;
                }
                weakSelf.isMetaLoading = NO;
                [weakSelf needUpdateUiOnDataChange];
            });
        }];
    }
}

-(void)selectTimePeriod:(BFModelTimePeriod*)timePeriod
{
    self.presets[self.currentlySelectedPreset].timeRange = timePeriod;
    [self metricRequestsChanged];
    [self fetchData];
}

-(void)timePeriodChangedTo:(BFModelTimePeriod*)timePeriod
{
    // Legacy, when by scrolling chart we could change timeRange
    //
    // Don't change perioud here
    //[self selectTimePeriod:timePeriod];
    //[self configureHeader];
}

-(void)selectedDate:(NSDate *)date
{
    self.selectedDate = date;
    [self configureHeader];
    [self reloadMetricRequestsSelectors];
}

-(void)selectedDatesField
{
    //[self openPreferencesPane];
}


-(NSArray<BFViewModelObjectIdModifier*>*)accessibleObjectIdModifiers
{
    if (self.objectMeta.groupMeta != nil) {
        return @[[BFViewModelObjectIdModifierEqual instance], [BFViewModelObjectIdModifierGroup instance]];
    } else {
        return @[];
    }
}

-(void)selectObjectIdModifierAtIdx:(NSInteger)idx
{
    BFViewModelObjectIdModifier * modifier = [[self accessibleObjectIdModifiers] objectAtIndex:idx];
    self.presets[self.currentlySelectedPreset].objIdModifier = modifier;
    [[BFBrieflyManager sharedManager].uiManager setPresets:self.presets selectedIdx:self.currentlySelectedPreset forObjectId:self.objectId widgetGroupId:self.widgetGroupId];
    
    [[BFBrieflyManager sharedManager].uiManager forceWidgetsReloadData];
    [self reloadMetricRequestsSelectors];
    [self configureHeader];
    [self fetchData];
    [self setState:BFStateWidgetExpandedChart animated:YES handler:^{
        [[BFBrieflyManager sharedManager].uiManager forceWidgetsReloadData];
    }];
}

-(NSInteger)selectedObjectIdModifier
{
    if ([self.objectIdModifier isKindOfClass:[BFViewModelObjectIdModifierGroup class]]) {
        return 1;
    } else {
        return 0;
    }
}

-(void)chartDataChanged
{
    [self reloadMetricRequestsSelectors];
}

@end
