//
//  BFPreferencesController.h
//  Pods
//
//  Created by Artem Bondar on 14.12.2017.
//
//

#import <UIKit/UIKit.h>

@interface BFPreferencesController : UITableViewController

+(UIViewController*)instantiateController;

@end
