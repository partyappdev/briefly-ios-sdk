//
//  BFControllerWidgetExpanded.m
//  Briefly
//
//  Created by Artem Bondar on 10.01.2018.
//

#import "BFControllerWidgetExpanded.h"
#import "BFControllerWidgetExpanded+DataManagement.h"
#import "BFControllerWidgetExpanded+Private.h"
#import "BFControllerWidgetExpanded+Calendar.h"
#import "BFControllerWidgetExpanded+MetricsSelector.h"
#import "BFControllerWidgetExpanded+Presets.h"

#import "BFBrieflyManager+ApiWrapper.h"
#import "BFBrieflyManager+Resources.h"
#import "BFUIManager+Storage.h"
#import "NSString+Utils.h"
#import "UIViewController+Tracking.h"
#import "UIColor+BFPalette.h"
#import "BFStatisticsLabel.h"

#import "BFCellCalendar.h"
#import "BFCellPropertyInfo.h"
#import "BFCellObjectIdModifier.h"
#import "BFCellMetricRequest.h"

@interface BFControllerWidgetExpanded () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic) NSString * startWidgetText;
@property (nonatomic) UIColor * startColor;
@property (nonatomic) UIColor * startTextColor;
@property (nonatomic) CGRect startRect;
@property (weak, nonatomic) UIView * startView;

@property (weak, nonatomic) IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet UIView * container;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintContainerLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintHeaderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintHeaderTopTextMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintHeaderCalendarBottomTextMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * constraintPresetsTopSpacing;

@property (weak, nonatomic) IBOutlet UIView * viewPresetsContainer;
@property (weak, nonatomic) IBOutlet UILabel * labelPresetsDescription;

@property (weak, nonatomic) IBOutlet UIView * viewHeaderFader;
@property (weak, nonatomic) IBOutlet UIView * viewHeader;
@property (weak, nonatomic) IBOutlet UIView * viewHeaderPreferences;
@property (weak, nonatomic) IBOutlet UILabel * labelHeader;
@property (weak, nonatomic) IBOutlet UILabel * labelHeaderCalendar;
@property (weak, nonatomic) IBOutlet UILabel * labelSubHeader;
@property (weak, nonatomic) IBOutlet UIButton * buttonClose;

@property (weak, nonatomic) UILabel * labelWidgetValue;
@property (weak, nonatomic) UIImageView * imageViewWidgetView;

@property (nonatomic) BFStateWidgetExpanded state;

@end

@implementation BFControllerWidgetExpanded

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBfTrackingEnabled:NO];
    self.container.clipsToBounds = YES;
    self.buttonClose.layer.cornerRadius = 8;
    self.buttonClose.clipsToBounds = YES;
    [self.buttonClose setTitleColor:[UIColor bf_BaseUIColor] forState:UIControlStateNormal];
    self.viewHeader.layer.cornerRadius = 8;
    self.viewHeader.clipsToBounds = YES;
    self.viewHeader.backgroundColor = [UIColor bf_BaseUIColor];
    self.viewHeaderPreferences.backgroundColor = [UIColor bf_BaseUIColorDarkened];
    
    // i'm too lazy to mess with CALayer stuff here,
    // so did it in a shitty way
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.buttonClose.frame.size.width, 1)];
    lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    lineView.backgroundColor = [UIColor bf_widgetPreferencesLightGray];
    [self.buttonClose addSubview:lineView];
    
    self.labelHeaderCalendar.text = @"BfWidgetProperties.CalendarHeader".bf_localized;

    [self.tableView registerClass:[BFCellMetricRequest class] forCellReuseIdentifier:[self metricCellReuseIdentifierAtIdx:1]];
    [self.tableView registerClass:[BFCellMetricRequestAction class] forCellReuseIdentifier:[self metricCellReuseIdentifierAtIdx:0]];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"placeholder"];
    
    // Presets bar
    [(UICollectionViewFlowLayout*)self.collectionViewPresets.collectionViewLayout setEstimatedItemSize:CGSizeMake(120, 32)];
    [(UICollectionViewFlowLayout*)self.collectionViewPresets.collectionViewLayout setSectionInset:UIEdgeInsetsMake(0, 16, 0, 16)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // start loading data
    [self initPresets];
    [self fetchData];
    [self setState:BFStateWidgetExpandedZero animated:NO handler:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view setNeedsUpdateConstraints];
    __weak __typeof__(self) weakSelf = self;
    [self setState:BFStateWidgetExpandedCollapsed animated:YES duration:0.2 handler:^{
        weakSelf.state = BFStateWidgetExpandedChart;
    }];
}

+(UIColor*)backgroundColor
{
    return [UIColor colorWithRed:223.0/255.0 green:48.0/255.0 blue:17.0/255.0 alpha:1.0];
}

+(UIColor*)textColor
{
    return [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
}

+(instancetype)instantiateToShowFromView:(UIView*)view
                              startColor:(UIColor*)startColor
                          startTextColor:(UIColor*)startTextColor
                         startWidgetText:(NSString*)startWidgetText
                             forObjectId:(NSString*)objectId
                             widgetGroup:(NSString*)widgetGroup
                        defaultAttribute:(NSString*)defaultAttribute
                                 enabled:(BOOL)enabled
{
    BFControllerWidgetExpanded * ctrl = (BFControllerWidgetExpanded*)[[BFBrieflyManager baseStoryboard] instantiateViewControllerWithIdentifier:@"widgetExpanding"];
    ctrl.widgetGroupId = widgetGroup;
    ctrl.objectId = objectId;
    ctrl.startView = view;
    ctrl.startRect = [view convertRect:view.bounds toView:nil];
    ctrl.startColor = startColor;
    ctrl.startTextColor = startTextColor;
    ctrl.startWidgetText = startWidgetText;
    ctrl.defaultAttribute = defaultAttribute;
    ctrl.enabled = enabled;
    
    return ctrl;
}

-(void)setupConstraintsAccordingState {
    
    // window constraints
    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            CGRect startRectConverted = [self.view convertRect:self.startRect toView:self.view];
            self.constraintContainerHeight.constant = startRectConverted.size.height;
            self.constraintContainerWidth.constant = startRectConverted.size.width;
            self.constraintContainerTop.constant = startRectConverted.origin.y;
            self.constraintContainerLeft.constant = startRectConverted.origin.x;
            self.constraintHeaderHeight.constant = self.startRect.size.height;
            self.constraintHeaderTopTextMargin.constant = 38;
            self.constraintHeaderCalendarBottomTextMargin.constant = 38;
            self.constraintPresetsTopSpacing.constant = 120;
            break;
        }
        case BFStateWidgetExpandedChart: {
            self.constraintContainerHeight.constant = self.sizeOfPropertyInfoSection;
            self.constraintContainerWidth.constant = self.view.bounds.size.width;
            self.constraintContainerTop.constant = (self.view.bounds.size.height - self.constraintContainerHeight.constant) / 2;
            self.constraintContainerLeft.constant = 0;
            self.constraintHeaderHeight.constant = [self headerHeight];
            self.constraintHeaderTopTextMargin.constant = 1;
            self.constraintHeaderCalendarBottomTextMargin.constant = 38;
            self.constraintPresetsTopSpacing.constant = 8;
            break;
        }
        case BFStateWidgetExpandedCalendar: {
            // TODO: use properties count
            self.constraintContainerHeight.constant = self.sizeOfCalendarSection;
            self.constraintContainerWidth.constant = self.view.bounds.size.width;
            self.constraintContainerTop.constant = (self.view.bounds.size.height - self.constraintContainerHeight.constant) / 2;
            self.constraintContainerLeft.constant = 0;
            self.constraintHeaderHeight.constant = [self headerHeight];
            self.constraintHeaderTopTextMargin.constant = 38;
            self.constraintHeaderCalendarBottomTextMargin.constant = 12;
            self.constraintPresetsTopSpacing.constant = 8;
            break;
        }
    }
    // internal constrations
    [self setTableViewContentTopInset:0];
}

-(void)setTableViewContentTopInset:(CGFloat)topInset
{
    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            self.tableView.contentInset = UIEdgeInsetsMake(0, topInset, 0, 0);
            break;
        }
        case BFStateWidgetExpandedChart: {
            self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 54 + 8, 0);
            break;
        }
        case BFStateWidgetExpandedCalendar: {
            self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 54 + 8, 0);
            break;
        }
    }
}

-(void)setupUIAccordingState {
    switch(self.state) {
        case BFStateWidgetExpandedZero: {
            [self.buttonClose setTitle:@"" forState:UIControlStateNormal];
            self.buttonClose.alpha = 0.0;
            self.labelWidgetValue.alpha = 1.0;
            self.viewHeaderFader.alpha = 0;
            self.viewHeader.alpha = 0;
            self.viewHeaderPreferences.alpha = 0;
            self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
            self.tableView.alpha = 0.0;
            self.container.alpha = 0.0;
            self.container.backgroundColor = self.startColor;
            self.container.layer.cornerRadius = 4;
            self.labelHeader.textColor = self.startTextColor;
            self.labelSubHeader.textColor = self.startTextColor;
            self.labelHeader.alpha = 0.0;
            self.labelSubHeader.alpha = 0.0;
            self.labelHeaderCalendar.alpha = 0.0;
            self.viewPresetsContainer.alpha = 0.0;
            break;
        }
        case BFStateWidgetExpandedCollapsed: {
            [self.buttonClose setTitle:@"" forState:UIControlStateNormal];
            self.buttonClose.alpha = 0.0;
            self.labelWidgetValue.alpha = 1.0;
            self.viewHeaderFader.alpha = 0;
            self.viewHeader.alpha = 0;
            self.viewHeaderPreferences.alpha = 0;
            self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
            self.tableView.alpha = 0.0;
            self.container.alpha = 1.0;
            self.container.backgroundColor = self.startColor;
            self.container.layer.cornerRadius = 4;
            self.labelHeader.textColor = self.startTextColor;
            self.labelSubHeader.textColor = self.startTextColor;
            self.labelHeader.alpha = 0.0;
            self.labelSubHeader.alpha = 0.0;
            self.labelHeaderCalendar.alpha = 0.0;
            self.viewPresetsContainer.alpha = 0.0;
            break;
        }
        case BFStateWidgetExpandedChart: {
            [self.buttonClose setTitle:@"BfWidgetProperties.Close".bf_localized forState:UIControlStateNormal];
            self.buttonClose.alpha = 1.0;
            self.labelWidgetValue.alpha = 0.0;
            self.viewHeaderFader.alpha = 1.0;
            self.viewHeader.alpha = 1.0;
            self.viewHeaderPreferences.alpha = 1.0;
            self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
            self.tableView.alpha = 1.0;
            self.container.alpha = 1.0;
            self.container.backgroundColor = [UIColor whiteColor];
            self.container.layer.cornerRadius = 16;
            self.labelHeader.textColor = [BFControllerWidgetExpanded textColor];
            self.labelSubHeader.textColor = [BFControllerWidgetExpanded textColor];
            self.labelHeader.alpha = 1.0;
            self.labelSubHeader.alpha = 0.5;
            self.labelHeaderCalendar.alpha = 0.0;
            self.viewPresetsContainer.alpha = (self.thisPaneOnScreen && self.enabled) ? 1.0 : 0.0;
            break;
        }
        case BFStateWidgetExpandedCalendar: {
            [self.buttonClose setTitle:@"BfWidgetProperties.Back".bf_localized forState:UIControlStateNormal];
            self.buttonClose.alpha = 1.0;
            self.labelWidgetValue.alpha = 0.0;
            self.viewHeaderFader.alpha = 1.0;
            self.viewHeader.alpha = 1.0;
            self.viewHeaderPreferences.alpha = 0.0;
            self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
            self.tableView.alpha = 1.0;
            self.container.alpha = 1.0;
            self.container.backgroundColor = [UIColor whiteColor];
            self.container.layer.cornerRadius = 16;
            self.labelHeader.textColor = [BFControllerWidgetExpanded textColor];
            self.labelSubHeader.textColor = [BFControllerWidgetExpanded textColor];
            self.labelHeader.alpha = 0.0;
            self.labelSubHeader.alpha = 0.0;
            self.labelHeaderCalendar.alpha = 1.0;
            self.viewPresetsContainer.alpha = (self.thisPaneOnScreen && self.enabled) ? 1.0 : 0.0;
            break;
        }
    }
}

-(void)updateUiDataAnimatedReload:(BOOL)animated
{
    // Animated transitions between this two states
    if (self.tableView.visibleCells.count > 0 && animated) {
        [self.tableView beginUpdates];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationMiddle];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationMiddle];
        //[self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:UITableViewRowAnimationMiddle];
        //[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:UITableViewRowAnimationMiddle];
        [self.tableView endUpdates];
    } else {
        [self reloadTableView];
    }
    
    // Configure header
    [self configureHeader];
}

-(void)configureHeader
{
    if (self.objectMeta && self.metric) {
        if (self.selectedDate) {
            // Set the particular (selected) date in header
            NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
            formatter.dateStyle = NSDateFormatterLongStyle;
            formatter.timeStyle = NSDateFormatterNoStyle;
            self.labelSubHeader.text = [formatter stringFromDate:self.selectedDate];
        } else {
            // Describe full shown dates region
            self.labelSubHeader.text = self.currentTimePeriod.numanReadable;
        }
        self.labelHeader.text = [self.objectIdModifier modifierDescriptionWithObjectMeta:self.objectMeta];
    } else {
        // Loading - have nothing to show up
        self.labelHeader.text = @"-";
        self.labelSubHeader.text = @"";
    }
    self.labelPresetsDescription.text = self.textForPresetsBarDescription;
}

-(void)setState:(BFStateWidgetExpanded)state
{
    [self setState:state animated:YES handler:nil];
}

-(void)setState:(BFStateWidgetExpanded)state
       animated:(BOOL)animated
        handler:(void(^)(void))handler {
    
    [self setState:state animated:animated duration:0.4 handler:handler];
}

-(void)setState:(BFStateWidgetExpanded)state
       animated:(BOOL)animated
       duration:(NSTimeInterval)duration
        handler:(void(^)(void))handler
{
    // We show animations only on graph->calendar or calendar->graph transitions
    BOOL needAnimatedReload = ((_state == BFStateWidgetExpandedChart) && (state == BFStateWidgetExpandedCalendar)) || ((state == BFStateWidgetExpandedChart) && (_state == BFStateWidgetExpandedCalendar));
    _state = state;
    if (animated) {
        self.view.userInteractionEnabled = NO;
        [self setupConstraintsAccordingState];
        [self.view setNeedsUpdateConstraints];
        [self updateUiDataAnimatedReload:needAnimatedReload];
        [UIView animateWithDuration:duration animations:^{
            [self.view layoutIfNeeded];
            [self setupUIAccordingState];
        } completion:^(BOOL finished) {
            
            // HACK for title
            [self.labelHeader layoutSubviews];
            
            self.view.userInteractionEnabled = YES;
            if (finished && handler) {
                handler();
            }
        }];
    } else {
        [self setupConstraintsAccordingState];
        [self.view layoutIfNeeded];
        [self setupUIAccordingState];
        [self updateUiDataAnimatedReload:NO];
        if (handler) {
            handler();
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            return 1;
        }
        case BFStateWidgetExpandedChart: {
            return 3;
        }
        case BFStateWidgetExpandedCalendar: {
            return 3;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            return 1;
        }
        case BFStateWidgetExpandedChart: {
            if (section == 0) {
                return 1;
            } else if (section == 1) {
                return [self metricRequestCellsCount];
            } else if (section == 2) {
                return 1;
            }
        }
        case BFStateWidgetExpandedCalendar: {
            
            if (section == 0) {
                return 1;
            } else if (section == 1) {
                return 1;
            } else {
                return [self accessibleObjectIdModifiers].count;
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // First section is a maintanence: placeholder under the title
    if (indexPath.section == 0) {
        return [tableView dequeueReusableCellWithIdentifier:@"placeholder" forIndexPath:indexPath];
    }

    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            return nil;
        }
        case BFStateWidgetExpandedChart: {
            if (indexPath.section == 2) {
                //
                // Chart
                //
                BFCellPropertyInfo * cell = (BFCellPropertyInfo*)[tableView dequeueReusableCellWithIdentifier:@"info" forIndexPath:indexPath];
                [cell configureWithChart:self.chart forProperty:self.metric meta:self.objectMeta datesRange:[self currentDatesRange]  selectedDate:self.selectedDate delegate:self];
                return cell;
            } else {
                //
                // Metric descriptor
                //
                BFCellMetricRequest * cell = (BFCellMetricRequest*)[tableView dequeueReusableCellWithIdentifier:[self metricCellReuseIdentifierAtIdx:indexPath.item] forIndexPath:indexPath];
                [self configureMetricRequestCell:cell forIdx:indexPath.item];
                return cell;
            }
        }
        case BFStateWidgetExpandedCalendar: {
            if (indexPath.section == 1) {
                //
                // Calendar
                //
                BFCellCalendar * cell = (BFCellCalendar*)[tableView dequeueReusableCellWithIdentifier:@"calendar" forIndexPath:indexPath];
            
                [cell configureWithDatesRange:[self currentDatesRange]];
                return cell;
            } else {
                //
                // Group selector
                //
                BFCellObjectIdModifier * cell = (BFCellObjectIdModifier*)[tableView dequeueReusableCellWithIdentifier:@"objectModifier" forIndexPath:indexPath];
                BFViewModelObjectIdModifier * modifier = [self accessibleObjectIdModifiers][indexPath.item];
                [cell configureWithText:[modifier modifierDescriptionWithObjectMeta:self.objectMeta] selected:[self selectedObjectIdModifier] == indexPath.item];
                return cell;
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // First section is a maintanence: placeholder under the title
    if (indexPath.section == 0) {
        [self deselectAll];
        return;
    }

    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            return;
        }
        case BFStateWidgetExpandedChart: {
            if (indexPath.section == 1) {
                [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:NO];
                if (self.enabled) {
                    [self toggleMetricRequestCellAtIdx:indexPath.item];
                }
            }
            return;
        }
        case BFStateWidgetExpandedCalendar: {
            
            if (indexPath.section == 1) {
                return;
            } else {
                if (self.enabled) {
                    [self selectObjectIdModifierAtIdx:indexPath.item];
                }
                return;
            }
        }
    }
}

-(CGFloat)headerHeight
{
    return 38;
    // return self.startRect.size.height;
}

-(CGFloat)sizeOfCalendarSection
{
    CGFloat maxHeight = self.view.bounds.size.height - 120;
    CGFloat oneButtonControlsHeight = [self headerHeight] + 54 + 16;
    CGFloat secondSectionSize = [self accessibleObjectIdModifiers].count * [self groupSelectorHeight];
    CGFloat secondSectionOffset = [self accessibleObjectIdModifiers].count > 0 ? 8 : 0;
    return MIN([self expandedCellSize] + oneButtonControlsHeight + secondSectionSize + secondSectionOffset, maxHeight);
}

-(CGFloat)sizeOfPropertyInfoSection
{
    CGFloat maxHeight = self.view.bounds.size.height - 120;
    CGFloat oneButtonControlsHeight = [self headerHeight] + 54 + 16;
    CGFloat sectionSize = [self expandedCellSize] + [self metricRequestVisibleCellsCount] * [self metricRequestCellHeight];
    return MIN(sectionSize + oneButtonControlsHeight, maxHeight);
}

-(CGFloat)metricRequestCellHeight
{
    return 44;
}

-(CGFloat)groupSelectorHeight
{
    return 36;
}

-(CGFloat)expandedCellSize
{
    return 260;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    // First section is a maintanence: placeholder under the title
    if (indexPath.section == 0) {
        CGFloat baseHeight = [self headerHeight] + 12;
        
        // This section can hide one of cells under top bar, so we
        // can shrink a bit the placeholder
        if (self.state == BFStateWidgetExpandedChart) {
            CGFloat shift = ([self metricRequestCellsCount] - [self metricRequestVisibleCellsCount]) * [self metricRequestCellHeight];
            baseHeight -= shift;
        }
        return baseHeight;
    }

    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            // do nothing
            return 1;
        }
        case BFStateWidgetExpandedChart: {
            if (indexPath.section == 1) {
                //
                // metric descriptor
                //
                return [self metricRequestCellHeight];
            } else {
                //
                // chart
                //
                return [self expandedCellSize];
            }
        }
        case BFStateWidgetExpandedCalendar: {
            if (indexPath.section == 1) {
                //
                // calendar
                //
                return [self expandedCellSize];
            } else {
                //
                // group selector
                //
                return [self groupSelectorHeight];
            }
            break;
        }
    }
}

-(void)reloadTableView
{
    [self.tableView reloadData];
}

-(void)needUpdateUiOnDataChange
{
    //[self updateUiData];
    [self setState:self.state animated:YES handler:^{}];
}

-(IBAction)pressClose:(id)sender
{
    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed: {
            // do nothing
            return;
        }
        case BFStateWidgetExpandedChart: {
            __weak __typeof__(self) weakSelf = self;
            [self setState:BFStateWidgetExpandedCollapsed animated:YES handler:^{
                [weakSelf setState:BFStateWidgetExpandedZero animated:YES duration:0.2 handler:^{
                    [weakSelf dismissViewControllerAnimated:NO completion:nil];
                }];
            }];
            return;
        }
        case BFStateWidgetExpandedCalendar: {
            [self setState:BFStateWidgetExpandedChart];
            return;
        }
    }
}

-(void)openPreferencesPane
{
    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed:
        case BFStateWidgetExpandedCalendar: {
            // do nothing
            return;
        }
        case BFStateWidgetExpandedChart: {
            [self setState:BFStateWidgetExpandedCalendar];
            return;
        }
    }
}

-(void)reloadMetricRequestsSelectors
{
    switch(self.state) {
        case BFStateWidgetExpandedZero:
        case BFStateWidgetExpandedCollapsed:
        case BFStateWidgetExpandedCalendar: {
            // do nothing
            return;
        }
        case BFStateWidgetExpandedChart: {
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
            return;
        }
    }
}

-(void)makeTableViewManualDataChanges:(void(^)(UITableView*))dataChanges
{
    [self.tableView beginUpdates];
    dataChanges(self.tableView);
    [self.tableView endUpdates];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // The next reload data will fix the inset
    CGFloat maxInset = ([self metricRequestCellsCount] - [self metricRequestVisibleCellsCount]) * [self metricRequestCellHeight];
    if (self.state == BFStateWidgetExpandedChart) {
        if (scrollView.contentOffset.y < 0) {
            CGFloat newInset = MIN(-scrollView.contentOffset.y, maxInset);
            [self setTableViewContentTopInset:newInset];
        } else {
            [self setTableViewContentTopInset:0];
        }
    }
}

-(void)deselectAll
{
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

-(IBAction)pressDatePrefs:(id)sender
{
    if (self.enabled) {
        [self openPreferencesPane];
    }
}

-(void)willDissapear
{
    [super willDissapear];
    // The presets bar isn't in container, so we should
    // handle BFPushableController transitions here
    [UIView animateWithDuration:0.3 animations:^{
        self.viewPresetsContainer.alpha = 0.0;
    }];
}
-(void)didAppear {
    
    [super didAppear];
    // The presets bar isn't in container, so we should
    // handle BFPushableController transitions here
    [UIView animateWithDuration:0.3 animations:^{
        self.viewPresetsContainer.alpha = 1.0;
    }];
}


//
// Getters/setters overrides
//
-(void)setObjectMeta:(BFModelObjectMeta *)objectMeta
{
    _objectMeta = objectMeta;
    [self objectMetaChanged];
}

-(NSArray<BFViewModelPreset *> *)presets
{
    if (_presets == nil) {
        [self loadPresets];
    }
    return _presets;
}

-(NSInteger)currentlySelectedPreset
{
    if (_currentlySelectedPreset == NSNotFound) {
        [self loadPresets];
    }
    return _currentlySelectedPreset;
}


@end
