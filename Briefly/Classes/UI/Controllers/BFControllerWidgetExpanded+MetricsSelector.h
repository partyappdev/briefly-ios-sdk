//
//  BFControllerWidgetExpanded+MetricsSelector.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFControllerWidgetExpanded.h"
#import "BFViewModelConstructorMainPage.h"

@class BFCellMetricRequest;

@interface BFControllerWidgetExpanded (MetricsSelector) <BFViewModelConstructorMainPageDelegate>

-(NSInteger)metricRequestCellsCount;
-(NSInteger)metricRequestVisibleCellsCount;
-(void)configureMetricRequestCell:(UITableViewCell*)cell forIdx:(NSInteger)idx;
-(void)toggleMetricRequestCellAtIdx:(NSInteger)idx;
-(NSString*)metricCellReuseIdentifierAtIdx:(NSInteger)idx;
@end
