//
//  BFControllerWidgetExpanded.h
//  Briefly
//
//  Created by Artem Bondar on 10.01.2018.
//

#import <UIKit/UIKit.h>
#import "BFControllerPushable.h"

@interface BFControllerWidgetExpanded : BFControllerPushable

+(instancetype)instantiateToShowFromView:(UIView*)view
                              startColor:(UIColor*)startColor
                          startTextColor:(UIColor*)startTextColor
                         startWidgetText:(NSString*)startWidgetText
                             forObjectId:(NSString*)objectId
                             widgetGroup:(NSString*)widgetGroup
                        defaultAttribute:(NSString*)defaultAttribute
                                 enabled:(BOOL)enabled;

@end
