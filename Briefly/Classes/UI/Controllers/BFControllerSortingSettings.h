//
//  BFControllerSortingSettings.h
//  Briefly
//
//  Created by Artem Bondar on 07.04.2018.
//

#import <UIKit/UIKit.h>
#import "BFSorter.h"

@interface BFControllerSortingSettings : UIViewController

+(instancetype)instantiateToShowFromView:(UIView*)view
                               forSorter:(BFSorter*)sorter;

@end
