//
//  BFControllerWidgetExpanded+Presets.m
//  Briefly
//
//  Created by Artem Bondar on 14.03.2018.
//

#import "BFControllerWidgetExpanded+Private.h"
#import "BFControllerWidgetExpanded+Presets.h"
#import "BFControllerWidgetExpanded+DataManagement.h"
#import "NSString+Utils.h"

#import "BFBrieflyManager.h"
#import "BFUIManager+Storage.h"
#import "BFCellWidgetPreset.h"
#import "BFViewModelPreset.h"

@implementation BFControllerWidgetExpanded (Presets)

-(void)initPresets
{
    self.currentlySelectedPreset = NSNotFound;
    [self loadPresets];
}

-(BOOL)emptyPreset
{
    return self.presets.count == 0 || (self.presets.count == 1 && self.presets.firstObject.defaultPreset == YES);
}

-(NSString*)textForPresetsBarDescription
{
    if ([[BFBrieflyManager sharedManager].uiManager onceCreatedPresets] || self.presets.firstObject.metricRequest == nil) {
        return @"";
    } else {
        return @"BfWidgetProperties.Presets.Description".bf_localized;
    }
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (![self emptyPreset]) {
        return self.presets.count + 1;
    } else {
        if (self.presets.firstObject.metricRequest) {
            return 1;
        } else {
            return 0;
        }
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0) {
        BFCellWidgetPreset * cell = (BFCellWidgetPreset*)[collectionView dequeueReusableCellWithReuseIdentifier:@"text" forIndexPath:indexPath];
        NSString * text = [self emptyPreset] ? @"BfWidgetProperties.Presets.Create".bf_localized :  @" + ";
        [cell configureWithText:text enabled:YES idx:-1 delegate:nil];
        return cell;
    } else {
        BFCellWidgetPreset * cell = (BFCellWidgetPreset*)[collectionView dequeueReusableCellWithReuseIdentifier:@"preset" forIndexPath:indexPath];
        BOOL selected = indexPath.item - 1 == self.currentlySelectedPreset;
        NSString * text = self.presets[indexPath.item-1].name;
        [cell configureWithText:text
                        enabled:selected
                            idx:indexPath.item - 1
                       delegate:self];
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0) {
        // Create a new metrics preset
        // ask for a preset name
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"BfWidgetProperties.Presets.Create.Title".bf_localized message:@"BfWidgetProperties.Presets.Create.Desription".bf_localized preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"-";
            textField.text = @"BfWidgetProperties.Presets.Create.Placeholder".bf_localized;
            //textField.secureTextEntry = YES;
        }];
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"BfWidgetProperties.Presets.Create.Ok".bf_localized style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self createNewPresetWithName:[[alertController textFields][0] text]];
        }];
        [alertController addAction:confirmAction];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"BfWidgetProperties.Presets.Create.Cancel".bf_localized style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            // do nothing
        }];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        [self selectPresetIdx:indexPath.item - 1];
        [self.collectionViewPresets reloadData];
    }
}

-(void)createNewPresetWithName:(NSString*)name
{
    if (name.length < 1 || name.length > 16) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"BfWidgetProperties.Presets.EmptyTitle.Title".bf_localized message:@"BfWidgetProperties.Presets.EmptyTitle.Desription".bf_localized preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"-";
            //textField.secureTextEntry = YES;
        }];
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"BfWidgetProperties.Presets.EmptyTitle.Ok".bf_localized style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertController addAction:confirmAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    // Name is OK, create preset
    BFViewModelPreset * newPreset = [self.presets[self.currentlySelectedPreset] copyThisWithName:name];
    [[BFBrieflyManager sharedManager].uiManager setOnceCreatedPreset];
    [self configureHeader];
    if (self.presets.firstObject.defaultPreset) {
        // if currently only one preset here - then recreate
        [self storePresets:@[newPreset]];
        [self.collectionViewPresets reloadData];
        //
        // we don't need reload all metric related stuff - only the metric bar
        // is affected
        //
    } else {
        //
        // It's very important!!! currentlySelectedPreset should be set firstly
        //
        /*[self.collectionViewPresets performBatchUpdates:^{
            self.currentlySelectedPreset = 0;
            [self storePresets:[@[newPreset] arrayByAddingObjectsFromArray:self.presets]];
            [self.collectionViewPresets insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:0]]];
        } completion:^(BOOL finished) {
        }];*/
        
        self.currentlySelectedPreset = 0;
        [self storePresets:[@[newPreset] arrayByAddingObjectsFromArray:self.presets]];
        [self.collectionViewPresets reloadData];
    }
}

-(void)pressedDeletePresetAtIdx:(NSInteger)idx
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"BfWidgetProperties.Presets.Delete.Title".bf_localized message:@"BfWidgetProperties.Presets.Delete.Desription".bf_localized preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"BfWidgetProperties.Presets.Delete.Ok".bf_localized style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // delete the
        [self deletePresetAtIdx:idx];
    }];
    [alertController addAction:confirmAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"BfWidgetProperties.Presets.Delete.Cancel".bf_localized style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        // do nothing
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)deletePresetAtIdx:(NSInteger)idx
{
    if (self.presets.count == 1) {
        // in that case - return to default preset
        [self.presets.firstObject remakeToDefault];
        self.currentlySelectedPreset = 0;
        [self storePresets:self.presets];
        [self.collectionViewPresets reloadData];
    } else {
        /*[self.collectionViewPresets performBatchUpdates:^{
            self.currentlySelectedPreset = 0;
            NSMutableArray * updatedArray = [NSMutableArray arrayWithArray:self.presets];
            [updatedArray removeObjectAtIndex:idx];
            [self storePresets:updatedArray];
            [self.collectionViewPresets deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:idx + 1 inSection:0]]];
        } completion:^(BOOL finished) {
        }];*/
        self.currentlySelectedPreset = 0;
        NSMutableArray * updatedArray = [NSMutableArray arrayWithArray:self.presets];
        [updatedArray removeObjectAtIndex:idx];
        [self storePresets:updatedArray];
        [self.collectionViewPresets reloadData];
    }
}

-(void)reloadPresetsAfterMetricsChange
{
    [self.collectionViewPresets reloadData];
}

@end
