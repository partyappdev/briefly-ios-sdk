//
//  BFControllerWidgetExpanded+MetricsSelector.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFControllerWidgetExpanded+MetricsSelector.h"
#import "BFControllerWidgetExpanded+DataManagement.h"
#import "BFControllerWidgetExpanded+Private.h"

#import "BFCellMetricRequest.h"

#import "BFViewModelMetricRequestSingle.h"
#import "BFViewModelMetricRequestMulti.h"
#import "BFViewModelConstructorMainPage.h"

#import "BFControllerMetricRequestConstructor.h"

#import "NSString+Utils.h"

@implementation BFControllerWidgetExpanded (MetricsSelector)

-(NSInteger)metricRequestCellsCount
{
    return self.flatterenMetricRequests.count + 1;
}

-(NSInteger)metricRequestVisibleCellsCount
{
    return MAX(self.flatterenMetricRequests.count, 1);
}

-(BOOL)canAddMoreRequests
{
    return self.flatterenMetricRequests.count < 3;
}

-(NSArray*)flatterenMetricRequests
{
    if ([self.metric isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
        BFViewModelMetricRequestMulti * array = (BFViewModelMetricRequestMulti*)self.metric;
        return array.childMetricRequests;
    } else if (self.metric) {
        return @[self.metric];
    } else {
        return @[];
    }
}

-(NSString*)metricCellReuseIdentifierAtIdx:(NSInteger)idx
{
    if (idx == 0) {
        return @"metric-action";
    }
    return @"metric";
}

-(void)configureMetricRequestCell:(UITableViewCell*)cell forIdx:(NSInteger)idx
{
    if (idx == 0) {
        BFCellMetricRequestAction * actionCell = (BFCellMetricRequestAction*)cell;
        [actionCell configureWithText:@"BfWidgetProperties.AddMetric".bf_localized enabled:[self canAddMoreRequests]];
        return;
    }
    BFCellMetricRequest * metricCell = (BFCellMetricRequest*)cell;
    NSInteger effectiveIdx = idx - 1;
    NSNumber * value = [self currentValueForAttributeAtIdx:effectiveIdx];
    [metricCell configureWithMetricRequest:[self requestAtIdx:effectiveIdx] objectIdModifier:self.objectIdModifier meta:self.objectMeta value:value idx:effectiveIdx];
}

-(BFViewModelMetricRequestSingle*)requestAtIdx:(NSInteger)idx
{
    return (BFViewModelMetricRequestSingle*)self.flatterenMetricRequests[idx];
}

-(void)toggleMetricRequestCellAtIdx:(NSInteger)idx
{
    if (self.objectMeta) {
        if (idx == 0) {
            
            if (!self.canAddMoreRequests) {
                [self deselectAll];
                return;
            }
            // Add metric pressed. Simple stuff. just create the metric at the first place,
            // and open it
            [self insertNewRequestWithHandler:^{
                NSInteger newIdx = [self metricRequestCellsCount] - 1 - 1;
                BFViewModelConstructorMainPage * model = [BFViewModelConstructorMainPage withRequest:[self requestAtIdx:newIdx] timeRange:self.currentTimePeriod meta:self.objectMeta idx:newIdx justCreated:NO canDelete:[self flatterenMetricRequests].count > 1];
                model.delegate = self;
                BFControllerMetricRequestConstructor * ctrl = [BFControllerMetricRequestConstructor instantiateWithModel:model];
                
                [self presentViewController:ctrl animated:YES completion:^{
                    [self deselectAll];
                }];
            }];
            return;
        }
        BFViewModelConstructorMainPage * model = [BFViewModelConstructorMainPage withRequest:[self requestAtIdx:idx-1] timeRange:self.currentTimePeriod meta:self.objectMeta idx:idx-1 justCreated:NO canDelete:[self flatterenMetricRequests].count > 1];
        model.delegate = self;
        BFControllerMetricRequestConstructor * ctrl = [BFControllerMetricRequestConstructor instantiateWithModel:model];
        
        [self presentViewController:ctrl animated:YES completion:^{
            [self deselectAll];
        }];
        
    }
}


-(NSNumber*)currentValueForAttributeAtIdx:(NSInteger)idx
{
    // We have calues on graph only for selected values
    if (idx > self.flatterenMetricRequests.count || idx < 0) {
        return nil;
    }
    BFViewModelMetricRequestSingle * request = self.flatterenMetricRequests[idx];
    
    // if we are in loading state - we have nothing to show
    if (!self.chart) {
        return nil;
    }
    
    // Now calculate the value to be shown in slider
    __block NSNumber * summValue = @(0);
    
    NSInteger selectedIdx = [self.chart idxForDate:self.selectedDate];
    if (selectedIdx != NSNotFound) {
        NSArray<NSNumber*> * multival = [self.chart valuesAtRange:BFRangeMake(selectedIdx, 1)].firstObject.multivalue;
        if (idx >= multival.count) {
            return @(0);
        }
        return multival[idx];
    }
    
    BFRange interestRange = self.chart.startRange;
    //BFRange interestRange = BFRangesEqual(self.lastSteadyRange, BFRangeMake(0, 0)) ? self.displayedChart.startRange : self.lastSteadyRange;
    NSArray * points = [self.chart valuesAtRange:interestRange];
    __block NSInteger totalPoints = 0;
    [points enumerateObjectsUsingBlock:^(BFViewModelChartDataPoint * _Nonnull pt, NSUInteger i, BOOL * _Nonnull stop) {
        
        // Check boundaries, just to be sure, it's OK
        if (idx >= pt.multivalue.count) {
            return;
        }
        totalPoints++;
        switch (request.timeRangeAggregation) {
            case BFModelTimeRangeAggregationAt:
                summValue = pt.multivalue[idx];
                break;
            case BFModelTimeRangeAggregationSum:
            case BFModelTimeRangeAggregationAvg:
                summValue = @(summValue.doubleValue + pt.multivalue[idx].doubleValue);
                break;
            case BFModelTimeRangeAggregationMax:
                summValue = @(MAX(summValue.doubleValue, pt.multivalue[idx].doubleValue));
                break;
            case BFModelTimeRangeAggregationMin:
                summValue = @(MIN(summValue.doubleValue, pt.multivalue[idx].doubleValue));
                break;
            case BFModelTimeRangeAggregationRange:
                //
                // add assert here
                //
                break;
        }
    }];
    
    if (request.timeRangeAggregation == BFModelTimeRangeAggregationAvg && totalPoints) {
        summValue = @(summValue.doubleValue / totalPoints);
    }
    return summValue;
}

-(void)requestChangedAt:(BFViewModelConstructorMainPage*)model
{
    if ([self.metric isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
        BFViewModelMetricRequestMulti * multi = (BFViewModelMetricRequestMulti*)self.metric;
        [self setMetricRequestAndFetch:[multi byUpdatingRequestAtIdx:model.idx to:model.request]];
        
    } else {
        [self setMetricRequestAndFetch:model.request];
    }
}

-(void)requestDeletedAt:(BFViewModelConstructorMainPage*)model
{
    if ([self.metric isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
        BFViewModelMetricRequestMulti * multi = (BFViewModelMetricRequestMulti*)self.metric;
        [self setMetricRequestAfterDelete:[multi byRemovingRequestAtIdx:model.idx]];
    } else {
        // WTF??? How can we go here
    }
}

-(void)insertNewRequestWithHandler:(void(^)(void))handler
{
    //
    // Find all used metricIds
    //
    NSMutableArray * metricIds = [NSMutableArray array];
    for (BFViewModelMetricRequestSingle * req in self.flatterenMetricRequests) {
        if (![req isKindOfClass:[BFViewModelMetricRequestSingle class]] || [req metricId] == nil)
            continue;
        [metricIds addObject:[req metricId]];
    }
    
    //
    // Genereta new request, and store it to model
    //
    BFViewModelMetricRequestSingle * newRequest = [BFViewModelMetricRequestSingle dummyWithAlreadyUsedMetricIds:metricIds andMeta:self.objectMeta];
    
    [self makeTableViewManualDataChanges:^(UITableView *tableView) {
        
        if ([self.metric isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
            BFViewModelMetricRequestMulti * multi = (BFViewModelMetricRequestMulti*)self.metric;
            [self setMetricRequestAfterInsertion:[multi byAppendingRequest:newRequest]];
        } else if (self.metric){
            BFViewModelMetricRequestMulti * array = [BFViewModelMetricRequestMulti withArray:@[self.metric, newRequest]];
            [self setMetricRequestAfterInsertion:array];
        } else {
            [self setMetricRequestAfterInsertion:newRequest];
        }
        
        // Do table view insertion, and after reload the data
        [UIView animateWithDuration:0.8 animations:^{
            [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:[self metricRequestCellsCount]-1 inSection:1]] withRowAnimation:UITableViewRowAnimationMiddle];
            [self setTableViewContentTopInset:0];
        } completion:^(BOOL finished) {
            [self setMetricRequestAndFetch:self.metric];
            handler();
        }];
    }];
}

@end
