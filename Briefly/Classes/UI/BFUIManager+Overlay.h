//
//  BFUIManager+Overlay.h
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "BFUIManager.h"

@interface BFUIManager (Overlay)

-(void)updateOverlayOnTimer;
-(void)recreateOverlayView;
-(void)needUpdateOverlayAfterScreenChange;

-(void)subscribeViewForChanges:(UIView*)view;
-(void)unsubscribeViewFromChanges:(UIView*)view;
-(void)forceWidgetsReloadData;
-(void)reloadOnlyHeatmaps;
-(void)updateSortingStateOverlay;

@end
