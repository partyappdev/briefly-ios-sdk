//
//  BFUIManager+Storage.m
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "BFUIManager+Storage.h"
#import "BFUIManager_Private.h"
#import "BFModelHeatMap.h"
#import "BFBrieflyManager+ApiWrapper.h"
#import "UIViewController+Utils.h"

#import "BFViewModelPreset.h"

@interface BFHeatmapRequest: NSObject

@property (strong, nonatomic) NSString * viewId;
@property (strong, nonatomic) NSString * screenId;
@property (strong, nonatomic) NSString * orientation;
@property (nonatomic) CGSize dimensions;
@property (strong, nonatomic) void(^resultsHandler)(UIImage*);
@property (strong, nonatomic) BOOL(^checkAliveHandler)(void);

// results section
@property (strong, nonatomic) BFHeatMap * resultsHeatmap;
@property (strong, nonatomic) UIImage * resultsImage;
@property (nonatomic) BOOL finishedProcessing;
@end

@implementation BFHeatmapRequest

+(instancetype)requestWithViewId:(NSString*)viewId
                  atScreenWithId:(NSString*)screenId
                      dimensions:(CGSize)dimensions
                     orientation:(NSString*)orientation
                         handler:(void(^)(UIImage*))handler
               checkAliveHandler:(BOOL(^)(void))checkAliveHandler
{
    BFHeatmapRequest * newObj = [BFHeatmapRequest new];
    newObj.viewId = viewId;
    newObj.screenId = screenId;
    newObj.orientation = orientation;
    newObj.dimensions = dimensions;
    newObj.resultsHandler = handler;
    newObj.checkAliveHandler = checkAliveHandler;
    return newObj;
}

-(BOOL)isProcessing
{
    return !self.finishedProcessing;
}

-(NSString *)debugDescription
{
    NSString * isProcessing = [self isProcessing] ? @"YES": @"NO";
    NSString * isAlive = self.checkAliveHandler() ? @"YES": @"NO";
    return [NSString stringWithFormat:@"BFHeatmapRequest:%@ (screen_id: %@): orient:%@ isProcessing:%@ isAlive:%@", self.viewId, self.screenId, self.orientation, isProcessing, isAlive];
}

-(NSString *)description
{
    return [self debugDescription];
}

@end

@implementation BFUIManager (StorageInternal)

-(void)storeTapGesture:(CGPoint)pt
       atViewWithWebId:(NSString*)viewWebId
        withDimensions:(CGSize)dimensions
           orientation:(NSString*)orientation
               handler:(void(^)(BOOL))handler
{
    if (self.heatmapMode == BFHeatmapsModeDebugLocal) {
        NSString * dimId = [NSString stringWithFormat:@"%ld/%ld", (long)dimensions.width, (long)dimensions.height];
        NSString * localId = [NSString stringWithFormat:@"%@-%@-%@", viewWebId, orientation, dimId];
        BFHeatMap * heatMap = self.localHeatmaps[localId];
        if (heatMap == nil) {
            heatMap = [[BFHeatMap alloc] initWithScreenId:localId size:128];
            self.localHeatmaps[localId] = heatMap;
        }
        CGPoint normalizedPoint = CGPointMake(pt.x / dimensions.width, pt.y / dimensions.height);
        [heatMap addPointAtX:normalizedPoint.x y:normalizedPoint.y];
        handler(YES);
    } else {
        __weak __typeof__(self) weakSelf = self;
        NSValue * ptObject = [NSValue valueWithCGPoint:pt];
        [self.host addTouches:@[ptObject]
                  orientation:orientation
                       viewId:viewWebId
                      groupId:@"all"
                        width:dimensions.width
                       height:dimensions.height
                  withHandler:^(BOOL succeed) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          @try {
                              handler(succeed);
                          }
                          @catch(NSException * excpetion) {
                              [weakSelf.host logRecord:excpetion.debugDescription logLevel:BFLogLevelFatal];
                          }
                      });
         }];
    }
}

-(void)internalHeatmapForViewWithWebId:(NSString*)viewId
                     withDimensions:(CGSize)dimensions
                            handler:(void(^)(UIImage*))handler
                  checkAliveHandler:(BOOL(^)(void))checkAliveHandler
{
    NSString * orientation = self.currentOrientation;
    NSString * screenId = self.currentControllerUid;
    if (!screenId) {
        // don't show anything for unknown screen
        [self.host logRecord:[NSString stringWithFormat:@"skip heatmap reading because of unknown screen: %@", viewId] logLevel:BFLogLevelDebug];
        handler(nil);
        return;
    }
    
    // STEP 0. Checking the request's queue stats
    // first of all: check if we gone to the next screen?
    // TODO: check an orientation too
    if (![self isNewScreenIdSuitableWithStorageState:screenId]) {
        [self.host logRecord:[NSString stringWithFormat:@"heatmaps goint to new screen: %@", screenId] logLevel:BFLogLevelDebug];
        [self dropHeatmapRequestsQueue];
    }
    
    // STEP 1. Create new request object
    BFHeatmapRequest * req = [BFHeatmapRequest requestWithViewId:viewId
                                                  atScreenWithId:screenId
                                                      dimensions:dimensions
                                                     orientation:orientation
                                                         handler:handler
                                               checkAliveHandler:checkAliveHandler];
    [self.heatmapRequests addObject:req];
    __block __weak BFHeatmapRequest *weakReq = req;

    // STEP 2. Load the heatmap
    __weak __typeof__(self) weakSelf = self;
    if (self.heatmapMode == BFHeatmapsModeDebugLocal) {
        // Local is stupid as hell: nothing special
        NSString * dimId = [NSString stringWithFormat:@"%ld/%ld", (long)dimensions.width, (long)dimensions.height];
        NSString * localId = [NSString stringWithFormat:@"%@-%@-%@", viewId, orientation, dimId];
        __block BFHeatMap * heatMap = self.localHeatmaps[localId];
        if (heatMap == nil) {
            heatMap = [[BFHeatMap alloc] initWithScreenId:localId size:128];
            self.localHeatmaps[localId] = heatMap;
        }
        [weakSelf handlerLocalGetHeatmapResults:heatMap forRequest:weakReq];
        /*[heatMap heatmapForDimensions:dimensions withHandler:^(UIImage * image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(image);
            });
        }];*/
    } else {
        [self.host getHeatmapForViewId:viewId
                           orientation:orientation
                                 width:dimensions.width
                                height:dimensions.height
                           withHandler:^(BOOL succeed, BFModelHeatMap *heatmap) {
            [weakSelf handlerGetHeatmapResults:heatmap forRequest:weakReq];
        }];
    }
}

-(void)handlerGetHeatmapResults:(BFModelHeatMap*)heatmap
                     forRequest:(BFHeatmapRequest*)req
{
    // check, do we have to generate a heatmap, or results are outdated?
    if (req == nil) {
        [self checkProcessingQueueState];
    } else {
        if (heatmap != nil) {
            req.resultsHeatmap = [[BFHeatMap alloc] initWithHeatMap:heatmap];
        }
        req.finishedProcessing = YES;
        [self checkProcessingQueueState];
    }
}

-(void)handlerLocalGetHeatmapResults:(BFHeatMap*)heatmap
                          forRequest:(BFHeatmapRequest*)req
{
    // check, do we have to generate a heatmap, or results are outdated?
    if (req == nil) {
        [self checkProcessingQueueState];
    } else {
        if (heatmap != nil) {
            req.resultsHeatmap = heatmap;
        }
        req.finishedProcessing = YES;
        [self checkProcessingQueueState];
    }
}

-(void)checkProcessingQueueState
{
    dispatch_async(dispatch_get_main_queue(), ^{
    // and now do all the processing
    //
    // firstly let's remove non-alive requests
    NSString * debugString = [NSString stringWithFormat:@"before filering: %@", self.heatmapRequests];
    NSPredicate * aliveOnly = [NSPredicate predicateWithBlock:^BOOL(BFHeatmapRequest* evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        return evaluatedObject.checkAliveHandler();
    }];
    [self.heatmapRequests filterUsingPredicate:aliveOnly];
    
    // Now let's check, are everyone's done
    BOOL done = YES;
    for (BFHeatmapRequest * iterReq in self.heatmapRequests) {
        if ([iterReq isProcessing]) {
            done = NO;
        }
    }
    
    if (!done) {
        // wait for others
        return;
    }
    NSString * debugString2 = [NSString stringWithFormat:@"after filering: %@", self.heatmapRequests];
    [self.host logRecord:[NSString stringWithFormat:@"process heatmap results:\n%@\n%@\n", debugString, debugString2] logLevel:BFLogLevelDebug];
    // STEP 0:
    //
    // normalize all results
    NSInteger maxDensity = 0;
    for (BFHeatmapRequest * iterReq in self.heatmapRequests) {
        NSInteger iMaxDensity = [iterReq.resultsHeatmap maxDensityForDimensions:iterReq.dimensions];
        if (iMaxDensity > maxDensity) {
            maxDensity = iMaxDensity;
        }
    }
    // and update heatmaps
    for (BFHeatmapRequest * iterReq in self.heatmapRequests) {
        iterReq.resultsHeatmap.maxDensityScreenWide = maxDensity;
    }

    // STEP 1:
    //
    // show all results
    for (BFHeatmapRequest * iterReq in self.heatmapRequests) {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
            // build maps for every
            if (iterReq.resultsHeatmap) {
                [iterReq.resultsHeatmap heatmapForDimensions:iterReq.dimensions withHandler:^(UIImage * image) {
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                        iterReq.resultsHandler(image);
                    });
                }];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    iterReq.resultsHandler(nil);
                });
            }
        });
    }
    });
}

-(void)dropHeatmapRequestsQueue
{
    [self.host logRecord:[NSString stringWithFormat:@"heatmaps req queue dropped: %@", self.heatmapRequests] logLevel:BFLogLevelDebug];
    self.heatmapRequests = [NSMutableSet set];
}

-(BOOL)isNewScreenIdSuitableWithStorageState:(NSString*)screenId
{
    NSString * oldScreenId = [(BFHeatmapRequest*)self.heatmapRequests.anyObject screenId];
    return oldScreenId == nil || [oldScreenId isEqualToString:screenId];
}


-(NSNumber*)selectedMetricIdxForObjectId:(NSString*)objectId
{
    if (objectId == nil) {
        return nil;
    }
    NSString * key = [NSString stringWithFormat:@"bf_sv_obj_id_%@_sorter_mertic_idx", objectId];
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)setSelectedMetricIdx:(NSNumber*)idx forObjectId:(NSString*)objectId
{
    if (objectId == nil) {
        return;
    }
    NSString * key = [NSString stringWithFormat:@"bf_sv_obj_id_%@_sorter_mertic_idx", objectId];
    if (idx != nil) {
        [[NSUserDefaults standardUserDefaults] setObject:idx forKey:key];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
}

-(NSNumber*)sortOrderForObjectId:(NSString*)objId
{
    NSString * key = [NSString stringWithFormat:@"bf_sort_order_%@", objId];
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)setSortOrder:(BOOL)ascending forObjectId:(NSString*)objId
{
    NSString * key = [NSString stringWithFormat:@"bf_sort_order_%@", objId];
    [[NSUserDefaults standardUserDefaults] setBool:ascending forKey:key];
}


@end

@implementation BFUIManager (Storage)

-(void)addEventViewShown:(NSString*)screenId
{
    // Now the storage is stupid: just send the data without
    // collecting
    [self.host addMetricData:@(1) atTime:[NSDate date] forObjectId:screenId objectName:@"Экран" objectGroup:nil objectGroupName:nil forMetricId:@"show" metricName:@"Показы" metricUnit:nil defaultClientRequest:@"obj.group(sum).show.group_by(day).sum().at()" missingDataPolicy:kBFMissingPointsTreatingPolicyNoData withHandler:nil];
}

-(void)addEventViewTouched:(NSString*)screenId
{
    // Now the storage is stupid: just send the data without
    // collecting
    [self.host addMetricData:@(1) atTime:[NSDate date] forObjectId:screenId objectName:@"Экран" objectGroup:nil objectGroupName:nil forMetricId:@"touch" metricName:@"Нажатия" metricUnit:nil defaultClientRequest:@"obj.group(sum).touch.group_by(day).sum().at()" missingDataPolicy:kBFMissingPointsTreatingPolicyNoData withHandler:nil];
}

-(void)addEventViewLeaved:(NSString*)screenId withTimeInSeconds:(NSInteger)seconds
{
    // Now the storage is stupid: just send the data without
    // collecting
    if (seconds < 0) {
        return;
    }
    [self.host addMetricData:@(seconds) atTime:[NSDate date] forObjectId:screenId objectName:@"Экран" objectGroup:nil objectGroupName:nil forMetricId:@"time_spent" metricName:@"Время на экране" metricUnit:@"sec" defaultClientRequest:@"obj.group(avg).time_spent.group_by(day).avg().avg(week)" missingDataPolicy:kBFMissingPointsTreatingPolicyNoData withHandler:nil];
}

-(BOOL)isSortingOnForWidgetGroup:(NSString*)widgetGroup
{
    NSString * key = [NSString stringWithFormat:@"bf_sort_for_wg_%@", widgetGroup];
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

-(void)setSortingOn:(BOOL)sortingOn
     forWidgetGroup:(NSString*)widgetGroup
{
    NSString * key = [NSString stringWithFormat:@"bf_sort_for_wg_%@", widgetGroup];
    [[NSUserDefaults standardUserDefaults] setBool:sortingOn forKey:key];
}

-(NSArray<BFViewModelPreset*>*)presetsForSingleValueWidgetWithObjectId:(NSString*)objectId
{
    if (objectId == nil) {
        return nil;
    }
    //
    // try to read from cache
    //
    NSArray * cachedVal = self.cachcedPresets[objectId];
    if (cachedVal != nil) {
        return cachedVal;
    }
    
    //
    // read serialized
    //
    NSString * key = [NSString stringWithFormat:@"bf_sv_obj_id_%@_presets", objectId];
    NSArray * presets = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if (presets == nil) {
        return nil;
    }
    
    //
    // deserialize
    //
    NSMutableArray * presetsWithObjects = [NSMutableArray array];
    for (NSDictionary * dict in presets) {
        BFViewModelPreset * preset = [BFViewModelPreset fromSerializedDescription:dict];
        if (preset != nil) {
            [presetsWithObjects addObject:preset];
        }
    }
    self.cachcedPresets[objectId] = presetsWithObjects;
    return presetsWithObjects;
}

-(void)setPresets:(NSArray*)presets forSingleValueWidgetWithObjectId:(NSString*)objectId
{
    if (objectId == nil) {
        return;
    }

    NSString * key = [NSString stringWithFormat:@"bf_sv_obj_id_%@_presets", objectId];
    if (presets != nil) {
        self.cachcedPresets[objectId] = presets;
        NSMutableArray * presetsDescriptions = [NSMutableArray array];
        for (BFViewModelPreset * preset in presets) {
            [presetsDescriptions addObject:[preset serializedDescription]];
        }
        [[NSUserDefaults standardUserDefaults] setObject:presetsDescriptions forKey:key];
    } else {
        [self.cachcedPresets removeObjectForKey:objectId];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
}

-(NSNumber*)selectedPresetsIdxForSingleValueWidgetWithObjectId:(NSString*)objectId
{
    if (objectId == nil) {
        return nil;
    }
    NSString * key = [NSString stringWithFormat:@"bf_sv_obj_id_%@_presets_selected", objectId];
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)setSelectedPresetIdx:(NSNumber*)selected forSingleValueWidgetWithObjectId:(NSString*)objectId
{
    if (objectId == nil) {
        return;
    }
    NSString * key = [NSString stringWithFormat:@"bf_sv_obj_id_%@_presets_selected", objectId];
    if (selected != nil) {
        [[NSUserDefaults standardUserDefaults] setObject:selected forKey:key];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
}

-(BOOL)onceCreatedPresets
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"bf_sv_once_created_presets"];
}

-(void)setOnceCreatedPreset
{
    [[NSUserDefaults standardUserDefaults] setObject:@(1) forKey:@"bf_sv_once_created_presets"];
}

@end
