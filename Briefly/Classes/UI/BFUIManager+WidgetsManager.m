//
//  BFUIManager+WidgetsManager.m
//  Briefly
//
//  Created by Artem Bondar on 15.03.2018.
//

#import "BFUIManager+Storage.h"
#import "BFUIManager_Private.h"
#import "BFModelHeatMap.h"
#import "BFBrieflyManager+ApiWrapper.h"
#import "UIViewController+Utils.h"

#import "BFViewModelMetricRequest.h"
#import "BFViewModelPreset.h"
#import "BFViewModelObjectIdModifier.h"
#import "BFModelTimePeriod.h"
#import "BFSorter+Private.h"

#import "BFControllerSortingSettings.h"

@implementation BFUIManager (WidgetsManager)

-(NSString*)storageIdWithObjectId:(NSString*)objectId andWidgetGroupId:(NSString*)widgetGroupId
{
    if (widgetGroupId != nil) {
        return [NSString stringWithFormat:@"wg_%@", widgetGroupId];
    } else if (objectId != nil){
        return [NSString stringWithFormat:@"ob_%@", objectId];
    } else {
        return nil;
    }
}

-(BFViewModelPreset*)currentPresetForObjectId:(NSString*)objectId
                                widgetGroupId:(NSString*)widgetGroupId
                      andDefaultRequestString:(NSString*)defaultRequestString
{
    if (objectId == nil && widgetGroupId == nil) {
        return nil;
    }
    NSArray<BFViewModelPreset*>* presets = [self presetsListForObjectId:objectId widgetGroupId:widgetGroupId andDefaultRequestString:defaultRequestString];
    NSInteger idx = [self currentlySelectedPresetIdxForObjectId:objectId widgetGroupId:widgetGroupId];
    if (idx == NSNotFound) {
        return nil;
    }
    return presets[idx];
}

-(NSArray<BFViewModelPreset*>*)presetsListForObjectId:(NSString*)objectId
                                        widgetGroupId:(NSString*)widgetGroupId
                              andDefaultRequestString:(NSString*)defaultRequestString
{
    if (objectId == nil && widgetGroupId == nil) {
        return nil;
    }
    NSString * oid = [self storageIdWithObjectId:objectId andWidgetGroupId:widgetGroupId];
    NSArray<BFViewModelPreset*> * presets = [self presetsForSingleValueWidgetWithObjectId:oid];
    
    BOOL noPresetsAtAll = (presets == nil || presets.count == 0);
    //
    // Special case: if some widget have set it's default property after
    // setting up object id, then we should rewrite old preset to new (if old preset is really empty)
    BOOL onlyEmptyPresetHere = NO;
    if (presets.count == 1) {
        onlyEmptyPresetHere = presets.firstObject.metricRequest == nil;
    }

    if (noPresetsAtAll || onlyEmptyPresetHere) {
        //
        // It's a first start - we don't have presets at all - in that case build them
        //
        BFViewModelMetricRequest * request = [BFViewModelMetricRequest fromSerializedString:defaultRequestString];
        BFModelTimePeriod * timeRange = [request defaultTimeTangeArg];
        if (timeRange == nil) {
            timeRange = [BFModelTimePeriod defaultRange];
        }
        BFViewModelObjectIdModifier * objIdModifier = [BFViewModelObjectIdModifierEqual instance];
        BFViewModelPreset * defaultPreset = [BFViewModelPreset defaultPresetWithRequest:request timeRange:timeRange objIdModifier:objIdModifier];
        presets = @[defaultPreset];
        [self setPresets:presets forSingleValueWidgetWithObjectId:oid];
        [self setSelectedPresetIdx:@(0) forSingleValueWidgetWithObjectId:oid];
        //
        // Now we stored just created preset, and can continue
        //
    }
    return presets;
}

-(NSInteger)currentlySelectedPresetIdxForObjectId:(NSString*)objectId
                                 widgetGroupId:(NSString*)widgetGroupId
{
    if (objectId == nil && widgetGroupId == nil) {
        return NSNotFound;
    }
    NSString * oid = [self storageIdWithObjectId:objectId andWidgetGroupId:widgetGroupId];
    NSNumber * currentlySelected = [[BFBrieflyManager sharedManager].uiManager selectedPresetsIdxForSingleValueWidgetWithObjectId:oid];
    if (currentlySelected == nil) {
        return NSNotFound;
    }
    return currentlySelected.integerValue;
}

-(void)setPresets:(NSArray<BFViewModelPreset*>*)presets
      selectedIdx:(NSInteger)idx
      forObjectId:(NSString*)objectId
    widgetGroupId:(NSString*)widgetGroupId
{
    NSString * oid = [self storageIdWithObjectId:objectId andWidgetGroupId:widgetGroupId];
    [self setPresets:presets forSingleValueWidgetWithObjectId:oid];
    [self setSelectedPresetIdx:@(idx) forSingleValueWidgetWithObjectId:oid];
}

-(void)viewSortStatusPressed
{
    if ([self.currentController conformsToProtocol:@protocol(BFSortableController)]) {
        id<BFSortableController> ctrl = (id<BFSortableController>)self.currentController;
        BFSorter * sorter = [ctrl sortableControllerSorter];
        
        if (sorter.state == BFSorterStateValid) {
            [sorter uncheck];
        } else {
            [sorter check];
        }
    }
}

-(void)viewSortStatusPressedSettings
{
    if ([self.currentController conformsToProtocol:@protocol(BFSortableController)]) {
        id<BFSortableController> ctrl = (id<BFSortableController>)self.currentController;
        
        BFSorter * sorter = [ctrl sortableControllerSorter];
        BFControllerSortingSettings * settings = [BFControllerSortingSettings instantiateToShowFromView:self.sortStatus forSorter:sorter];
        [self.currentTopViewController presentViewController:settings animated:NO completion:nil];
    }
}

-(void)registerSorter:(BFSorter*)sorter
{
    [self.sorters addObject:sorter];
}

-(BOOL)sortOrderAscendingForWidgetGroup:(NSString*)widgetGroup
{
    NSString * storageId = [self storageIdWithObjectId:nil andWidgetGroupId:widgetGroup];
    return [self sortOrderForObjectId:storageId].boolValue;
}

-(void)setSortOrder:(BOOL)ascending
     forWidgetGroup:(NSString*)widgetGroup
{
    NSString * storageId = [self storageIdWithObjectId:nil andWidgetGroupId:widgetGroup];
    [self setSortOrder:ascending forObjectId:storageId];
}

-(NSNumber*)selectedMetricIdxForSortingInWidgetGroup:(NSString*)widgetGroup
{
    NSString * storageId = [self storageIdWithObjectId:nil andWidgetGroupId:widgetGroup];
    return [self selectedMetricIdxForObjectId:storageId];
}

-(void)setSelectedMetricIdx:(NSNumber*)idx forSortingInWidgetGroup:(NSString*)widgetGroup
{
    NSString * storageId = [self storageIdWithObjectId:nil andWidgetGroupId:widgetGroup];
    [self setSelectedMetricIdx:idx forObjectId:storageId];
}

@end
