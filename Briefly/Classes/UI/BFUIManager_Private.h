//
//  BFUIManager_Private.h
//  Pods
//
//  Created by Artem Bondar on 13.12.2017.
//
//

#import "BFUIManager.h"
#import "BFBrieflyManager.h"
#import "BFHeatmapImageView.h"
#import "BFViewSortStatus.h"
#import "BFStatisticsLabelScreen.h"

@interface BFUIManager ()

+(instancetype)instanceWithHost:(BFBrieflyManager*)host;

@property (nonatomic) BFHeatmapsMode heatmapMode;
@property (weak, nonatomic) BFBrieflyManager * host;

@property (nonatomic) BOOL showHeatmapsByUserPreference;
@property (nonatomic) BOOL showUIActionsStatsByUserPreference;
@property (nonatomic) BOOL showWidgetsByUserPreference;
@property (weak, nonatomic) UIWindow * currentWindow;
@property (strong, nonatomic) UIViewController * currentController;
@property (strong, nonatomic) NSString * currentControllerUid;
@property (strong, nonatomic) BFHeatmapImageView * overlayView;
@property (strong, nonatomic) BFStatisticsLabelScreen * overlayStatisticsLabel;
@property (strong, nonatomic) UIButton * overlayButton;

@property (nonatomic) BOOL recording;
@property (strong, nonatomic) NSString * currentOrientation;
@property (strong, nonatomic) NSHashTable<UIView*> * analyticsViews;
@property (strong, nonatomic) NSHashTable<BFSorter*> * sorters;

@property (strong, nonatomic) NSMutableDictionary * localHeatmaps;
@property (strong, nonatomic) NSMutableDictionary * cachcedPresets;

-(void)checkCurrentScreenTimerTick;
-(NSString*)uiEventsTrackingIdForView:(UIView*)view;

// web tracking id takes in account the perScreenTrackingId flag, and
// modify id if needed. All web request should use this id.
-(NSString*)webTrackingIdForRootView:(BOOL)rootView withId:(NSString*)trackingId;

// heatmaps storage:
@property (strong, nonatomic) NSMutableSet * heatmapRequests;

// sorting state:
@property (weak, nonatomic) BFViewSortStatus * sortStatus;

@end

@interface BFUIManager (StorageInternal)

-(void)internalHeatmapForViewWithWebId:(NSString*)viewId
                     withDimensions:(CGSize)dimensions
                            handler:(void(^)(UIImage*))handler
                  checkAliveHandler:(BOOL(^)(void))checkAliveHandler;

-(void)dropHeatmapRequestsQueue;
-(void)storeTapGesture:(CGPoint)pt
        atViewWithWebId:(NSString*)viewWebId
        withDimensions:(CGSize)dimensions
           orientation:(NSString*)orientation
               handler:(void(^)(BOOL))handler;

-(NSNumber*)selectedMetricIdxForObjectId:(NSString*)objectId;
-(void)setSelectedMetricIdx:(NSNumber*)idx forObjectId:(NSString*)widgetGroup;

-(void)setSortOrder:(BOOL)ascending forObjectId:(NSString*)objId;
-(NSNumber*)sortOrderForObjectId:(NSString*)objId;

@end
