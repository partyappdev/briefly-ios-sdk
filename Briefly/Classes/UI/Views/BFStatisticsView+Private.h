//
//  BFStatisticsLabelBase+Private.h
//  Briefly
//
//  Created by Artem Bondar on 20.03.2018.
//

#import "BFStatisticsView.h"

@class BFViewModelPreset;

@interface BFStatisticsView ()

@property (nonatomic) NSArray * values;
@property (nonatomic) NSArray * units;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL isLastRequestFailed;

-(BOOL)shouldShowWidgets;
-(void)configureUI;
-(void)recreateUIElements;
-(void)openDetailsFromView:(UIView*)view withColor:(UIColor*)color enabled:(BOOL)enabled;
-(NSString*)sharedUnit;
-(NSInteger)requestsCount;
-(BFViewModelPreset*)currentPreset;

@end
