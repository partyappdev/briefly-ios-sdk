//
//  BFPurchasesStatisticLabel.m
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "BFPurchasesStatisticLabel.h"
#import "BFBrieflyManager+ApiWrapper.h"
#import "UIView+Tracking.h"

@implementation BFPurchasesStatisticLabel

-(void)loadValueWithHandler:(void(^)(BOOL, NSNumber*, NSString*))handler
{
    [[BFBrieflyManager sharedManager] purchaseStatisticsForItemId:self.itemId period:BFStatisticsPeriodPreviousMonth withHandler:^(BOOL succeed, NSNumber *total) {
        handler(succeed, total, nil);
    }];
}

-(NSString *)analyticsObjectId
{
    return self.itemId;
}

-(void)setItemId:(NSString *)itemId
{
    _itemId = itemId;
    if (self.window != nil) {
        [self bf_onNeedReloadData];
    }
}

@end
