//
//  BFCellWidgetPreset.h
//  Briefly
//
//  Created by Artem Bondar on 14.03.2018.
//

#import <UIKit/UIKit.h>


@protocol BFCellWidgetPresetDelegate

-(void)pressedDeletePresetAtIdx:(NSInteger)idx;

@end

@interface BFCellWidgetPreset : UICollectionViewCell

@property (weak, nonatomic) id <BFCellWidgetPresetDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel * labelText;
@property (weak, nonatomic) IBOutlet UIView * viewContainer;
@property (weak, nonatomic) IBOutlet UIView * viewClosePresser;
@property (nonatomic) NSInteger idx;


-(void)configureWithText:(NSString*)text
                 enabled:(BOOL)enabled
                     idx:(NSInteger)idx
                delegate:(id <BFCellWidgetPresetDelegate>)delegate;

@end
