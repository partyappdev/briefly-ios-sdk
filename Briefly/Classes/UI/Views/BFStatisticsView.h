//
//  BFStatisticLabelBase.h
//  Base64
//
//  Created by Artem Bondar on 20.03.2018.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BFStatisticsView : UIView

@property (strong, nonatomic) IBInspectable NSString * analyticsObjectId;
@property (strong, nonatomic) IBInspectable NSString * analyticsDefaultProperty;
@property (strong, nonatomic) IBInspectable NSString * widgetGroup;

@end
