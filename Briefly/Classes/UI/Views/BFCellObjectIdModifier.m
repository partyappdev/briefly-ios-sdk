//
//  BFCellObjectIdModifier.m
//  Briefly
//
//  Created by Artem Bondar on 14.02.2018.
//

#import "BFCellObjectIdModifier.h"
#import "UIColor+BFPalette.h"

@implementation BFCellObjectIdModifier

- (void)awakeFromNib {
    [super awakeFromNib];
    self.viewContainer.layer.cornerRadius = 8;
}

-(void)configureWithText:(NSString*)text
                selected:(BOOL)selected
{
    if (selected) {
        self.viewContainer.backgroundColor = [UIColor bf_BaseUIColor];
        self.labelText.textColor = [UIColor bf_widgetTextColor];
    } else {
        self.viewContainer.backgroundColor = [UIColor bf_widgetDisabled];
        self.labelText.textColor = [UIColor bf_widgetTextColor];
    }
    self.labelText.text = text;
}

@end
