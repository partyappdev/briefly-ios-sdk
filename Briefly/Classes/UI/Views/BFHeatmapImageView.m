//
//  BFHeatmapImageView.m
//  Pods
//
//  Created by Artem Bondar on 12.12.2017.
//
//

#import "BFHeatmapImageView.h"
#import "BFBrieflyManager.h"
#import "BFBrieflyManager+Resources.h"
#import "BFUIManager+Overlay.h"

@implementation BFHeatmapImageView

-(void)didMoveToWindow
{
    [super didMoveToWindow];
    if (self.window != nil) {
        [self.superview bringSubviewToFront:self];
        [self bf_reloadData];
    } else {
        self.image = nil;
    }
}

-(void)setBf_trackingId:(NSString *)bf_trackingId
{
    _bf_trackingId = bf_trackingId;
    if (self.window) {
        [self bf_reloadData];
    }
}

-(BOOL)isRootHeatmap
{
    return NO;
}

-(void)bf_reloadData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.tag += 1;
        // Need double checking logic here: after dispatch async everything could change:
        // so we should check for self.window to not spawn requests
    if (self.bf_trackingId && self.window) {
        __weak __typeof__(self) weakSelf = self;
        __block NSInteger lastTag = weakSelf.tag;
        if ([self bf_haveSpinnerOnLoading]) {
            self.image = [BFBrieflyManager imageNamed:@"bf_heatmap_loading.png"];
            self.contentMode = UIViewContentModeCenter;
        
            // animate loading
            CABasicAnimation* rotationAnimation;
            rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
            rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0];
            rotationAnimation.duration = 0.7;
            rotationAnimation.cumulative = YES;
            rotationAnimation.repeatCount = HUGE_VALF;
            [self.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
            //self.backgroundColor = [UIColor clearColor];
        } else {
            self.image = nil;
        }
        [[BFBrieflyManager sharedManager].uiManager heatmapForViewWithId:self.bf_trackingId
                                                              isRootView:self.isRootHeatmap
                                                          withDimensions:self.bounds.size handler:^(UIImage *res) {
            if (lastTag == weakSelf.tag) {
                if ([weakSelf bf_haveSpinnerOnLoading]) {
                    [weakSelf.layer removeAnimationForKey:@"rotationAnimation"];
                    //weakSelf.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.07];
                }
                weakSelf.image = res;
                weakSelf.contentMode = UIViewContentModeScaleToFill;
            }
        } checkAliveHandler:^BOOL{
            return weakSelf.window != nil && lastTag == weakSelf.tag;
        }];
    } else {
        //self.backgroundColor = [UIColor clearColor];
        //self.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.15];
        self.image = nil;
    }
    });
}

-(CGRect)alignmentRectForFrame:(CGRect)frame
{
    return self.superview.bounds;
}

-(CGRect)frameForAlignmentRect:(CGRect)alignmentRect
{
    return self.superview.bounds;
}

-(CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize
{
    return self.superview.bounds.size;
}

-(CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority verticalFittingPriority:(UILayoutPriority)verticalFittingPriority
{
    return self.superview.bounds.size;
}

-(CGSize)sizeThatFits:(CGSize)size
{
    return self.superview.bounds.size;
}

-(BOOL)bf_haveSpinnerOnLoading
{
    return NO;
}

@end

@implementation BFHeatmapFullscreenImageView

-(BOOL)bf_haveSpinnerOnLoading
{
    return YES;
}

-(BOOL)isRootHeatmap
{
    return YES;
}

@end
