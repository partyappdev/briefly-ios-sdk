//
//  BFStatisticsLabel.h
//  Pods
//
//  Created by Artem Bondar on 04.12.17.
//
//

#import <UIKit/UIKit.h>
#import "BrieflyFacade.h"
#import "BFStatisticsView.h"

IB_DESIGNABLE
@interface BFStatisticsLabel : BFStatisticsView

@property (strong, nonatomic) IBInspectable UIColor * fillColor;
@property (strong, nonatomic) IBInspectable UIColor * textColor;

+(UIEdgeInsets)internalLabelInsets;
+(UIFont*)internalLabelFont;

@end
