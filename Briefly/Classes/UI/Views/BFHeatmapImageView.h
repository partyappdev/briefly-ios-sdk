//
//  BFHeatmapImageView.h
//  Pods
//
//  Created by Artem Bondar on 12.12.2017.
//
//

#import <UIKit/UIKit.h>

@interface BFHeatmapImageView : UIImageView
@property (strong, nonatomic) NSString * bf_trackingId;

// to reload data without
-(void)bf_reloadData;

@end

@interface BFHeatmapFullscreenImageView : BFHeatmapImageView

@end
