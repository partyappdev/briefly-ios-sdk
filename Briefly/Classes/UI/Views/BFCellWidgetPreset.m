//
//  BFCellWidgetPreset.m
//  Briefly
//
//  Created by Artem Bondar on 14.03.2018.
//

#import "BFCellWidgetPreset.h"
#import "UIColor+BfPalette.h"

@implementation BFCellWidgetPreset

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.viewContainer.clipsToBounds = YES;
    self.viewContainer.layer.cornerRadius = 8;
}

-(void)configureWithText:(NSString*)text
                 enabled:(BOOL)enabled
                     idx:(NSInteger)idx
                delegate:(id <BFCellWidgetPresetDelegate>)delegate
{
    self.delegate = delegate;
    if (enabled) {
        self.viewContainer.backgroundColor = [UIColor bf_widgetsPresetsBase];
        self.viewClosePresser.backgroundColor = [UIColor bf_widgetsPresetsBaseDark];
    } else {
        self.viewContainer.backgroundColor = [UIColor bf_widgetsPresetsDisabled];
        self.viewClosePresser.backgroundColor = [UIColor bf_widgetsPresetsDisabled];
    }
    self.labelText.text = text;
    self.idx = idx;
}

-(IBAction)pressDelete:(id)sender
{
    [self.delegate pressedDeletePresetAtIdx:self.idx];
}

@end
