//
//  BFCellPropertyInfo.h
//  Briefly
//
//  Created by Artem Bondar on 12.01.2018.
//

#import <UIKit/UIKit.h>
#import "BFBarChart.h"
#import "BFViewModelChart.h"
#import "BFViewModelMetricRequest.h"

@protocol BFCellPropertyInfoDelegate
-(void)timePeriodChangedTo:(BFModelTimePeriod*)timePeriod;
-(void)selectedDate:(NSDate*)date;
-(void)selectedDatesField;

@end

@interface BFCellPropertyInfo : UITableViewCell <BFBarChartDelegate>

@property (weak, nonatomic) id<BFCellPropertyInfoDelegate> delegate;

@property (weak, nonatomic) IBOutlet UICollectionView * collectionView;
@property (weak, nonatomic) IBOutlet BFBarChart * barChart;
@property (weak, nonatomic) IBOutlet UIView * loadingView;
@property (weak, nonatomic) IBOutlet UILabel * labelLoadingFailed;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loadingIndicator;

-(void)configureWithChart:(BFViewModelChart*)chart
                 forProperty:(BFViewModelMetricRequest*)property
                     meta:(BFModelObjectMeta*)meta
           datesRange:(NSArray<NSDate*>*)datesRange
              selectedDate:(NSDate*)selectedDate
                 delegate:(id<BFCellPropertyInfoDelegate>)delegate;

@end
