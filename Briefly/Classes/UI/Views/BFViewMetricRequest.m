//
//  BFViewMetricRequest.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFViewMetricRequest.h"
#import "BFBrieflyManager+Resources.h"
#import "UIColor+BfPalette.h"
#import "BFViewModelMetricRequest.h"
#import "BFViewModelObjectIdModifier.h"

@implementation BFViewMetricRequestBase

-(void)configureWithColor:(UIColor*)color textColor:(UIColor*)textColor
{
    self.textColor = textColor;
    self.color = color;
    [self updateColors];
}

-(void)updateColors
{
    if (![self highlighted]) {
        self.viewBoundingBox.backgroundColor = [self.color colorWithAlphaComponent:0.07];
    } else {
        self.viewBoundingBox.backgroundColor = [self.color colorWithAlphaComponent:0.20];
    }
    self.viewBoundingBox.layer.borderColor = self.textColor.CGColor;
    self.viewColorLabel.backgroundColor = self.color;
}

-(void)setHighlighted:(BOOL)highlighted
{
    _highlighted = highlighted;
    [self updateColors];
}

@end

@implementation BFViewMetricRequest

-(void)configureWithMetricRequest:(BFViewModelMetricRequest*)request
                 objectIdModifier:(BFViewModelObjectIdModifier*)objIdModifier
                             meta:(BFModelObjectMeta*)meta
                            value:(NSNumber*)value
                              idx:(NSInteger)idx
{
    UIColor * color = [UIColor bf_widgetColorBarWithIdx:idx];
    UIColor * textColor = [UIColor bf_widgetColorWithIdx:idx];
    if (idx == -1) {
        color = [UIColor bf_widgetDisabled];
        textColor = [UIColor bf_widgetDisabledDarkened];
    }
    BOOL group = ![objIdModifier isKindOfClass:[BFViewModelObjectIdModifierEqual class]];
    NSString * metricName = [request uiNameWithObjectsMeta:meta];
    NSString * metricValue = [request stringForValue:value withObjectsMeta:meta];
    NSString * metricDescription = [request stringDescriptionForMetricWithObjGroupingAggr:group];
    NSString * metricValueDescription = [request stringDescriptionForTimeAggregation];
    
    [self configureWithMetricName:metricName
                metricDescription:metricDescription
                            value:metricValue
                 valueDescription:metricValueDescription
                            color:color
                        textColor:textColor];
}

-(void)configureWithMetricName:(NSString*)metricName
             metricDescription:(NSString*)metricDescription
                         value:(NSString*)value
              valueDescription:(NSString*)valueDescription
                         color:(UIColor*)color
                     textColor:(UIColor*)textColor
{
    [super configureWithColor:color textColor:textColor];
    self.labelMetricName.text = metricName;
    self.labelMetricDescription.text = metricDescription;
    self.labelValue.text = value;
    self.labelValueDescription.text = valueDescription;

}

-(void)updateColors
{
    [super updateColors];
    self.labelMetricName.textColor = self.textColor;
    self.labelMetricDescription.textColor = self.textColor;
    self.labelValue.textColor = self.textColor;
    self.labelValueDescription.textColor = self.textColor;
}

+(BFViewMetricRequest*)instantiate
{
    return (BFViewMetricRequest*)[[BFBrieflyManager locateBundle] loadNibNamed:@"BFViewMetricRequest" owner:nil options:nil][0];
}

@end


@implementation BFViewMetricRequestManageActionButton

-(void)configureWithActionText:(NSString*)actionText
                         color:(UIColor*)color
{
    [super configureWithColor:color textColor:color];
    self.labelDescription.text = actionText;
}

-(void)updateColors
{
    [super updateColors];
    self.labelDescription.textColor = self.color;
}

+(BFViewMetricRequestManageActionButton*)instantiate
{
    return (BFViewMetricRequestManageActionButton*)[[BFBrieflyManager locateBundle] loadNibNamed:@"BFViewMetricRequest" owner:nil options:nil][1];
}

@end
