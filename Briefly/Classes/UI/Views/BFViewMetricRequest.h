//
//  BFViewMetricRequest.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import <UIKit/UIKit.h>

@class BFViewModelMetricRequest;
@class BFModelObjectMeta;
@class BFViewModelObjectIdModifier;

@interface BFViewMetricRequestBase : UIView

@property (weak, nonatomic) IBOutlet UIView * viewBoundingBox;
@property (weak, nonatomic) IBOutlet UIView * viewColorLabel;

@property (strong, nonatomic) UIColor * color;
@property (strong, nonatomic) UIColor * textColor;
@property (nonatomic) BOOL highlighted;

@end


@interface BFViewMetricRequest : BFViewMetricRequestBase

@property (weak, nonatomic) IBOutlet UILabel * labelMetricName;
@property (weak, nonatomic) IBOutlet UILabel * labelMetricDescription;

@property (weak, nonatomic) IBOutlet UILabel * labelValue;
@property (weak, nonatomic) IBOutlet UILabel * labelValueDescription;

-(void)configureWithMetricRequest:(BFViewModelMetricRequest*)request
                 objectIdModifier:(BFViewModelObjectIdModifier*)objIdModifier
                             meta:(BFModelObjectMeta*)meta
                            value:(NSNumber*)value
                              idx:(NSInteger)idx;

-(void)configureWithMetricName:(NSString*)metricName
             metricDescription:(NSString*)metricDescription
                         value:(NSString*)value
              valueDescription:(NSString*)valueDescription
                         color:(UIColor*)color
                     textColor:(UIColor*)textColor;

+(BFViewMetricRequest*)instantiate;

@end


@interface BFViewMetricRequestManageActionButton : BFViewMetricRequestBase

@property (weak, nonatomic) IBOutlet UILabel * labelDescription;

-(void)configureWithActionText:(NSString*)actionText
                         color:(UIColor*)color;

+(BFViewMetricRequestManageActionButton*)instantiate;

@end
