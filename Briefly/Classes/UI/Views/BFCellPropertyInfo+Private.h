//
//  BFCellPropertyInfo+Private.h
//  Briefly
//
//  Created by Artem Bondar on 30/01/2018.
//

#import "BFCellPropertyInfo.h"
#import "BFPublicModels.h"

@interface BFCellPropertyInfo ()

@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL lastLoadingFailed;

@property (strong, nonatomic) BFViewModelChart * displayedChart;
@property (strong, nonatomic) BFViewModelMetricRequest * displayedMetric;
@property (strong, nonatomic) BFModelObjectMeta * displayedMeta;
@property (strong, nonatomic) NSArray<NSDate*>* displayedDates;
@property (strong, nonatomic) NSDate * displayedSelectedDate;

@end
