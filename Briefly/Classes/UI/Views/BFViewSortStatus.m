//
//  BFViewSortStatus.m
//  Briefly
//
//  Created by Artem Bondar on 26.03.2018.
//

#import "BFViewSortStatus.h"
#import "BFBrieflyManager+Resources.h"
#import "NSString+Utils.h"
#import "UIColor+BfPalette.h"
#import "BFSorter+Private.h"

@interface BFViewSortStatus ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView * imageViewStatus;
@property (weak, nonatomic) IBOutlet UILabel * statusLabel;
@property (weak, nonatomic) IBOutlet UIView * viewStatusPane;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer * recognizer;
@property (nonatomic) BOOL canOpenSettings;
@end

@implementation BFViewSortStatus

-(instancetype)init
{
    self = [super init];
    [self configureUI];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self configureUI];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self configureUI];
    return self;
}

-(void)configureUI
{
    self.layer.cornerRadius = 4;
    if (self.statusLabel == nil) {
        CGRect rect = CGRectMake(self.bounds.size.height, 0, self.bounds.size.width - self.bounds.size.height, self.bounds.size.height);
        UILabel * newLabel = [[UILabel alloc] initWithFrame:rect];
        newLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        newLabel.textColor = [UIColor whiteColor];
        newLabel.font = [UIFont boldSystemFontOfSize:10];
        newLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:newLabel];
        self.statusLabel = newLabel;
    }
    
    if (self.imageViewStatus == nil) {
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:UIEdgeInsetsInsetRect(CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.height), UIEdgeInsetsMake(6, 6, 6, 6))];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        imageView.tintColor = [UIColor whiteColor];
        imageView.contentMode = UIViewContentModeCenter;
        [self addSubview:imageView];
        self.imageViewStatus = imageView;
    }
    
    if (self.activityIndicator == nil) {
        UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.height)];
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        activityIndicator.color = [UIColor whiteColor];
        activityIndicator.hidden = YES;
        activityIndicator.hidesWhenStopped = YES;
        [self addSubview:activityIndicator];
        self.activityIndicator = activityIndicator;
    }
}

-(void)sorterStateChanged:(BFSorter*)sorter
    haveSortingController:(BOOL)haveSortingController
                ascending:(BOOL)ascending
{
    if (!haveSortingController) {
        self.hidden = YES;
        return;
    }
    self.hidden = NO;
    switch (sorter.state) {
        case BFSorterStateUnchecked:
            self.viewStatusPane.backgroundColor = [UIColor bfDarkGray];
            self.statusLabel.text = @"BfSorterState.Unchecked".bf_localized;
            self.imageViewStatus.image = [[BFBrieflyManager imageNamed:@"bf_sort_none.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [self.activityIndicator stopAnimating];
            self.canOpenSettings = NO;
            break;
        case BFSorterStateInvalid:
            self.viewStatusPane.backgroundColor = [UIColor bfRed];
            self.statusLabel.text = @"BfSorterState.Invalid".bf_localized;
            self.imageViewStatus.image = [[BFBrieflyManager imageNamed:@"bf_sort_none.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [self.activityIndicator stopAnimating];
            self.canOpenSettings = NO;
            break;
        case BFSorterStateValid: {
            BOOL stillAnimating = NO;
            BOOL failedMetaLoading = NO;
            switch (sorter.metaState) {
                case BFSorterMetaStateNone:
                    self.statusLabel.text = @"BfSorterState.Valid.MetaNone".bf_localized;
                    break;
                case BFSorterMetaStateFailed:
                    self.statusLabel.text = @"BfSorterState.Valid.MetaFailed".bf_localized;
                    failedMetaLoading = YES;
                    break;
                case BFSorterMetaStateLoading:
                    stillAnimating = YES;
                    self.statusLabel.text = @"BfSorterState.Valid.MetaLoading".bf_localized;
                    break;
                case BFSorterMetaStateLoaded: {
                    NSArray * metricNames = [sorter humanReadableMetricNames];
                    if (metricNames && [sorter currentMetricIdx] < metricNames.count) {
                        NSString * str = [metricNames objectAtIndex:[sorter currentMetricIdx]];
                        self.statusLabel.text = str;
                    } else {
                        self.statusLabel.text = @"BfSorterState.Valid.NoMeta".bf_localized;
                    }
                    break;
                }
                    
            }
            self.viewStatusPane.backgroundColor = failedMetaLoading ? [UIColor bfOrange] : [UIColor bfGreen];

            if (stillAnimating) {
                [self.activityIndicator startAnimating];
            } else {
                if (ascending) {
                    self.imageViewStatus.image = [[BFBrieflyManager imageNamed:@"bf_sort_asc.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                } else {
                    self.imageViewStatus.image = [[BFBrieflyManager imageNamed:@"bf_sort_desc.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                }
                [self.activityIndicator stopAnimating];
            }
            self.canOpenSettings = YES;
            break;
        }
        case BFSorterStateUpdating:
            self.viewStatusPane.backgroundColor = [UIColor bfOrange];
            self.statusLabel.text = @"BfSorterState.Updating".bf_localized;
            self.imageViewStatus.image = nil;
            [self.activityIndicator startAnimating];
            self.canOpenSettings = NO;
            break;
    }
}

-(CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake([self.statusLabel sizeThatFits:size].width + 50, size.height);
}

-(IBAction)pressed:(id)sender
{
    [self.delegate viewSortStatusPressed];
}

-(IBAction)pressedSettings:(id)sender
{
    if (self.canOpenSettings) {
        [self.delegate viewSortStatusPressedSettings];
    } else {
        [self.delegate viewSortStatusPressed];
    }
}

@end
