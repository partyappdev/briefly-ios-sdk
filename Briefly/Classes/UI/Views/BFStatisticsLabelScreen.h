//
//  BFStatisticsLabelScreen.h
//  Briefly
//
//  Created by Artem Bondar on 20.03.2018.
//

#import <Briefly/Briefly.h>
#import "BFStatisticsView.h"

@interface BFStatisticsLabelScreen : BFStatisticsView

-(void)addToView:(UIView*)view
        topInset:(CGFloat)topInset
       leftInset:(CGFloat)leftInset;

@end

@interface BFStatisticsLabelScreenFull : BFStatisticsLabelScreen

@end
