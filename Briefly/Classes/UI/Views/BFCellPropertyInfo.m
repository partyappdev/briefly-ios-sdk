//
//  BFCellPropertyInfo.m
//  Briefly
//
//  Created by Artem Bondar on 12.01.2018.
//

#import "BFCellPropertyInfo.h"
#import "BFCellPropertyInfo+Private.h"
#import "BFBrieflyManager+Resources.h"
#import "BFFSCalendar.h"
#import "NSString+Utils.h"

#import "BFViewModelMetricRequestMulti.h"
#import "BFViewModelMetricRequestSingle.h"

@implementation BFCellPropertyInfo

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)configureWithChart:(BFViewModelChart*)chart
              forProperty:(BFViewModelMetricRequest*)property
                     meta:(BFModelObjectMeta*)meta
               datesRange:(NSArray<NSDate*>*)datesRange
              selectedDate:(NSDate*)selectedDate
                 delegate:(id<BFCellPropertyInfoDelegate>)delegate
{
    self.delegate = delegate;

    // Store displayed items for minor updates handling
    self.displayedChart = chart;
    self.displayedMetric = property;
    self.displayedMeta = meta;
    self.displayedDates = datesRange;
    self.displayedSelectedDate = selectedDate;

    // Configure chart
    [self configureChart];
}

-(void)configureChart
{
    // if loading - just keep it the same
    if (self.isLoading) {
        return;
    }
    [self.barChart setDelegate:self];
    [self.barChart setSelectedDate:self.displayedSelectedDate];
    [self.barChart setModel:self.displayedChart];
}

//////////////////////////////////////////////////////////////////
//
// Chart delegate
//
//////////////////////////////////////////////////////////////////

-(void)selectedDate:(NSDate*)date
{
    self.displayedSelectedDate = date;
    [self.delegate selectedDate:date];
}

-(void)selectedDatesField
{
    [self.delegate selectedDatesField];
}

-(void)loadingStateChangedTo:(BOOL)loading lastFailed:(BOOL)failed
{
    if (self.displayedMetric == nil) {
        self.labelLoadingFailed.text = @"BfNotSelectedLong".bf_localized;
        self.loadingIndicator.hidden = YES;
        self.loadingView.hidden = YES;
        [self.loadingIndicator stopAnimating];
    } else {
        if (loading) {
            self.labelLoadingFailed.text = nil;
            self.loadingIndicator.hidden = NO;
            self.loadingView.hidden = NO;
            [self.loadingIndicator startAnimating];
        } else {
            self.labelLoadingFailed.text = nil;
            self.loadingIndicator.hidden = YES;
            self.loadingView.hidden = YES;
            [self.loadingIndicator stopAnimating];
        }
        if (failed && !loading) {
            self.labelLoadingFailed.hidden = NO;
            self.labelLoadingFailed.text = @"BfLoadFailedLong".bf_localized;
        } else {
            self.labelLoadingFailed.hidden = YES;
            self.labelLoadingFailed.text = nil;
        }
    }
}

-(void)stadyRangeChangedFor:(BFRange)range
{
    [self.delegate timePeriodChangedTo:[self.displayedChart timePeriodForRange:range]];
}

@end
