//
//  BFStatisticLabelBase.m
//  Base64
//
//  Created by Artem Bondar on 20.03.2018.
//

#import "BFStatisticsView.h"
#import "BFStatisticsView+Private.h"
#import "UIView+Visualization.h"
#import "UIView+Tracking.h"

#import "BFBrieflyManager.h"
#import "BFBrieflyManager+ApiWrapper.h"
#import "BFUIManager+WidgetsManager.h"
#import "BFUIManager+Overlay.h"
#import "BFUIManager+Storage.h"

#import "BFControllerWidgetExpanded.h"

#import "BFViewModelMetricRequest.h"
#import "BFViewModelMetricRequestMulti.h"
#import "BFViewModelObjectIdModifier.h"
#import "BFViewModelPreset.h"

#import "UIColor+BfPalette.h"
#import "NSString+Utils.h"

@interface BFStatisticsView ()

@end

@implementation BFStatisticsView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self recreateUIElements];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self recreateUIElements];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self recreateUIElements];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self recreateUIElements];
}

-(void)setAnalyticsObjectId:(NSString *)analyticsObjectId
{
    _analyticsObjectId = analyticsObjectId;
    if (self.window != nil && [self shouldShowWidgets]) {
        [self bf_onNeedReloadData];
    }
}

-(void)setAnalyticsDefaultProperty:(NSString *)analyticsDefaultProperty
{
    _analyticsDefaultProperty = analyticsDefaultProperty;
    if (self.window != nil && [self shouldShowWidgets]) {
        [self bf_onNeedReloadData];
    }
}

-(void)setWidgetGroup:(NSString *)widgetGroup
{
    _widgetGroup = widgetGroup;
    if (self.window != nil && [self shouldShowWidgets]) {
        [self bf_onNeedReloadData];
    }
}

-(void)setValues:(NSArray*)values
{
    _values = values;
    [self configureUI];
}

-(void)setIsLoading:(BOOL)isLoading
{
    _isLoading = isLoading;
    [self configureUI];
}

-(void)setIsLastRequestFailed:(BOOL)isLastRequestFailed
{
    _isLastRequestFailed = isLastRequestFailed;
    [self configureUI];
}

-(BOOL)shouldShowWidgets
{
    return [[BFBrieflyManager sharedManager].uiManager shouldShowWidgetsForObjectId:self.analyticsObjectId widgetGroup:self.widgetGroup];
}

-(void)configureUI
{
    // should be implemented by childs
}

-(void)recreateUIElements
{
    // should be implemented by childs
}

-(void)didMoveToWindow
{
    [super didMoveToWindow];
    if (self.window != nil) {
        if ([self shouldShowWidgets]) {
            [self bf_onNeedReloadData];
        }
        [[BFBrieflyManager sharedManager].uiManager subscribeViewForChanges:self];
    } else {
        [[BFBrieflyManager sharedManager].uiManager unsubscribeViewFromChanges:self];
    }
    [self configureUI];
}

-(void)bf_onChangeWidgetsOnState
{
    [self configureUI];
}

-(BFViewModelPreset*)currentPreset
{
    return [[BFBrieflyManager sharedManager].uiManager currentPresetForObjectId:self.analyticsObjectId widgetGroupId:self.widgetGroup andDefaultRequestString:self.analyticsDefaultProperty];
}

-(void)bf_onNeedReloadData
{
    if (self.analyticsObjectId == nil || self.currentPreset.emptyPreset) {
        return;
    }
    
    __weak __typeof__(self) weakSelf = self;
    __block BFViewModelPreset * preset = self.currentPreset;
    __block NSString * objId = self.analyticsObjectId;
    self.isLoading = YES;
    self.units = nil;
    self.values = nil;
    [self loadValuesWithHandler:^(BOOL succeed, NSArray *values, NSArray* units) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // since start someone could change the itemId
            // so we should be ready to handle an outdated call
            BOOL samePreset = [preset isEqual:weakSelf.currentPreset];
            BOOL sameObjectId = [objId isEqualToString:weakSelf.analyticsObjectId];
            if (sameObjectId && samePreset) {
                weakSelf.isLoading = NO;
                weakSelf.isLastRequestFailed = !succeed;
                if (values != nil) {
                    weakSelf.units = units;
                    weakSelf.values = values;
                }
            }
        });
    }];
}

-(NSString *)trackingId
{
    // We don't track touches for the widgets
    return nil;
}

-(void)loadValuesWithHandler:(void(^)(BOOL, NSArray<NSNumber*>*, NSArray<NSString*>*))handler
{
    BFViewModelPreset * preset = self.currentPreset;
    NSArray * requests = [preset apiMetricRequestsWithObjectId:self.analyticsObjectId];
    if (requests == nil || requests.count == 0) {
        handler(YES, nil, nil);
        return;
    }

    // Now execute the request
    [[BFBrieflyManager sharedManager] calculateMetricRequests:requests withPriority:BFBrieflyApiPriorityNormal withHandler:^(BOOL succeed, NSArray<BFModelCalculateMetricResponseItem *> * results) {
        
        if (succeed && results.firstObject != nil) {
            NSMutableArray * values = [NSMutableArray array];
            NSMutableArray * units = [NSMutableArray array];
            for (BFModelCalculateMetricResponseItem * item in results) {
                
                if (item.value == nil || [NSString bf_humanReadableUnit:item.unit] == nil) {
                    handler(NO, nil, nil);
                    return;
                }
                [units addObject:[NSString bf_humanReadableUnit:item.unit]];
                [values addObject:item.value];
            }
            handler(YES, values, units);
        } else {
            handler(NO, nil, nil);
        }
    }];
}

-(NSString*)sharedUnit
{
    NSString * sharedUnit = self.units.firstObject;
    for (NSString * unit in self.units) {
        if (![sharedUnit isEqualToString:unit]) {
            return nil;
        }
    }
    return sharedUnit;
}

-(NSInteger)requestsCount
{
    BFViewModelMetricRequest * req = self.currentPreset.metricRequest;
    if ([req isKindOfClass:[BFViewModelMetricRequestMulti class]]) {
        return [(BFViewModelMetricRequestMulti*)req childMetricRequests].count;
    }
    return 1;
}

-(void)openDetailsFromView:(UIView*)view withColor:(UIColor*)color enabled:(BOOL)enabled
{
    UIViewController * ctrl = [BFControllerWidgetExpanded instantiateToShowFromView:view startColor:color startTextColor:color startWidgetText:nil forObjectId:self.analyticsObjectId widgetGroup:self.widgetGroup defaultAttribute:self.analyticsDefaultProperty enabled:enabled];
    [[[[BFBrieflyManager sharedManager] uiManager] currentTopViewController] presentViewController:ctrl animated:NO completion:nil];
}

@end
