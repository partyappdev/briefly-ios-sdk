//
//  BFStatisticsLabelScreen.m
//  Briefly
//
//  Created by Artem Bondar on 20.03.2018.
//

#import "BFStatisticsLabelScreen.h"
#import "BFStatisticsView+Private.h"
#import "BFBrieflyManager+Resources.h"

#import "UIView+Visualization.h"
#import "UIView+Tracking.h"

#import "BFControllerWidgetExpanded.h"

#import "BFViewModelMetricRequest.h"
#import "BFViewModelMetricRequestMulti.h"
#import "BFViewModelObjectIdModifier.h"
#import "BFViewModelPreset.h"

#import "UIColor+BfPalette.h"
#import "NSString+Utils.h"


@interface BFStatisticsLabelScreen ()

@property (weak, nonatomic) UITapGestureRecognizer* recognizer;

@property (strong, nonatomic) NSArray* images;
@property (strong, nonatomic) NSMutableArray* textLabels;
@property (strong, nonatomic) NSMutableArray* imageViews;
@property (weak, nonatomic) UIView* backgroundView;
@property (weak, nonatomic) UIActivityIndicatorView * activityIndicator;
@property (nonatomic) BOOL needRecreateLabels;
@property (nonatomic) BOOL showTime;

@end

@implementation BFStatisticsLabelScreen

-(NSString *)analyticsDefaultProperty
{
    NSString * reqViews = @"obj.group(sum).show.group_by(day).sum().sum(week)";
    NSString * reqTouches = @"obj.group(sum).touch.group_by(day).sum().sum(week)";
    NSString * reqTime = @"obj.group(sum).time_spent.group_by(day).avg().avg(week)";
    if (self.showTime) {
        return [NSString stringWithFormat:@"%@,%@,%@", reqViews, reqTouches, reqTime];
    } else {
        return [NSString stringWithFormat:@"%@,%@", reqViews, reqTouches];
    }
}

-(UIColor*)widgetColor
{
    return [UIColor whiteColor];
}

-(NSString *)widgetGroup
{
    if (self.showTime) {
        return @"bf_screen_timed";
    } else {
        return @"bf_screen";
    }
}

-(void)setValues:(NSArray*)values
{
    if (self.values.count != values.count) {
        self.needRecreateLabels = YES;
    }
    // Setup image views
    NSMutableArray * images = [NSMutableArray array];
    
    [images addObject:[[BFBrieflyManager imageNamed:@"icon-views"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    [images addObject:[[BFBrieflyManager imageNamed:@"icon-tap"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    [images addObject:[[BFBrieflyManager imageNamed:@"icon-time"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    /*if (values.count > 0) {
        [images addObject:[BFBrieflyManager imageNamed:@"icon-views"]];
    }
    if (values.count > 1) {
        [images addObject:[BFBrieflyManager imageNamed:@"icon-tap"]];
    }
    if (values.count > 2) {
        [images addObject:[BFBrieflyManager imageNamed:@"icon-time"]];
    }*/
    self.images = images;
    [super setValues:values];
}

-(void)forEveryTextLabelDo:(void(^)(UILabel*))block
{
    [self.textLabels enumerateObjectsUsingBlock:^(NSValue *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        block((UILabel*)obj.nonretainedObjectValue);
    }];
}

-(void)forEveryImageViewDo:(void(^)(UIImageView*))block
{
    [self.imageViews enumerateObjectsUsingBlock:^(NSValue *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        block((UIImageView*)obj.nonretainedObjectValue);
    }];
}

-(void)forLabelAtIdx:(NSInteger)idx do:(void(^)(UILabel*))block
{
    if (idx >= self.textLabels.count) {
        return;
    }
    if (idx < 0) {
        return;
    }
    NSValue * val = self.textLabels[idx];
    block((UILabel*)val.nonretainedObjectValue);
}

-(void)forImageViewAtIdx:(NSInteger)idx do:(void(^)(UIImageView*))block
{
    if (idx >= self.imageViews.count) {
        return;
    }
    if (idx < 0) {
        return;
    }
    NSValue * val = self.imageViews[idx];
    block((UIImageView*)val.nonretainedObjectValue);
}

-(void)configureUI
{
    self.hidden = ![self shouldShowWidgets];
    [self recreateUIElements];
    if (self.isLoading) {
        [self.activityIndicator startAnimating];
        [self forEveryTextLabelDo:^(UILabel * label) {
            label.text = nil;
        }];
        [self forEveryImageViewDo:^(UIImageView * imageView) {
            imageView.image = nil;
        }];
    } else if (self.isLastRequestFailed) {
        [self.activityIndicator stopAnimating];
        [self forEveryTextLabelDo:^(UILabel * label) {
            label.text = nil;
            label.alpha = 1.0;
        }];
        [self forEveryImageViewDo:^(UIImageView * imageView) {
            imageView.image = nil;
        }];
        [self forLabelAtIdx:0 do:^(UILabel *lbl) {
            lbl.alpha = 1.0;
            [lbl setText:@"BfLoadFailedShotr".bf_localized];
        }];
        
    } else {
        [self.activityIndicator stopAnimating];
        if ([self.currentPreset emptyPreset]) {
            [self forEveryTextLabelDo:^(UILabel * label) {
                label.alpha = 1.0;
                label.text = nil;
            }];
            [self forEveryImageViewDo:^(UIImageView * imageView) {
                imageView.image = nil;
            }];
            [self forLabelAtIdx:0 do:^(UILabel * label) {
                label.alpha = 1.0;
                label.text = @"BfNotSelected".bf_localized;
            }];
        } else if (self.values == nil) {
            [self forEveryTextLabelDo:^(UILabel * label) {
                label.alpha = 1.0;
                label.text = nil;
            }];
            [self forEveryImageViewDo:^(UIImageView * imageView) {
                imageView.image = nil;
            }];
            [self forLabelAtIdx:0 do:^(UILabel * label) {
                label.alpha = 1.0;
                label.text = @"-";
            }];
        } else {
            [self.values enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self forLabelAtIdx:idx do:^(UILabel * label) {
                    label.alpha = 1.0;
                    label.text = [NSString bf_humanReadableStringFromNumber:obj unit:idx == 2 ? @"sec" : @""];
                }];
            }];
            [self.images enumerateObjectsUsingBlock:^(id  _Nonnull image, NSUInteger idx, BOOL * _Nonnull stop) {
                [self forImageViewAtIdx:idx do:^(UIImageView *imgView) {
                    imgView.image = image;
                    imgView.alpha = 1.0;
                }];
            }];
        }
    }
    [self setNeedsLayout];
}

-(UIColor*)textColor
{
    return [[UIColor blackColor] colorWithAlphaComponent:0.8];
}

-(void)recreateUIElements
{

    self.backgroundColor = [UIColor clearColor];
    
    // It doesn't have animation, so rasterization can make performance better
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    
    // install a label inside
    if (self.textLabels == nil || self.needRecreateLabels) {
        
        // remove previously created views (if have any)
        self.needRecreateLabels = NO;
        [self.backgroundView removeFromSuperview];
        [self forEveryTextLabelDo:^(UILabel * label) {
            [label removeFromSuperview];
        }];
        [self forEveryImageViewDo:^(UIImageView *imgView) {
            [imgView removeFromSuperview];
        }];
        
        // create a new ones
        self.textLabels = [NSMutableArray array];
        self.imageViews = [NSMutableArray array];
        NSInteger sections = [self requestsCount];
        
        NSLayoutXAxisAnchor * previousXancor = self.leadingAnchor;
        for (NSInteger i = 0; i < sections; ++i) {
            CGFloat leftOffset = 2;

            //
            // Create image view
            //
            UIImageView * imageView = [[UIImageView alloc] init];
            imageView.userInteractionEnabled = NO;
            imageView.translatesAutoresizingMaskIntoConstraints = NO;
            imageView.contentMode = UIViewContentModeCenter;
            imageView.tintColor = self.textColor;
            [self insertSubview:imageView atIndex:0];
            //CGFloat rightOffset = 4;
            [NSLayoutConstraint activateConstraints:@[
                                                      [imageView.leadingAnchor constraintEqualToAnchor:previousXancor constant:leftOffset],
                                                      
                                                      [imageView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:0],
                                                      
                                                      [imageView.topAnchor constraintEqualToAnchor:self.topAnchor constant:0],
                                                      
                                                      [imageView.widthAnchor constraintEqualToConstant:24]
                                                      
                                                      ]];
            
            previousXancor = imageView.trailingAnchor;
            [self.imageViews addObject:[NSValue valueWithNonretainedObject:imageView]];
            
            //
            // Create a label
            //
            UILabel * label = [[UILabel alloc] initWithFrame:self.bounds];
            label.userInteractionEnabled = NO;
            label.translatesAutoresizingMaskIntoConstraints = NO;
            label.textColor = self.textColor;
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [BFStatisticsLabel internalLabelFont];
            label.minimumScaleFactor = 0.5;
            [label setContentHuggingPriority:600 + i forAxis:UILayoutConstraintAxisHorizontal];
            [label setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
            [self addSubview:label];
            [NSLayoutConstraint activateConstraints:@[
                                                      [label.leadingAnchor constraintEqualToAnchor:previousXancor constant:leftOffset],
                                                      
                                                      [label.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:[BFStatisticsLabel internalLabelInsets].bottom],
                                                      
                                                      [label.topAnchor constraintEqualToAnchor:self.topAnchor constant:[BFStatisticsLabel internalLabelInsets].top]
                                                      
                                                      ]];
            previousXancor = label.trailingAnchor;
            if (i == sections - 1) {
                NSLayoutConstraint * lastConstraint = [label.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-4];
                lastConstraint.priority = 750;
                [NSLayoutConstraint activateConstraints:@[lastConstraint]];
            }
            [self.textLabels addObject:[NSValue valueWithNonretainedObject:label]];
        }
        
        //
        // install a transparent background
        //
        UIView * backgroundView = [[UIView alloc] init];
        backgroundView.layer.cornerRadius = [self cornerRadius];
        backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        backgroundView.backgroundColor = [[self widgetColor] colorWithAlphaComponent:0.8];
        [self insertSubview:backgroundView atIndex:0];
        
        NSLayoutXAxisAnchor * leading = self.leadingAnchor;
        NSLayoutXAxisAnchor * trailing = self.trailingAnchor;
        if (previousXancor) {
            trailing = previousXancor;
        }
        [NSLayoutConstraint activateConstraints:@[
                                                  [backgroundView.leadingAnchor constraintEqualToAnchor:leading constant:0],
                                                  
                                                  [backgroundView.trailingAnchor constraintEqualToAnchor:trailing constant:[BFStatisticsLabel internalLabelInsets].right],
                                                  
                                                  [backgroundView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:0],
                                                                                            [backgroundView.topAnchor constraintEqualToAnchor:self.topAnchor constant:0]
                                                  
                                                  ]];
        self.backgroundView = backgroundView;
        UITapGestureRecognizer * recogn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressed)];
        [self.backgroundView addGestureRecognizer:recogn];
        self.recognizer = recogn;
    }
    
    // install an activity indicator inside
    if (self.activityIndicator == nil) {
        UIActivityIndicatorView * activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activity.translatesAutoresizingMaskIntoConstraints = NO;
        activity.color = [UIColor blackColor];
        activity.hidesWhenStopped = YES;
        [self addSubview:activity];
        [NSLayoutConstraint activateConstraints:@[
                                                  [activity.centerXAnchor constraintEqualToAnchor:self.centerXAnchor constant:0],
                                                  
                                                  [activity.centerYAnchor constraintEqualToAnchor:self.centerYAnchor constant:0],
                                                  
                                                  [activity.widthAnchor constraintEqualToConstant:24],
                                                  
                                                  [activity.heightAnchor constraintEqualToConstant:24]
                                                  
                                                  ]];
        self.activityIndicator = activity;
    }
}

-(void)pressed
{
    [self openDetailsFromView:self.backgroundView withColor:[self widgetColor] enabled:NO];
}

+(UIEdgeInsets)internalLabelInsets
{
    return UIEdgeInsetsMake(0, 8, 0, 8);
}

+(UIFont*)internalLabelFont
{
    return [UIFont boldSystemFontOfSize:12];
}

-(CGFloat)cornerRadius
{
    return 0;
}

-(void)addToView:(UIView*)view
        topInset:(CGFloat)topInset
       leftInset:(CGFloat)leftInset
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:self];
    [view addConstraints:@[
                           [self.leadingAnchor constraintEqualToAnchor:view.leadingAnchor constant:leftInset],
                           [self.topAnchor constraintEqualToAnchor:view.topAnchor constant:topInset],
                           [self.heightAnchor constraintEqualToConstant:32],
                           ]];
}

-(BOOL)showTime
{
    return YES;
}

@end

@implementation BFStatisticsLabelScreenFull

-(CGFloat)cornerRadius
{
    return 4;
}

-(BOOL)showTime
{
    return YES;
}

-(void)pressed
{
    [super pressed];
    [UIView animateWithDuration:0.4 animations:^{
        self.alpha = 0;
    }];
}

@end
