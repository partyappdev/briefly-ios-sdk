//
//  BFStatisticsLabel.m
//  Pods
//
//  Created by Artem Bondar on 04.12.17.
//
//

#import "BFStatisticsLabel.h"
#import "BFStatisticsView+Private.h"
#import "UIView+Visualization.h"
#import "UIView+Tracking.h"

#import "BFBrieflyManager.h"
#import "BFBrieflyManager+ApiWrapper.h"
#import "BFUIManager+WidgetsManager.h"
#import "BFUIManager+Overlay.h"
#import "BFUIManager+Storage.h"

#import "BFControllerWidgetExpanded.h"

#import "BFViewModelMetricRequest.h"
#import "BFViewModelMetricRequestMulti.h"
#import "BFViewModelObjectIdModifier.h"
#import "BFViewModelPreset.h"

#import "UIColor+BfPalette.h"
#import "NSString+Utils.h"

@interface BFStatisticsLabel ()

@property (weak, nonatomic) UITapGestureRecognizer* recognizer;

@property (strong, nonatomic) NSMutableArray* textLabels;
@property (strong, nonatomic) NSMutableArray* backgrounds;
@property (weak, nonatomic) UIActivityIndicatorView * activityIndicator;
@property (nonatomic) BOOL needRecreateLabels;

@end

@implementation BFStatisticsLabel

-(void)setFillColor:(UIColor *)fillColor
{
    _fillColor = fillColor;
}

-(void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    //self.textLabel.textColor = textColor;
    self.activityIndicator.color = textColor;
}

-(void)setValues:(NSArray*)values
{
    if (self.values.count != values.count) {
        self.needRecreateLabels = YES;
    }
    [super setValues:values];
}

-(void)forEveryTextLabelDo:(void(^)(UILabel*))block
{
    [self.textLabels enumerateObjectsUsingBlock:^(NSValue *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        block((UILabel*)obj.nonretainedObjectValue);
    }];
}

-(void)forEveryTextLabelBgDo:(void(^)(UIView*))block
{
    [self.backgrounds enumerateObjectsUsingBlock:^(NSValue *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        block((UIView*)obj.nonretainedObjectValue);
    }];
}

-(void)forLabelAtIdx:(NSInteger)idx do:(void(^)(UILabel*))block
{
    if (idx >= self.textLabels.count) {
        return;
    }
    if (idx < 0) {
        return;
    }
    NSValue * val = self.textLabels[idx];
    block((UILabel*)val.nonretainedObjectValue);
}

-(void)configureUI
{
    self.hidden = ![self shouldShowWidgets];
    [self recreateUIElements];
    if (self.isLoading) {
        [self.activityIndicator startAnimating];
        [self forEveryTextLabelDo:^(UILabel * label) {
            label.text = nil;
        }];
    } else if (self.isLastRequestFailed) {
        [self.activityIndicator stopAnimating];
        [self forEveryTextLabelDo:^(UILabel * label) {
            label.text = nil;
        }];
        [self forLabelAtIdx:0 do:^(UILabel *lbl) {
            [lbl setText:@"BfLoadFailedShotr".bf_localized];
        }];
        
    } else {
        [self.activityIndicator stopAnimating];
        if ([self.currentPreset emptyPreset]) {
            [self forEveryTextLabelDo:^(UILabel * label) {
                label.text = nil;
            }];
            [self forLabelAtIdx:0 do:^(UILabel * label) {
                label.text = @"BfNotSelected".bf_localized;
            }];
        } else if (self.values == nil) {
            [self forEveryTextLabelDo:^(UILabel * label) {
                label.text = nil;
            }];
            [self forLabelAtIdx:0 do:^(UILabel * label) {
                label.text = @"-";
            }];
        } else {
            NSString * sharedUnit = [self sharedUnit];
            [self.values enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString* sharedUnit = self.sharedUnit;
                [self forLabelAtIdx:idx do:^(UILabel * label) {
                    if (sharedUnit != nil) {
                        label.text = [NSString bf_humanReadableStringFromNumber:obj];
                    } else {
                        label.text = [NSString bf_humanReadableStringFromNumber:obj unit:self.units[idx]];
                    }
                }];
            }];
            if (sharedUnit != nil) {
                [self forLabelAtIdx:self.textLabels.count-1 do:^(UILabel *lbl) {
                    lbl.text = sharedUnit;
                }];
            }
        }
    }
    [self setNeedsLayout];
}

-(void)recreateUIElements
{
    self.backgroundColor = UIColor.clearColor;
    
    // It doesn't have animation, so rasterization can make performance better
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    
    // add a tap gesture recognizer
    if (self.recognizer == nil) {
        UITapGestureRecognizer * recogn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressed)];
        [self addGestureRecognizer:recogn];
        self.recognizer = recogn;
    }
    
    // install a label inside
    if (self.textLabels == nil || self.needRecreateLabels) {
        
        // remove previously created views (if have any)
        self.needRecreateLabels = NO;
        [self forEveryTextLabelDo:^(UILabel * label) {
            [label removeFromSuperview];
        }];
        [self forEveryTextLabelBgDo:^(UIView *bg) {
            [bg removeFromSuperview];
        }];
        
        // create a new ones
        self.textLabels = [NSMutableArray array];
        self.backgrounds = [NSMutableArray array];
        NSInteger requestsCount = [self requestsCount];
        BOOL haveSharedUnit = self.sharedUnit != nil;
        NSInteger sections = requestsCount + (haveSharedUnit ? 1 : 0);
        
        NSLayoutXAxisAnchor * previousXancor = self.leadingAnchor;
        for (NSInteger i = 0; i < sections; ++i) {
            UILabel * label = [[UILabel alloc] initWithFrame:self.bounds];
            label.translatesAutoresizingMaskIntoConstraints = NO;
            label.textColor = [UIColor bf_widgetTextColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [BFStatisticsLabel internalLabelFont];
            label.minimumScaleFactor = 0.5;
            [label setContentHuggingPriority:600 + i forAxis:UILayoutConstraintAxisHorizontal];
            [label setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
            [self addSubview:label];
            CGFloat leftOffset = (i == 0) ? 4 : 8;
            [NSLayoutConstraint activateConstraints:@[
                                                      [label.leadingAnchor constraintEqualToAnchor:previousXancor constant:leftOffset],
                                                      
                                                      [label.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:[BFStatisticsLabel internalLabelInsets].bottom],
                                                      
                                                      [label.topAnchor constraintEqualToAnchor:self.topAnchor constant:[BFStatisticsLabel internalLabelInsets].top]
                                                      
                                                      ]];
            previousXancor = label.trailingAnchor;
            
            if (i == sections - 1) {
                NSLayoutConstraint * lastConstraint = [label.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-4];
                lastConstraint.priority = 750;
                [NSLayoutConstraint activateConstraints:@[lastConstraint]];
            }
            [self.textLabels addObject:[NSValue valueWithNonretainedObject:label]];
            
            // Now create background view
            UIView * background = [[UIView alloc] init];
            background.translatesAutoresizingMaskIntoConstraints = NO;
            background.layer.cornerRadius = 4;
            if (i == sections - 1 && haveSharedUnit) {
                background.backgroundColor = [UIColor bf_widgetUnitColor];
            } else {
                background.backgroundColor = [UIColor bf_widgetColorWithIdx:i];
            }
            [self insertSubview:background atIndex:0];
            CGFloat rightOffset = 4;
            [NSLayoutConstraint activateConstraints:@[
                                                      [background.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:0],
                                                      
                                                      [background.trailingAnchor constraintEqualToAnchor:label.trailingAnchor constant:rightOffset],
                                                      
                                                      [background.bottomAnchor constraintEqualToAnchor:label.bottomAnchor constant:0],
                                                      
                                                      [background.topAnchor constraintEqualToAnchor:label.topAnchor constant:0]
                                                      
                                                      ]];
            [self.backgrounds addObject:[NSValue valueWithNonretainedObject:background]];
        }
    }
    
    // install an activity indicator inside
    if (self.activityIndicator == nil) {
        UIActivityIndicatorView * activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activity.translatesAutoresizingMaskIntoConstraints = NO;
        activity.color = self.textColor;
        activity.hidesWhenStopped = YES;
        [self addSubview:activity];
        [NSLayoutConstraint activateConstraints:@[
                                                  [activity.centerXAnchor constraintEqualToAnchor:self.centerXAnchor constant:0],
                                                  
                                                  [activity.centerYAnchor constraintEqualToAnchor:self.centerYAnchor constant:0],
                                                  
                                                  [activity.widthAnchor constraintEqualToConstant:24],
                                                  
                                                  [activity.heightAnchor constraintEqualToConstant:24]
                                                  
                                                  ]];
        self.activityIndicator = activity;
    }
}

-(void)pressed
{
    UIView * biggestView = [(NSValue*)self.backgrounds.lastObject nonretainedObjectValue];
    if (biggestView == nil) {
        biggestView = self;
    }
    [self openDetailsFromView:biggestView withColor:[UIColor bf_widgetColorWithIdx:0] enabled:YES];
}

+(UIEdgeInsets)internalLabelInsets
{
    return UIEdgeInsetsMake(0, 8, 0, 8);
}

+(UIFont*)internalLabelFont
{
    return [UIFont boldSystemFontOfSize:12];
}

@end

