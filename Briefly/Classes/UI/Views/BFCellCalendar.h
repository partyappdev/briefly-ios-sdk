//
//  BFCellGraph.h
//  Briefly
//
//  Created by Artem Bondar on 30/01/2018.
//

#import <UIKit/UIKit.h>

@class BFFSCalendar;

@interface BFCellCalendar : UITableViewCell

@property (weak, nonatomic) IBOutlet BFFSCalendar * calendar;

-(void)configureWithDatesRange:(NSArray<NSDate*>*)datesRange;

@end
