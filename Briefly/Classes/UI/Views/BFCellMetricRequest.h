//
//  BFCellMetricRequest.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import <UIKit/UIKit.h>

@class BFViewModelMetricRequest;
@class BFModelObjectMeta;
@class BFViewModelObjectIdModifier;

@interface BFCellMetricRequest : UITableViewCell

-(void)configureWithMetricRequest:(BFViewModelMetricRequest*)request
                 objectIdModifier:(BFViewModelObjectIdModifier*)objIdModifier
                             meta:(BFModelObjectMeta*)meta
                            value:(NSNumber*)value
                              idx:(NSInteger)idx;

@end

@interface BFCellMetricRequestAction : UITableViewCell

-(void)configureWithText:(NSString*)text enabled:(BOOL)enabled;

@end
