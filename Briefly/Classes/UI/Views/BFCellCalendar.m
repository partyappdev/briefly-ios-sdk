//
//  BFCellGraph.m
//  Briefly
//
//  Created by Artem Bondar on 30/01/2018.
//

#import "BFCellCalendar.h"
#import "BFFSCalendar.h"

@interface BFCellCalendar ()

@property (strong, nonatomic) NSArray<NSDate*>* displayedDates;

@end


@implementation BFCellCalendar

- (void)awakeFromNib {
    [super awakeFromNib];
    self.calendar.placeholderType = BFFSCalendarPlaceholderTypeNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureWithDatesRange:(NSArray<NSDate*>*)datesRange
{
    // Store displayed items for minor updates handling
    self.displayedDates = datesRange;
    
    // Reselect dates
    for (NSDate * dt in [NSArray arrayWithArray:self.calendar.selectedDates]) {
        [self.calendar deselectDate:dt];
    }
    for (NSDate * dt in datesRange) {
        [self.calendar selectDate:dt];
    }
}

@end
