//
//  BFPurchasesStatisticLabel.h
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import <UIKit/UIKit.h>
#import "BFStatisticsLabel.h"

IB_DESIGNABLE
@interface BFPurchasesStatisticLabel : BFStatisticsLabel

@property (strong, nonatomic) IBInspectable NSString * itemId;

@end
