//
//  BFCellObjectIdModifier.h
//  Briefly
//
//  Created by Artem Bondar on 14.02.2018.
//

#import <UIKit/UIKit.h>

@interface BFCellObjectIdModifier : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView * viewContainer;
@property (weak, nonatomic) IBOutlet UILabel * labelText;

-(void)configureWithText:(NSString*)text selected:(BOOL)selected;

@end
