//
//  BFCellMetricRequest.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFCellMetricRequest.h"
#import "BFViewModelMetricRequest.h"
#import "BFPublicModels.h"
#import "BFViewMetricRequest.h"
#import "UIColor+BfPalette.h"

@interface BFCellMetricRequest ()

@property (weak, nonatomic) BFViewMetricRequest * viewRequestPresenter;

@end

@implementation BFCellMetricRequest

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self setupPresenter];
    return self;
}

-(instancetype)init
{
    self = [super init];
    [self setupPresenter];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setupPresenter];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self setupPresenter];
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupPresenter];
}

-(void)setupPresenter
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.viewRequestPresenter != nil) {
        return;
    }
    BFViewMetricRequest * view = [BFViewMetricRequest instantiate];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:view];
    [self.contentView addConstraints:@[
                                       [view.topAnchor constraintEqualToAnchor:self.contentView.topAnchor],
                                       [view.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor],
                                       [view.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor],
                                       [view.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor]
                                       ]];
    self.viewRequestPresenter = view;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.viewRequestPresenter.highlighted = selected;
}

-(void)configureWithMetricRequest:(BFViewModelMetricRequest*)request
                 objectIdModifier:(BFViewModelObjectIdModifier*)objIdModifier
                             meta:(BFModelObjectMeta*)meta
                            value:(NSNumber*)value
                              idx:(NSInteger)idx
{
    [self.viewRequestPresenter configureWithMetricRequest:request
                                         objectIdModifier:objIdModifier
                                                     meta:meta
                                                    value:value
                                                      idx:idx];
}

@end



@interface BFCellMetricRequestAction ()

@property (weak, nonatomic) BFViewMetricRequestManageActionButton * actionPresenter;

@end

@implementation BFCellMetricRequestAction

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self setupPresenter];
    return self;
}

-(instancetype)init
{
    self = [super init];
    [self setupPresenter];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setupPresenter];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self setupPresenter];
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupPresenter];
}

-(void)setupPresenter
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.actionPresenter != nil) {
        return;
    }
    BFViewMetricRequestManageActionButton * view = [BFViewMetricRequestManageActionButton instantiate];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:view];
    [self.contentView addConstraints:@[
                                       [view.topAnchor constraintEqualToAnchor:self.contentView.topAnchor],
                                       [view.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor],
                                       [view.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor],
                                       [view.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor]
                                       ]];
    self.actionPresenter = view;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.actionPresenter.highlighted = selected;
}

-(void)configureWithText:(NSString*)text enabled:(BOOL)enabled
{
    [self.actionPresenter configureWithActionText:text color:enabled ? [UIColor bf_BaseUIColor] : [UIColor bf_widgetDisabled]];
}

@end
