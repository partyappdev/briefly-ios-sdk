//
//  BFViewSortStatus.h
//  Briefly
//
//  Created by Artem Bondar on 26.03.2018.
//

#import <UIKit/UIKit.h>
#import "BFSorter.h"
#import "UIView+Visualization.h"

@protocol BFViewSortStatusDelegate
-(void)viewSortStatusPressed;
-(void)viewSortStatusPressedSettings;
@end

@interface BFViewSortStatus : BFShadowedView

@property (weak, nonatomic) id <BFViewSortStatusDelegate> delegate;

-(void)sorterStateChanged:(BFSorter*)sorter
    haveSortingController:(BOOL)haveSortingController
                ascending:(BOOL)ascending;

@end
