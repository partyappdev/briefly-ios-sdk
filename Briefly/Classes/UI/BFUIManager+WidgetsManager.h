//
//  BFUIManager+WidgetsManager.h
//  Briefly
//
//  Created by Artem Bondar on 15.03.2018.
//

#import "BFUIManager.h"
#import "BFViewSortStatus.h"

@class BFViewModelPreset;
@class BFSorter;

@interface BFUIManager (WidgetsManager) <BFViewSortStatusDelegate>

-(BFViewModelPreset*)currentPresetForObjectId:(NSString*)objectId
                                widgetGroupId:(NSString*)widgetGroupId
                      andDefaultRequestString:(NSString*)defaultRequestString;

-(NSArray<BFViewModelPreset*>*)presetsListForObjectId:(NSString*)objectId
                                        widgetGroupId:(NSString*)widgetGroupId
                              andDefaultRequestString:(NSString*)defaultRequestString;

-(NSInteger)currentlySelectedPresetIdxForObjectId:(NSString*)objectId
                                 widgetGroupId:(NSString*)widgetGroupId;

-(void)setPresets:(NSArray<BFViewModelPreset*>*)presets
      selectedIdx:(NSInteger)idx
     forObjectId:(NSString*)objectId
   widgetGroupId:(NSString*)widgetGroupId;

-(void)registerSorter:(BFSorter*)sorter;
-(BOOL)sortOrderAscendingForWidgetGroup:(NSString*)widgetGroup;
-(void)setSortOrder:(BOOL)ascending
     forWidgetGroup:(NSString*)widgetGroup;

-(NSNumber*)selectedMetricIdxForSortingInWidgetGroup:(NSString*)widgetGroup;
-(void)setSelectedMetricIdx:(NSNumber*)idx forSortingInWidgetGroup:(NSString*)widgetGroup;

@end
