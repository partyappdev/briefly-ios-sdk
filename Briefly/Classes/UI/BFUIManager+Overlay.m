//
//  BFUIManager+Overlay.m
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "BFUIManager+Overlay.h"
#import "BFUIManager_Private.h"
#import "BFUIManager+Storage.h"
#import "BFBrieflyManager+Resources.h"
#import "BFUIManager+WidgetsManager.h"
#import "BFBrieflyManager.h"
#import "BFButton.h"
#import "UIViewController+Utils.h"
#import "UIView+Tracking.h"
#import "BFHeatmapImageView.h"
#import "BFPreferencesController.h"
#import "BFSorter+Private.h"

@implementation BFUIManager (Overlay)

-(void)updateOverlayOnTimer
{
    // hm, its hacky but it works, so....
    // (should use superview - but for some reason it's null)
    BOOL ovarlayIsOnTop = [self.currentWindow.subviews.lastObject isEqual:self.overlayView] || [self.currentWindow.subviews.lastObject isEqual:self.sortStatus] || [self.currentWindow.subviews.lastObject isEqual:self.overlayStatisticsLabel];
    if (!ovarlayIsOnTop || (self.overlayView && !CGRectEqualToRect(self.overlayView.frame, self.currentWindow.bounds))) {
        [self recreateOverlayView];
        [self reloadOnlyHeatmaps];
    }
    
    //
    // Update the settings for ui statistic labels (except fullscreen one)
    //
    if ([self shouldShowUIActionsStatistic]) {
        for (UIView * analyticsView in self.analyticsViews) {
            NSString * objectId = [self uiEventsTrackingIdForView:analyticsView];
            if (objectId) {
                for (UIView * view in analyticsView.subviews) {
                    if ([view isKindOfClass:[BFStatisticsLabelScreen class]]) {
                        BFStatisticsLabelScreen * statsLabel = (BFStatisticsLabelScreen*)view;
                        if (!(statsLabel.analyticsObjectId == nil && objectId == nil) && ![statsLabel.analyticsObjectId isEqualToString:objectId]) {
                            
                            statsLabel.analyticsObjectId = objectId;
                        }
                        break;
                    }
                }
            }
        }
    }
}

-(void)recreateOverlayView
{
    [self.overlayView removeFromSuperview];
    [self.overlayButton removeFromSuperview];
    [self.sortStatus removeFromSuperview];
    [self.overlayStatisticsLabel removeFromSuperview];

    if ([self shouldShowHeatmaps]) {
        BFHeatmapImageView * view = [[BFHeatmapFullscreenImageView alloc] initWithFrame:self.currentWindow.bounds];
        view.userInteractionEnabled = NO;
        view.translatesAutoresizingMaskIntoConstraints = NO;
        if (self.currentControllerUid) {
            view.bf_trackingId = self.currentControllerUid;
        }
        [self.currentWindow addSubview:view];
        self.overlayView = view;
    }
    
    // Add overlay for sorting controllers
    if ([self shouldShowWidgetsForObjectId:nil widgetGroup:nil]) {
        CGFloat sorterWidth = [self.sortStatus sizeThatFits:CGSizeMake([UIScreen mainScreen].bounds.size.width/3*2, 38)].width;
        BFViewSortStatus * view = [[BFBrieflyManager locateBundle] loadNibNamed:@"BFViewSorterState" owner:nil options:nil].firstObject;
        
        CGFloat topInset = 24;
        if (@available(iOS 11.0, *)) {
            topInset = MAX(self.currentWindow.safeAreaInsets.top, 24);
        }
        view.frame = CGRectMake(self.currentWindow.bounds.size.width - sorterWidth - 8, topInset, sorterWidth, 38);
        view.delegate = self;
        self.sortStatus = view;
        [self.currentWindow addSubview:view];
        [self updateSortingStateOverlay];
    }

    /*if ([self shouldShowUIActionsStatistic] && self.currentControllerUid) {
        BFStatisticsLabelScreen * statisticsLabel = [[BFStatisticsLabelScreenFull alloc] initWithFrame:CGRectMake(4, 20, 120, 32)];
        [statisticsLabel addToView:self.currentWindow topInset:24 leftInset:4];
        statisticsLabel.analyticsObjectId = [self uiEventsTrackingIdForView:nil];
        self.overlayStatisticsLabel = statisticsLabel;
    }*/
}

-(void)needUpdateOverlayAfterScreenChange
{
    // We update all heatmaps here because what can happen:
    // - return back from controller
    // - didMoveToWindow called, but top controller is still old
    // - we get wrong data
    //
    if (self.currentControllerUid) {
        [self recreateOverlayView];
        [self reloadOnlyHeatmaps];
    }
    self.overlayView.bf_trackingId = self.currentControllerUid;
    [self updateSortingStateOverlay];
}

-(void)subscribeViewForChanges:(UIView*)view
{
    [self.analyticsViews addObject:view];
}

-(void)unsubscribeViewFromChanges:(UIView*)view
{
    [self.analyticsViews removeObject:view];
}

-(void)forceWidgetsReloadData
{
    //
    // reload data for widgets
    //
    for (UIView* widget in self.analyticsViews) {
        [widget bf_onNeedReloadData];
    }
    //
    // and update current sorter (and sortable controller)
    //
    for (BFSorter * sorter in self.sorters) {
        [sorter widgetsStateChanged];
    }
}

-(void)reloadOnlyHeatmaps
{
    for (UIView* widget in self.analyticsViews) {
        if ([widget trackingId]) {
            [widget bf_onNeedReloadData];
        }
    }
    [self.overlayView bf_reloadData];
}

-(void)updateSortingStateOverlay
{
    BFSorter * sorter;
    if ([self.currentController conformsToProtocol:@protocol(BFSortableController)]) {
        id<BFSortableController> ctrl = (id<BFSortableController>)self.currentController;
        sorter = [ctrl sortableControllerSorter];
    }
    [self.sortStatus sorterStateChanged:sorter haveSortingController:sorter != nil ascending:sorter.ascending];
    
    CGSize desiredSize = [self.sortStatus sizeThatFits:CGSizeMake([UIScreen mainScreen].bounds.size.width/3*2, 38)];
    if (!CGSizeEqualToSize(desiredSize, self.sortStatus.bounds.size)) {
        
        CGFloat topInset = 24;
        if (@available(iOS 11.0, *)) {
            topInset = MAX(self.currentWindow.safeAreaInsets.top, 24);
        }
        CGRect desiredFrame = CGRectMake(self.currentWindow.bounds.size.width - desiredSize.width - 8, topInset, desiredSize.width, desiredSize.height);
        
        [UIView animateWithDuration:0.4 animations:^{
            self.sortStatus.frame = desiredFrame;
        }];
    }
}


@end
