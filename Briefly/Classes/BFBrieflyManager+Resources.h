//
//  BFBrieflyManager+Resources.h
//  Pods
//
//  Created by Artem Bondar on 14.12.2017.
//
//

#import "BFBrieflyManager.h"

@interface BFBrieflyManager (Resources)

+(UIStoryboard*)baseStoryboard;
+(UIImage*)imageNamed:(NSString*)imageName;
+(NSBundle*)locateBundle;

@end
