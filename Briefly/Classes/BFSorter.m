//
//  BFSorter.m
//  Briefly
//
//  Created by Artem Bondar on 23.03.2018.
//

#import "BFSorter.h"
#import "BFBrieflyManager+ApiWrapper.h"
#import "BFUIManager+WidgetsManager.h"
#import "BFUIManager+Overlay.h"
#import "BFViewModelPreset.h"
#import "BFViewModelMetricRequest.h"
#import "BFSorter+Private.h"


@interface BFSorterItem : NSObject
@property (strong, nonatomic) NSNumber * index;
@property (strong, nonatomic) NSNumber * value;
@end
@implementation BFSorterItem
@end


@interface BFSorter ()

//
// Objects are grouped by sections
//
@property (strong, nonatomic) NSDictionary<NSNumber*, NSDictionary<NSNumber*, NSString*>*> * originalObjectIdsMapping;
@property (strong, nonatomic) NSMutableDictionary<NSIndexPath*, NSIndexPath*> * oldToNewIndexPathMapping;
@property (nonatomic) NSInteger currentLoadUid;


@end

@implementation BFSorter


+(instancetype)sorterWithDataSource:(id<BFSortableDataSource>)dataSource
{
    BFSorter * sorter = [BFSorter new];
    sorter.currentDataSource = dataSource;
    return sorter;
}

-(void)setState:(BFSorterState)state
{
    _state = state;
    [[BFBrieflyManager sharedManager].uiManager updateSortingStateOverlay];
}

-(void)setActive:(BOOL)active
{
    BOOL changed = active != _active;
    _active = active;
    if (changed) {
        [self.currentDataSource sortableDataSourceSortOrderChanged];
    }
}

-(void)updateActiveState
{
    self.active = [[BFBrieflyManager sharedManager].uiManager shouldShowSorterControlsForWidgetGroup:self.currentDataSource.sortableDataSourceWidgetGroupId];
}

-(void)setCurrentDataSource:(id<BFSortableDataSource>)currentDataSource
{
    BOOL changed = currentDataSource != _currentDataSource;
    _currentDataSource = currentDataSource;
    if (changed) {
        [self updateActiveState];
    }
}

-(void)widgetsStateChanged
{
    [self updateActiveState];
    [self dataChangedAndNeedReload:YES];
}

-(void)dataChangedAndNeedReload:(BOOL)needReload
{
    //
    // Activate this sorter, by saving dataSource, and registering in
    // Briefly UI Manager
    //
    [[BFBrieflyManager sharedManager].uiManager registerSorter:self];

    //
    // Load data, only if this sorter is active
    //
    if (!self.active || self.state == BFSorterStateUnchecked) {
        self.originalObjectIdsMapping = nil;
        self.oldToNewIndexPathMapping = nil;
        self.currentLoadUid += 1;
        return;
    }

    __block NSMutableSet<NSString*>* flatterenObjectIdRequest = [NSMutableSet set];
    __block NSMutableDictionary<NSNumber*, NSMutableDictionary<NSNumber*, NSString*>*> * sections = [NSMutableDictionary dictionary];
    [self.currentDataSource sortableDataSourceEnumerateAllSortableObjects:^(NSIndexPath *indexPath, NSString *objectId) {
        
        //
        // add object id to set, and add to a corresponding dictionary
        //
        [flatterenObjectIdRequest addObject:objectId];
        
        //
        // Get section dictionary
        //
        NSMutableDictionary<NSNumber*, NSString*>* section = sections[@(indexPath.section)];
        if (section == nil) {
            section = [NSMutableDictionary dictionary];
        }
        
        //
        // Set value
        //
        section[@(indexPath.item)] = objectId;
        sections[@(indexPath.section)] = section;
    }];
    BOOL changed = [self isObjectsTreeChanged:sections];
    self.originalObjectIdsMapping = sections;
    
    //
    // Build request and reload data
    //
    if (!self.metaObjectId) {
        self.metaObjectId = [self.currentDataSource sortableDataSourceAnyObjectId];
    }

    if (needReload || changed) {
        self.state = BFSorterStateInvalid;
        // Find a request to make:
        NSString * widgetGroup = [self.currentDataSource sortableDataSourceWidgetGroupId];
        if (widgetGroup == nil) {
            return;
        }
        
        BOOL ascending = [[BFBrieflyManager sharedManager].uiManager sortOrderAscendingForWidgetGroup:widgetGroup];
        __block BFViewModelPreset * preset = [[BFBrieflyManager sharedManager].uiManager currentPresetForObjectId:nil widgetGroupId:widgetGroup andDefaultRequestString:nil];
        __block NSMutableArray * requests = [NSMutableArray arrayWithCapacity:flatterenObjectIdRequest.count];
        NSArray * objectIds = flatterenObjectIdRequest.allObjects;
        [objectIds enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString * request = [preset apiRequestAtIdx:self.currentMetricIdx forObjectId:obj];
            if (request) {
                [requests addObject:request];
            }
            if (self.metaObjectId == nil) {
                self.metaObjectId = obj;
            }
        }];
        
        if (requests.count > 0) {
            self.currentLoadUid += 1;
            self.state = BFSorterStateUpdating;
            __block NSInteger currentLoadUid = self.currentLoadUid;
            __weak __typeof__(self) weakSelf = self;
            [[BFBrieflyManager sharedManager] calculateMetricRequests:requests withPriority:BFBrieflyApiPriorityHigh withHandler:^(BOOL succeed, NSArray<BFModelCalculateMetricResponseItem *> *results) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (succeed && results.count == requests.count && weakSelf.currentLoadUid == currentLoadUid) {
                        __block NSMutableDictionary * objIdToValue = [NSMutableDictionary dictionary];
                        [results enumerateObjectsUsingBlock:^(BFModelCalculateMetricResponseItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            objIdToValue[objectIds[idx]] = [obj value];
                        }];
                        
                        //
                        // The story is simple. Read sections one by one
                        //
                        NSMutableDictionary<NSIndexPath*, NSIndexPath*> * oldIndexPathToNewMapping = [NSMutableDictionary dictionary];
                        [weakSelf.originalObjectIdsMapping enumerateKeysAndObjectsUsingBlock:^(NSNumber * _Nonnull section, NSDictionary<NSNumber *,NSString *> * _Nonnull items, BOOL * _Nonnull stop) {
                            
                            NSMutableArray * oldIndexes = [NSMutableArray arrayWithArray:items.allKeys];
                            NSSortDescriptor *sortDescrAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
                            [oldIndexes sortUsingDescriptors:@[sortDescrAsc]];
                            
                            NSMutableArray<BFSorterItem*>* newWithValues = [NSMutableArray array];
                            [items enumerateKeysAndObjectsUsingBlock:^(NSNumber * _Nonnull index, NSString * _Nonnull objId, BOOL * _Nonnull stop) {
                                BFSorterItem * sorterItem = [BFSorterItem new];
                                sorterItem.value = objIdToValue[objId];
                                sorterItem.index = index;
                                [newWithValues addObject:sorterItem];
                            }];
                            // Sort new indexes by value
                            
                            NSSortDescriptor *sortDescr = [NSSortDescriptor sortDescriptorWithKey:@"value" ascending:ascending];
                            [newWithValues sortUsingDescriptors:@[sortDescr]];
                            
                            // Build new mappings
                            [oldIndexes enumerateObjectsUsingBlock:^(NSNumber*  _Nonnull oldItem, NSUInteger idx, BOOL * _Nonnull stop) {
                                NSIndexPath * oldIndexPath = [NSIndexPath indexPathForItem:oldItem.integerValue inSection:section.integerValue];
                                NSNumber * newItem = newWithValues[idx].index;
                                NSIndexPath * newIndexPath = [NSIndexPath indexPathForItem:newItem.integerValue inSection:section.integerValue];
                                oldIndexPathToNewMapping[oldIndexPath] = newIndexPath;
                            }];
                        }];
                        
                        weakSelf.oldToNewIndexPathMapping = oldIndexPathToNewMapping;
                        weakSelf.state = BFSorterStateValid;
                        [self.currentDataSource sortableDataSourceSortOrderChanged];
                    } else {
                        weakSelf.state = BFSorterStateInvalid;
                    }
                });
            }];
        } else {
            // No data? Whatever...
            self.state = BFSorterStateValid;
        }
    }
    
    //
    // And also if possible - load the meta
    //
    if (self.metaObjectId && self.objectMetaForSorter == nil) {
        [[BFBrieflyManager sharedManager] getMetaForObjectWithId:self.metaObjectId withHandler:^(BOOL succeed, BFModelObjectMeta * objectMeta) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (succeed && objectMeta) {
                    self.metaLoadingFailed = NO;
                    self.objectMetaForSorter = objectMeta;
                } else {
                    self.metaLoadingFailed = YES;
                }
                [[BFBrieflyManager sharedManager].uiManager updateSortingStateOverlay];
            });
        }];
    }
}

-(BFSorterMetaState)metaState
{
    if (self.metaObjectId == nil) {
        return BFSorterMetaStateNone;
    } else if (self.objectMetaForSorter) {
        return BFSorterMetaStateLoaded;
    } else if (self.metaLoadingFailed) {
        return BFSorterMetaStateFailed;
    } else {
        return BFSorterMetaStateLoading;
    }
}

-(BOOL)isObjectsTreeChanged:(NSDictionary<NSNumber*, NSDictionary<NSNumber*, NSString*>*> *)newObjectTree
{
    if (![self.originalObjectIdsMapping.allKeys isEqualToArray:newObjectTree.allKeys]) {
        return YES;
    }
    __block BOOL same = YES;
    [self.originalObjectIdsMapping enumerateKeysAndObjectsUsingBlock:^(NSNumber * _Nonnull key, NSDictionary<NSNumber *,NSString *> * _Nonnull obj, BOOL * _Nonnull stop) {
        
        NSDictionary<NSNumber *,NSString *> * otherObj = newObjectTree[key];
        if (![obj.allKeys isEqualToArray:otherObj.allKeys]) {
            same = NO;
            *stop = YES;
            return;
        }
        
        [obj enumerateKeysAndObjectsUsingBlock:^(NSNumber * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
            NSString * obj2 = otherObj[key];
            
            if (![obj isEqualToString:obj2]) {
                same = NO;
                *stop = YES;
                return;
            }
        }];
        
    }];
    return !same;
}

-(NSIndexPath*)sortedIndexPathForItemAtIndexPath:(NSIndexPath*)indexPath
{
    if (self.state == BFSorterStateValid && self.active) {
        NSIndexPath * newIndexPath = self.oldToNewIndexPathMapping[indexPath];
        if (newIndexPath) {
            return newIndexPath;
        }
    }
    return indexPath;
}

-(BOOL)ascending
{
    NSString * widgetGroup = [self.currentDataSource sortableDataSourceWidgetGroupId];
    return [[BFBrieflyManager sharedManager].uiManager sortOrderAscendingForWidgetGroup:widgetGroup];
}

-(void)check
{
    NSString * widgetGroup = [self.currentDataSource sortableDataSourceWidgetGroupId];
    [[BFBrieflyManager sharedManager].uiManager setSortOrder:NO forWidgetGroup:widgetGroup];
    self.state = BFSorterStateInvalid;
    [self dataChangedAndNeedReload:YES];
}

-(void)uncheck
{
    NSString * widgetGroup = [self.currentDataSource sortableDataSourceWidgetGroupId];
    if (self.ascending) {
        self.state = BFSorterStateUnchecked;
        [self.currentDataSource sortableDataSourceSortOrderChanged];
    } else {
        [[BFBrieflyManager sharedManager].uiManager setSortOrder:YES forWidgetGroup:widgetGroup];
        [self dataChangedAndNeedReload:YES];
        [self.currentDataSource sortableDataSourceSortOrderChanged];
    }
}

-(NSString*)objIdForSettings
{
    return nil;
}

-(NSString*)widgetGroupForSettings
{
    return nil;
}

-(NSInteger)currentMetricIdx
{
    NSString * widgetGroup = [self.currentDataSource sortableDataSourceWidgetGroupId];
    if (widgetGroup == nil) {
        return 0;
    }

    NSInteger idx = [[BFBrieflyManager sharedManager].uiManager selectedMetricIdxForSortingInWidgetGroup:widgetGroup].integerValue;
    BFViewModelPreset * preset = [[BFBrieflyManager sharedManager].uiManager currentPresetForObjectId:nil widgetGroupId:widgetGroup andDefaultRequestString:nil];
    
    if (idx >= 0 && idx < preset.metricRequest.flatterenMetricRequests.count) {
        return idx;
    }
    return 0;
}

-(void)setCurrentMetricIdx:(NSInteger)currentMetricIdx
{
    NSString * widgetGroup = [self.currentDataSource sortableDataSourceWidgetGroupId];
    
    if (widgetGroup == nil) {
        return;
    }
    BFViewModelPreset * preset = [[BFBrieflyManager sharedManager].uiManager currentPresetForObjectId:nil widgetGroupId:widgetGroup andDefaultRequestString:nil];
    if (currentMetricIdx >= 0 && currentMetricIdx < preset.metricRequest.flatterenMetricRequests.count) {
        [[BFBrieflyManager sharedManager].uiManager setSelectedMetricIdx:@(currentMetricIdx) forSortingInWidgetGroup:widgetGroup];
    } else {
        [[BFBrieflyManager sharedManager].uiManager setSelectedMetricIdx:nil forSortingInWidgetGroup:widgetGroup];
    }
    [self dataChangedAndNeedReload:YES];
}

-(NSArray<NSString*>*)humanReadableMetricNames
{
    if (!self.objectMetaForSorter) {
        return nil;
    }
    NSString * widgetGroup = [self.currentDataSource sortableDataSourceWidgetGroupId];
    if (widgetGroup == nil) {
        return nil;
    }

    BFViewModelPreset * preset = [[BFBrieflyManager sharedManager].uiManager currentPresetForObjectId:nil widgetGroupId:widgetGroup andDefaultRequestString:nil];
    __block NSMutableArray * array = [NSMutableArray array];

    [preset.metricRequest.flatterenMetricRequests  enumerateObjectsUsingBlock:^(BFViewModelMetricRequest * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [array addObject:[obj uiNameWithObjectsMeta:self.objectMetaForSorter]];
    }];
    return array;
}

@end
