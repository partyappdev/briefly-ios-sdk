//
//  BFSorter+Private.h
//  Briefly
//
//  Created by Artem Bondar on 26.03.2018.
//

#import "BFSorter.h"
#import "BFPublicModels.h"

typedef NS_ENUM(NSUInteger, BFSorterState) {
    BFSorterStateUnchecked,
    BFSorterStateInvalid,
    BFSorterStateUpdating,
    BFSorterStateValid
};

typedef NS_ENUM(NSUInteger, BFSorterMetaState) {
    BFSorterMetaStateNone,
    BFSorterMetaStateLoading,
    BFSorterMetaStateFailed,
    BFSorterMetaStateLoaded
};

@interface BFSorter ()

@property (nonatomic) NSInteger currentMetricIdx;
@property (nonatomic) BOOL userAskedForSorting;
@property (nonatomic) BOOL active;

@property (nonatomic) BFSorterState state;
@property (weak, nonatomic) id <BFSortableDataSource> currentDataSource;


@property (strong, nonatomic) BFModelObjectMeta * objectMetaForSorter;
@property (nonatomic) BOOL metaLoadingFailed;
@property (nonatomic, strong) NSString * metaObjectId;

-(void)widgetsStateChanged;
-(void)uncheck;
-(void)check;
-(BOOL)ascending;
-(BFSorterMetaState)metaState;
-(NSArray<NSString*>*)humanReadableMetricNames;

@end
