//
//  BFSorter.h
//  Briefly
//
//  Created by Artem Bondar on 23.03.2018.
//

#import <UIKit/UIKit.h>
#import "BFSortableDataSource.h"

@interface BFSorter : NSObject

+(instancetype)sorterWithDataSource:(id<BFSortableDataSource>)dataSource;

-(void)dataChangedAndNeedReload:(BOOL)needReload;

-(NSIndexPath*)sortedIndexPathForItemAtIndexPath:(NSIndexPath*)indexPath;
-(NSString*)objIdForSettings;
-(NSString*)widgetGroupForSettings;

@end
