//
//  BFBrieflyManager.m
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "BFBrieflyManager.h"
#import "BFBrieflyManager+Private.h"

#import "BFUIManager_Private.h"

#import "BFApiRequestEntity.h"
#import "BFModelAccessToken.h"
#import "NSString+Utils.h"

NSString * bfBaseUrlDev = @"https://t823weq3t5.execute-api.us-east-1.amazonaws.com/v1";
NSString * bfBaseUrlStaging = @"https://ntmjcdv5ia.execute-api.us-east-1.amazonaws.com/v1";
NSString * bfBaseUrlProd = @"https://d4757l7yqk.execute-api.us-east-1.amazonaws.com/v1";
NSInteger dayInSeconds = 60 * 60 * 24;

@interface BFBrieflyManager ()

@property (strong, nonatomic) BFUIManager * uiManager;
@property (strong, nonatomic) NSString * apiKey;
@property (strong, nonatomic) NSURLSession * httpSession;
@property (nonatomic) BFLogLevel currentLogLevel;
@property (strong, nonatomic) BFModelAccessToken * currentAccess;
@property (nonatomic, strong) CRQueue *processingQueue;

@end

@implementation BFBrieflyManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        // do initialization
        self.clientId = @"ios-client";
        self.version = @"1.0.0";
        self.stage = BFStageLive;
        self.perScreenTrackingId = YES;
        self.uiManager = [BFUIManager instanceWithHost:self];
        self.cache = [BFResposesCache new];
        self.currentLogLevel = BFLogLevelFatal;
        self.currentAccess = [BFModelAccessToken tokenFromUserDefaults];
        self.httpSession = [NSURLSession sharedSession];
        self.processingQueue = [[CRQueue alloc] initWithMaxConcurentTasksCount:1 maxRetryCount:3 executionQueue:dispatch_queue_create("com.desqlabs.briefly.requeests.cache", DISPATCH_QUEUE_SERIAL) debugLogging:NO];
        [self.processingQueue setTaskSquashingLogic:self.squashingLogic];
        
        // For widget-expanded requests (meta/graph)
        [self.processingQueue setCapacityLevels:@{@90: @2, @99: @1, @100: @1, @102: @1}];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appBecameActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appBecameInactive) name:UIApplicationWillResignActiveNotification object:nil];
    }
    return self;
}

+ (instancetype)sharedManager {
    static BFBrieflyManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[BFBrieflyManager alloc] init];
    });
    return sharedMyManager;
}

-(void)logRecord:(NSString*)record
{
    [self logRecord:record logLevel:BFLogLevelError];
}

-(void)logRecord:(NSString*)record logLevel:(BFLogLevel)logLevel
{
    if (self.currentLogLevel <= logLevel) {
        NSString * logLevelPrefix = @"UNK";
        switch(logLevel) {
            case BFLogLevelTrace:
                logLevelPrefix = @"TRC";
                break;
            case BFLogLevelDebug:
                logLevelPrefix = @"DBG";
                break;
            case BFLogLevelWarning:
                logLevelPrefix = @"WRN";
                break;
            case BFLogLevelError:
                logLevelPrefix = @"ERR";
                break;
            case BFLogLevelFatal:
                logLevelPrefix = @"FAT";
                break;
        }
        
        NSLog(@"Briefly(%@): %@", logLevelPrefix, record);
    }
}

-(void)setApiKey:(NSString *)apiKey
{
    _apiKey = apiKey;
    NSURLSessionConfiguration * config = [NSURLSessionConfiguration defaultSessionConfiguration];
    [config setHTTPAdditionalHeaders:@{@"AUTH_PUB_KEY": apiKey}];
    self.httpSession = [NSURLSession sessionWithConfiguration:config];
}


-(void)makeApiCallToEndpoint:(NSString*)endpoint
                   parametrs:(NSDictionary*)parametrs
                    priority:(NSInteger)priority
                    logLevel:(BFLogLevel)logLevel
             needAccessToken:(BOOL)needAccessToken
                  andHandler:(void(^)(BOOL succeed, NSObject* params, NSObject* response))handler
{
    __weak __typeof__(self) weakSelf = self;
    [self.processingQueue addTask:^(id params, CRQueueActionHandler handler) {
        
        NSDictionary * taskParams = (NSDictionary*)params;
        if (needAccessToken && weakSelf.currentAccess == nil) {
            [weakSelf logRecord:[NSString stringWithFormat:@"Try to make api call, without access token: %@, %@",endpoint, taskParams.debugDescription] logLevel:logLevel];
            handler(NO, nil, NO);
            return;
        }
        NSString * stageString = bfBaseUrlStaging;
        switch(weakSelf.stage) {
            case BFStageTest:
                stageString = bfBaseUrlStaging;
                break;
            case BFStageLive:
                stageString = bfBaseUrlProd;
                break;
            case BFStageInternalDevelopment:
                stageString = bfBaseUrlDev;
                break;
        }
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", stageString, endpoint]];
        
        // Convert the dictionary into JSON data.
        NSData *JSONData = [NSJSONSerialization dataWithJSONObject:taskParams
                                                           options:0
                                                             error:nil];
        
        // Create a POST request with our JSON as a request body.
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"POST";
        request.HTTPBody = JSONData;
        [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
        [request setValue:weakSelf.apiKey forHTTPHeaderField:@"API_KEY"];
        if (needAccessToken) {
            [request setValue:weakSelf.currentAccess.jwtAccessToken forHTTPHeaderField:@"BF_ACCESS_TOKEN"];
        }
        __block NSDate * startDate = [NSDate date];
        NSURLSessionTask * postTask = [weakSelf.httpSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            @try {
                // Performance evaluations
                if (weakSelf.currentLogLevel <= BFLogLevelDebug) {
                    NSDate * endDate = [NSDate date];
                    [weakSelf logRecord:[NSString stringWithFormat:@"api call: %@: %lf secs", endpoint, [endDate timeIntervalSinceDate:startDate]]];
                }
                if (!error) {
                    NSInteger statusCode = [response isKindOfClass:[NSHTTPURLResponse class]] ? [((NSHTTPURLResponse*)response) statusCode] : -1;
                    if (statusCode == 200) {
                        NSError * errorOnParsing;
                        NSDictionary * res = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options:0 error:&errorOnParsing];
                        
                        if (errorOnParsing) {
                            [weakSelf logRecord:[NSString stringWithFormat:@"api call failed on parsing result (%@; %@): %@",endpoint, taskParams.debugDescription, error.debugDescription] logLevel:logLevel];
                            // TODO: send analytics to our service
                            handler(NO, nil, NO);
                            return;
                        }
                        
                        if (res == nil) {
                            [weakSelf logRecord:[NSString stringWithFormat:@"api call failed on parsing result (%@; %@): %@",endpoint, taskParams.debugDescription, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]] logLevel:logLevel];
                            // TODO: send analytics to our service
                            // TODO: make at lest several attempts to make request
                            handler(NO, nil, NO);
                            return;
                        }
                        
                        // seems OK
                        [weakSelf logRecord:[NSString stringWithFormat:@"api call succeed: (%@; %@)", endpoint, taskParams] logLevel:BFLogLevelDebug];
                        if (weakSelf.currentLogLevel <= BFLogLevelTrace) {
                            NSString * results = [res debugDescription];
                            if (results.length > 10000) {
                                results = @"<Too long response to show>";
                            }
                            [weakSelf logRecord:[NSString stringWithFormat:@"Api call results: %@", results] logLevel:BFLogLevelTrace];
                        }
                        handler(YES, res, YES);
                    } else {
                        [weakSelf logRecord:[NSString stringWithFormat:@"api call failed with code %ld (%@; %@): %@", (long)statusCode, endpoint, taskParams.debugDescription, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]] logLevel:BFLogLevelWarning];
                        // TODO: send analytics to our service
                        handler(NO, nil, YES);
                    }
                } else {
                    [weakSelf logRecord:[NSString stringWithFormat:@"api call failed (%@; %@): %@",endpoint, taskParams.debugDescription, error.debugDescription] logLevel:logLevel];
                    // TODO: send analytics to our service
                    // TODO: make at lest several attempts to make request
                    handler(NO, nil, YES);
                }
            }
            @catch(NSException * exception) {
                [weakSelf logRecord:[NSString stringWithFormat:@"api call failed (%@; %@): %@",endpoint, taskParams.debugDescription, exception.debugDescription] logLevel:logLevel];
                handler(NO, nil, NO);
            }
        }];
        [postTask resume];

    } taskHandler:handler params:parametrs priority:priority taskTypeId:endpoint abadonnedCallback:nil];

}

-(NSInteger)currentTimestamp
{
    return (NSInteger)([[NSDate date] timeIntervalSince1970] * 1000000);
}

-(BOOL)handleOpeningUrl:(NSURL*)url withOptions:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    if (url.scheme.length > 1 && [[url.scheme substringWithRange:(NSRange){0, 2}] isEqualToString:@"bf"]) {
        BFModelAccessToken * newToken = [BFModelAccessToken tokenWithString:url.path.pathComponents.lastObject];
        if (newToken != nil) {
            self.currentAccess = newToken;
            [newToken storeToUserDefaults];
            
            // show success message
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"BfTokenSucceed.Title".bf_localized message:@"BfTokenSucceed.Description".bf_localized preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"BfTokenSucceed.Ok".bf_localized style:UIAlertActionStyleDefault handler:nil]];
            [self.uiManager.currentController presentViewController:alert animated:YES completion:nil];
            
            return YES;
        } else {
            // show error message
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"BfTokenFailed.Title".bf_localized message:@"BfTokenFailed.Description".bf_localized preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"BfTokenFailed.Ok".bf_localized style:UIAlertActionStyleDefault handler:nil]];
            [self.uiManager.currentController presentViewController:alert animated:YES completion:nil];
        }
    }
    return NO;
}

-(BOOL)accessIsUser
{
    return (self.currentAccess == nil) || ([self.currentAccess userToken]);
}

-(BOOL)accessShowsHeatmaps
{
    return (self.currentAccess != nil) && ([self.currentAccess haveAccessToHeatmaps]);
}
-(BOOL)accessHaveToWidgetGroupWithId:(NSString*)widgetGroupId
                         andObjectId:(NSString*)objectId
{
    return (self.currentAccess != nil) && ([self.currentAccess haveAccessToWidgetGroupWithId:widgetGroupId andObjectId:objectId]);
}

#pragma mark PublicApi

-(void)configureWithApiKey:(NSString*)apiKey widgetsOn:(BOOL)widgetsOn
{
    // To have a backwards compatibility with old FF builds here:
    // we just hardcode a temporary read access token here
    NSString * accessToken = @"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhIjoiZmYwMDAwMDAiLCJpYXQiOjE1MTgwMzEzODgsImp0aSI6IjYxRHN2bTdOZDRHUzZRNUhyLVZ1UVEiLCJuYmYiOjE1MTgwMzEzODgsInMiOiJnIn0.uTn8GyIHpp1uO6StgG6KSnbe5FaXmuO_yFSYI1k7FLcHb71gMWwtZEM4kMysGPde_pro6irINfPaG5Z1DBTkqiWLdKjVY5K8O7Ojk80-jDpDzw5VpCw_U7jO1tWDxYQZwLtLG1LYGM0zYpJgYugHUIUFfV4idb-Q4ATOa_9JWP2EIbyGBMTWowa9iSVW_UBGCOqURobHELc5sVtyElveOCt13SWpQYsDwW4EYZqlP8cUT9hAz6XScTiP9asiEVuknEOwmX0lThKD40FRc0girZQj-RyesKIIzZi2K9CE4qMOZFkabIBy_D7ork4QenoJkgpyjx4w5GYKQ7zBdU1rsw";
    
    if (widgetsOn) {
        [self configureWithApiKey:apiKey accessToken:accessToken];
    } else {
        [self configureWithApiKey:apiKey];
    }
}

-(void)configureWithApiKey:(NSString*)apiKey
{
    self.apiKey = apiKey;
    [self.uiManager startInteractionRecordings];
}

-(void)configureWithApiKey:(NSString*)apiKey accessToken:(NSString*)accessToken
{
    NSAssert(accessToken != nil, @"Access token shouldn't be nil");
    [self setCurrentAccessToken:accessToken];
    [self configureWithApiKey:apiKey];
}

-(void)setClientId:(NSString*)clientId version:(NSString*)version
{
    self.clientId = clientId;
    self.version = version;
}

-(void)setUserId:(NSString*)userId
{
    self.currentUserId = userId;
}

-(void)turnOnDebugLogging
{
    self.currentLogLevel = BFLogLevelTrace;
    [self.processingQueue setDebugLogging:YES];
}

-(void)turnOffDebugLogging
{
    self.currentLogLevel = BFLogLevelFatal;
    [self.processingQueue setDebugLogging:NO];
}

-(void)setHeatmapsDebugMode:(BFHeatmapsMode)mode
{
    self.uiManager.heatmapMode = mode;
}

-(void)appBecameActive
{
    [self.uiManager appBecameActive];
}

-(void)appBecameInactive
{
    [self.uiManager appBecameInactive];
}

-(NSString*)currentAccessToken
{
    return self.currentAccess.jwtAccessToken;
}

-(void)setCurrentAccessToken:(NSString*)accessToken
{
    self.currentAccess = [BFModelAccessToken tokenWithString:accessToken];
}

@end
