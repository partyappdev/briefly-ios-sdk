//
//  BFBarChartLayout.m
//  Briefly
//
//  Created by Artem Bondar on 02/02/2018.
//

#import "BFBarChartLayout.h"
#import "BFViewModelChart.h"

@implementation BFBarChartLayout

+(instancetype)layoutForModel:(BFViewModelChart*)model
                   withBounds:(CGRect)bounds
                   linesCount:(NSInteger)linesCount
             bottomLineHeight:(CGFloat)bottomLineHeight
                   leftOffset:(CGFloat)leftOffset
                  windowWidth:(NSInteger)windowWidth
                     maxValue:(CGFloat)maxValue
                  selectedIdx:(NSInteger)selectedIdx
{
    if (model == nil) {
        return nil;
    }

    BFBarChartLayout * lt = [BFBarChartLayout new];

    // Store base parametrs
    lt.barsOnScreen = MIN(model.dataWindowWidth, windowWidth);
    lt.leftOffset = leftOffset;
    lt.linesCount = linesCount;
    lt.bottomLine = bottomLineHeight;
    
    // Calculate base layout parametrs
    CGFloat bottomOffset = 2 * lt.bottomLine;
    lt.effectiveHeight = bounds.size.height - bottomOffset;
    lt.effectiveWidth = bounds.size.width - lt.leftOffset;
    lt.oneBarSize = lt.effectiveWidth / lt.barsOnScreen;
    lt.oneLineHeight = lt.effectiveHeight / lt.linesCount;
    
    // Now calculate the size for content
    lt.scrollContentSize = CGSizeMake(lt.oneBarSize * (CGFloat)(model.lengthX) + lt.leftOffset, bounds.size.height);
    lt.contentRect = CGRectMake(0, 0, lt.oneBarSize * (CGFloat)(model.lengthX) + lt.leftOffset, bounds.size.height);
    
    // And calculate bar layout insets
    UIEdgeInsets barInsets = lt.oneBarSize > 16 ? UIEdgeInsetsMake(0, (lt.oneBarSize - 16)*.5, 0, (lt.oneBarSize - 16)*.5) : UIEdgeInsetsMake(0, 2, 0, 2);

    // Create lines
    NSMutableArray * lines = [NSMutableArray array];
    for (NSInteger i = 0; i < lt.linesCount + 1; ++i) {
        BFBarChartLineLayout * line = [BFBarChartLineLayout new];
        line.pointStart = CGPointMake(lt.leftOffset, lt.effectiveHeight - i * lt.oneLineHeight);
        line.pointEnd = CGPointMake(lt.contentRect.size.width, lt.effectiveHeight - i * lt.oneLineHeight);
        [lines addObject:line];
    }
    lt.lines = lines;
    
    // Nothing else is layouted
    if (model == nil || model.isUnloadedState) {
        return lt;
    }
    
    // Create bars (all having lol)
    NSMutableArray * bars = [NSMutableArray array];
    NSArray<BFViewModelChartDataPoint*> *points = [model valuesAtRange:(BFRange){0, model.lengthX}];
    for (NSInteger i = 0; i < model.lengthX; ++i) {
        
        if (model.cummulative) {
            CGFloat maxY = lt.effectiveHeight;
            NSInteger lastMeaningfulBarIdx = 0;
        
            BFViewModelChartDataPoint * point = points[i];
            if (!point.dataLoaded) {
                continue;
            }
            for (NSInteger b = 0; b < point.multivalue.count; ++b) {
                if ([(NSNumber*)(point.multivalue[b]) doubleValue] != 0) {
                    lastMeaningfulBarIdx = b;
                }
            }
            // now build bars by itself
            for (NSInteger b = 0; b < point.multivalue.count; ++b) {
                CGFloat dY = ([(NSNumber*)(point.multivalue[b]) doubleValue] / maxValue) * lt.effectiveHeight;
            
                if (dY == 0) {
                    continue;
                }
                BFBarChartBarLayout * bar = [BFBarChartBarLayout new];
                CGFloat minY = maxY - dY;
                bar.frame = CGRectMake(lt.leftOffset + i * lt.oneBarSize, minY, lt.oneBarSize, dY);
                bar.frame = UIEdgeInsetsInsetRect(bar.frame, barInsets);
                bar.roundingSize = b == lastMeaningfulBarIdx ? CGSizeMake(4, 4) : CGSizeMake(0, 0);
                bar.barIdx = i;
                bar.barSegmentIdx = b;
                maxY = minY;
                [bars addObject:bar];
            }
        } else {
            // It's a bit simplier
            BFViewModelChartDataPoint * point = points[i];
            BOOL thinLines = (lt.oneBarSize / point.multivalue.count) < 16;
            CGFloat interBarInset = thinLines ? 1.5 : 3;
            CGFloat actualOneBarWidth = point.multivalue.count > 0 ? ((lt.oneBarSize - 2*interBarInset) / point.multivalue.count) : 0;
            for (NSInteger b = 0; b < point.multivalue.count; ++b) {
                CGFloat dY = ([(NSNumber*)(point.multivalue[b]) doubleValue] / maxValue) * lt.effectiveHeight;
                
                if (dY == 0) {
                    continue;
                }
                BFBarChartBarLayout * bar = [BFBarChartBarLayout new];
                CGFloat minY = lt.effectiveHeight - dY;
                bar.frame = CGRectMake(lt.leftOffset + i * lt.oneBarSize + interBarInset + actualOneBarWidth * b, minY, actualOneBarWidth, dY);
                bar.roundingSize = thinLines ? CGSizeMake(4, 4) : CGSizeMake(6, 6);
                bar.barIdx = i;
                bar.barSegmentIdx = b;
                bar.tiny = thinLines;
                [bars addObject:bar];
            }
        }
    }
    lt.bars = bars;
    
    // Create left labels
    NSMutableArray * leftLabels = [NSMutableArray array];
    if (maxValue != 0) {
        // Create labels container
        lt.leftBarRect = CGRectMake(0, 0, lt.leftOffset, bounds.size.height);
        
        // Create labels
        for (NSInteger i = 0; i < lt.linesCount+1; ++i) {
            BFBarChartLabelLayout * leftLabel = [BFBarChartLabelLayout new];
            CGFloat yCoord = lt.effectiveHeight / lt.linesCount * i;
            CGFloat height = 18;
            if (i > 0) {
                yCoord -= height / 2;
                /*if (i == lt.linesCount) {
                    yCoord -= height / 2;
                }*/
            }
            leftLabel.idx = i;
            leftLabel.frame = CGRectMake(0, yCoord, lt.leftOffset - 8, height);
            [leftLabels addObject:leftLabel];
        }
    }
    lt.leftLabels = leftLabels;
    
    // Create bottom labels
    NSArray<BFViewModelChartSection*> * sections = [model sectionsAtRange:(BFRange){0, model.lengthX}];
    CGFloat datesHeight = lt.bottomLine;
    
    // enumerate all sections
    NSMutableArray * sectionBarsSelected = [NSMutableArray array];
    NSMutableArray * sectionBars = [NSMutableArray array];
    NSMutableArray * sectionLabels = [NSMutableArray array];
    NSMutableArray * dateLabels = [NSMutableArray array];
    [sections enumerateObjectsUsingBlock:^(BFViewModelChartSection * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        // firstly add the sections backgrounds
        BFBarChartBarLayout * sectionBar = [BFBarChartBarLayout new];
        CGFloat startX = lt.leftOffset + ((CGFloat)obj.sectionStartX) * lt.oneBarSize;
        CGFloat width = ((CGFloat)obj.sectionDatesLabel.count) * lt.oneBarSize;
        sectionBar.frame = CGRectMake(startX, lt.effectiveHeight, width, datesHeight);
        sectionBar.barIdx = idx;
        [sectionBars addObject:sectionBar];
        
        // if this section has intersection with selected period
        // also add a period mask
        BFRange selectedSubrange = BFRangeIntersection([model startRange], BFRangeMake(obj.sectionStartX, obj.sectionDatesLabel.count));
        if (selectedIdx != NSNotFound) {
            selectedSubrange = BFRangeIntersection((BFRange){selectedIdx,1}, BFRangeMake(obj.sectionStartX, obj.sectionDatesLabel.count));
        }
        if (selectedSubrange.length > 0) {
            BFBarChartBarLayout * sectionBarSelected = [BFBarChartBarLayout new];
            CGFloat startX = lt.leftOffset + ((CGFloat)selectedSubrange.location) * lt.oneBarSize;
            CGFloat width = ((CGFloat)selectedSubrange.length) * lt.oneBarSize;
            sectionBarSelected.frame = CGRectMake(startX, lt.effectiveHeight, width, datesHeight);
            sectionBarSelected.barIdx = idx;
            [sectionBarsSelected addObject:sectionBarSelected];
        }
        
        // add section header
        BFBarChartLabelLayout * sectionLabel = [BFBarChartLabelLayout new];
        sectionLabel.text = obj.sectionLabel;
        sectionLabel.frame = CGRectOffset(sectionBar.frame, 0, lt.bottomLine);
        [sectionLabels addObject:sectionLabel];

        // add dates labels
        CGFloat estimatedOneLabelWidth = 16;
        BOOL onlyEvenLabels = lt.oneBarSize < estimatedOneLabelWidth;
        [obj.sectionDatesLabel enumerateObjectsUsingBlock:^(NSString * _Nonnull lblText, NSUInteger idx, BOOL * _Nonnull stop) {
            
            BFBarChartLabelLayout * dateLabel = [BFBarChartLabelLayout new];
            NSInteger i = idx + obj.sectionStartX;
            if (i < 0 || i >= model.lengthX) {
                return;
            }
            if (onlyEvenLabels && i + 1 == selectedIdx && i % 2 == 0) {
                // skip even before selected value
                return;
            }
            if (onlyEvenLabels && i - 1 == selectedIdx && i % 2 == 0) {
                // skip even after selected value
                return;
            }
            if (onlyEvenLabels && i % 2 == 1 && i != selectedIdx) {
                // skip odd non selected
                return;
            }
            dateLabel.text = lblText;
            dateLabel.idx = i;
            CGFloat x = onlyEvenLabels ? lt.leftOffset + ((CGFloat)i - 0.5) * lt.oneBarSize : lt.leftOffset + (CGFloat)i * lt.oneBarSize;
            CGFloat width = onlyEvenLabels ? 2 * lt.oneBarSize : lt.oneBarSize;
            dateLabel.frame = CGRectMake(x, lt.effectiveHeight, width, datesHeight);
            dateLabel.highlighted = BFLocationInRange(i, selectedSubrange);
            [dateLabels addObject:dateLabel];
        }];
    }];
    lt.datesLabels = dateLabels;
    lt.sectionBars = sectionBars;
    lt.sectionLabels = sectionLabels;
    lt.sectionSelectionsBars = sectionBarsSelected;
    
    return lt;
}

-(CGPoint)contentOffsetForRange:(BFRange)range
{
    // if the range is longer, than max width - then split it to the end
    if (range.length > self.barsOnScreen) {
        NSInteger diff = range.length - self.barsOnScreen;
        return CGPointMake((CGFloat)(1) * (CGFloat)(range.location + diff) * self.oneBarSize, 0);
    } else {
        return CGPointMake((CGFloat)(1) * (CGFloat)(range.location) * self.oneBarSize, 0);
    }
}

-(BFBarChartBarLayout*)barLayoutForIdx:(NSInteger)idx segmentIdx:(NSInteger)segmentIdx
{
    for (BFBarChartBarLayout * layout in self.bars) {
        if (layout.barIdx == idx && layout.barSegmentIdx == segmentIdx) {
            return layout;
        }
    }
    return nil;
}

-(CGRect)rectForRange:(BFRange)range
{
    return CGRectMake(self.leftOffset + range.location * self.oneBarSize, 0, self.oneBarSize * range.length, self.effectiveHeight);
}

-(CGRect)boundsForData
{
    return CGRectMake(self.leftOffset, 0, self.effectiveWidth, self.effectiveHeight);
}

-(BFRange)rangeForContentOffset:(CGFloat)offset
{
    NSInteger idx = (NSInteger)round(offset / self.oneBarSize);
    NSInteger width = self.effectiveWidth / self.oneBarSize;
    return (BFRange){idx, width};
}

-(NSInteger)barIdxForPoint:(CGPoint)point
{
    return (point.x - self.leftOffset) / self.oneBarSize;
}

-(BOOL)isDatesBarAtPoint:(CGPoint)point
{
    if (point.y > self.effectiveHeight) {
        return YES;
    }
    return NO;
}

@end

@implementation BFBarChartLabelLayout
@end

@implementation BFBarChartBarLayout
@end

@implementation BFBarChartLineLayout
@end
