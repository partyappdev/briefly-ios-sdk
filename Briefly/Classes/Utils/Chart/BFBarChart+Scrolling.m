//
//  BFBarChart+Scrolling.m
//  Briefly
//
//  Created by Artem Bondar on 31/01/2018.
//

#import "BFBarChart+Scrolling.h"
#import "BFBarChart+Labels.h"
#import "BFBarChart+Private.h"

@implementation BFBarChart (Scrolling)


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateLoadingMoreIndicatorState];
    [self loadMoreIfNeeded];
    [self layoutSectionLabels];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset

{
    NSInteger barIdx = (NSInteger)round((targetContentOffset->x) / self.layout.oneBarSize);
    targetContentOffset->x = ((CGFloat)barIdx * self.layout.oneBarSize);
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.scrollingOn = YES;
    // and drop the selection
    //[self dropSelection];
}

// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    self.scrollingOn = NO;
    if (!decelerate) {
        [self endScrolling];
    } else {
        self.deceleratingOn = YES;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.deceleratingOn = NO;
    [self endScrolling];
}

-(void)endScrolling
{
    if (self.deceleratePreventedUpdate) {
        self.deceleratePreventedUpdate = NO;
        [self updateBars];
    }
    NSInteger barIdx = [self.layout rangeForContentOffset:self.scrollView.contentOffset.x].location;
    [self scrollingFinishedAtBar:barIdx];
}

@end
