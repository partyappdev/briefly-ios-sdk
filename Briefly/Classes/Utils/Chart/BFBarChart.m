//
//  BFBarChart.m
//  Briefly
//
//  Created by Artem Bondar on 12.01.2018.
//

#import "BFBarChart.h"
#import "BFBarChart+Labels.h"
#import "BFBarChart+Scrolling.h"
#import "BFBarChart+Private.h"

#import "NSString+Utils.h"
#import "UIColor+BFPalette.h"

@interface BFBarChart () <BFViewModelChartDelegate>

@property (weak, nonatomic) CALayer * currentGraph;

@end

@implementation BFBarChart

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    // base params
    self.maxBarsWidth = 10;
    self.linesCount = 4;
    self.bottomLine = 16;
    self.leftOffset = 54;

    self.clipsToBounds = YES;

    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [self addGestureRecognizer:recognizer];
}

-(void)setModel:(BFViewModelChart *)model
{
    _model = model;
    self.lastSteadyDataRange = [self.model startRange];
    [model fetchFirstRegionIfNeeded];
    [model subscribeForChanges:self];
    [self chartDataChanged];
}

-(void)scrollingFinishedAtBar:(NSInteger)barIdx
{
    [self animatedChangeOfSteadyRange:(BFRange){barIdx, self.layout.barsOnScreen}];
    [self.delegate stadyRangeChangedFor:self.lastSteadyDataRange];
}

-(NSNumber*)summValueForMultival:(NSArray*)multival
{
    NSNumber * pointsMaxValue  = @(0);
    for (NSNumber * num in multival) {
        pointsMaxValue = @(num.doubleValue + pointsMaxValue.doubleValue);
    }
    return pointsMaxValue;
}

-(void)animatedChangeOfSteadyRange:(BFRange)newRange
{
    CGFloat maxValue = [self.model maxValueAtRange:newRange].doubleValue;
    if (maxValue == 0) {
        maxValue = self.lastSteadyMaxValue;
    }
    // this mean we don't have real changes in data - only layout
    BFBarChartLayout * newLayout = [BFBarChartLayout layoutForModel:self.model withBounds:self.bounds linesCount:self.linesCount bottomLineHeight:self.bottomLine leftOffset:self.leftOffset windowWidth:self.maxBarsWidth maxValue:maxValue selectedIdx:[self.model idxForDate:self.selectedDate]];
    
    // now animate the bars, and update left labels values
    for (BFBarChartBarLayout * oldBarLayout in self.layout.bars) {
        BFBarChartBarLayout * newBarLayout = [newLayout barLayoutForIdx:oldBarLayout.barIdx segmentIdx:oldBarLayout.barSegmentIdx];
        
        // Find a layer
        CAShapeLayer * barLayer = nil;
        for (CAShapeLayer * sublayer in self.currentGraph.sublayers) {
            if ([(NSNumber*)[sublayer valueForKey:@"bar_idx"] isEqualToNumber:@(oldBarLayout.barIdx)] && [(NSNumber*)[sublayer valueForKey:@"bar_segment_idx"] isEqualToNumber:@(oldBarLayout.barSegmentIdx)]) {
                barLayer = sublayer;
                break;
            }
        }
        if (barLayer) {
            CABasicAnimation *morph = [CABasicAnimation animationWithKeyPath:@"path"];
            morph.duration  = 0.3;
            morph.removedOnCompletion = NO;
            morph.fromValue = (__bridge id)barLayer.path;
            morph.toValue   = (__bridge id)[self pathForBarWithLayoutAttribute:newBarLayout].CGPath;
            [barLayer addAnimation:morph forKey:@"change line path"];
            barLayer.path = [self pathForBarWithLayoutAttribute:newBarLayout].CGPath;
        }
    }
    
    // and change label's names
    NSArray * labelTexts = [self.model linesLabelsAtRange:newRange forLinesCount:self.layout.linesCount];
    for (UILabel * lbl in self.viewLeftLabels.subviews) {
        NSInteger i = lbl.tag;
        lbl.text = labelTexts[i];
    }

    self.lastSteadyDataRange = newRange;
    self.lastSteadyMaxValue = maxValue;
    self.layout = newLayout;
}

-(UIBezierPath*)pathForBarWithLayoutAttribute:(BFBarChartBarLayout*)layout
{
    return [UIBezierPath bezierPathWithRoundedRect:layout.frame byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:layout.roundingSize];
}

- (void)updateBars
{
    // Store preious content offset to avoid jumping
    BOOL needPreserveContentOffset = NO;
    CGPoint contentOffset = self.scrollView.contentOffset;;
    //
    // If we'll try to preserve the content offset, when scrolling
    // we'll get the wrong dates-range (offset = date-range * oneBarSize)
    //
    if (self.scrollView && self.lastPresentedOneBarSize == self.layout.oneBarSize) {
        needPreserveContentOffset = YES;
    }

    // To avoid excessive layouts remember last chart configuration
    self.lastLayoutBounds = self.bounds;
    self.lastPresentedOneBarSize = self.layout.oneBarSize;
    
    // Create scrollable congtentView if needed
    if (self.scrollView == nil) {
        UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:self.bounds];
        scrollview.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        scrollview.delegate = self;
        scrollview.showsHorizontalScrollIndicator = NO;
        scrollview.contentSize = self.layout.scrollContentSize;
        self.scrollView = scrollview;
        [self addSubview:scrollview];
    } else {
        self.scrollView.contentSize = self.layout.scrollContentSize;
    }

    if (self.viewContent == nil) {
        UIView * contentScrollable = [[UIView alloc] init];
        contentScrollable.frame = self.layout.contentRect;
        contentScrollable.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.viewContent = contentScrollable;
        [self.scrollView addSubview:contentScrollable];
    } else {
        // remove all subviews
        self.viewContent.frame = self.layout.contentRect;
        NSArray * subviews = [NSArray arrayWithArray:self.viewContent.subviews];
        for (UIView * subview in subviews) {
            [subview removeFromSuperview];
        }
        [self.currentGraph removeFromSuperlayer];
    }

    if (!needPreserveContentOffset) {
        self.scrollView.contentOffset = [self.layout contentOffsetForRange:self.lastSteadyDataRange];
    }

    // Create the base layer
    CALayer * baseLayer = [CALayer layer];

    // Create lines
    for (BFBarChartLineLayout * lineLayout in self.layout.lines) {
        CAShapeLayer * line = [CAShapeLayer layer];
        UIBezierPath * path = [UIBezierPath bezierPath];
        [path moveToPoint:lineLayout.pointStart];
        [path addLineToPoint:lineLayout.pointEnd];
        line.path = path.CGPath;
        line.strokeColor = [UIColor bf_widgetPreferencesLightGray].CGColor;
        [baseLayer addSublayer:line];
    }
    
    // TODO: return selection/loading mask
    // Create selection mask
    /*if (self.selectedBarIdx != nil) {
        NSInteger i = self.selectedBarIdx.integerValue;
        CGFloat y = 0;
        NSNumber* dataVal = [self summValueForMultival:self.values[i]];
        BOOL isMax = [dataVal isEqualToNumber:@(self.maxValue)];
        CAShapeLayer * bar = [CAShapeLayer layer];
        CGRect barRect = CGRectMake(leftOffset + i * self.oneBarSize, y, self.oneBarSize, self.effectiveHeight - y);
        barRect = UIEdgeInsetsInsetRect(barRect, barInsets);
        UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:barRect byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(0, 0)];
        bar.path = path.CGPath;
        bar.lineCap = kCALineCapRound;
        bar.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:4],[NSNumber numberWithInt:4], nil];
        bar.strokeColor = !isMax ? [UIColor bf_widgetPreferencesLightGray].CGColor : [UIColor clearColor].CGColor;
        bar.fillColor = [[UIColor bf_widgetPreferencesLightGray] colorWithAlphaComponent:0.5].CGColor;
        [baseLayer addSublayer:bar];
    }*/
    
    // Create bars (all having lol)
    NSInteger selectedIdx = [self.model idxForDate:self.selectedDate];
    for (BFBarChartBarLayout * barLayout in self.layout.bars) {
        CAShapeLayer * bar = [CAShapeLayer layer];
        [bar setValue:@(barLayout.barIdx) forKey:@"bar_idx"];
        [bar setValue:@(barLayout.barSegmentIdx) forKey:@"bar_segment_idx"];
        
        bar.path = [self pathForBarWithLayoutAttribute:barLayout].CGPath;
        
        if (barLayout.barIdx == selectedIdx) {
            bar.strokeColor = [UIColor bf_widgetColorSelectedWithIdx:barLayout.barSegmentIdx].CGColor;
            bar.fillColor = [UIColor bf_widgetColorSelectedWithIdx:barLayout.barSegmentIdx].CGColor;
        } else if (selectedIdx == NSNotFound) {
            bar.strokeColor = [UIColor bf_widgetColorWithIdx:barLayout.barSegmentIdx].CGColor;
            bar.fillColor = [UIColor bf_widgetColorBarWithIdx:barLayout.barSegmentIdx].CGColor;
        } else {
            bar.strokeColor = [[UIColor bf_widgetColorWithIdx:barLayout.barSegmentIdx] colorWithAlphaComponent:0.4].CGColor;
            bar.fillColor = [[UIColor bf_widgetColorBarWithIdx:barLayout.barSegmentIdx] colorWithAlphaComponent:0.4].CGColor;
        }
        if (barLayout.tiny) {
            bar.strokeColor = [UIColor clearColor].CGColor;
        }
        [baseLayer addSublayer:bar];
    }
    
    // Create left labels
    [self.viewLeftLabels removeFromSuperview];
    if (self.lastSteadyMaxValue != 0) {
        
        // Create labels container
        UIView * leftLabels = [[UIView alloc] initWithFrame:self.layout.leftBarRect];
        leftLabels.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin;
        [self addSubview:leftLabels];
        leftLabels.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
        self.viewLeftLabels = leftLabels;
        
        // Create labels
        NSArray * labelTexts = [self.model linesLabelsAtRange:self.lastSteadyDataRange forLinesCount:self.layout.linesCount];
        for (BFBarChartLabelLayout * labelLayout in self.layout.leftLabels) {
            UILabel * label = [self leftLabel];
            label.tag = labelLayout.idx;
            label.text = labelTexts[labelLayout.idx];
            label.frame = labelLayout.frame;
            [leftLabels addSubview:label];
        }
    }

    // Create bottom labels backgrounds
    for (BFBarChartBarLayout * sectionBgLayout in self.layout.sectionBars) {
        UIColor * clr = sectionBgLayout.barIdx % 2 != 1 ? [UIColor bf_widgetPreferencesGraphSection] : [UIColor bf_widgetPreferencesGraphSectionEven];
        CAShapeLayer * bar = [CAShapeLayer layer];
        UIBezierPath * path = [UIBezierPath bezierPathWithRect:sectionBgLayout.frame];
        bar.path = path.CGPath;
        bar.fillColor = [clr colorWithAlphaComponent:1.0].CGColor;
        [baseLayer addSublayer:bar];
    }
    
    // Create bottom labels selection section backgrounds
    for (BFBarChartBarLayout * sectionBgLayout in self.layout.sectionSelectionsBars) {
        UIColor * clr = sectionBgLayout.barIdx % 2 != 1 ? [UIColor bf_BaseUIColor] : [UIColor bf_BaseUIColorDarkened];
        CAShapeLayer * bar = [CAShapeLayer layer];
        UIBezierPath * path = [UIBezierPath bezierPathWithRect:sectionBgLayout.frame];
        bar.path = path.CGPath;
        bar.fillColor = [clr colorWithAlphaComponent:1.0].CGColor;
        [baseLayer addSublayer:bar];
    }
    
    // Crate bottom section labels
    CGFontRef cgFont = CGFontCreateWithFontName((CFStringRef)[UIFont boldSystemFontOfSize:10].fontName);
    for (BFBarChartLabelLayout * sectionLabelLayout in self.layout.sectionLabels) {

        CATextLayer * textLayer = [CATextLayer layer];
        textLayer.string = sectionLabelLayout.text;
        textLayer.font = cgFont;
        textLayer.fontSize = 10;
        textLayer.alignmentMode = @"center";
        textLayer.contentsScale = [UIScreen mainScreen].scale;
        textLayer.foregroundColor = [self labelTextColor].CGColor;
        textLayer.frame = sectionLabelLayout.frame;
        [baseLayer addSublayer:textLayer];
    }
    [self layoutSectionLabels];

    // Create dates labels

    for (BFBarChartLabelLayout * dateLabelLayout in self.layout.datesLabels) {
        CATextLayer * textLayer = [CATextLayer layer];
        textLayer.string = dateLabelLayout.text;
        textLayer.font = cgFont;
        textLayer.fontSize = 10;
        textLayer.alignmentMode = @"center";
        textLayer.contentsScale = [UIScreen mainScreen].scale;
        if (dateLabelLayout.highlighted) {
            textLayer.foregroundColor = [self labelTextColorHighlighted].CGColor;
        } else {
            textLayer.foregroundColor = [self labelTextColor].CGColor;
        }
        textLayer.frame = dateLabelLayout.frame;
        [baseLayer addSublayer:textLayer];
    }
    
    [self.viewContent.layer insertSublayer:baseLayer atIndex:0];
    self.currentGraph = baseLayer;
    
    if (needPreserveContentOffset) {
        self.scrollView.contentOffset = contentOffset;
    }
}

-(CGRect)dataRectWithUnloadedData
{
    CGRect unloadedDataRect = CGRectNull;
    CGRect dataRect = CGRectOffset(self.layout.boundsForData, self.scrollView.contentOffset.x, 0);
    CGRect loadedRect = [self.layout rectForRange:self.model.loadedRange];
    
    // Check if our data window is larger, than loaded data range
    if (CGRectGetMinX(dataRect) < CGRectGetMinX(loadedRect)) {
        // we have a place for
        unloadedDataRect = CGRectMake(CGRectGetMinX(dataRect), CGRectGetMinY(dataRect), CGRectGetMinX(loadedRect) - CGRectGetMinX(dataRect), CGRectGetHeight(dataRect));
    } else if (CGRectGetMaxX(dataRect) > CGRectGetMaxX(loadedRect)) {
        unloadedDataRect = CGRectMake(CGRectGetMaxX(loadedRect), CGRectGetMinY(dataRect), CGRectGetMaxX(dataRect) - CGRectGetMaxX(loadedRect), CGRectGetHeight(dataRect));
    }
    return unloadedDataRect;
}

-(void)updateLoadingMoreIndicatorState
{
    // it's easy, just try to find a place
    CGRect unloadedDataRect = CGRectIntersection([self dataRectWithUnloadedData], CGRectOffset(self.layout.boundsForData, self.scrollView.contentOffset.x, 0));
    
    if (CGRectGetWidth(unloadedDataRect) > 4) {
        // add/start indicator
        if (!self.activityIndicatorLoadingMore) {
            UIActivityIndicatorView * act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            act.hidesWhenStopped = YES;
            act.frame = CGRectMake(0, 0, 24, 24);
            [self.viewContent addSubview:act];
            self.activityIndicatorLoadingMore = act;
        }
        self.activityIndicatorLoadingMore.center = CGPointMake(unloadedDataRect.origin.x + unloadedDataRect.size.width/2, unloadedDataRect.origin.y + unloadedDataRect.size.height/2);
        [self.activityIndicatorLoadingMore startAnimating];
    } else {
        // stop indicator
        [self.activityIndicatorLoadingMore stopAnimating];
    }
}

-(void)chartDataChanged
{
    BOOL needAnimatedChangeOfMax = YES;
    if (!self.model.isUnloadedState && self.lastSteadyMaxValue == 0) {
        CGFloat newMax = [self.model maxValueAtRange:self.lastSteadyDataRange].doubleValue;
        if (newMax != 0) {
            self.lastSteadyMaxValue = newMax;
            needAnimatedChangeOfMax = NO;
        }
    }
    self.layout = [BFBarChartLayout layoutForModel:self.model withBounds:self.bounds linesCount:self.linesCount bottomLineHeight:self.bottomLine leftOffset:self.leftOffset windowWidth:self.maxBarsWidth maxValue:self.lastSteadyMaxValue selectedIdx:[self.model idxForDate:self.selectedDate]];
    
    [self updateBars];
    
    if (needAnimatedChangeOfMax) {
        [self animatedChangeOfSteadyRange:self.lastSteadyDataRange];
    }
    
    // TODO handle failed state
    //
    // and notify the delegate
    [self.delegate loadingStateChangedTo:[self.model isUnloadedState] && !self.model.lastRequestFailed lastFailed:self.model.lastRequestFailed];
    [self.delegate stadyRangeChangedFor:self.lastSteadyDataRange];
}

-(void)loadMoreIfNeeded
{
    if (self.scrollView.contentSize.width > 0) {
        [self.model loadDataRangeIfNeeded:[self.layout rangeForContentOffset:self.scrollView.contentOffset.x]];
    }
}

-(void)layoutSubviews
{
    if (!CGRectEqualToRect(self.lastLayoutBounds, self.bounds)) {
        [self updateBars];
    }
    [self layoutSectionLabels];
    [super layoutSubviews];
}

-(void)panned:(UIPanGestureRecognizer*)recognizer
{
    // Don't allow selection on empty data
    /*if (!self.values.count || self.maxValue == 0.0) {
        return;
    }

    CGPoint pressPoint = [recognizer locationInView:self];
    CGFloat idx = (pressPoint.x - self.leftOffset) / self.oneBarSize;
    
    if (idx >= 0 && idx < self.values.count) {
        NSInteger intIdx = idx;
        if (self.selectedBarIdx.integerValue != intIdx) {
            [self.delegate selectedBar:@(intIdx)];
        } else {
            // do nothing
        }
    }*/
}

-(void)dropSelection
{
    if (self.selectedDate) {
        self.selectedDate = nil;
        [self chartDataChanged];
        [self.delegate selectedDate:self.selectedDate];
    }
}

-(void)tapped:(UITapGestureRecognizer*)recognizer
{
    // Don't allow selection on empty data
    if (self.model.isUnloadedState) {
        return;
    }

    // Handle selection
    CGPoint pressPoint = [recognizer locationInView:self.viewContent];
    
    if ([self.layout isDatesBarAtPoint:pressPoint]) {
        [self.delegate selectedDatesField];
        return;
    }
    CGFloat idx = [self.layout barIdxForPoint:pressPoint];
    
    if ([self.model idxForDate:self.selectedDate] != idx) {
        // select other
        self.selectedDate = [self.model dateForIdx:idx];
    } else {
        // deselect previous
        self.selectedDate = nil;
    }
    [self chartDataChanged];
    [self.delegate selectedDate:self.selectedDate];
}

@end
