//
//  BFBarChart+Labels.m
//  Briefly
//
//  Created by Artem Bondar on 25/01/2018.
//

#import "BFBarChart+Labels.h"
#import "BFBarChart+Private.h"

#import "UIColor+BFPalette.h"

@implementation BFBarChart (Labels)

-(void)dropAllLabels
{
    NSMutableArray * labels = [NSMutableArray array];
    for (UILabel * label in self.subviews) {
        if ([label isKindOfClass:[UILabel class]]) {
            [labels addObject:label];
        }
    }
    
    for (UILabel * label in labels) {
        [label removeFromSuperview];
    }
}

-(UIColor*)labelTextColor
{
    return [UIColor bf_widgetPreferencesGraphSectionText];
}

-(UIColor*)labelTextColorHighlighted
{
    return [UIColor bf_widgetTextColor];
}

-(UILabel*)noDataLabel
{
    for (UILabel * label in self.subviews) {
        if (label.tag == 100000000) {
            return label;
        }
    }
    UILabel * lbl = [[UILabel alloc] init];
    lbl.translatesAutoresizingMaskIntoConstraints = NO;
    lbl.textColor = [UIColor bf_widgetColorBarWithIdx:0];
    lbl.font = [UIFont systemFontOfSize:14];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.tag = 100000000;
    return lbl;
}

-(UILabel*)leftLabel
{
    UILabel * lbl = [[UILabel alloc] init];
    lbl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    lbl.textColor = [self labelTextColor];
    lbl.font = [UIFont systemFontOfSize:12];
    lbl.textAlignment = NSTextAlignmentRight;
    return lbl;
}

-(UILabel*)bottomLabel
{
    UILabel * lbl = [[UILabel alloc] init];
    lbl.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    lbl.textColor = [self labelTextColor];
    lbl.font = [UIFont boldSystemFontOfSize:10];
    lbl.textAlignment = NSTextAlignmentCenter;
    return lbl;
}

-(BFBarChartSectionLabel*)monthLabel
{
    BFBarChartSectionLabel * lbl = [[BFBarChartSectionLabel alloc] init];
    lbl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    lbl.textColor = [self labelTextColor];
    lbl.font = [UIFont boldSystemFontOfSize:10];
    lbl.textAlignment = NSTextAlignmentCenter;
    return lbl;
}

-(UILabel*)bottomLabelWithTag:(NSInteger)tag
{
    for (UILabel * label in self.subviews) {
        if (label.tag == tag + 10000) {
            return label;
        }
    }
    UILabel * lbl = [self bottomLabel];
    lbl.tag = tag + 10000;
    return lbl;
}

-(UILabel*)leftLabelWithTag:(NSInteger)tag
{
    for (UILabel * label in self.subviews) {
        if (label.tag == tag) {
            return label;
        }
    }
    UILabel * lbl = [self leftLabel];
    lbl.tag = tag;
    return lbl;
}

-(UILabel*)monthLabelWithTag:(NSInteger)tag
{
    for (UILabel * label in self.subviews) {
        if (label.tag == tag + 10000000) {
            return label;
        }
    }
    UILabel * lbl = [self leftLabel];
    lbl.tag = tag + 10000000;
    return lbl;
}

-(void)layoutSectionLabels
{
    for (BFBarChartSectionLabel * label in self.viewContent.subviews) {
        if (![label isKindOfClass:[BFBarChartSectionLabel class]]) {
            continue;
        }
        CGRect screenRect = CGRectIntersection(self.scrollView.bounds, label.layout.frame);
        if (screenRect.size.width > 0) {
            label.frame = screenRect;
        } else {
            label.frame = label.layout.frame;
        }
    }
}


@end


@implementation BFBarChartSectionLabel

@end
