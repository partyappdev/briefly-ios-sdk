//
//  BFBarChart.h
//  Briefly
//
//  Created by Artem Bondar on 12.01.2018.
//

#import <UIKit/UIKit.h>
#import "BFViewModelChart.h"
#import "BFRange.h"

@protocol BFBarChartDelegate

-(void)selectedDatesField;
-(void)selectedDate:(NSDate*)date;
-(void)loadingStateChangedTo:(BOOL)loading lastFailed:(BOOL)failed;
-(void)stadyRangeChangedFor:(BFRange)range;

@end

@interface BFBarChart : UIView

@property (strong, nonatomic) BFViewModelChart * model;
@property (strong, nonatomic) NSDate * selectedDate;
@property (strong, nonatomic) IBOutlet id <BFBarChartDelegate> delegate;

@end
