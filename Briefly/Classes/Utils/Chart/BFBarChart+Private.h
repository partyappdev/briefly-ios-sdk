//
//  BFBarChart+Private.h
//  Briefly
//
//  Created by Artem Bondar on 31/01/2018.
//

#import "BFBarChart.h"
#import "BFBarChartLayout.h"
#import "BFRange.h"

@interface BFBarChart ()

@property (nonatomic) NSInteger maxX;

// Base chart parametrs
@property (nonatomic) NSInteger maxBarsWidth;
@property (nonatomic) CGFloat leftOffset;
@property (nonatomic) NSInteger linesCount;
@property (nonatomic) CGFloat bottomLine;

// Current layout
@property (strong, nonatomic) BFBarChartLayout * layout;
@property (nonatomic) CGRect lastLayoutBounds;

// Data representation parametrs
@property (nonatomic) BFRange lastSteadyDataRange;
@property (nonatomic) CGFloat lastSteadyMaxValue;
@property (nonatomic) CGFloat lastPresentedOneBarSize;

// Internal views
@property (weak, nonatomic) UIScrollView * scrollView;
@property (weak, nonatomic) UIView * viewContent;
@property (weak, nonatomic) UIView * viewLeftLabels;
@property (weak, nonatomic) UIActivityIndicatorView * activityIndicatorLoadingMore;

@property (nonatomic) CGFloat scrollingOn;
@property (nonatomic) CGFloat deceleratingOn;
@property (nonatomic) CGFloat deceleratePreventedUpdate;

- (void)scrollingFinishedAtBar:(NSInteger)barIdx;
- (void)updateLoadingMoreIndicatorState;
- (void)loadMoreIfNeeded;
- (void)updateBars;
-(void)dropSelection;

@end
