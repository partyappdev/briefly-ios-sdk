//
//  BFBarChartLayout.h
//  Briefly
//
//  Created by Artem Bondar on 02/02/2018.
//

#import <UIKIt/UIKIt.h>
#import "BFRange.h"

// external dependencies
@class BFViewModelChart;

// internal classes
@class BFBarChartLabelLayout;
@class BFBarChartBarLayout;
@class BFBarChartLineLayout;

///
///
/// Layout implementations for the chart
///
///
@interface BFBarChartLayout : NSObject

//
// constructor
//
+(instancetype)layoutForModel:(BFViewModelChart*)model
                   withBounds:(CGRect)bounds
                   linesCount:(NSInteger)linesCount
             bottomLineHeight:(CGFloat)bottomLineHeight
                   leftOffset:(CGFloat)leftOffset
                  windowWidth:(NSInteger)windowWidth
                     maxValue:(CGFloat)maxValue
                  selectedIdx:(NSInteger)selectedIdx;

//
// base parametrs
//
@property (nonatomic) CGFloat leftOffset;
@property (nonatomic) NSInteger linesCount;
@property (nonatomic) CGFloat bottomLine;
@property (nonatomic) NSInteger barsOnScreen;

//
// calculated layout parametrs
//
@property (nonatomic) CGFloat effectiveHeight;
@property (nonatomic) CGFloat effectiveWidth;
@property (nonatomic) CGFloat oneBarSize;
@property (nonatomic) CGFloat oneLineHeight;

@property (strong, nonatomic) NSArray<BFBarChartLabelLayout*>* sectionLabels;
@property (strong, nonatomic) NSArray<BFBarChartLabelLayout*>* datesLabels;
@property (strong, nonatomic) NSArray<BFBarChartLabelLayout*>* leftLabels;
@property (strong, nonatomic) NSArray<BFBarChartBarLayout*>* sectionBars;
@property (strong, nonatomic) NSArray<BFBarChartBarLayout*>* sectionSelectionsBars;
@property (strong, nonatomic) NSArray<BFBarChartBarLayout*>* bars;
@property (strong, nonatomic) NSArray<BFBarChartLineLayout*>* lines;

@property (nonatomic) CGRect contentRect;
@property (nonatomic) CGRect leftBarRect;
@property (nonatomic) CGSize scrollContentSize;

-(CGPoint)contentOffsetForRange:(BFRange)range;
-(BFBarChartBarLayout*)barLayoutForIdx:(NSInteger)idx segmentIdx:(NSInteger)segmentIdx;
-(CGRect)rectForRange:(BFRange)range;
-(CGRect)boundsForData;
-(BFRange)rangeForContentOffset:(CGFloat)offset;
-(NSInteger)barIdxForPoint:(CGPoint)point;
-(BOOL)isDatesBarAtPoint:(CGPoint)point;

@end


// Layout item for label
@interface BFBarChartLabelLayout : NSObject

@property (nonatomic) CGRect frame;
@property (nonatomic) NSString * text;
@property (nonatomic) NSInteger idx;
@property (nonatomic) BOOL highlighted;

@end

// Layout item for bar
@interface BFBarChartBarLayout : NSObject

@property (nonatomic) CGRect frame;
@property (nonatomic) CGSize roundingSize;
@property (nonatomic) NSInteger barIdx;
@property (nonatomic) NSInteger barSegmentIdx;
@property (nonatomic) BOOL tiny;

@end

// Layout item for line
@interface BFBarChartLineLayout : NSObject

@property (nonatomic) CGPoint pointStart;
@property (nonatomic) CGPoint pointEnd;

@end

