//
//  BFBarChart+Scrolling.h
//  Briefly
//
//  Created by Artem Bondar on 31/01/2018.
//

#import "BFBarChart.h"

@interface BFBarChart (Scrolling) <UIScrollViewDelegate>

@end
