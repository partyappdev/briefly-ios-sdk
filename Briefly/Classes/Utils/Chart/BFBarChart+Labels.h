//
//  BFBarChart+Labels.h
//  Briefly
//
//  Created by Artem Bondar on 25/01/2018.
//

#import "BFBarChart.h"
#import "BFBarChartLayout.h"

@class BFBarChartSectionLabel;

@interface BFBarChart (Labels)

-(void)dropAllLabels;
-(void)layoutSectionLabels;

-(UIColor*)labelTextColor;
-(UIColor*)labelTextColorHighlighted;

-(UILabel*)leftLabel;

-(UILabel*)bottomLabel;
-(BFBarChartSectionLabel*)monthLabel;
-(UILabel*)bottomLabelWithTag:(NSInteger)tag;
-(UILabel*)leftLabelWithTag:(NSInteger)tag;
-(UILabel*)monthLabelWithTag:(NSInteger)tag;
-(UILabel*)noDataLabel;

@end


@interface BFBarChartSectionLabel : UILabel

@property (strong, nonatomic) BFBarChartLabelLayout * layout;

@end
