//
//  BFResposesCache.m
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "BFResposesCache.h"

const NSInteger BFValididtyTimeIntervalHalfHour = 30 * 60;
const NSInteger BFValididtyTimeIntervalDay = 24 * 60 * 60;
const NSInteger BFValididtyTimeIntervalWeek = 24 * 60 * 60 * 7;
const NSInteger BFValididtyTimeIntervalMonth = 24 * 60 * 60 * 30;

@interface BFResposesCacheItem : NSObject
@property (strong, nonatomic) NSObject * item;
@property (nonatomic) NSDate* validUntil;
@end

@implementation BFResposesCacheItem

-(instancetype)initWithItem:(NSObject*)item validityTime:(NSInteger)validityTime
{
    self = [super init];
    if (self) {
        self.item = item;
        if (validityTime != 0) {
            self.validUntil = [[NSDate date] dateByAddingTimeInterval:validityTime];
        }
    }
    return self;
}

@end

@interface BFResposesCache ()

@property (strong, atomic) NSCache * cache;
@property (nonatomic) dispatch_queue_t cahceQueue;

@end



@implementation BFResposesCache

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cache = [[NSCache alloc] init];
        self.cache.totalCostLimit = 10000;
        self.cahceQueue = dispatch_queue_create("com.desqlabs.briefly.queue.cache", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

-(void)nonoutdatedCacheValuesAtOnceForKeys:(NSArray<NSString*>*)keys withHandler:(void(^)(NSArray*))handler
{
    NSMutableArray * returnArray = [NSMutableArray arrayWithCapacity:keys.count];
    for (NSString * key in keys) {
        BFResposesCacheItem * item = [self.cache objectForKey:key];
        if (item == nil) {
            handler(nil);
            return;
        }
        if (item.validUntil != nil) {
            if ([[NSDate date] compare: item.validUntil] != NSOrderedAscending) {
                
                // it's outdated
                handler(nil);
                return;
            }
        }
        [returnArray addObject:item.item];
    }
    handler(returnArray);
}

-(void)nonoutdatedCacheValueForKey:(NSString*)key withHandler:(void(^)(NSObject*))handler
{
    /*dispatch_async(self.cahceQueue, ^{
        BFResposesCacheItem * item = [self.cache objectForKey:key];
        if (item.validUntil != nil) {
            if ([[NSDate date] compare: item.validUntil] != NSOrderedAscending) {
                
                // it's outdated
                [self.cache removeObjectForKey:key];
                handler(nil);
                return;
            }
        }
        handler(item.item);
        return;
    });*/

    // Do read sync
    BFResposesCacheItem * item = [self.cache objectForKey:key];
    if (item.validUntil != nil) {
        if ([[NSDate date] compare: item.validUntil] != NSOrderedAscending) {
            
            // it's outdated
            handler(nil);
            dispatch_async(self.cahceQueue, ^{
                [self.cache removeObjectForKey:key];
            });
            return;
        }
    }
    handler(item.item);
    return;
}

-(void)setCacheValue:(NSObject*)value forKey:(NSString*)key
{
    [self setCacheValue:value forKey:key validityTime:BFValididtyTimeIntervalDay];
}

-(void)setCacheValue:(NSObject*)value forKey:(NSString*)key validityTime:(NSInteger)validityTime
{
    dispatch_async(self.cahceQueue, ^{

        [self.cache setObject:[[BFResposesCacheItem alloc] initWithItem:value validityTime:validityTime] forKey:key cost:1];
    });
}

@end
