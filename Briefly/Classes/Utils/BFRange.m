//
//  BFRange.m
//  Briefly
//
//  Created by Artem Bondar on 02/02/2018.
//

#import "BFRange.h"

BFRange BFRangeUnion(BFRange range1, BFRange range2)
{
    if (range1.length == 0) {
        return range2;
    }
    if (range2.length == 0) {
        return range1;
    }

    NSInteger min;
    NSInteger max;
    if (range1.location > range2.location) {
        min = range2.location;
    } else {
        min = range1.location;
    }
    if (range1.location + range1.length > range2.location + range2.length) {
        max = range1.location + range1.length;
    } else {
        max = range2.location + range2.length;
    }
    return BFRangeMake(min, max - min);
}

BFRange BFRangeIntersection(BFRange range1, BFRange range2)
{
    if (range1.length == 0 || range2.length == 0) {
        return BFRangeMake(0, 0);
    }

    NSInteger loc1 = range1.location;
    NSInteger loc2 = range1.location + range1.length - 1;
    
    NSInteger bloc1 = range2.location;
    NSInteger bloc2 = range2.location + range2.length - 1;
    if (!BFLocationInRange(loc1, range2) && !BFLocationInRange(loc2, range2)) {
        if (!BFLocationInRange(bloc1, range1) && !BFLocationInRange(bloc2, range1)) {
            return BFRangeMake(0, 0);
        } else {
            return range2;
        }
    }
    if (BFLocationInRange(loc1, range2) && !BFLocationInRange(loc2, range2)) {
        return BFRangeMake(loc1, bloc2 - loc1 + 1);
    }
    if (BFLocationInRange(loc2, range2) && !BFLocationInRange(loc1, range2)) {
        return BFRangeMake(bloc1, loc2 - bloc1 + 1);
    }
    return range1;
}
