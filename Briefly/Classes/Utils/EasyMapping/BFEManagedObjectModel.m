//
//  BFEManagedObjectModel.m
//  EasyMappingExample
//
//  Created by Denys Telezhkin on 22.06.14.
//  Copyright (c) 2014 EasyKit. All rights reserved.
//

#import "BFEManagedObjectModel.h"
#import "BFEManagedObjectMapper.h"
#import "BFEManagedObjectMapping.h"
#import "BFESerializer.h"

@implementation BFEManagedObjectModel

#pragma mark - constructors

+(instancetype)objectWithProperties:(NSDictionary *)properties inContext:(NSManagedObjectContext *)context
{
    return [BFEManagedObjectMapper objectFromExternalRepresentation:properties
                                                       withMapping:[self objectMapping]
                                            inManagedObjectContext:context];
}

#pragma mark - serialization

- (NSDictionary *)serializedObjectInContext:(NSManagedObjectContext *)context
{
    return [BFESerializer serializeObject:self
                             withMapping:[self.class objectMapping]
                             fromContext:context];
}

#pragma mark - BFEManagedMappingProtocol

+(BFEManagedObjectMapping *)objectMapping
{
    return [[BFEManagedObjectMapping alloc] initWithEntityName:NSStringFromClass(self)];
}

@end
