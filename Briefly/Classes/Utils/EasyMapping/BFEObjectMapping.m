//
//  EasyMapping
//
//  Copyright (c) 2012-2014 Lucas Medeiros.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "BFEObjectMapping.h"
#import "BFEPropertyMapping.h"
#import "BFERelationshipMapping.h"
#import "BFEMappingProtocol.h"
#import "NSDateFormatter+BFEasyMappingAdditions.h"
#import "BFEPropertyHelper.h"

@implementation BFEObjectMapping

+ (BFEObjectMapping *)mappingForClass:(Class)objectClass withBlock:(void (^)(BFEObjectMapping *))mappingBlock
{
    BFEObjectMapping *mapping = [[BFEObjectMapping alloc] initWithObjectClass:objectClass];
    if (mappingBlock)
    {
        mappingBlock(mapping);
    }
    return mapping;
}

+ (BFEObjectMapping *)mappingForClass:(Class)objectClass withRootPath:(NSString *)rootPath withBlock:(void (^)(BFEObjectMapping *))mappingBlock
{
    BFEObjectMapping *mapping = [[BFEObjectMapping alloc] initWithObjectClass:objectClass withRootPath:rootPath];
    if (mappingBlock)
    {
       mappingBlock(mapping);
    }
    return mapping;
}

- (id)initWithObjectClass:(Class)objectClass
{
    self = [super init];
    if (self) {
        _objectClass = objectClass;
        _propertyMappings = [NSMutableDictionary dictionary];
        _hasOneMappings = [NSMutableArray array];
        _hasManyMappings = [NSMutableArray array];
    }
    return self;
}

- (id)initWithObjectClass:(Class)objectClass withRootPath:(NSString *)rootPath
{
    self = [self initWithObjectClass:objectClass];
    if (self) {
        _rootPath = rootPath;
    }
    return self;
}

- (void)mapKeyPath:(NSString *)keyPath toProperty:(NSString *)property
{
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    
    BFEPropertyMapping *mapping = [BFEPropertyMapping mappingWithKeyPath:keyPath forProperty:property];
    [self addPropertyMappingToDictionary:mapping];
}

-(void)mapKeyPath:(NSString *)keyPath toProperty:(NSString *)property withDateFormatter:(NSDateFormatter *)formatter
{
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    NSParameterAssert(formatter);
    
    [self mapKeyPath:keyPath
          toProperty:property
      withValueBlock:^id(NSString * key, id value) {
          if (value == NSNull.null) return value;
          return [value isKindOfClass:[NSString class]] ? [formatter dateFromString:value] : nil;
      } reverseBlock:^id(id value) {
          return [value isKindOfClass:[NSDate class]] ? [formatter stringFromDate:value] : nil;
      }];
}

- (void)mapPropertiesFromArray:(NSArray *)propertyNamesArray
{
    NSParameterAssert([propertyNamesArray isKindOfClass:[NSArray class]]);
    
    for (NSString *key in propertyNamesArray) {
        [self mapKeyPath:key toProperty:key];
    }
}

-(void)mapPropertiesFromArrayToPascalCase:(NSArray *)propertyNamesArray
{
    NSParameterAssert([propertyNamesArray isKindOfClass:[NSArray class]]);
    for (NSString *key in propertyNamesArray) {
        NSString *pascalKey = [key stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[key substringToIndex:1] uppercaseString]];
        
        [self mapKeyPath:pascalKey toProperty:key];
    }
}

- (void)mapPropertiesFromUnderscoreToCamelCase:(NSArray *)propertyNamesArray
{
    for (NSString *key in propertyNamesArray) {
        NSString *convertedKey = [BFEPropertyHelper convertStringFromUnderScoreToCamelCase: key];
        
        [self mapKeyPath:key toProperty:convertedKey];
    }
}

- (void)mapPropertiesFromDictionary:(NSDictionary *)propertyDictionary
{
    NSParameterAssert([propertyDictionary isKindOfClass:[NSDictionary class]]);
    
    [propertyDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [self mapKeyPath:key toProperty:obj];
    }];
}

-(void)mapPropertiesFromMappingObject:(BFEObjectMapping *)mappingObj
{
    NSParameterAssert([mappingObj isKindOfClass:BFEObjectMapping.class]);
    
    for (NSString *key in mappingObj.propertyMappings) {
        [self addPropertyMappingToDictionary:mappingObj.propertyMappings[key]];
    }
    
    for (BFERelationshipMapping *relationship in mappingObj.hasOneMappings) {
        [self.hasOneMappings addObject:relationship];
    }
    
    for (BFERelationshipMapping *relationship in mappingObj.hasManyMappings) {
        [self.hasManyMappings addObject:relationship];
    }
}

- (void)mapKeyPath:(NSString *)keyPath toProperty:(NSString *)property
withValueBlock:(id (^)(NSString *, id))valueBlock
{
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    NSParameterAssert(valueBlock);
    
    BFEPropertyMapping *mapping = [BFEPropertyMapping mappingWithKeyPath:keyPath forProperty:property];
    mapping.valueBlock = valueBlock;
    [self addPropertyMappingToDictionary:mapping];
}

- (void)mapKeyPath:(NSString *)keyPath toProperty:(NSString *)property
withValueBlock:(id (^)(NSString *, id))valueBlock reverseBlock:(id (^)(id))reverseBlock
{
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    NSParameterAssert(valueBlock);
    NSParameterAssert(reverseBlock);
    
    BFEPropertyMapping *mapping = [BFEPropertyMapping mappingWithKeyPath:keyPath forProperty:property];
    mapping.valueBlock = valueBlock;
    mapping.reverseBlock = reverseBlock;
    [self addPropertyMappingToDictionary:mapping];
}

- (BFERelationshipMapping *)hasOne:(Class)objectClass forKeyPath:(NSString *)keyPath
{
    return [self hasOne:objectClass forKeyPath:keyPath forProperty:keyPath withObjectMapping:nil];
}

- (BFERelationshipMapping *)hasOne:(Class)objectClass forKeyPath:(NSString *)keyPath forProperty:(NSString *)property
{
    return [self hasOne:objectClass forKeyPath:keyPath forProperty:property withObjectMapping:nil];
}

- (BFERelationshipMapping *)hasOne:(Class)objectClass forKeyPath:(NSString *)keyPath forProperty:(NSString *)property withObjectMapping:(BFEObjectMapping*)objectMapping
{
    if (!objectMapping) {
        NSParameterAssert([objectClass conformsToProtocol:@protocol(BFEMappingProtocol)] ||
                          [objectClass conformsToProtocol:@protocol(BFEManagedMappingProtocol)]);
    }
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    
    BFERelationshipMapping * relationship = [BFERelationshipMapping mappingForClass:objectClass
                                                                      withKeyPath:keyPath
                                                                      forProperty:property];
    relationship.mappingResolver = [self defaultMappingResolveBlockFor:objectClass
                                                                  with:objectMapping];
    relationship.serializationResolver = [self defaultSerializationResolveBlockFor:objectClass
                                                                              with:objectMapping];
    [self.hasOneMappings addObject:relationship];
    
    return relationship;
}

- (BFERelationshipMapping *)hasOne:(Class)objectClass forDictionaryFromKeyPaths:(NSArray *)keyPaths forProperty:(NSString *)property withObjectMapping:(BFEObjectMapping *)mapping
{
    if (!mapping) {
        NSParameterAssert([objectClass conformsToProtocol:@protocol(BFEMappingProtocol)] ||
                          [objectClass conformsToProtocol:@protocol(BFEManagedMappingProtocol)]);
    }
    NSParameterAssert(keyPaths);
    NSParameterAssert(property);
    
    BFERelationshipMapping * relationship = [BFERelationshipMapping mappingForClass:objectClass
                                                                      withKeyPath:@""
                                                                      forProperty:property];
    relationship.nonNestedKeyPaths = keyPaths;
    relationship.mappingResolver = [self defaultMappingResolveBlockFor:objectClass
                                                                  with:mapping];
    relationship.serializationResolver = [self defaultSerializationResolveBlockFor:objectClass
                                                                              with:mapping];
    [self.hasOneMappings addObject:relationship];
    
    return relationship;
}

- (BFERelationshipMapping *)hasMany:(Class)objectClass forKeyPath:(NSString *)keyPath
{
    return [self hasMany:objectClass forKeyPath:keyPath forProperty:keyPath withObjectMapping:nil];
}

- (BFERelationshipMapping *)hasMany:(Class)objectClass forKeyPath:(NSString *)keyPath forProperty:(NSString *)property
{
    return [self hasMany:objectClass forKeyPath:keyPath forProperty:property withObjectMapping:nil];
}

- (BFERelationshipMapping *)hasMany:(Class)objectClass forKeyPath:(NSString *)keyPath forProperty:(NSString *)property withObjectMapping:(BFEObjectMapping*)objectMapping
{
    if (!objectMapping) {
        NSParameterAssert([objectClass conformsToProtocol:@protocol(BFEMappingProtocol)] ||
                          [objectClass conformsToProtocol:@protocol(BFEManagedMappingProtocol)]);
    }
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    
    BFERelationshipMapping * relationship = [BFERelationshipMapping mappingForClass:objectClass
                                                                      withKeyPath:keyPath
                                                                      forProperty:property];
    relationship.mappingResolver = [self defaultMappingResolveBlockFor:objectClass
                                                                  with:objectMapping];
    relationship.serializationResolver = [self defaultSerializationResolveBlockFor:objectClass
                                                                              with:objectMapping];
    [self.hasManyMappings addObject:relationship];
    
    return relationship;
}

- (void)addPropertyMappingToDictionary:(BFEPropertyMapping *)propertyMapping
{
    [self.propertyMappings setObject:propertyMapping forKey:propertyMapping.keyPath];
}

-(BFEMappingResolvingBlock)defaultMappingResolveBlockFor:(Class)objectClass with:(BFEObjectMapping *) mapping {
    return ^(id representation ){
        if (mapping != nil) {
            return mapping;
        }
        return [objectClass objectMapping];
    };
}

-(BFESerializationResolvingBlock)defaultSerializationResolveBlockFor:(Class)objectClass with:(BFEObjectMapping *)objectMapping {
    return ^(id object) {
        if (objectMapping != nil) {
            return objectMapping;
        }
        return [objectClass objectMapping];
    };
}

@end
