//
//  BFERelationshipMapping.h
//  EasyMappingExample
//
//  Created by Denys Telezhkin on 14.06.14.
//  Copyright (c) 2014 EasyKit. All rights reserved.
//

#import "BFEObjectMapping.h"
#import "BFEMappingProtocol.h"
#import "BFEMappingBlocks.h"

NS_ASSUME_NONNULL_BEGIN

typedef BFEObjectMapping * _Nonnull (^BFEMappingResolvingBlock)(id representation);
typedef BFEObjectMapping * _Nonnull (^BFESerializationResolvingBlock)(id object);

@interface BFERelationshipMapping : NSObject

@property (nonatomic, strong, nullable) BFEMappingResolvingBlock mappingResolver;

@property (nonatomic, strong, nullable) BFESerializationResolvingBlock serializationResolver;

@property (nonatomic, strong) NSString * keyPath;

@property (nonatomic, strong) NSString * property;

@property (nonatomic, strong) Class <BFEMappingProtocol> objectClass;

@property (nonatomic, strong, nullable) NSArray<NSString *> * nonNestedKeyPaths;

@property (nonatomic, strong, nullable) BFEMappingConditionBlock condition;

- (nullable NSDictionary *)extractObjectFromRepresentation:(NSDictionary *)representation;

- (BFEObjectMapping *)mappingForRepresentation:(id)representation;

- (BFEObjectMapping *)mappingForObject:(id)object;
    
- (instancetype)init DEPRECATED_MSG_ATTRIBUTE("Please use mappingForClass:withKeyPath:forProperty: method to create BFERelationshipMapping");
    
+ (instancetype)mappingForClass:(Class <BFEMappingProtocol>)objectClass
                    withKeyPath:(NSString *)keyPath
                    forProperty:(NSString *)property;

@end

NS_ASSUME_NONNULL_END
