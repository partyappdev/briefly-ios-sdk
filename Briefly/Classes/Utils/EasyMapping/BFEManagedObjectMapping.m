//
//  EasyMapping
//
//  Copyright (c) 2012-2014 Lucas Medeiros.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "BFEManagedObjectMapping.h"

@implementation BFEManagedObjectMapping

@synthesize propertyMappings = _propertyMappings;
@synthesize hasManyMappings = _hasManyMappings;
@synthesize hasOneMappings = _hasOneMappings;
@synthesize rootPath = _rootPath;

+ (BFEManagedObjectMapping *)mappingForEntityName:(NSString *)entityName withBlock:(void (^)(BFEManagedObjectMapping * mapping))mappingBlock
{
    BFEManagedObjectMapping * mapping = [[BFEManagedObjectMapping alloc] initWithEntityName:entityName];
    if (mappingBlock)
    {
        mappingBlock(mapping);
    }
    return mapping;
}

+ (BFEManagedObjectMapping *)mappingForEntityName:(NSString *)entityName withRootPath:(NSString *)rootPath withBlock:(void (^)(BFEManagedObjectMapping * mapping))mappingBlock
{
    BFEManagedObjectMapping * mapping = [[BFEManagedObjectMapping alloc] initWithEntityName:entityName withRootPath:rootPath];
    if (mappingBlock)
    {
        mappingBlock(mapping);
    }
    return mapping;
}

- (id)initWithEntityName:(NSString *)entityName
{
    self = [super init];
    if (self)
    {
        _entityName = entityName;
        _propertyMappings = [NSMutableDictionary dictionary];
        _hasOneMappings = [NSMutableArray array];
        _hasManyMappings = [NSMutableArray array];
    }
    return self;
}

- (id)initWithEntityName:(NSString *)entityName withRootPath:(NSString *)rootPath
{
    self = [self initWithEntityName:entityName];
    if (self)
    {
        _rootPath = rootPath;
    }
    return self;
}

- (BFEPropertyMapping *)primaryKeyPropertyMapping
{
    __block BFEPropertyMapping * primaryKeyMapping = nil;
    [self.propertyMappings enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop)
    {
        BFEPropertyMapping * mapping = obj;
        if ([mapping.property isEqualToString:self.primaryKey])
        {
            primaryKeyMapping = mapping;
            *stop = YES;
        }
    }];
    return primaryKeyMapping;
}

-(void)mapKeyPath:(NSString *)keyPath toProperty:(NSString *)property withValueBlock:(BFEManagedMappingValueBlock)valueBlock
{
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    NSParameterAssert(valueBlock);
    
    BFEPropertyMapping *mapping = [BFEPropertyMapping mappingWithKeyPath:keyPath forProperty:property];
    mapping.managedValueBlock = valueBlock;
    [self addPropertyMappingToDictionary:mapping];
}

-(void)mapKeyPath:(NSString *)keyPath toProperty:(NSString *)property withValueBlock:(BFEManagedMappingValueBlock)valueBlock reverseBlock:(BFEManagedMappingReverseValueBlock)reverseBlock
{
    NSParameterAssert(keyPath);
    NSParameterAssert(property);
    NSParameterAssert(valueBlock);
    NSParameterAssert(reverseBlock);
    
    BFEPropertyMapping *mapping = [BFEPropertyMapping mappingWithKeyPath:keyPath forProperty:property];
    mapping.managedValueBlock = valueBlock;
    mapping.managedReverseBlock = reverseBlock;
    [self addPropertyMappingToDictionary:mapping];
}

- (void)addPropertyMappingToDictionary:(BFEPropertyMapping *)mapping
{
    [self.propertyMappings setObject:mapping forKey:mapping.keyPath];
}

@end
