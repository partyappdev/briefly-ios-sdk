//
//  EasyMapping
//
//  Copyright (c) 2012-2014 Lucas Medeiros.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "BFEManagedObjectMapper.h"
#import "BFEPropertyHelper.h"
#import "BFECoreDataImporter.h"
#import "BFERelationshipMapping.h"

@interface BFEManagedObjectMapper ()
@property (nonatomic, strong) BFECoreDataImporter * importer;

+ (instancetype)mapperWithImporter:(BFECoreDataImporter *)importer;

@end

@implementation BFEManagedObjectMapper

+ (instancetype)mapperWithImporter:(BFECoreDataImporter *)importer
{
    BFEManagedObjectMapper * mapper = [self new];
    mapper.importer = importer;
    return mapper;
}

- (id)objectFromExternalRepresentation:(NSDictionary *)externalRepresentation
                           withMapping:(BFEManagedObjectMapping *)mapping
{
    NSManagedObject * object = [self.importer existingObjectForRepresentation:externalRepresentation
                                                                      mapping:mapping
                                                                      context:self.importer.context];
    if (!object)
    {
        object = [NSEntityDescription insertNewObjectForEntityForName:mapping.entityName
                                               inManagedObjectContext:self.importer.context];
    }
    [self fillObjectProperties:object
    fromExternalRepresentation:externalRepresentation
                   withMapping:mapping];
    [self.importer cacheObject:object
                   withMapping:mapping];
    [self fillObjectOneRelationships:object
          fromExternalRepresentation:externalRepresentation
                         withMapping:mapping];
    [self fillObjectManyRelationships:object
           fromExternalRepresentation:externalRepresentation
                          withMapping:mapping];
    
    return object;
}

- (void)fillObjectProperties:(id)object fromExternalRepresentation:(NSDictionary *)externalRepresentation
               withMapping:(BFEManagedObjectMapping *)mapping
{
    NSDictionary * representation = [BFEPropertyHelper extractRootPathFromExternalRepresentation:externalRepresentation
                                                                                    withMapping:mapping];
    [mapping.propertyMappings enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop)
    {
        [BFEPropertyHelper setProperty:obj
                             onObject:object
                   fromRepresentation:representation
                            inContext:self.importer.context
         respectPropertyType:mapping.respectPropertyFoundationTypes
         ignoreMissingFields:mapping.ignoreMissingFields];
    }];
}

- (void)fillObjectOneRelationships:(id)object
        fromExternalRepresentation:(NSDictionary *)externalRepresentation
                       withMapping:(BFEManagedObjectMapping *)mapping
{
    NSDictionary *representation = [BFEPropertyHelper extractRootPathFromExternalRepresentation:externalRepresentation
                                                                                   withMapping:mapping];
    for (BFERelationshipMapping *relationship in mapping.hasOneMappings) {
        NSDictionary * value = [relationship extractObjectFromRepresentation:representation];
        if(mapping.ignoreMissingFields && !value)
        {
            continue;
        }
        if (value && value != (id)[NSNull null]) {
            id result = [self objectFromExternalRepresentation:value withMapping:(BFEManagedObjectMapping *)[relationship mappingForRepresentation:value]];
            [BFEPropertyHelper setValue:result onObject:object forKeyPath:relationship.property];
        } else {
            [BFEPropertyHelper setValue:nil onObject:object forKeyPath:relationship.property];
        }
        
    }
}

- (void)fillObjectManyRelationships:(id)object
         fromExternalRepresentation:(NSDictionary *)externalRepresentation
                        withMapping:(BFEManagedObjectMapping *)mapping
{
    NSDictionary *representation = [BFEPropertyHelper extractRootPathFromExternalRepresentation:externalRepresentation
                                                                                    withMapping:mapping];
    for (BFERelationshipMapping *relationship in mapping.hasManyMappings) {
        NSArray * arrayToBeParsed = [representation valueForKeyPath:relationship.keyPath];
        if(mapping.ignoreMissingFields && !arrayToBeParsed)
        {
            continue;
        }
        
        if (arrayToBeParsed && arrayToBeParsed != (id)[NSNull null])
        {
            NSArray * parsedArray = [self arrayOfObjectsFromExternalRepresentation:arrayToBeParsed
                                                                       withRelationship:relationship];
            id parsedObjects = [BFEPropertyHelper propertyRepresentation:parsedArray
                                                              forObject:object
                                                       withPropertyName:[relationship property]];
            if(mapping.incrementalData) {
                [BFEPropertyHelper addValue:parsedObjects onObject:object forKeyPath:relationship.property];
            }
            else {
                [BFEPropertyHelper setValue:parsedObjects onObject:object forKeyPath:relationship.property];
            }
        } else if (!mapping.incrementalData) {
            [BFEPropertyHelper setValue:nil onObject:object forKeyPath:relationship.property];
        }
    }
}

- (NSArray *)arrayOfObjectsFromExternalRepresentation:(NSArray *)externalRepresentation
                                     withRelationship:(BFERelationshipMapping *)mapping
{
    NSMutableArray * array = [NSMutableArray array];
    for (NSDictionary * representation in externalRepresentation)
    {
        id parsedObject = [self objectFromExternalRepresentation:representation withMapping:(BFEManagedObjectMapping *)[mapping mappingForRepresentation:representation]];
        [array addObject:parsedObject];
    }
    return [NSArray arrayWithArray:array];
}

- (NSArray *)arrayOfObjectsFromExternalRepresentation:(NSArray *)externalRepresentation
                                          withMapping:(BFEManagedObjectMapping *)mapping
{
    
    NSMutableArray * array = [NSMutableArray array];
    for (NSDictionary * representation in externalRepresentation)
    {
        id parsedObject = [self objectFromExternalRepresentation:representation withMapping:mapping];
        [array addObject:parsedObject];
    }
    return [NSArray arrayWithArray:array];
}

- (NSArray *)syncArrayOfObjectsFromExternalRepresentation:(NSArray *)externalRepresentation
                                              withMapping:(BFEManagedObjectMapping *)mapping
                                             fetchRequest:(NSFetchRequest *)fetchRequest
{
    NSAssert(mapping.primaryKey, @"A mapping with a primary key is required");
    BFEPropertyMapping * primaryKeyPropertyMapping = [mapping primaryKeyPropertyMapping];

    // Create a dictionary that maps primary keys to existing objects
    NSArray * existing = [self.importer.context executeFetchRequest:fetchRequest error:NULL];
    NSDictionary * existingByPK = [NSDictionary dictionaryWithObjects:existing
                                                              forKeys:[existing valueForKey:primaryKeyPropertyMapping.property]];

    NSMutableArray * array = [NSMutableArray array];
    for (NSDictionary * representation in externalRepresentation)
    {
        // Look up the object by its primary key
        id primaryKeyValue = [BFEPropertyHelper getValueOfManagedProperty:primaryKeyPropertyMapping
                                                      fromRepresentation:representation
                                                               inContext:self.importer.context];
        id object = [existingByPK objectForKey:primaryKeyValue];

        // Create a new object if necessary
        if (!object)
            object = [NSEntityDescription insertNewObjectForEntityForName:mapping.entityName
                                                   inManagedObjectContext:self.importer.context];

        [self fillObjectProperties:object
        fromExternalRepresentation:representation
                       withMapping:mapping];
        [self fillObjectOneRelationships:object
              fromExternalRepresentation:representation
                             withMapping:mapping];
        [self fillObjectManyRelationships:object
               fromExternalRepresentation:representation
                              withMapping:mapping];
        [array addObject:object];
    }

    // Any object returned by the fetch request not in the external represntation has to be deleted
    NSMutableSet * toDelete = [NSMutableSet setWithArray:existing];
    [toDelete minusSet:[NSSet setWithArray:array]];
    for (NSManagedObject * o in toDelete)
        [self.importer.context deleteObject:o];

    return [NSArray arrayWithArray:array];
}


#pragma mark - CoreData Importer
/*
 All methods below perform a redirection to instance methods, that use CoreData importer class
 */

+ (id)objectFromExternalRepresentation:(NSDictionary *)externalRepresentation
                           withMapping:(BFEManagedObjectMapping *)mapping
                inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSParameterAssert([mapping isKindOfClass:[BFEManagedObjectMapping class]]);
    NSParameterAssert(context);
    
    BFECoreDataImporter * importer = [BFECoreDataImporter importerWithMapping:mapping
                                                     externalRepresentation:externalRepresentation
                                                                    context:context];
    return [[self mapperWithImporter:importer] objectFromExternalRepresentation:externalRepresentation
                                                                    withMapping:mapping];
}

+ (id)            fillObject:(id)object
  fromExternalRepresentation:(NSDictionary *)externalRepresentation
                 withMapping:(BFEManagedObjectMapping *)mapping
      inManagedObjectContext:(NSManagedObjectContext*)context
{
    NSParameterAssert([mapping isKindOfClass:[BFEManagedObjectMapping class]]);
    NSParameterAssert(context);
    
    BFECoreDataImporter * importer = [BFECoreDataImporter importerWithMapping:mapping
                                                     externalRepresentation:externalRepresentation
                                                                    context:context];
    BFEManagedObjectMapper *mapper = [self mapperWithImporter:importer];
    [mapper fillObjectProperties:object
      fromExternalRepresentation:externalRepresentation
                     withMapping:mapping];
    [mapper fillObjectOneRelationships:object
            fromExternalRepresentation:externalRepresentation
                           withMapping:mapping];
    [mapper fillObjectManyRelationships:object
             fromExternalRepresentation:externalRepresentation
                            withMapping:mapping];
    return object;
}

+ (NSArray *)arrayOfObjectsFromExternalRepresentation:(NSArray *)externalRepresentation
                                          withMapping:(BFEManagedObjectMapping *)mapping
                               inManagedObjectContext:(NSManagedObjectContext*)context
{
    NSParameterAssert([mapping isKindOfClass:[BFEManagedObjectMapping class]]);
    NSParameterAssert(context);
    
    BFECoreDataImporter * importer = [BFECoreDataImporter importerWithMapping:mapping
                                                     externalRepresentation:externalRepresentation
                                                                    context:context];

    return [[self mapperWithImporter:importer] arrayOfObjectsFromExternalRepresentation:externalRepresentation
                                                                            withMapping:mapping];
}

+ (NSArray *)syncArrayOfObjectsFromExternalRepresentation:(NSArray *)externalRepresentation
                                              withMapping:(BFEManagedObjectMapping *)mapping
                                             fetchRequest:(NSFetchRequest *)fetchRequest
                                   inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSParameterAssert([mapping isKindOfClass:[BFEManagedObjectMapping class]]);
    NSParameterAssert(context);
    
    BFECoreDataImporter * importer = [BFECoreDataImporter importerWithMapping:mapping
                                                     externalRepresentation:externalRepresentation
                                                                    context:context];
    return [[self mapperWithImporter:importer] syncArrayOfObjectsFromExternalRepresentation:externalRepresentation
                                                                                withMapping:mapping
                                                                               fetchRequest:fetchRequest];
}

@end
