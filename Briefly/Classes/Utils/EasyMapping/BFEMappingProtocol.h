//
//  BFEMappingProtocol.h
//  EasyMappingExample
//
//  Created by Denys Telezhkin on 22.06.14.
//  Copyright (c) 2014 EasyKit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BFEObjectMapping.h"
#import "BFEManagedObjectMapping.h"

NS_ASSUME_NONNULL_BEGIN

/**
 BFEMappingProtocol must be implemented by NSObject subclasses, that will be mapped from JSON representation.
 
 EasyMapping provides convenience BFEObjectModel class, that already implements this protocol.
 */
@protocol BFEMappingProtocol

/**
 BFEObjectMapping instance, that will be used in mapping process.
 
 @return object mapping
 */
+(BFEObjectMapping *)objectMapping;

@end

/**
 BFEManagedMappingProtocol must be implemented by NSManagedObject subclasses, that will be mapped from JSON representation.
 
 EasyMapping provides convenience BFEManagedObjectModel class, that already implements this protocol.
 */
@protocol BFEManagedMappingProtocol

/**
 BFEManagedObjectMapping instance, that will be used in mapping process.
 
 @return object mapping
 */
+(BFEManagedObjectMapping *)objectMapping;

@end

NS_ASSUME_NONNULL_END
