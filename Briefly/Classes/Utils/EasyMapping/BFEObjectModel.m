//
//  BFEObjectModel.m
//  EasyMappingExample
//
//  Created by Denys Telezhkin on 22.06.14.
//  Copyright (c) 2014 EasyKit. All rights reserved.
//

#import "BFEObjectModel.h"
#import "BFEMapper.h"
#import "BFESerializer.h"

@implementation BFEObjectModel

#pragma mark - constructors

+(instancetype)objectWithProperties:(NSDictionary *)properties
{
    return [BFEMapper objectFromExternalRepresentation:properties
                                          withMapping:[self objectMapping]];
}

-(instancetype)initWithProperties:(NSDictionary *)properties
{
    if (self = [super init])
    {
        [BFEMapper fillObject:self
  fromExternalRepresentation:properties
                 withMapping:[self.class objectMapping]];
    }
    return self;
}

#pragma mark - serialization

- (NSDictionary *)serializedObject
{
    return [BFESerializer serializeObject:self
                             withMapping:[self.class objectMapping]];
}

#pragma mark - BFEMappingProtocol

+(BFEObjectMapping *)objectMapping
{
    return [[BFEObjectMapping alloc] initWithObjectClass:self];
}

@end
