//
//  BFTableViewCellWithTextField.h
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import <UIKit/UIKit.h>

@protocol BFTableViewCellWithTextFieldDelegate
-(void)textChangedTo:(NSString*)text atTextFieldWithId:(NSString*)textFieldId;
@end

@interface BFTableViewCellWithTextField : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel * labelText;
@property (weak, nonatomic) IBOutlet UITextField * textField;

@property (strong, nonatomic) NSString * textFieldId;
@property (weak, nonatomic) id <BFTableViewCellWithTextFieldDelegate> delegate;

@end
