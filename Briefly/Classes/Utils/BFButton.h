//
//  BFButton.h
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import <UIKit/UIKit.h>

@interface BFButton : UIButton

@end

@interface BFButtonVector : BFButton

@end

@interface BFButtonMenu : BFButtonVector

@end
