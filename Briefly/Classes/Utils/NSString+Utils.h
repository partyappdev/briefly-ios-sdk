//
//  NSString+Utils.h
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

+(nullable NSString*)bf_humanReadableStringFromNumber:(nullable NSNumber*)number
                                     unit:(nullable NSString*)unit;

+(nullable NSString*)bf_humanReadableStringFromNumber:(nullable NSNumber*)number;
+(nonnull NSString*)bf_humanReadableUnit:(nullable NSString*)unit;

-(nullable NSString*)bf_localized;
-(nullable NSString*)bf_localizedWithFormattedValue:(NSInteger)val;

@end
