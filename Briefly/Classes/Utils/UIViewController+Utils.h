//
//  UIViewController+UID.h
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (BfUtils)

-(NSString*)uid;
-(NSString*)orientationDescription;
-(NSString*)treeDescriptionOfThisController;

@end
