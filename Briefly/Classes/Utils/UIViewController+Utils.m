//
//  UIViewController+UID.m
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "UIViewController+Utils.h"
#import "UIViewController+Tracking.h"
#import "BFPreferencesController.h"
#import "BFControllerWidgetExpanded.h"
#import "BFControllerPushable.h"

@implementation UIViewController (BfUtils)

-(NSString*)uid
{
    if (self.trackingId != nil) {
        return self.trackingId;
    }
    if ([self isMemberOfClass:[UIViewController class]] ||
        [self isMemberOfClass:[UITableViewController class]] ||
        [self isMemberOfClass:[UICollectionViewController class]] ||
        [self isMemberOfClass:[UIPageViewController class]] ||
        [self isMemberOfClass:[UIActivityViewController class]] ||
        [self isMemberOfClass:[UINavigationController class]] ||
        [self isMemberOfClass:[UITabBarController class]] ||
        [self isMemberOfClass:[UISplitViewController class]] ||
        [self isMemberOfClass:[UISearchController class]] ||
        [self isMemberOfClass:[BFPreferencesController class]] ||
        [self isMemberOfClass:[BFControllerWidgetExpanded class]] ||
        [self isKindOfClass:[BFControllerPushable class]]) {
        
        // base classes shouldn't be tracked
        return nil;
    }
    NSString * classString = NSStringFromClass([self class]);
    return classString;
}

-(NSString*)orientationDescription
{
    // Performance matters here!
    switch(self.traitCollection.verticalSizeClass) {
        case UIUserInterfaceSizeClassCompact:
            switch(self.traitCollection.horizontalSizeClass) {
                case UIUserInterfaceSizeClassCompact:
                    return @"com/com";
                case UIUserInterfaceSizeClassRegular:
                    return @"com/reg";
                default:
                    return @"com/un";
            }
        case UIUserInterfaceSizeClassRegular:
            switch(self.traitCollection.horizontalSizeClass) {
                case UIUserInterfaceSizeClassCompact:
                    return @"reg/com";
                case UIUserInterfaceSizeClassRegular:
                    return @"reg/reg";
                default:
                    return @"reg/un";
            }
        default:
            switch(self.traitCollection.horizontalSizeClass) {
                case UIUserInterfaceSizeClassCompact:
                    return @"un/com";
                case UIUserInterfaceSizeClassRegular:
                    return @"un/reg";
                default:
                    return @"un/un";
            }
    }
}

-(NSString*)treeDescriptionOfThisController
{
    NSMutableArray * childsDescriptions = [NSMutableArray array];
    NSString * presentedString = [self.presentedViewController treeDescriptionOfThisController];
    for (UIViewController * controller in self.childViewControllers) {
        [childsDescriptions addObject:[controller treeDescriptionOfThisController]];
    }
    if (presentedString == nil) {
        presentedString = @"null";
    }
    NSString * childsString = @"";
    for (NSString * oneChildDescription in childsDescriptions) {
        if (childsString.length > 0) {
            childsString = [childsString stringByAppendingString:@", "];
        }
        childsString = [childsString stringByAppendingString:oneChildDescription];
    }
    return [NSString stringWithFormat:@"{\"class\":\"%@\",\"tracking_id\": \"%@\",\"presented\":%@,\"childs\":[%@]}", NSStringFromClass(self.class), self.trackingId, presentedString, childsString];
}

@end
