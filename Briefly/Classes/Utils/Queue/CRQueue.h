//
//  CRQueue.h
//  Crowd
//
//  Created by Artem Bondar on 19.02.2018.
//  Copyright © 2018 Artem Bondar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRBlockTypes.h"

@class CRQueueTask;

//
// Base class: CRQueue
//
@interface CRQueue : NSObject

-(instancetype)initWithMaxConcurentTasksCount:(NSInteger)maxConcurentTasks
                                maxRetryCount:(NSInteger)maxRetryCount
                               executionQueue:(dispatch_queue_t)queue
                                 debugLogging:(BOOL)debugLogging;

-(CRQueueTask*)addTask:(CRQueueAction)action
           taskHandler:(CRQueueActionResponseHandler)handler
                 params:(NSObject*)params
               priority:(NSInteger)priority
             taskTypeId:(NSString*)taskTypeId
      abadonnedCallback:(CRQueueActionIsAbadonnedCallback)abadonnedCallback;

-(void)setTaskSquashingLogic:(CRQueueSquashingLogic)taskSquashingLogic;
-(void)setDebugLogging:(BOOL)debugLogging;
-(void)setCapacityLevels:(NSDictionary<NSNumber*, NSNumber*>*)capacityLevels;
-(void)setAdditionalConcurenceCapacity:(NSInteger)capacity forHighPriorityRequestsLevel:(NSInteger)highPriorityLevel;

@end
