//
//  CRQueue+Private.h
//  Base64
//
//  Created by Artem Bondar on 19.02.2018.
//

#import "CRQueue.h"

@interface CRQueue ()

-(BOOL)isTaskOnExecution:(CRQueueTask*)task;
-(BOOL)isTaskInQueue:(CRQueueTask*)task;

@end
