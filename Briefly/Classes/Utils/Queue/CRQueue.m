//
//  CRQueue.m
//  Crowd
//
//  Created by Artem Bondar on 19.02.2018.
//  Copyright © 2018 Artem Bondar. All rights reserved.
//

#import "CRQueue.h"
#import "CRQueueTask+Private.h"

@interface CRQueue ()

@property (nonatomic) BOOL debugLogging;
@property (nonatomic) NSInteger maxRetryCount;
@property (nonatomic) NSInteger maxConcurentTasks;
@property (nonatomic) NSInteger maxConcurentTasksHighPriprity;
@property (nonatomic) NSInteger highPriority;
@property (nonatomic, strong) dispatch_queue_t executionQueue;

@property (nonatomic, strong) CRQueueSquashingLogic taskSquashingLogic;

@property (nonatomic, strong) NSMutableArray<CRQueueTask*>* tasksWaiting;
@property (nonatomic, strong) NSMutableArray<CRQueueTask*>* tasksOnExecution;

@property (nonatomic, strong) NSArray<NSNumber*>* priorityLevels;
@property (nonatomic, strong) NSDictionary<NSNumber*, NSNumber*> * additionalCapacityForPriorityLevel;

@end

@implementation CRQueue

- (instancetype)init
{
    return [self initWithMaxConcurentTasksCount:3 maxRetryCount:3 executionQueue:dispatch_queue_create("com.desqlabs.requests.queue", DISPATCH_QUEUE_SERIAL) debugLogging:NO];
}

-(instancetype)initWithMaxConcurentTasksCount:(NSInteger)maxConcurentTasks
                                maxRetryCount:(NSInteger)maxRetryCount
                               executionQueue:(dispatch_queue_t)queue
                                 debugLogging:(BOOL)debugLogging
{
    self = [super init];
    if (self) {
        self.highPriority = NSIntegerMax;
        self.maxConcurentTasksHighPriprity = 0;
        self.maxConcurentTasks = maxConcurentTasks;
        self.maxRetryCount = maxRetryCount;
        self.executionQueue = queue;
        self.debugLogging = debugLogging;
        self.tasksWaiting = [NSMutableArray array];
        self.tasksOnExecution = [NSMutableArray array];
    }
    return self;
}

-(CRQueueTask*)addTask:(CRQueueAction)action
           taskHandler:(CRQueueActionResponseHandler)handler
                params:(NSObject*)params
              priority:(NSInteger)priority
            taskTypeId:(NSString*)taskTypeId
     abadonnedCallback:(CRQueueActionIsAbadonnedCallback)abadonnedCallback
{
    __weak typeof(self) weakSelf = self;
    CRQueueTask * task = [[CRQueueTask alloc] initWithQueue:self
                                                     action:action
                                              actionHandler:handler
                                                     params:params
                                                   priority:priority
                                                 taskTypeId:taskTypeId
                                          abadonnedCallback:abadonnedCallback];

    dispatch_async(self.executionQueue, ^{
        [weakSelf.tasksWaiting addObject:task];
        [weakSelf squashSameSortAndRemoveAbaddonedTasks];
        [weakSelf startExecutionIfPossible];
    });
    return task;
}

-(void)setTaskSquashingLogic:(CRQueueSquashingLogic)taskSquashingLogic;
{
    _taskSquashingLogic = taskSquashingLogic;
}

-(void)setAdditionalConcurenceCapacity:(NSInteger)capacity forHighPriorityRequestsLevel:(NSInteger)highPriorityLevel
{
    [self setCapacityLevels:@{@(highPriorityLevel): @(capacity)}];
}

-(void)setCapacityLevels:(NSDictionary<NSNumber*, NSNumber*>*)capacityLevels
{
    if (capacityLevels.allKeys.count == 0) {
        self.priorityLevels = nil;
        self.additionalCapacityForPriorityLevel = nil;
        return;
    }
    self.priorityLevels = [capacityLevels.allKeys sortedArrayUsingSelector:@selector(compare:)];
    __block NSNumber * currentCapacity = @(0);
    __block NSMutableDictionary * additionalCapacityForLevel = [NSMutableDictionary dictionary];
    [self.priorityLevels enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        currentCapacity = @(currentCapacity.integerValue + capacityLevels[obj].integerValue);
        additionalCapacityForLevel[obj] = currentCapacity;
    }];
    self.additionalCapacityForPriorityLevel = additionalCapacityForLevel;
}

-(NSInteger)capacityForPriorityLevel:(NSNumber*)level
{
    __block NSNumber * priorityLevel = nil;
    [self.priorityLevels enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (level.integerValue < obj.integerValue) {
            *stop = YES;
            return;
        }
        priorityLevel = obj;
    }];
    if (priorityLevel == nil) {
        return self.maxConcurentTasks;
    } else {
        return self.maxConcurentTasks + self.additionalCapacityForPriorityLevel[priorityLevel].integerValue;
    }
}

-(void)squashSameSortAndRemoveAbaddonedTasks
{
    //
    // WARNING!! SHOULD BE EXECUTED ON THE executionQueue ONLY!!!!!
    //

    // First of all: go throught the waiting tasks and remove all abaddoned
    //
    // We don't delete abaddoned tasks, which are in execution, to keep the logic a bit more
    // simple
    NSMutableArray * tasksToDelete = [NSMutableArray array];
    for (CRQueueTask * task in self.tasksWaiting) {
        
        if ([task isAbaddoned]) {
            [tasksToDelete addObject:task];
            if (self.debugLogging) {
                NSLog(@"CRQueue: delete task: %@", task.taskDebugDescription);
            }
        }
    }
    [self.tasksWaiting removeObjectsInArray:tasksToDelete];
    
    // Second: now we can try to squash the tasks in waiting queue
    //
    // We read both executing queue, and
    // it's not a very performant solution O(N^2), but the most robust.
    if (self.taskSquashingLogic) {
        for (NSInteger idx = 0; idx < self.tasksWaiting.count + self.tasksOnExecution.count; ++idx) {
            
            NSMutableArray<CRQueueTask*>* sqhashedTasks = [NSMutableArray array];
            CRQueueTask * parentTask;
            NSInteger childStartIdx;
            BOOL parentTaskOnExecution;
            if (idx < self.tasksOnExecution.count) {
                parentTask = [self.tasksOnExecution objectAtIndex:idx];
                childStartIdx = 0;
                parentTaskOnExecution = YES;
            } else if (idx < self.tasksWaiting.count + self.tasksOnExecution.count) {
                NSInteger effectiveIdx = idx - self.tasksOnExecution.count;
                parentTask = [self.tasksWaiting objectAtIndex:effectiveIdx];
                childStartIdx = effectiveIdx + 1;
                parentTaskOnExecution = NO;
            } else {
                break;
            }

            // Iterate all requests, which stays after this item in queue
            for (NSInteger childIdx = childStartIdx; childIdx < self.tasksWaiting.count; ++childIdx) {
                CRQueueTask * childTask = [self.tasksWaiting objectAtIndex:childIdx];
                NSObject* squashedParams = self.taskSquashingLogic(parentTask.params, parentTask.taskTypeId, childTask.params, childTask.taskTypeId);
                
                // This is a different tasks, and params, can't be squashed
                if (squashedParams == nil) {
                    continue;
                }
                
                // Check, if params changed in compare to original parentTask ones
                // (this is unacceptable for executing task, because it's on execution, and
                // params can't be changed now)
                if (![parentTask.params isEqual:squashedParams] && parentTaskOnExecution) {
                    continue;
                }
                
                // Well, looks, like, we can easily squash this task one into other
                [parentTask addChildTask:childTask withSquashedParams:squashedParams];
                [sqhashedTasks addObject:childTask];
                if (self.debugLogging) {
                    NSLog(@"CRQueue: squashed task: %@", childTask.taskDebugDescription);
                    NSLog(@"CRQueue: into parent task: %@", parentTask.taskDebugDescription);
                }
            }
            // Now remove all squashed tasks from queue
            [self.tasksWaiting removeObjectsInArray:sqhashedTasks];
        }
    }
    
    // And finally: Sort all the least tasks
    [self.tasksWaiting sortUsingComparator:^NSComparisonResult(CRQueueTask* obj1, CRQueueTask* obj2) {
        if (obj1.priority > obj2.priority) {
            return NSOrderedAscending;
        } else if (obj1.priority < obj2.priority) {
            return NSOrderedDescending;
        } else {
            return [obj1.requestDate compare:obj2.requestDate];
        }
    }];
}

-(CRQueueTask*)firstNonPendingWaitingTask
{
    __block CRQueueTask* task = nil;
    [self.tasksWaiting enumerateObjectsUsingBlock:^(CRQueueTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj canGoToExecution]) {
            task = obj;
            *stop = YES;
        }
    }];
    return task;
}

-(void)startExecutionIfPossible
{
    //
    // WARNING!! SHOULD BE EXECUTED ON THE executionQueue ONLY!!!!!
    //
    //
    // Cycle idea:
    // do, while number of tasks on execution is less, than a capacity
    // which is allowed for the task with biggest priority in queue
    //
    __weak typeof(self) weakSelf = self;
    
    if (self.debugLogging) {
        NSLog(@"CRQueue: startIfPossible first task: %@. (on execution: %@)", self.firstNonPendingWaitingTask.taskDebugDescription, @(self.tasksOnExecution.count));
    }
    while (self.tasksOnExecution.count < [self capacityForPriorityLevel:(self.firstNonPendingWaitingTask ? @(self.firstNonPendingWaitingTask.priority) : nil)]) {

        // start the execution for one of tasks
        CRQueueTask * taskForExecution = self.firstNonPendingWaitingTask;

        if (taskForExecution == nil) {
            break;
        }

        //
        // We have one task, ready for execution
        //
        [self.tasksWaiting removeObject:taskForExecution];
        
        // If task don't have action - then just call the hadler
        // with no results
        if (taskForExecution.action == nil) {
            if (self.debugLogging) {
                NSLog(@"CRQueue: FATAL! task without action: %@", taskForExecution.taskDebugDescription);
            }
            [taskForExecution callTaskHandlerWithSucceed:NO andResults:nil];
            continue;
        }
        
        if (self.debugLogging) {
            NSLog(@"CRQueue: startIfPossible startExecutionForTask: %@", taskForExecution.taskDebugDescription);
        }
        // put it to the executing queue
        [self.tasksOnExecution addObject:taskForExecution];
        
        // and finally do the execution
        taskForExecution.action(taskForExecution.params, ^(BOOL succeed, NSObject *response, BOOL canRetry) {
            
            dispatch_async(weakSelf.executionQueue, ^{
                // before calling handlers, check, if there are tasks with the same request
                // in queue. In that case we can already call their handlers
                //
                [weakSelf squashSameSortAndRemoveAbaddonedTasks];

                // Task is executed. Remove it from execution queue
                [weakSelf.tasksOnExecution removeObject:taskForExecution];
                
                // Now find out, do we need to call the handler, or retry the failed task
                if (!succeed) {
                    [taskForExecution taskFailed];
                    if (canRetry && [taskForExecution canRetryWithMaxRetryCount:weakSelf.maxRetryCount]) {
                        [weakSelf.tasksWaiting addObject:taskForExecution];
                        [weakSelf squashSameSortAndRemoveAbaddonedTasks];
                    } else {
                        [taskForExecution callTaskHandlerWithSucceed:succeed andResults:response];
                    }
                } else {
                    [taskForExecution callTaskHandlerWithSucceed:succeed andResults:response];
                }
                
                // after that, we can try to start a new task
                // we don't need to squash and resort here, because:
                // - if callback to user code don't submit/cancel tasks, then the queue
                //   is sorted by the call at start of this block
                // - if callback to user code does changes to queue, it's state will be sorted inside this functions
                //
                // TODO: think about StackOverflow caused by sync blocks execution here (in case of sync actions)
                [weakSelf startExecutionIfPossible];
            });
        });
    }
    //
    // End of cycle
    //
    // So we send everything, we could on execution. Let's check, do we have pending tasks?
    //
    // We are a bit paranoid here: we find a minimal stale time of all pending tasks
    // and schedule call for this time, whatever the priority of this task
    //
    // It's not so smart - but robust, hahaha
    //

    NSNumber * minimalDelayForPendingTasks;
    for (CRQueueTask * task in self.tasksWaiting) {
        
        if ([task isPending]) {
            // it's a pending task, let's find out it's start
            if (minimalDelayForPendingTasks == nil || minimalDelayForPendingTasks.floatValue > [task timeIntervalToTheNextAllowedCall]) {
                minimalDelayForPendingTasks = @([task timeIntervalToTheNextAllowedCall]);
            }
        }
    }
    
    if (self.debugLogging) {
        NSLog(@"CRQueue: startIfPossible end of cycle. Pending: %@ (on execution: %@)", minimalDelayForPendingTasks, @(self.tasksOnExecution.count));
    }
    if (minimalDelayForPendingTasks) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(minimalDelayForPendingTasks.floatValue * NSEC_PER_SEC)), self.executionQueue, ^{
            [weakSelf startExecutionIfPossible];
        });
    }
}

-(BOOL)isTaskOnExecution:(CRQueueTask*)task
{
    return [self.tasksOnExecution containsObject:task];
}

-(BOOL)isTaskInQueue:(CRQueueTask*)task
{
    return [self isTaskOnExecution:task] || [self.tasksWaiting containsObject:task];
}

@end
