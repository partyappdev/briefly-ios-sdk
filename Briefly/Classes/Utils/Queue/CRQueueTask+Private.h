//
//  CRQueueTask+Private.h
//  Crowd
//
//  Created by Artem Bondar on 19.02.2018.
//  Copyright © 2018 Artem Bondar. All rights reserved.
//

#import "CRQueueTask.h"
#import "CRQueue.h"

@interface CRQueueTask ()

@property (nonatomic, weak) CRQueue *queue;
@property (nonatomic) NSInteger priority;
@property (nonatomic, strong) CRQueueAction action;
@property (nonatomic, strong) CRQueueActionResponseHandler actionHandler;
@property (nonatomic, strong) CRQueueActionIsAbadonnedCallback abadonnedTaskCallback;
@property (nonatomic, strong) NSObject* params;
@property (nonatomic, strong) NSString* taskTypeId;

// Task state
@property (nonatomic, strong) NSDate *requestDate;
@property (nonatomic, strong) NSDate *nextAllowedRequestDate;
@property (nonatomic) BOOL cancelled;
@property (nonatomic) NSInteger failsCount;

// Squashed tasks
@property (nonatomic, strong) NSArray<CRQueueTask*>* tasksChild;
@property (nonatomic, weak) CRQueueTask* parentTask;

// Methods
-(instancetype)initWithQueue:(CRQueue*)queue
                      action:(CRQueueAction)action
                  actionHandler:(CRQueueActionResponseHandler)actionHandler
                       params:(NSObject*)params
                     priority:(NSInteger)priority
                   taskTypeId:(NSString*)taskTypeId
            abadonnedCallback:(CRQueueActionIsAbadonnedCallback)abadonnedCallback;

-(BOOL)isAbaddoned;
-(BOOL)canGoToExecution;
-(BOOL)canRetryWithMaxRetryCount:(NSInteger)maxRetries;
-(NSString*)taskDebugDescription;
-(void)addChildTask:(CRQueueTask*)childTask withSquashedParams:(NSObject*)params;
-(void)taskFailed;
-(void)callTaskHandlerWithSucceed:(BOOL)succeed andResults:(NSObject*)results;

-(BOOL)isPending;
-(NSTimeInterval)timeIntervalToTheNextAllowedCall;
@end
