//
//  CRQueueTask.m
//  Crowd
//
//  Created by Artem Bondar on 19.02.2018.
//  Copyright © 2018 Artem Bondar. All rights reserved.
//

#import "CRQueueTask.h"
#import "CRQueueTask+Private.h"
#import "CRQueue+Private.h"

@implementation CRQueueTask

-(instancetype)initWithQueue:(CRQueue*)queue
                      action:(CRQueueAction)action
               actionHandler:(CRQueueActionResponseHandler)actionHandler
                      params:(NSObject*)params
                    priority:(NSInteger)priority
                  taskTypeId:(NSString*)taskTypeId
           abadonnedCallback:(CRQueueActionIsAbadonnedCallback)abadonnedCallback;
{
    CRQueueTask * newTask = [CRQueueTask new];
    newTask.queue = queue;
    newTask.action = action;
    newTask.actionHandler = actionHandler;
    newTask.params = params;
    newTask.priority = priority;
    newTask.taskTypeId = taskTypeId;
    newTask.abadonnedTaskCallback = abadonnedCallback;
    newTask.requestDate = [NSDate date];
    newTask.cancelled = NO;
    newTask.tasksChild = [NSArray array];
    return newTask;
}

-(BOOL)isAbaddoned
{
    // Check, if it abaddoned by itself
    BOOL abaddoned = self.cancelled || (self.abadonnedTaskCallback && self.abadonnedTaskCallback(self.params));
    
    // and check, if all child tasks are also abaddoned
    for (CRQueueTask * childTask in self.tasksChild) {
        abaddoned &= [childTask isAbaddoned];
    }
    return abaddoned;
}

-(BOOL)canGoToExecution
{
    if (self.nextAllowedRequestDate == nil) {
        return YES;
    } else {
        return [self.nextAllowedRequestDate compare:[NSDate date]] == NSOrderedAscending;
    }
}

-(void)taskFailed
{
    self.failsCount += 1;
    self.nextAllowedRequestDate = [[NSDate date] dateByAddingTimeInterval:pow(2, self.failsCount) - 1];
}

-(BOOL)isPending
{
    return self.nextAllowedRequestDate != nil && ![self.queue isTaskOnExecution:self];
}

-(NSTimeInterval)timeIntervalToTheNextAllowedCall
{
    if (self.nextAllowedRequestDate) {
        return [self.nextAllowedRequestDate timeIntervalSinceDate:[NSDate date]];
    } else {
        return 0;
    }
}

-(BOOL)canRetryWithMaxRetryCount:(NSInteger)maxRetries
{
    return self.failsCount <= maxRetries;
}

-(void)addChildTask:(CRQueueTask*)childTask withSquashedParams:(NSObject*)params
{
    self.params = params;
    self.tasksChild = [self.tasksChild arrayByAddingObject:childTask];
    childTask.parentTask = self;
    childTask.params = params;
    
    if (childTask.priority > self.priority) {
        self.priority = childTask.priority;
    }
}

-(void)callTaskHandlerWithSucceed:(BOOL)succeed andResults:(NSObject*)results
{
    if (self.actionHandler) {
        self.actionHandler(succeed, self.params, results);
    }
    for (CRQueueTask *task in self.tasksChild) {
        [task callTaskHandlerWithSucceed:succeed andResults:results];
    }
}

-(NSString*)taskDebugDescription
{
    NSString * descr = [NSString stringWithFormat:@"{\"task\": {\"created\":\"%@\", \"priority\":%@, \"type\":\"%@\", \"nex_allowed_date\": \"%@\", \"cancelled\": \"%@\"}, \"params\":\"%@\", \"childs\": [", self.requestDate, @(self.priority), self.taskTypeId, self.nextAllowedRequestDate, @(self.cancelled), self.params];
    
    for (CRQueueTask * task in self.tasksChild) {
        descr = [descr stringByAppendingString:task.taskDebugDescription];
        descr = [descr stringByAppendingString:@","];
    }
    descr = [descr stringByAppendingString:@"]}"];
    return descr;
}

@end
