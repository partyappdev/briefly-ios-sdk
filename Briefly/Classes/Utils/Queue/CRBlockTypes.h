//
//  CRBlockTypes.h
//  Crowd
//
//  Created by Artem Bondar on 19.02.2018.
//  Copyright © 2018 Artem Bondar. All rights reserved.
//

#ifndef CRBlockTypes_h
#define CRBlockTypes_h

#import <Foundation/Foundation.h>

//
// Private handlers signatures
//
typedef void (^CRQueueActionHandler)(BOOL succeed, NSObject* response, BOOL canRetry);

//
// Public handlers signatures
//
typedef void (^CRQueueAction)(id params, CRQueueActionHandler handler);
typedef void (^CRQueueActionResponseHandler)(BOOL succeed, NSObject* params, NSObject* response);
typedef BOOL (^CRQueueActionIsAbadonnedCallback)(NSObject* params);

typedef NSObject* (^CRQueueSquashingLogic)(NSObject* params1, NSString *taskTypeId1, NSObject* params2, NSString *taskTypeId2);

#endif /* CRBlockTypes_h */
