//
//  BFGestureRecognizer.h
//  Pods
//
//  Created by Artem Bondar on 30.11.17.
//
//

#import <UIKit/UIKit.h>

@interface BFTapGestureRecognizer : UITapGestureRecognizer

@property (strong, nonatomic) NSArray<UITouch*> * currentTouches;

@end

@interface BFPanGestureRecognizer : UIPanGestureRecognizer

@property (strong, nonatomic) NSArray<UITouch*> * currentTouches;

@end
