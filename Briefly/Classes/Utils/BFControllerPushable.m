//
//  BFControllerPushable.m
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import "BFControllerPushable.h"

@interface BFControllerPushable ()


@end

@implementation BFControllerPushable

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIPanGestureRecognizer * recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
    [self.pushableContainer addGestureRecognizer:recognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.thisPaneOnScreen = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.thisPaneOnScreen = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(CGFloat)offset
{
    return [UIScreen mainScreen].bounds.size.width * 1.3;
}

-(void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion
{
    // if the presenting controller is pushable - then we present with animation
    if ([viewControllerToPresent isKindOfClass:[BFControllerPushable class]]) {
        // Show new controller without any animation
        BFControllerPushable * child = (BFControllerPushable*)viewControllerToPresent;
        child.view.hidden = YES;
        [super presentViewController:viewControllerToPresent animated:NO completion:^{
            child.pushableContainer.transform = CGAffineTransformTranslate(CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1), [BFControllerPushable offset], 0);
            child.view.hidden = NO;
            [self willDissapear];
            [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.4 options:0 animations:^{
                
                child.pushableContainer.transform = CGAffineTransformIdentity;
                self.pushableContainer.transform = CGAffineTransformTranslate(CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8), -[BFControllerPushable offset], 0);
            } completion:^(BOOL finished) {
                if (completion)
                    completion();
            }];
        }];
        // Manage the animated appearenc
    } else {
        [super presentViewController:viewControllerToPresent animated:flag completion:completion];
    }
}

-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if ([self.presentingViewController isKindOfClass:[BFControllerPushable class]]) {
        BFControllerPushable * parent = (BFControllerPushable*)self.presentingViewController;
        [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.4 options:0 animations:^{
            
            parent.pushableContainer.transform = CGAffineTransformIdentity;
            self.pushableContainer.transform = CGAffineTransformTranslate(CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1), [BFControllerPushable offset], 0);
        } completion:^(BOOL finished) {
            [super dismissViewControllerAnimated:NO completion:completion];
            [parent didAppear];
        }];
    } else {
        [super dismissViewControllerAnimated:flag completion:completion];
    }
}

-(void)panGestureHandler:(UIPanGestureRecognizer*)recognizer
{
    if (![self.presentingViewController isKindOfClass:[BFControllerPushable class]]) {
        return;
    }

    BFControllerPushable * parent = (BFControllerPushable*)self.presentingViewController;
    CGFloat dx = [recognizer translationInView:recognizer.view].x / [UIScreen mainScreen].bounds.size.width;

    if (recognizer.state != UIGestureRecognizerStateEnded && recognizer.state != UIGestureRecognizerStateFailed && recognizer.state != UIGestureRecognizerStateCancelled) {
        //
        // in transition
        //
        if (dx > 0) {
            CGFloat parentScale = 0.8 + (0.2 * dx);
            CGFloat myScale = 1.0 + (0.1 * dx);
            CGFloat parentOffset = -[BFControllerPushable offset] * (1.0 - dx);
            CGFloat myOffset = [BFControllerPushable offset] * (dx);
            parent.pushableContainer.transform = CGAffineTransformTranslate(CGAffineTransformScale(CGAffineTransformIdentity, parentScale, parentScale), parentOffset, 0);
            self.pushableContainer.transform = CGAffineTransformTranslate(CGAffineTransformScale(CGAffineTransformIdentity, myScale, myScale), myOffset, 0);
        }
    } else {
        //
        // Finished: go back, or dismiss
        //
        if (dx > 0.4) {
            // Go back
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            // Stay here
            [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.4 options:0 animations:^{
                
                self.pushableContainer.transform = CGAffineTransformIdentity;
                parent.pushableContainer.transform = CGAffineTransformTranslate(CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8), -[BFControllerPushable offset], 0);
            } completion:nil];
        }
    }
}

-(void)willDissapear {
    self.thisPaneOnScreen = NO;
}

-(void)didAppear {
    self.thisPaneOnScreen = YES;
}

@end
