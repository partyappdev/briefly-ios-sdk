//
//  NSString+Utils.m
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "NSString+Utils.h"
#import "BFBrieflyManager.h"
#import "BFBrieflyManager+Resources.h"

@implementation NSString (Utils)

+(nullable NSString*)bf_humanReadableStringFromNumber:(NSNumber*)number
{
    NSString * numStr;
    if (number.integerValue < 10) {
        if ((double)(number.integerValue) == number.doubleValue) {
            numStr = [NSString stringWithFormat:@"%ld", (long)number.integerValue];
        } else {
            numStr = [NSString stringWithFormat:@"%.01lf", number.doubleValue];
        }
    } else if (number.integerValue < 1000) {
        numStr = [NSString stringWithFormat:@"%ld", (long)number.integerValue];
    } else if (number.integerValue < 1000000) {
        numStr = [NSString stringWithFormat:@"%.01lfK", number.doubleValue / 1000.0];
    } else if (number.integerValue < 1000000000) {
        numStr = [NSString stringWithFormat:@"%.01lfK", number.doubleValue / 1000000.0];
    } else {
        numStr = [NSString stringWithFormat:@"%.01lfB", number.doubleValue / 1000000000.0];
    }
    return numStr;
}

+(nullable NSString*)bf_humanReadableSeconds:(NSNumber*)number
{
    if (number.integerValue < 60 * 60 * 24){
        NSInteger seconds = number.integerValue % 60;
        NSInteger minutes = (number.integerValue / 60) % 60;
        NSInteger hours = (number.integerValue / 60) / 60;
        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    } else {
        return [NSString bf_humanReadableStringFromNumber:@(number.integerValue/60/60) unit:@"ч"];
    }
}

+(nullable NSString*)bf_humanReadableStringFromNumber:(NSNumber*)number
                                                 unit:(NSString*)unit
{
    if ([unit isEqualToString:@"sec"]) {
        // time is different story
        return [NSString bf_humanReadableSeconds:number];
    }
    NSString * numStr = [NSString bf_humanReadableStringFromNumber:number];
    return [NSString stringWithFormat:@"%@ %@", numStr, [NSString bf_humanReadableUnit:unit]];
}

+(nonnull NSString*)bf_humanReadableUnit:(NSString*)unit
{
    if (unit != nil) {
        return unit;
    } else {
        return @"BfPieces".bf_localized;
    }
}

-(nullable NSString*)bf_localized
{
    return [[BFBrieflyManager locateBundle] localizedStringForKey:self value:self table:nil];
}

-(nullable NSString*)bf_localizedWithFormattedValue:(NSInteger)val
{
    // I did it because stringdict can't be loaded from different bundle (and cocoapods requires it). So this should be fixed as soon, as
    // we start distribute this bundle as a Framework
    //
    // VERY HACKY: for Russian.
    if (val == 0) {
        return [NSString stringWithFormat:[NSString stringWithFormat:@"%@.zero", self].bf_localized, val];
    }
    val = val % 100;
    NSInteger val1 = val % 10;
    if (val > 10 && val < 20) {
        return [NSString stringWithFormat:[NSString stringWithFormat:@"%@.many", self].bf_localized, val];
    }
    if (val1 > 1 && val1 < 5) {
        return [NSString stringWithFormat:[NSString stringWithFormat:@"%@.few", self].bf_localized, val];
    }
    if (val1 == 1) {
        return [NSString stringWithFormat:[NSString stringWithFormat:@"%@.one", self].bf_localized, val];
    }
    return [NSString stringWithFormat:[NSString stringWithFormat:@"%@.many", self].bf_localized, val];
}

@end
