//
//  UIDate+BfUtils.h
//  Briefly
//
//  Created by Artem Bondar on 01/02/2018.
//

#import <UIKit/UIKit.h>

@interface NSDate (BfUtils)

-(NSInteger)bfDaysFromDate:(NSDate*)date;
-(NSDate*)bfDateByShiftingForDays:(NSInteger)days;
-(NSDate*)bfStartOfTheMonthWithCalendar:(NSCalendar*)calendar;
-(NSDate*)bfEndOfTheMonthWithCalendar:(NSCalendar*)calendar;

@end
