//
//  BFGestureRecognizer.m
//  Pods
//
//  Created by Artem Bondar on 30.11.17.
//
//

#import "BFGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>

@implementation BFTapGestureRecognizer

-(instancetype)initWithTarget:(id)target action:(SEL)action
{
    self = [super initWithTarget:target action:action];
    self.cancelsTouchesInView = NO;
    self.delaysTouchesBegan = NO;
    self.delaysTouchesEnded = NO;
    return self;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.currentTouches = touches.allObjects;
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer
{
    if ([preventedGestureRecognizer isKindOfClass:[BFTapGestureRecognizer class]] || [preventedGestureRecognizer isKindOfClass:[BFPanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
    return NO;
}

- (BOOL)shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}
- (BOOL)shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

@end


@implementation BFPanGestureRecognizer

-(instancetype)initWithTarget:(id)target action:(SEL)action
{
    self = [super initWithTarget:target action:action];
    self.cancelsTouchesInView = NO;
    self.delaysTouchesBegan = NO;
    self.delaysTouchesEnded = NO;
    return self;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.currentTouches = touches.allObjects;
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer
{
    if ([preventedGestureRecognizer isKindOfClass:[BFTapGestureRecognizer class]] || [preventedGestureRecognizer isKindOfClass:[BFPanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
    return NO;
}

- (BOOL)shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}
- (BOOL)shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

@end
