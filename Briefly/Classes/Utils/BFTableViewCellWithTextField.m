//
//  BFTableViewCellWithTextField.m
//  Briefly
//
//  Created by Artem Bondar on 13.03.2018.
//

#import "BFTableViewCellWithTextField.h"

@implementation BFTableViewCellWithTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if (selected) {
        [self.textField becomeFirstResponder];
    }
}

-(IBAction)textChanged:(id)sender
{
    [self.delegate textChangedTo:self.textField.text atTextFieldWithId:self.textFieldId];
}

@end
