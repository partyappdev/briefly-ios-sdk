//
//  BFResposesCache.h
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import <Foundation/Foundation.h>

extern const NSInteger BFValididtyTimeIntervalHalfHour;
extern const NSInteger BFValididtyTimeIntervalDay;
extern const NSInteger BFValididtyTimeIntervalWeek;
extern const NSInteger BFValididtyTimeIntervalMonth;

@interface BFResposesCache : NSObject

-(void)nonoutdatedCacheValuesAtOnceForKeys:(NSArray<NSString*>*)keys withHandler:(void(^)(NSArray*))handler;
-(void)nonoutdatedCacheValueForKey:(NSString*)key withHandler:(void(^)(NSObject*))handler;
-(void)setCacheValue:(NSObject*)value forKey:(NSString*)key;
-(void)setCacheValue:(NSObject*)value forKey:(NSString*)key validityTime:(NSInteger)validityTime;

@end
