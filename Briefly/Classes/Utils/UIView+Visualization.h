//
//  UIView+Visualization.h
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Visualization)

-(void)updateOrCreateBackgroudLayerWithColor:(UIColor*)color forcedly:(BOOL)forcedly;

@end

@interface BFShadowedView : UIView

@end
