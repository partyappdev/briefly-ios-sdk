//
//  UIDate+BfUtils.m
//  Briefly
//
//  Created by Artem Bondar on 01/02/2018.
//

#import "NSDate+BfUtils.h"

@implementation NSDate (BfUtils)

-(NSInteger)bfDaysFromDate:(NSDate*)date
{
    return [self timeIntervalSinceDate:date] / (60*60*24);
}

-(NSDate*)bfDateByShiftingForDays:(NSInteger)days
{
    return [self dateByAddingTimeInterval:(60*60*24) * days];
}

-(NSDate*)bfStartOfTheMonthWithCalendar:(NSCalendar*)calendar
{
    NSDateComponents *comp = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    [comp setDay:1];
    return [calendar dateFromComponents:comp];
}

-(NSDate*)bfEndOfTheMonthWithCalendar:(NSCalendar*)calendar
{
    NSRange days = [calendar rangeOfUnit:NSCalendarUnitDay
                                  inUnit:NSCalendarUnitMonth
                                 forDate:self];
    NSDateComponents *comp = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    [comp setDay:days.location + days.length - 1];
    return [calendar dateFromComponents:comp];
}

@end
