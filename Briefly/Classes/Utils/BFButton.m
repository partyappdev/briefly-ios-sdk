//
//  BFButton.m
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "BFButton.h"

NSInteger gBfButtonShadowOffset = 2;

@interface BFButton ()

@property (weak, nonatomic) CALayer * layerButton;
@property (weak, nonatomic) CALayer * layerShadow;

@property (weak, nonatomic) CALayer * layerButtonDown;

@end

@implementation BFButton


-(void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = NO;
    [self updateLayers];
    [self setTitleColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.0] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0] forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    //[self setTitleColor:[UIColor colorWithRed:1/255.0 green:124/255.0 blue:177/255.0 alpha:1.0] forState:UIControlStateHighlighted];
}

-(void)updateLayers
{
    // TODO: update without remove
    self.layer.masksToBounds = NO;
    [self.layerButton removeFromSuperlayer];
    [self.layerShadow removeFromSuperlayer];
    [self.layerButtonDown removeFromSuperlayer];
    
    CGRect buttonRect = self.bounds;
    
    if (!self.selected) {
        
        //CAShapeLayer * layerButton = [CAShapeLayer layer];
        CAShapeLayer * layerShadow = [CAShapeLayer layer];
        
        CGRect shadowRect = CGRectOffset(self.bounds, 0, gBfButtonShadowOffset);
        BOOL pushed = self.highlighted;
        
        // button
        CGRect buttonRectEffective = pushed ? shadowRect : buttonRect;
        /*UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:pushed ? shadowRect : buttonRect cornerRadius:self.bounds.size.height/2];
         layerButton.path = path.CGPath;
         layerButton.fillColor = [self colorForButton].CGColor;
         self.layerButton = layerButton;
         [self.layer insertSublayer:self.layerButton atIndex:0];*/
        
        ///////////////////////////////////////////////////////
        // base button
        ///////////////////////////////////////////////////////
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = [self bounds];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:buttonRectEffective cornerRadius:self.bounds.size.height/2].CGPath;
        maskLayer.fillColor = [[UIColor blackColor] CGColor];
        maskLayer.masksToBounds = NO;
        
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = buttonRectEffective;
        gradientLayer.startPoint = CGPointMake(0, 1);
        gradientLayer.endPoint = CGPointMake(0, 0);
        gradientLayer.colors = @[(id)[[UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0] CGColor]];
        gradientLayer.locations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f]];
        gradientLayer.mask = maskLayer;
        self.layerButtonDown = gradientLayer;
        
        
        ///////////////////////////////////////////////////////////
        // top layer
        ///////////////////////////////////////////////////////////
        maskLayer = [CAShapeLayer layer];
        maskLayer.frame = buttonRectEffective;
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(buttonRectEffective, 4, 4) cornerRadius:CGRectInset(buttonRectEffective, 4, 4).size.height/2].CGPath;
        maskLayer.fillColor = [[UIColor blackColor] CGColor];
        maskLayer.masksToBounds = NO;
        
        gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = buttonRectEffective;
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(0, 1);
        if (pushed) {
            gradientLayer.colors = @[(id)[[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:0.0] CGColor], (id)[[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:0.0] CGColor]];
        } else {
            gradientLayer.colors = @[(id)[[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0] CGColor], (id)[[UIColor whiteColor] CGColor]];
        }
        gradientLayer.locations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f]];
        gradientLayer.mask = maskLayer;
        self.layerButton = gradientLayer;
        
        [self.layer insertSublayer:self.layerButton atIndex:0];
        [self.layer insertSublayer:self.layerButtonDown atIndex:0];
        
        // shadow
        UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:shadowRect cornerRadius:self.bounds.size.height/2];
        layerShadow.path = path.CGPath;
        layerShadow.fillColor = [self shadowColor].CGColor;
        layerShadow.shadowColor = [self shadowColor].CGColor;
        layerShadow.shadowOffset = CGSizeMake(0, 6);
        layerShadow.shadowRadius = 12;
        layerShadow.shadowOpacity = 1.0;
        self.layerShadow = layerShadow;
        [self.layer insertSublayer:self.layerShadow atIndex:0];
    } else {
        // and top layer
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = buttonRect;
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(buttonRect, 2, 2) cornerRadius:CGRectInset(buttonRect, 2, 2).size.height/2].CGPath;
        maskLayer.fillColor = [[UIColor blackColor] CGColor];
        
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = buttonRect;
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(0, 1);
        gradientLayer.colors = @[(id)[[UIColor colorWithRed:27/255.0 green:180/255.0 blue:247/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:0/255.0 green:158/255.0 blue:247/226.0 alpha:1.0] CGColor]];
        gradientLayer.locations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f]];
        gradientLayer.mask = maskLayer;
        self.layerButton = gradientLayer;
        if (!self.highlighted) {
            [self.layer insertSublayer:self.layerButton atIndex:0];
        }
        
        // base button layer
        CGRect baseRectAboveShadow = CGRectMake(buttonRect.origin.x, buttonRect.origin.y + 2, buttonRect.size.width, buttonRect.size.height - 2);
        maskLayer = [CAShapeLayer layer];
        maskLayer.frame = [self bounds];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:baseRectAboveShadow cornerRadius:baseRectAboveShadow.size.height/2].CGPath;
        maskLayer.fillColor = [[UIColor blackColor] CGColor];
        
        gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = buttonRect;
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(0, 1);
        gradientLayer.colors = @[(id)[[UIColor colorWithRed:8/255.0 green:163/255.0 blue:230/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:56/255.0 green:195/255.0 blue:255/255.0 alpha:1.0] CGColor]];
        gradientLayer.locations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f]];
        gradientLayer.mask = maskLayer;
        self.layerButtonDown = gradientLayer;
        [self.layer insertSublayer:self.layerButtonDown atIndex:0];
        
        // and shadow
        maskLayer = [CAShapeLayer layer];
        maskLayer.frame = [self bounds];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:buttonRect cornerRadius:self.bounds.size.height/2].CGPath;
        maskLayer.fillColor = [[UIColor blackColor] CGColor];
        
        gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = buttonRect;
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(0, 1);
        gradientLayer.colors = @[(id)[[UIColor colorWithRed:1/255.0 green:124/255.0 blue:177/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:56/255.0 green:195/255.0 blue:255/255.0 alpha:1.0] CGColor]];
        gradientLayer.locations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:1.0f]];
        gradientLayer.mask = maskLayer;
        self.layerShadow = gradientLayer;
        [self.layer insertSublayer:self.layerShadow atIndex:0];
    }
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

-(void)setImage:(UIImage *)image forState:(UIControlState)state
{
    // Add tinting:
    [super setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:state];
}

-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self updateLayers];
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self updateLayers];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self updateLayers];
}

-(UIColor*)colorForButton
{
    BOOL pushed = self.selected || self.highlighted;
    return pushed ? [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0] : [UIColor whiteColor];
}

-(UIColor*)shadowColor
{
    return [[UIColor blackColor] colorWithAlphaComponent:0.6];
}

@end


@interface BFButtonVector ()
@property (weak, nonatomic) CAShapeLayer * layerArrow;
@end

@implementation BFButtonVector

-(void)updateLayers
{
    // TODO: update without remove
    [self.layerArrow removeFromSuperlayer];
    
    CAShapeLayer * layerArrow = [CAShapeLayer layer];
    
    // LeftArrow
    layerArrow.path = [self pathForArrow].CGPath;
    layerArrow.fillColor = [UIColor clearColor].CGColor;
    layerArrow.strokeColor = [UIColor blackColor].CGColor;//[self titleColorForState:self.state].CGColor;
    layerArrow.lineWidth = 3;
    //layerArrow.lineJoin = @"bevel";
    self.layerArrow = layerArrow;
    [self.layer insertSublayer:self.layerArrow atIndex:0];
    
    [super updateLayers];
}

-(UIBezierPath*)pathForArrow
{
    // Should be implemented by childs
    return nil;
}

@end


@implementation BFButtonMenu

-(UIBezierPath*)pathForArrow
{
    BOOL pushed = self.selected || self.highlighted;
    CGPoint centerPoint = CGPointMake(CGRectGetMidX(self.bounds),CGRectGetMidY(self.bounds) + (pushed ? gBfButtonShadowOffset : 0));
    CGFloat width = self.bounds.size.width;
    // button
    UIBezierPath * path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(centerPoint.x - 0.20 * width, centerPoint.y - 0.15 * width)];
    [path addLineToPoint:CGPointMake(centerPoint.x + 0.20 * width, centerPoint.y - 0.15 * width)];
    
    [path moveToPoint:CGPointMake(centerPoint.x - 0.20 * width, centerPoint.y)];
    [path addLineToPoint:CGPointMake(centerPoint.x + 0.20 * width, centerPoint.y)];
    
    [path moveToPoint:CGPointMake(centerPoint.x - 0.20 * width, centerPoint.y + 0.15 * width)];
    [path addLineToPoint:CGPointMake(centerPoint.x + 0.20 * width, centerPoint.y + 0.15 * width)];
    
    return path;
}

@end
