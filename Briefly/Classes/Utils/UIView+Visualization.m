//
//  UIView+Visualization.m
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "UIView+Visualization.h"

@implementation UIView (Visualization)

-(void)updateOrCreateBackgroudLayerWithColor:(UIColor*)color forcedly:(BOOL)forcedly
{
    [self updateOrCreateBackgroudLayerWithColor:color
                                       forcedly:forcedly
                                          round:self.bounds.size.height * 0.5];
}

-(void)updateOrCreateBackgroudLayerWithColor:(UIColor*)color forcedly:(BOOL)forcedly round:(CGFloat)round
{
    CALayer * oldLayer;
    // try to find previously created layer
    for (CALayer * sublayer in self.layer.sublayers) {
        if ([sublayer valueForKey:@"bf_is_background_layer"] != nil) {
            oldLayer = sublayer;
            break;
        }
    }

    // check, do we have to update it?
    if (CGRectEqualToRect(oldLayer.frame, self.bounds) && !forcedly) {
        return;
    }

    // start updating: firstly remove old
    [oldLayer removeFromSuperlayer];
    
    // and initialize a new one
    CAShapeLayer * newLayer = [CAShapeLayer new];
    newLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:round].CGPath;
    newLayer.fillColor = color.CGColor;
    newLayer.shadowColor = UIColor.blackColor.CGColor;
    newLayer.shadowOffset = CGSizeMake(0, 4);
    newLayer.shadowRadius = 12;
    newLayer.shadowOpacity = 0.6;
    [newLayer setValue:@YES forKey:@"bf_is_background_layer"];
    [self.layer insertSublayer:newLayer atIndex:0];
}

@end

@implementation BFShadowedView

-(void)layoutSubviews
{
    [self updateOrCreateBackgroudLayerWithColor:[UIColor whiteColor] forcedly:YES round:8];
    [super layoutSubviews];
}

@end
