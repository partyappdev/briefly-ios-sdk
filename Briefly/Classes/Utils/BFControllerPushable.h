//
//  BFControllerPushable.h
//  Briefly
//
//  Created by Artem Bondar on 12.03.2018.
//

#import <UIKit/UIKit.h>

@interface BFControllerPushable : UIViewController

@property (weak, nonatomic) IBOutlet UIView * pushableContainer;
@property (nonatomic) BOOL thisPaneOnScreen;

-(void)willDissapear;
-(void)didAppear;

@end
