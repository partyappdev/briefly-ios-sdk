//
//  BFFSCalendarHeader.h
//  Pods
//
//  Created by Wenchao Ding on 29/1/15.
//
//

#import <UIKit/UIKit.h>


@class BFFSCalendar, BFFSCalendarAppearance, BFFSCalendarHeaderLayout, BFFSCalendarCollectionView;

@interface BFFSCalendarHeaderView : UIView

@property (weak, nonatomic) BFFSCalendarCollectionView *collectionView;
@property (weak, nonatomic) BFFSCalendarHeaderLayout *collectionViewLayout;
@property (weak, nonatomic) BFFSCalendar *calendar;

@property (assign, nonatomic) CGFloat scrollOfBFFSet;
@property (assign, nonatomic) UICollectionViewScrollDirection scrollDirection;
@property (assign, nonatomic) BOOL scrollEnabled;
@property (assign, nonatomic) BOOL needsAdjustingViewFrame;
@property (assign, nonatomic) BOOL needsAdjustingMonthPosition;

- (void)setScrollOfBFFSet:(CGFloat)scrollOfBFFSet animated:(BOOL)animated;
- (void)reloadData;
- (void)configureAppearance;

@end


@interface BFFSCalendarHeaderCell : UICollectionViewCell

@property (weak, nonatomic) UILabel *titleLabel;
@property (weak, nonatomic) BFFSCalendarHeaderView *header;

@end

@interface BFFSCalendarHeaderLayout : UICollectionViewFlowLayout

@end

@interface BFFSCalendarHeaderTouchDeliver : UIView

@property (weak, nonatomic) BFFSCalendar *calendar;
@property (weak, nonatomic) BFFSCalendarHeaderView *header;

@end
