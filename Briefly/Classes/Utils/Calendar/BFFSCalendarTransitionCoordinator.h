//
//  BFFSCalendarTransitionCoordinator.h
//  BFFSCalendar
//
//  Created by dingwenchao on 3/13/16.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//

#import "BFFSCalendar.h"
#import "BFFSCalendarCollectionView.h"
#import "BFFSCalendarCollectionViewLayout.h"
#import "BFFSCalendarScopeHandle.h"

typedef NS_ENUM(NSUInteger, BFFSCalendarTransition) {
    BFFSCalendarTransitionNone,
    BFFSCalendarTransitionMonthToWeek,
    BFFSCalendarTransitionWeekToMonth
};
typedef NS_ENUM(NSUInteger, BFFSCalendarTransitionState) {
    BFFSCalendarTransitionStateIdle,
    BFFSCalendarTransitionStateChanging,
    BFFSCalendarTransitionStateFinishing,
};

@interface BFFSCalendarTransitionCoordinator : NSObject <UIGestureRecognizerDelegate>

@property (weak, nonatomic) BFFSCalendar *calendar;
@property (weak, nonatomic) BFFSCalendarCollectionView *collectionView;
@property (weak, nonatomic) BFFSCalendarCollectionViewLayout *collectionViewLayout;

@property (assign, nonatomic) BFFSCalendarTransition transition;
@property (assign, nonatomic) BFFSCalendarTransitionState state;

@property (assign, nonatomic) CGSize cachedMonthSize;

@property (readonly, nonatomic) BFFSCalendarScope representingScope;

- (instancetype)initWithCalendar:(BFFSCalendar *)calendar;

- (void)performScopeTransitionFromScope:(BFFSCalendarScope)fromScope toScope:(BFFSCalendarScope)toScope animated:(BOOL)animated;
- (void)performBoundingRectTransitionFromMonth:(NSDate *)fromMonth toMonth:(NSDate *)toMonth duration:(CGFloat)duration;

- (void)handleScopeGesture:(id)sender;

@end


@interface BFFSCalendarTransitionAttributes : NSObject

@property (assign, nonatomic) CGRect sourceBounds;
@property (assign, nonatomic) CGRect targetBounds;
@property (strong, nonatomic) NSDate *sourcePage;
@property (strong, nonatomic) NSDate *targetPage;
@property (assign, nonatomic) NSInteger focusedRowNumber;
@property (assign, nonatomic) NSDate *focusedDate;
@property (strong, nonatomic) NSDate *firstDayOfMonth;

@end

