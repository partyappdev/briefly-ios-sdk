//
//  BFFSCalendarAppearance.m
//  Pods
//
//  Created by DingWenchao on 6/29/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//
//  https://github.com/WenchaoD
//

#import "BFFSCalendarAppearance.h"
#import "BFFSCalendarDynamicHeader.h"
#import "BFFSCalendarExtensions.h"

@interface BFFSCalendarAppearance ()

@property (weak  , nonatomic) BFFSCalendar *calendar;

@property (strong, nonatomic) NSMutableDictionary *backgroundColors;
@property (strong, nonatomic) NSMutableDictionary *titleColors;
@property (strong, nonatomic) NSMutableDictionary *subtitleColors;
@property (strong, nonatomic) NSMutableDictionary *borderColors;

@end

@implementation BFFSCalendarAppearance

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _titleFont = [UIFont systemFontOfSize:BFFSCalendarStandardTitleTextSize];
        _subtitleFont = [UIFont systemFontOfSize:BFFSCalendarStandardSubtitleTextSize];
        _weekdayFont = [UIFont systemFontOfSize:BFFSCalendarStandardWeekdayTextSize];
        _headerTitleFont = [UIFont systemFontOfSize:BFFSCalendarStandardHeaderTextSize];
        
        _headerTitleColor = BFFSCalendarStandardTitleTextColor;
        _headerDateFormat = @"MMMM yyyy";
        _headerMinimumDissolvedAlpha = 0.2;
        _weekdayTextColor = BFFSCalendarStandardTitleTextColor;
        _caseOptions = BFFSCalendarCaseOptionsHeaderUsesDefaultCase|BFFSCalendarCaseOptionsWeekdayUsesDefaultCase;
        
        _backgroundColors = [NSMutableDictionary dictionaryWithCapacity:5];
        _backgroundColors[@(BFFSCalendarCellStateNormal)]      = [UIColor clearColor];
        _backgroundColors[@(BFFSCalendarCellStateSelected)]    = BFFSCalendarStandardSelectionColor;
        _backgroundColors[@(BFFSCalendarCellStateDisabled)]    = [UIColor clearColor];
        _backgroundColors[@(BFFSCalendarCellStatePlaceholder)] = [UIColor clearColor];
        _backgroundColors[@(BFFSCalendarCellStateToday)]       = BFFSCalendarStandardTodayColor;
        
        _titleColors = [NSMutableDictionary dictionaryWithCapacity:5];
        _titleColors[@(BFFSCalendarCellStateNormal)]      = [UIColor blackColor];
        _titleColors[@(BFFSCalendarCellStateSelected)]    = [UIColor whiteColor];
        _titleColors[@(BFFSCalendarCellStateDisabled)]    = [UIColor grayColor];
        _titleColors[@(BFFSCalendarCellStatePlaceholder)] = [UIColor lightGrayColor];
        _titleColors[@(BFFSCalendarCellStateToday)]       = [UIColor whiteColor];
        
        _subtitleColors = [NSMutableDictionary dictionaryWithCapacity:5];
        _subtitleColors[@(BFFSCalendarCellStateNormal)]      = [UIColor darkGrayColor];
        _subtitleColors[@(BFFSCalendarCellStateSelected)]    = [UIColor whiteColor];
        _subtitleColors[@(BFFSCalendarCellStateDisabled)]    = [UIColor lightGrayColor];
        _subtitleColors[@(BFFSCalendarCellStatePlaceholder)] = [UIColor lightGrayColor];
        _subtitleColors[@(BFFSCalendarCellStateToday)]       = [UIColor whiteColor];
        
        _borderColors[@(BFFSCalendarCellStateSelected)] = [UIColor clearColor];
        _borderColors[@(BFFSCalendarCellStateNormal)] = [UIColor clearColor];
        
        _borderRadius = 1.0;
        _eventDefaultColor = BFFSCalendarStandardEventDotColor;
        _eventSelectionColor = BFFSCalendarStandardEventDotColor;
        
        _borderColors = [NSMutableDictionary dictionaryWithCapacity:2];
        
#if TARGET_INTERFACE_BUILDER
        _fakeEventDots = YES;
#endif
        
    }
    return self;
}

- (void)setTitleFont:(UIFont *)titleFont
{
    if (![_titleFont isEqual:titleFont]) {
        _titleFont = titleFont;
        [self.calendar configureAppearance];
    }
}

- (void)setSubtitleFont:(UIFont *)subtitleFont
{
    if (![_subtitleFont isEqual:subtitleFont]) {
        _subtitleFont = subtitleFont;
        [self.calendar configureAppearance];
    }
}

- (void)setWeekdayFont:(UIFont *)weekdayFont
{
    if (![_weekdayFont isEqual:weekdayFont]) {
        _weekdayFont = weekdayFont;
        [self.calendar configureAppearance];
    }
}

- (void)setHeaderTitleFont:(UIFont *)headerTitleFont
{
    if (![_headerTitleFont isEqual:headerTitleFont]) {
        _headerTitleFont = headerTitleFont;
        [self.calendar configureAppearance];
    }
}

- (void)setTitleOfBFFSet:(CGPoint)titleOfBFFSet
{
    if (!CGPointEqualToPoint(_titleOfBFFSet, titleOfBFFSet)) {
        _titleOfBFFSet = titleOfBFFSet;
        [_calendar.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setSubtitleOfBFFSet:(CGPoint)subtitleOfBFFSet
{
    if (!CGPointEqualToPoint(_subtitleOfBFFSet, subtitleOfBFFSet)) {
        _subtitleOfBFFSet = subtitleOfBFFSet;
        [_calendar.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setImageOfBFFSet:(CGPoint)imageOfBFFSet
{
    if (!CGPointEqualToPoint(_imageOfBFFSet, imageOfBFFSet)) {
        _imageOfBFFSet = imageOfBFFSet;
        [_calendar.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setEventOfBFFSet:(CGPoint)eventOfBFFSet
{
    if (!CGPointEqualToPoint(_eventOfBFFSet, eventOfBFFSet)) {
        _eventOfBFFSet = eventOfBFFSet;
        [_calendar.visibleCells makeObjectsPerformSelector:@selector(setNeedsLayout)];
    }
}

- (void)setTitleDefaultColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(BFFSCalendarCellStateNormal)] = color;
    } else {
        [_titleColors removeObjectForKey:@(BFFSCalendarCellStateNormal)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)titleDefaultColor
{
    return _titleColors[@(BFFSCalendarCellStateNormal)];
}

- (void)setTitleSelectionColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(BFFSCalendarCellStateSelected)] = color;
    } else {
        [_titleColors removeObjectForKey:@(BFFSCalendarCellStateSelected)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)titleSelectionColor
{
    return _titleColors[@(BFFSCalendarCellStateSelected)];
}

- (void)setTitleTodayColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(BFFSCalendarCellStateToday)] = color;
    } else {
        [_titleColors removeObjectForKey:@(BFFSCalendarCellStateToday)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)titleTodayColor
{
    return _titleColors[@(BFFSCalendarCellStateToday)];
}

- (void)setTitlePlaceholderColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(BFFSCalendarCellStatePlaceholder)] = color;
    } else {
        [_titleColors removeObjectForKey:@(BFFSCalendarCellStatePlaceholder)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)titlePlaceholderColor
{
    return _titleColors[@(BFFSCalendarCellStatePlaceholder)];
}

- (void)setTitleWeekendColor:(UIColor *)color
{
    if (color) {
        _titleColors[@(BFFSCalendarCellStateWeekend)] = color;
    } else {
        [_titleColors removeObjectForKey:@(BFFSCalendarCellStateWeekend)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)titleWeekendColor
{
    return _titleColors[@(BFFSCalendarCellStateWeekend)];
}

- (void)setSubtitleDefaultColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(BFFSCalendarCellStateNormal)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(BFFSCalendarCellStateNormal)];
    }
    [self.calendar configureAppearance];
}

-(UIColor *)subtitleDefaultColor
{
    return _subtitleColors[@(BFFSCalendarCellStateNormal)];
}

- (void)setSubtitleSelectionColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(BFFSCalendarCellStateSelected)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(BFFSCalendarCellStateSelected)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)subtitleSelectionColor
{
    return _subtitleColors[@(BFFSCalendarCellStateSelected)];
}

- (void)setSubtitleTodayColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(BFFSCalendarCellStateToday)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(BFFSCalendarCellStateToday)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)subtitleTodayColor
{
    return _subtitleColors[@(BFFSCalendarCellStateToday)];
}

- (void)setSubtitlePlaceholderColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(BFFSCalendarCellStatePlaceholder)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(BFFSCalendarCellStatePlaceholder)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)subtitlePlaceholderColor
{
    return _subtitleColors[@(BFFSCalendarCellStatePlaceholder)];
}

- (void)setSubtitleWeekendColor:(UIColor *)color
{
    if (color) {
        _subtitleColors[@(BFFSCalendarCellStateWeekend)] = color;
    } else {
        [_subtitleColors removeObjectForKey:@(BFFSCalendarCellStateWeekend)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)subtitleWeekendColor
{
    return _subtitleColors[@(BFFSCalendarCellStateWeekend)];
}

- (void)setSelectionColor:(UIColor *)color
{
    if (color) {
        _backgroundColors[@(BFFSCalendarCellStateSelected)] = color;
    } else {
        [_backgroundColors removeObjectForKey:@(BFFSCalendarCellStateSelected)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)selectionColor
{
    return _backgroundColors[@(BFFSCalendarCellStateSelected)];
}

- (void)setTodayColor:(UIColor *)todayColor
{
    if (todayColor) {
        _backgroundColors[@(BFFSCalendarCellStateToday)] = todayColor;
    } else {
        [_backgroundColors removeObjectForKey:@(BFFSCalendarCellStateToday)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)todayColor
{
    return _backgroundColors[@(BFFSCalendarCellStateToday)];
}

- (void)setTodaySelectionColor:(UIColor *)todaySelectionColor
{
    if (todaySelectionColor) {
        _backgroundColors[@(BFFSCalendarCellStateToday|BFFSCalendarCellStateSelected)] = todaySelectionColor;
    } else {
        [_backgroundColors removeObjectForKey:@(BFFSCalendarCellStateToday|BFFSCalendarCellStateSelected)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)todaySelectionColor
{
    return _backgroundColors[@(BFFSCalendarCellStateToday|BFFSCalendarCellStateSelected)];
}

- (void)setEventDefaultColor:(UIColor *)eventDefaultColor
{
    if (![_eventDefaultColor isEqual:eventDefaultColor]) {
        _eventDefaultColor = eventDefaultColor;
        [self.calendar configureAppearance];
    }
}

- (void)setBorderDefaultColor:(UIColor *)color
{
    if (color) {
        _borderColors[@(BFFSCalendarCellStateNormal)] = color;
    } else {
        [_borderColors removeObjectForKey:@(BFFSCalendarCellStateNormal)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)borderDefaultColor
{
    return _borderColors[@(BFFSCalendarCellStateNormal)];
}

- (void)setBorderSelectionColor:(UIColor *)color
{
    if (color) {
        _borderColors[@(BFFSCalendarCellStateSelected)] = color;
    } else {
        [_borderColors removeObjectForKey:@(BFFSCalendarCellStateSelected)];
    }
    [self.calendar configureAppearance];
}

- (UIColor *)borderSelectionColor
{
    return _borderColors[@(BFFSCalendarCellStateSelected)];
}

- (void)setBorderRadius:(CGFloat)borderRadius
{
    borderRadius = MAX(0.0, borderRadius);
    borderRadius = MIN(1.0, borderRadius);
    if (_borderRadius != borderRadius) {
        _borderRadius = borderRadius;
        [self.calendar configureAppearance];
    }
}

- (void)setWeekdayTextColor:(UIColor *)weekdayTextColor
{
    if (![_weekdayTextColor isEqual:weekdayTextColor]) {
        _weekdayTextColor = weekdayTextColor;
        [self.calendar configureAppearance];
    }
}

- (void)setHeaderTitleColor:(UIColor *)color
{
    if (![_headerTitleColor isEqual:color]) {
        _headerTitleColor = color;
        [self.calendar configureAppearance];
    }
}

- (void)setHeaderMinimumDissolvedAlpha:(CGFloat)headerMinimumDissolvedAlpha
{
    if (_headerMinimumDissolvedAlpha != headerMinimumDissolvedAlpha) {
        _headerMinimumDissolvedAlpha = headerMinimumDissolvedAlpha;
        [self.calendar configureAppearance];
    }
}

- (void)setHeaderDateFormat:(NSString *)headerDateFormat
{
    if (![_headerDateFormat isEqual:headerDateFormat]) {
        _headerDateFormat = headerDateFormat;
        [self.calendar configureAppearance];
    }
}

- (void)setCaseOptions:(BFFSCalendarCaseOptions)caseOptions
{
    if (_caseOptions != caseOptions) {
        _caseOptions = caseOptions;
        [self.calendar configureAppearance];
    }
}

- (void)setSeparators:(BFFSCalendarSeparators)separators
{
    if (_separators != separators) {
        _separators = separators;
        [_calendar.collectionView.collectionViewLayout invalidateLayout];
    }
}

@end


@implementation BFFSCalendarAppearance (Deprecated)

- (void)setUseVeryShortWeekdaySymbols:(BOOL)useVeryShortWeekdaySymbols
{
    _caseOptions &= 15;
    self.caseOptions |= (useVeryShortWeekdaySymbols*BFFSCalendarCaseOptionsWeekdayUsesSingleUpperCase);
}

- (BOOL)useVeryShortWeekdaySymbols
{
    return (_caseOptions & (15<<4) ) == BFFSCalendarCaseOptionsWeekdayUsesSingleUpperCase;
}

- (void)setTitleVerticalOfBFFSet:(CGFloat)titleVerticalOfBFFSet
{
    self.titleOfBFFSet = CGPointMake(0, titleVerticalOfBFFSet);
}

- (CGFloat)titleVerticalOfBFFSet
{
    return self.titleOfBFFSet.y;
}

- (void)setSubtitleVerticalOfBFFSet:(CGFloat)subtitleVerticalOfBFFSet
{
    self.subtitleOfBFFSet = CGPointMake(0, subtitleVerticalOfBFFSet);
}

- (CGFloat)subtitleVerticalOfBFFSet
{
    return self.subtitleOfBFFSet.y;
}

- (void)setEventColor:(UIColor *)eventColor
{
    self.eventDefaultColor = eventColor;
}

- (UIColor *)eventColor
{
    return self.eventDefaultColor;
}

- (void)setCellShape:(BFFSCalendarCellShape)cellShape
{
    self.borderRadius = 1-cellShape;
}

- (BFFSCalendarCellShape)cellShape
{
    return self.borderRadius==1.0?BFFSCalendarCellShapeCircle:BFFSCalendarCellShapeRectangle;
}

- (void)setTitleTextSize:(CGFloat)titleTextSize
{
    self.titleFont = [UIFont fontWithName:self.titleFont.fontName size:titleTextSize];
}

- (void)setSubtitleTextSize:(CGFloat)subtitleTextSize
{
    self.subtitleFont = [UIFont fontWithName:self.subtitleFont.fontName size:subtitleTextSize];
}

- (void)setWeekdayTextSize:(CGFloat)weekdayTextSize
{
    self.weekdayFont = [UIFont fontWithName:self.weekdayFont.fontName size:weekdayTextSize];
}

- (void)setHeaderTitleTextSize:(CGFloat)headerTitleTextSize
{
    self.headerTitleFont = [UIFont fontWithName:self.headerTitleFont.fontName size:headerTitleTextSize];
}

- (void)invalidateAppearance
{
    [self.calendar configureAppearance];
}

- (void)setAdjustsFontSizeToFitContentSize:(BOOL)adjustsFontSizeToFitContentSize {}
- (BOOL)adjustsFontSizeToFitContentSize { return YES; }

@end


