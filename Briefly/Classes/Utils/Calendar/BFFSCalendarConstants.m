//
//  BFFSCalendarConstane.m
//  BFFSCalendar
//
//  Created by dingwenchao on 8/28/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//
//  https://github.com/WenchaoD
//

#import "BFFSCalendarConstants.h"

CGFloat const BFFSCalendarStandardHeaderHeight = 40;
CGFloat const BFFSCalendarStandardWeekdayHeight = 25;
CGFloat const BFFSCalendarStandardMonthlyPageHeight = 300.0;
CGFloat const BFFSCalendarStandardWeeklyPageHeight = 108+1/3.0;
CGFloat const BFFSCalendarStandardCellDiameter = 100/3.0;
CGFloat const BFFSCalendarStandardSeparatorThickness = 0.5;
CGFloat const BFFSCalendarAutomaticDimension = -1;
CGFloat const BFFSCalendarDefaultBounceAnimationDuration = 0.15;
CGFloat const BFFSCalendarStandardRowHeight = 38;
CGFloat const BFFSCalendarStandardTitleTextSize = 13.5;
CGFloat const BFFSCalendarStandardSubtitleTextSize = 10;
CGFloat const BFFSCalendarStandardWeekdayTextSize = 14;
CGFloat const BFFSCalendarStandardHeaderTextSize = 16.5;
CGFloat const BFFSCalendarMaximumEventDotDiameter = 4.8;
CGFloat const BFFSCalendarStandardScopeHandleHeight = 26;

NSInteger const BFFSCalendarDefaultHourComponent = 0;

NSString * const BFFSCalendarDefaultCellReuseIdentifier = @"_BFFSCalendarDefaultCellReuseIdentifier";
NSString * const BFFSCalendarBlankCellReuseIdentifier = @"_BFFSCalendarBlankCellReuseIdentifier";
NSString * const BFFSCalendarInvalidArgumentsExceptionName = @"Invalid argument exception";

CGPoint const CGPointInfinity = {
    .x =  CGFLOAT_MAX,
    .y =  CGFLOAT_MAX
};

CGSize const CGSizeAutomatic = {
    .width =  BFFSCalendarAutomaticDimension,
    .height =  BFFSCalendarAutomaticDimension
};



