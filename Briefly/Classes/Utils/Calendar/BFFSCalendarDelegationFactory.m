//
//  BFFSCalendarDelegationFactory.m
//  BFFSCalendar
//
//  Created by dingwenchao on 19/12/2016.
//  Copyright © 2016 wenchaoios. All rights reserved.
//

#import "BFFSCalendarDelegationFactory.h"

#define BFFSCalendarSelectorEntry(SEL1,SEL2) NSStringFromSelector(@selector(SEL1)):NSStringFromSelector(@selector(SEL2))

@implementation BFFSCalendarDelegationFactory

+ (BFFSCalendarDelegationProxy *)dataSourceProxy
{
    BFFSCalendarDelegationProxy *delegation = [[BFFSCalendarDelegationProxy alloc] init];
    delegation.protocol = @protocol(BFFSCalendarDataSource);
    delegation.deprecations = @{BFFSCalendarSelectorEntry(calendar:numberOfEventsForDate:, calendar:hasEventForDate:)};
    return delegation;
}

+ (BFFSCalendarDelegationProxy *)delegateProxy
{
    BFFSCalendarDelegationProxy *delegation = [[BFFSCalendarDelegationProxy alloc] init];
    delegation.protocol = @protocol(BFFSCalendarDelegateAppearance);
    delegation.deprecations = @{
                                BFFSCalendarSelectorEntry(calendarCurrentPageDidChange:, calendarCurrentMonthDidChange:),
                                BFFSCalendarSelectorEntry(calendar:shouldSelectDate:atMonthPosition:, calendar:shouldSelectDate:),
                                BFFSCalendarSelectorEntry(calendar:didSelectDate:atMonthPosition:, calendar:didSelectDate:),
                                BFFSCalendarSelectorEntry(calendar:shouldDeselectDate:atMonthPosition:, calendar:shouldDeselectDate:),
                                BFFSCalendarSelectorEntry(calendar:didDeselectDate:atMonthPosition:, calendar:didDeselectDate:)
                               };
    return delegation;
}

@end

#undef BFFSCalendarSelectorEntry

