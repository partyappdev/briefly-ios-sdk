//
//  BFFSCalendarConstane.h
//  BFFSCalendar
//
//  Created by dingwenchao on 8/28/15.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//
//  https://github.com/WenchaoD
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#pragma mark - Constants

CG_EXTERN CGFloat const BFFSCalendarStandardHeaderHeight;
CG_EXTERN CGFloat const BFFSCalendarStandardWeekdayHeight;
CG_EXTERN CGFloat const BFFSCalendarStandardMonthlyPageHeight;
CG_EXTERN CGFloat const BFFSCalendarStandardWeeklyPageHeight;
CG_EXTERN CGFloat const BFFSCalendarStandardCellDiameter;
CG_EXTERN CGFloat const BFFSCalendarStandardSeparatorThickness;
CG_EXTERN CGFloat const BFFSCalendarAutomaticDimension;
CG_EXTERN CGFloat const BFFSCalendarDefaultBounceAnimationDuration;
CG_EXTERN CGFloat const BFFSCalendarStandardRowHeight;
CG_EXTERN CGFloat const BFFSCalendarStandardTitleTextSize;
CG_EXTERN CGFloat const BFFSCalendarStandardSubtitleTextSize;
CG_EXTERN CGFloat const BFFSCalendarStandardWeekdayTextSize;
CG_EXTERN CGFloat const BFFSCalendarStandardHeaderTextSize;
CG_EXTERN CGFloat const BFFSCalendarMaximumEventDotDiameter;
CG_EXTERN CGFloat const BFFSCalendarStandardScopeHandleHeight;

UIKIT_EXTERN NSInteger const BFFSCalendarDefaultHourComponent;

UIKIT_EXTERN NSString * const BFFSCalendarDefaultCellReuseIdentifier;
UIKIT_EXTERN NSString * const BFFSCalendarBlankCellReuseIdentifier;
UIKIT_EXTERN NSString * const BFFSCalendarInvalidArgumentsExceptionName;

CG_EXTERN CGPoint const CGPointInfinity;
CG_EXTERN CGSize const CGSizeAutomatic;

#if TARGET_INTERFACE_BUILDER
#define BFFSCalendarDeviceIsIPad NO
#else
#define BFFSCalendarDeviceIsIPad [[UIDevice currentDevice].model hasPrefix:@"iPad"]
#endif

#define BFFSCalendarStandardSelectionColor   BFFSColorRGBA(31,119,219,1.0)
#define BFFSCalendarStandardTodayColor       BFFSColorRGBA(198,51,42 ,1.0)
#define BFFSCalendarStandardTitleTextColor   BFFSColorRGBA(14,69,221 ,1.0)
#define BFFSCalendarStandardEventDotColor    BFFSColorRGBA(31,119,219,0.75)

#define BFFSCalendarStandardLineColor        [[UIColor lightGrayColor] colorWithAlphaComponent:0.30]
#define BFFSCalendarStandardSeparatorColor   [[UIColor lightGrayColor] colorWithAlphaComponent:0.60]
#define BFFSCalendarStandardScopeHandleColor [[UIColor lightGrayColor] colorWithAlphaComponent:0.50]

#define BFFSColorRGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define BFFSCalendarInAppExtension [[[NSBundle mainBundle] bundlePath] hasSuffix:@".appex"]

#define BFFSCalendarFloor(c) floorf(c)
#define BFFSCalendarRound(c) roundf(c)
#define BFFSCalendarCeil(c) ceilf(c)
#define BFFSCalendarMod(c1,c2) fmodf(c1,c2)

#define BFFSCalendarHalfRound(c) (BFFSCalendarRound(c*2)*0.5)
#define BFFSCalendarHalfFloor(c) (BFFSCalendarFloor(c*2)*0.5)
#define BFFSCalendarHalfCeil(c) (BFFSCalendarCeil(c*2)*0.5)

#define BFFSCalendarUseWeakSelf __weak __typeof__(self) BFFSCalendarWeakSelf = self;
#define BFFSCalendarUseStrongSelf __strong __typeof__(self) self = BFFSCalendarWeakSelf;


#pragma mark - Deprecated

#define BFFSCalendarDeprecated(instead) DEPRECATED_MSG_ATTRIBUTE(" Use " # instead " instead")

BFFSCalendarDeprecated('borderRadius')
typedef NS_ENUM(NSUInteger, BFFSCalendarCellShape) {
    BFFSCalendarCellShapeCircle    = 0,
    BFFSCalendarCellShapeRectangle = 1
};

typedef NS_ENUM(NSUInteger, BFFSCalendarUnit) {
    BFFSCalendarUnitMonth = NSCalendarUnitMonth,
    BFFSCalendarUnitWeekOfYear = NSCalendarUnitWeekOfYear,
    BFFSCalendarUnitDay = NSCalendarUnitDay
};

static inline void BFFSCalendarSliceCake(CGFloat cake, NSInteger count, CGFloat *pieces) {
    CGFloat total = cake;
    for (int i = 0; i < count; i++) {
        NSInteger remains = count - i;
        CGFloat piece = BFFSCalendarRound(total/remains*2)*0.5;
        total -= piece;
        pieces[i] = piece;
    }
}



