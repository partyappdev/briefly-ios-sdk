//
//  BFFSCalendarExtensions.h
//  BFFSCalendar
//
//  Created by dingwenchao on 10/8/16.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (BFFSCalendarExtensions)

@property (nonatomic) CGFloat BFFS_width;
@property (nonatomic) CGFloat BFFS_height;

@property (nonatomic) CGFloat BFFS_top;
@property (nonatomic) CGFloat BFFS_left;
@property (nonatomic) CGFloat BFFS_bottom;
@property (nonatomic) CGFloat BFFS_right;

@end


@interface CALayer (BFFSCalendarExtensions)

@property (nonatomic) CGFloat BFFS_width;
@property (nonatomic) CGFloat BFFS_height;

@property (nonatomic) CGFloat BFFS_top;
@property (nonatomic) CGFloat BFFS_left;
@property (nonatomic) CGFloat BFFS_bottom;
@property (nonatomic) CGFloat BFFS_right;

@end


@interface NSCalendar (BFFSCalendarExtensions)

- (nullable NSDate *)BFFS_firstDayOfMonth:(NSDate *)month;
- (nullable NSDate *)BFFS_lastDayOfMonth:(NSDate *)month;
- (nullable NSDate *)BFFS_firstDayOfWeek:(NSDate *)week;
- (nullable NSDate *)BFFS_lastDayOfWeek:(NSDate *)week;
- (nullable NSDate *)BFFS_middleDayOfWeek:(NSDate *)week;
- (NSInteger)BFFS_numberOfDaysInMonth:(NSDate *)month;

@end

@interface NSMapTable (BFFSCalendarExtensions)

- (void)setObject:(nullable id)obj forKeyedSubscript:(id<NSCopying>)key;
- (id)objectForKeyedSubscript:(id<NSCopying>)key;

@end

@interface NSCache (BFFSCalendarExtensions)

- (void)setObject:(nullable id)obj forKeyedSubscript:(id<NSCopying>)key;
- (id)objectForKeyedSubscript:(id<NSCopying>)key;

@end


@interface NSObject (BFFSCalendarExtensions)

#define IVAR_DEF(SET,GET,TYPE) \
- (void)BFFS_set##SET##Variable:(TYPE)value forKey:(NSString *)key; \
- (TYPE)BFFS_##GET##VariableForKey:(NSString *)key;
IVAR_DEF(Bool, bool, BOOL)
IVAR_DEF(Float, float, CGFloat)
IVAR_DEF(Integer, integer, NSInteger)
IVAR_DEF(UnsignedInteger, unsignedInteger, NSUInteger)
#undef IVAR_DEF

- (void)BFFS_setVariable:(id)variable forKey:(NSString *)key;
- (id)BFFS_variableForKey:(NSString *)key;

- (nullable id)BFFS_performSelector:(SEL)selector withObjects:(nullable id)firstObject, ... NS_REQUIRES_NIL_TERMINATION;

@end

NS_ASSUME_NONNULL_END
