//
//  BFFSCalendarScopeHandle.h
//  BFFSCalendar
//
//  Created by dingwenchao on 4/29/16.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BFFSCalendar;

@interface BFFSCalendarScopeHandle : UIView <UIGestureRecognizerDelegate>

@property (weak, nonatomic) UIPanGestureRecognizer *panGesture;
@property (weak, nonatomic) BFFSCalendar *calendar;

- (void)handlePan:(id)sender;

@end
