//
//  BFFSCalendarDelegationFactory.h
//  BFFSCalendar
//
//  Created by dingwenchao on 19/12/2016.
//  Copyright © 2016 wenchaoios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BFFSCalendarDelegationProxy.h"

@interface BFFSCalendarDelegationFactory : NSObject

+ (BFFSCalendarDelegationProxy *)dataSourceProxy;
+ (BFFSCalendarDelegationProxy *)delegateProxy;

@end
