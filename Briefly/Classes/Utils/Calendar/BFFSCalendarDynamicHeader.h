//
//  BFFSCalendarDynamicHeader.h
//  Pods
//
//  Created by DingWenchao on 6/29/15.
//
//  动感头文件，仅供框架内部使用。
//  Private header, don't use it.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "BFFSCalendar.h"
#import "BFFSCalendarCell.h"
#import "BFFSCalendarHeaderView.h"
#import "BFFSCalendarStickyHeader.h"
#import "BFFSCalendarCollectionView.h"
#import "BFFSCalendarCollectionViewLayout.h"
#import "BFFSCalendarScopeHandle.h"
#import "BFFSCalendarCalculator.h"
#import "BFFSCalendarTransitionCoordinator.h"
#import "BFFSCalendarDelegationProxy.h"

@interface BFFSCalendar (Dynamic)

@property (readonly, nonatomic) BFFSCalendarCollectionView *collectionView;
@property (readonly, nonatomic) BFFSCalendarScopeHandle *scopeHandle;
@property (readonly, nonatomic) BFFSCalendarCollectionViewLayout *collectionViewLayout;
@property (readonly, nonatomic) BFFSCalendarTransitionCoordinator *transitionCoordinator;
@property (readonly, nonatomic) BFFSCalendarCalculator *calculator;
@property (readonly, nonatomic) BOOL floatingMode;
@property (readonly, nonatomic) NSArray *visibleStickyHeaders;
@property (readonly, nonatomic) CGFloat preferredHeaderHeight;
@property (readonly, nonatomic) CGFloat preferredWeekdayHeight;
@property (readonly, nonatomic) UIView *bottomBorder;

@property (readonly, nonatomic) NSCalendar *gregorian;
@property (readonly, nonatomic) NSDateComponents *components;
@property (readonly, nonatomic) NSDateFormatter *formatter;

@property (readonly, nonatomic) UIView *contentView;
@property (readonly, nonatomic) UIView *daysContainer;

@property (assign, nonatomic) BOOL needsAdjustingViewFrame;

- (void)invalidateHeaders;
- (void)adjustMonthPosition;
- (void)configureAppearance;

- (BOOL)isPageInRange:(NSDate *)page;
- (BOOL)isDateInRange:(NSDate *)date;

- (CGSize)sizeThatFits:(CGSize)size scope:(BFFSCalendarScope)scope;

@end

@interface BFFSCalendarAppearance (Dynamic)

@property (readwrite, nonatomic) BFFSCalendar *calendar;

@property (readonly, nonatomic) NSDictionary *backgroundColors;
@property (readonly, nonatomic) NSDictionary *titleColors;
@property (readonly, nonatomic) NSDictionary *subtitleColors;
@property (readonly, nonatomic) NSDictionary *borderColors;

@end

@interface BFFSCalendarWeekdayView (Dynamic)

@property (readwrite, nonatomic) BFFSCalendar *calendar;

@end

@interface BFFSCalendarCollectionViewLayout (Dynamic)

@property (readonly, nonatomic) CGSize estimatedItemSize;

@end

@interface BFFSCalendarDelegationProxy()<BFFSCalendarDataSource,BFFSCalendarDelegate,BFFSCalendarDelegateAppearance>
@end


