//
//  BFRange.h
//  Briefly
//
//  Created by Artem Bondar on 02/02/2018.
//

#import <Foundation/Foundation.h>
#import <Foundation/NSObjCRuntime.h>

typedef struct _BFRange {
    NSInteger location;
    NSInteger length;
} BFRange;

typedef BFRange *BFRangePointer;

NS_INLINE BFRange BFRangeMake(NSInteger loc, NSInteger len) {
    BFRange r;
    r.location = loc;
    r.length = len;
    return r;
}

NS_INLINE NSInteger BFRangeMax(BFRange range) {
    return (range.location + range.length);
}

NS_INLINE BOOL BFLocationInRange(NSInteger loc, BFRange range) {
    return (!(loc < range.location) && (loc - range.location) < range.length) ? YES : NO;
}

NS_INLINE BOOL BFRangesEqual(BFRange range1, BFRange range2) {
    return (range1.location == range2.location && range1.length == range2.length);
}

BFRange BFRangeUnion(BFRange range1, BFRange range2);
BFRange BFRangeIntersection(BFRange range1, BFRange range2);

