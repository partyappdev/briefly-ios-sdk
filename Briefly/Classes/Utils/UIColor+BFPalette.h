//
//  UIColor+FSPalette.h
//  FlurrySummary
//
//  Created by Arthur GUIBERT on 16/07/2014.
//  Copyright (c) 2014 Arthur GUIBERT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BSPalette)

+ (instancetype)bfRed;
+ (instancetype)bfOrange;
+ (instancetype)bfYellow;
+ (instancetype)bfGreen;
+ (instancetype)bfLightBlue;
+ (instancetype)bfDarkBlue;
+ (instancetype)bfPurple;
+ (instancetype)bfPink;
+ (instancetype)bfDarkGray;
+ (instancetype)bfLightGray;

+(UIColor*)bf_BaseUIColor;
+(UIColor*)bf_BaseUIColorDarkened;

// Widgets/char bars colors
+(UIColor*)bf_widgetColorWithIdx:(NSInteger)idx;
+(UIColor*)bf_widgetColorSelectedWithIdx:(NSInteger)idx;
+(UIColor*)bf_widgetColorBarWithIdx:(NSInteger)idx;
+(UIColor*)bf_widgetDisabledDarkened;
+(UIColor*)bf_widgetDisabled;

+(UIColor*)bf_widgetUnitColor;
+(UIColor*)bf_widgetTextColor;

+(UIColor*)bf_widgetPreferencesLightGray;
+(UIColor*)bf_widgetPreferencesGraphSection;
+(UIColor*)bf_widgetPreferencesGraphSectionEven;
+(UIColor*)bf_widgetPreferencesGraphSectionText;
+(UIColor*)bf_widgetPreferencesGraphSectionTextSelected;


+(UIColor*)bf_widgetsPresetsBase;
+(UIColor*)bf_widgetsPresetsBaseDark;
+(UIColor*)bf_widgetsPresetsDisabled;

@end
