//
//  UIColor+FSPalette.m
//  FlurrySummary
//
//  Created by Arthur GUIBERT on 16/07/2014.
//  Copyright (c) 2014 Arthur GUIBERT. All rights reserved.
//

#import "UIColor+BFPalette.h"

#define STATIC_COLOR(name, val) \
    static UIColor *name; do {\
    if (!name) name = val; \
    } while(0);

#define STATIC_ARRAY(name, ...) \
    static NSArray *name; do {\
    if (!name) name = @[__VA_ARGS__]; \
    } while(0);

@implementation UIColor (BFPalette)

+ (instancetype)bfRed
{
    STATIC_COLOR(_color, [UIColor colorWithRed:1.0f green:0.22f blue:0.22f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfOrange
{
    STATIC_COLOR(_color, [UIColor colorWithRed:1.0f green:0.58f blue:0.21f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfYellow
{
    STATIC_COLOR(_color, [UIColor colorWithRed:1.0f green:0.79f blue:0.28f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfGreen
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.27f green:0.85f blue:0.46f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfLightBlue
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.18f green:0.67f blue:0.84f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfDarkBlue
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.0f green:0.49f blue:0.96f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfPurple
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.35f green:0.35f blue:0.81f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfPink
{
    STATIC_COLOR(_color, [UIColor colorWithRed:1.0f green:0.17f blue:0.34f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfDarkGray
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.56f green:0.56f blue:0.58f alpha:1.0f]);
    return _color;
}

+ (instancetype)bfLightGray
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0f]);
    return _color;
}

+(UIColor*)bf_widgetColorSelectedWithIdx:(NSInteger)idx
{

    return [UIColor bf_widgetColorBarWithIdx:idx];
}

+(UIColor*)bf_widgetColorWithIdx:(NSInteger)idx
{
    STATIC_ARRAY(_colors, [UIColor colorWithRed:0.80 green:0.12 blue:0.0 alpha:1.0], [UIColor colorWithRed:0.29 green:0.55 blue:0.00 alpha:1.0], [UIColor colorWithRed:0.00 green:0.12 blue:0.55 alpha:1.0]);
    return _colors[idx % _colors.count];
}

+(UIColor*)bf_widgetColorBarWithIdx:(NSInteger)idx
{
    STATIC_ARRAY(_colors_b, [UIColor colorWithRed:1.00 green:0.00 blue:0.00 alpha:1.0], [UIColor colorWithRed:0.43 green:0.83 blue:0.00 alpha:1.0], [UIColor colorWithRed:0.34 green:0.26 blue:0.96 alpha:1.0]);
    return _colors_b[idx % _colors_b.count];
}

+(UIColor*)bf_widgetUnitColor
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.36 green:0.36 blue:0.36 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetTextColor
{
    STATIC_COLOR(_color, [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetPreferencesLightGray
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.91 green:0.91 blue:0.91 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetDisabled
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.77 green:0.77 blue:0.77 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetDisabledDarkened
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.62 green:0.62 blue:0.62 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetPreferencesGraphSection
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetPreferencesGraphSectionEven
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetPreferencesGraphSectionText
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.39 green:0.39 blue:0.39 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetPreferencesGraphSectionTextSelected
{
    STATIC_COLOR(_color, [UIColor blackColor]);
    return _color;
}

+(UIColor*)bf_BaseUIColor
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.94 green:0.19 blue:0.05 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_BaseUIColorDarkened
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.71 green:0.11 blue:0.00 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetsPresetsBase
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.64 green:0.17 blue:0.09 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetsPresetsBaseDark
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.45 green:0.16 blue:0.11 alpha:1.0]);
    return _color;
}

+(UIColor*)bf_widgetsPresetsDisabled
{
    STATIC_COLOR(_color, [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0]);
    return _color;
}

@end
