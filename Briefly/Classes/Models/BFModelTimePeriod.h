//
//  BFModelTimePeriod.h
//  Briefly
//
//  Created by Artem Bondar on 29/01/2018.
//

#import <Foundation/Foundation.h>


// Time period
@interface BFModelTimePeriod : NSObject
+(instancetype)defaultRange;
+(instancetype)periodWithSerializedString:(NSString*)string;

-(NSString*)serializedString;
-(NSArray<NSDate*>*)datesRange;
-(NSDictionary*)timerangeKeysForRequest;
-(NSString*)numanReadable;
-(BFModelTimePeriod*)periodByAddingDaysShift:(NSInteger)daysShift withCurrentDate:(NSDate*)currentDate;
-(BFModelTimePeriod*)previousRangeWithDaysLength:(NSInteger)days
                                         maxDate:(NSDate*)maxDate;
-(BFModelTimePeriod*)nextRangeWithDaysLength:(NSInteger)days
                                     maxDate:(NSDate*)maxDate;

@end




// Two dates period
@interface BFModelTimePeriodDatesRange : BFModelTimePeriod

+(instancetype)periodWithDateStart:(NSDate*)dateStart dateEnd:(NSDate*)dateEnd;
+(instancetype)periodWithLastDateString:(NSString*)dateEndString;

@property (strong, nonatomic) NSDate * dateStart;
@property (strong, nonatomic) NSDate * dateEnd;
-(NSString*)lastDateSerialised;

@end




// Two dates period
@interface BFModelTimePeriodTodayRange : BFModelTimePeriod

+(instancetype)periodWithRangeConstant:(NSString*)rangeConstant;
+(instancetype)periodWithRangeLength:(NSNumber*)length;
+(instancetype)periodWithRangeLengthDays:(NSNumber*)lengthDays;

@property (strong, nonatomic) NSString * timePeriod;

@end
