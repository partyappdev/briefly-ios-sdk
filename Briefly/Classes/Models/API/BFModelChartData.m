//
//  BFModelChartData.m
//  Pods
//
//  Created by Artem Bondar on 08.12.17.
//
//

#import "BFModelChartData.h"
#import "EasyMapping.h"

@implementation BFModelChartData

+(instancetype)objectFromJson:(NSDictionary*)json
{
    NSString * units = json[@"unit"];
    NSNumber * minX = json[@"min_x"];
    NSNumber * maxX = json[@"max_x"];
    NSArray * points = json[@"pts"];
    if (minX == nil || maxX == nil || points == nil) {
        return nil;
    }
    if ([units isKindOfClass:[NSNull class]]) {
        units = nil;
    }
    if ([minX isKindOfClass:[NSNull class]]) {
        minX = nil;
    }
    if ([maxX isKindOfClass:[NSNull class]]) {
        maxX = nil;
    }
    if ([points isKindOfClass:[NSNull class]]) {
        units = nil;
    }
    
    BFModelChartData * obj = [BFModelChartData new];
    obj.units = units;
    obj.minX = minX.integerValue;
    obj.maxX = maxX.integerValue;
    obj.points = [BFModelChartDataPoint objectsFromJson:points];
    return obj;
}

@end

@implementation BFModelChartDataPoint

+(BFEObjectMapping *)objectMapping
{
    return [BFEObjectMapping mappingForClass:self withBlock:^(BFEObjectMapping *mapping) {
        
        [mapping mapPropertiesFromDictionary:@{
                                               @"x" : @"x",
                                               @"values" : @"values",
                                               @"date": @"date"
                                               }];
    }];
}

+(instancetype)objectFromJson:(NSDictionary*)json
{
    return [BFEMapper objectFromExternalRepresentation:json withMapping:[BFModelChartDataPoint objectMapping]];
}

+(NSArray<BFModelChartDataPoint*>*)objectsFromJson:(NSArray*)json
{
    return [BFEMapper arrayOfObjectsFromExternalRepresentation:json withMapping:[BFModelChartDataPoint objectMapping]];
}

-(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"];
    NSDate * dt = [dateFormatter dateFromString:self.date];

    NSDateFormatter *dateFormatterUser = [[NSDateFormatter alloc] init];
    dateFormatterUser.timeStyle = NSDateFormatterNoStyle;
    dateFormatterUser.dateStyle = NSDateFormatterMediumStyle;
    return [dateFormatterUser stringFromDate:dt];
}

@end
