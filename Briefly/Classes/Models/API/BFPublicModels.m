//
//  BFPublicModels.m
//  Pods
//
//  Created by Artem Bondar on 05.12.17.
//
//

#import "BFPublicModels.h"
#import "BFPublicModels_Private.h"

@implementation BFModelCalculateMetricResponseItem

+(BFEObjectMapping *)objectMapping
{
    return [BFEObjectMapping mappingForClass:self withBlock:^(BFEObjectMapping *mapping) {
        
        [mapping mapPropertiesFromDictionary:@{
                                               @"request" : @"request",
                                               @"value" : @"value",
                                               @"unit": @"unit"
                                               }];
    }];
}

+(instancetype)objectFromJson:(NSDictionary*)json
{
    return [BFEMapper objectFromExternalRepresentation:json withMapping:[BFModelCalculateMetricResponseItem objectMapping]];
}

+(NSArray<BFModelCalculateMetricResponseItem*>*)objectsFromJson:(NSArray*)json byLeavingOnlyRequests:(NSArray*)requests
{
    NSArray<BFModelCalculateMetricResponseItem*>* nonfiltered = [BFEMapper arrayOfObjectsFromExternalRepresentation:json withMapping:[BFModelCalculateMetricResponseItem objectMapping]];
    
    if (requests) {
        NSMutableArray * filtered = [NSMutableArray array];
        for (NSString * request in requests) {
            for (BFModelCalculateMetricResponseItem * item in nonfiltered) {
                if ([item.request isEqualToString:request]) {
                    [filtered addObject:item];
                    break;
                }
            }
        }
        return filtered;
    } else {
        return nonfiltered;
    }
}


@end


@implementation BFModelObjectMetricMeta

+(BFEObjectMapping *)objectMapping
{
    return [BFEObjectMapping mappingForClass:self withBlock:^(BFEObjectMapping *mapping) {
        
        [mapping mapPropertiesFromDictionary:@{
                                               @"metric_id" : @"metricId",
                                               @"default_request": @"defaultRequest",
                                               @"name" : @"name",
                                               @"unit": @"unit"
                                               }];
    }];
}

+(instancetype)objectFromJson:(NSDictionary*)json
{
    return [BFEMapper objectFromExternalRepresentation:json withMapping:[BFModelObjectMetricMeta objectMapping]];
}

+(NSArray<BFModelObjectMetricMeta*>*)objectsFromJson:(NSArray*)json
{
    return [BFEMapper arrayOfObjectsFromExternalRepresentation:json withMapping:[BFModelObjectMetricMeta objectMapping]];
}

-(NSString*)nameForUI
{
    if (self.name != nil) {
        return [self.name stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    }
    return self.metricId;
}

@end


@implementation BFModelObjectMeta

+(instancetype)objectFromJson:(NSDictionary*)json
{
    BFModelObjectMeta * ret = [BFModelObjectMeta new];
    ret.metrics = [BFModelObjectMetricMeta objectsFromJson:json[@"metrics"]];
    if ([json.allKeys containsObject:@"name"] && ![json[@"name"] isKindOfClass:[NSNull class]]) {
        ret.name = json[@"name"];
    }
    if ([json.allKeys containsObject:@"group_meta"] && ![json[@"group_meta"] isKindOfClass:[NSNull class]]) {
        ret.groupMeta = [BFModelObjectMeta objectFromJson:json[@"group_meta"]];
    }
    return ret;
}

-(NSString*)objectNameForUI
{
    if (self.name != nil) {
        return self.name;
    } else {
        return [self objectId];
    }
}

@end
