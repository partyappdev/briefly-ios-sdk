//
//  BFModelAccessToken.m
//  Briefly
//
//  Created by Artem Bondar on 06/02/2018.
//

#import "BFModelAccessToken.h"
#import "BFTokenParser.h"

typedef enum : NSUInteger {
    BFModelAccessScopeDataPost,
    BFModelAccessScopeReadAnalytics,
    BFModelAccessScopeServerPost,
    BFModelAccessScopeServerAdmin
} BFModelAccessScope;

@interface BFModelAccessToken ()

@property (nonatomic) BFModelAccessScope scope;
@property (nonatomic) NSString *jwtToken;

@end

@implementation BFModelAccessToken

+(BFModelAccessScope)accessScopeFromString:(NSString*)string
{
    if ([string isEqualToString:@"g"]) {
        return BFModelAccessScopeReadAnalytics;
    } else if ([string isEqualToString:@"s"]) {
        return BFModelAccessScopeServerPost;
    } else if ([string isEqualToString:@"a"]) {
        return BFModelAccessScopeServerAdmin;
    }
    return BFModelAccessScopeDataPost;
}

+(instancetype)tokenWithString:(NSString*)token
{
    BFModelAccessToken * newObj = [BFModelAccessToken new];
    NSDictionary * rights = [BFTokenParser keysFromToken:token];
    if (rights == nil) {
        return nil;
    }
    newObj.jwtToken = token;
    newObj.scope = [BFModelAccessToken accessScopeFromString:rights[@"s"]];
    return newObj;
}

-(NSString*)jwtAccessToken
{
    return self.jwtToken;
}

-(BOOL)haveAccessToWidgetGroupWithId:(NSString*)widgetGroupId andObjectId:(NSString*)objectId
{
    return self.scope != BFModelAccessScopeDataPost;
}

-(BOOL)userToken
{
    // User token mean, this user don't have any access to analytics
    return self.scope == BFModelAccessScopeDataPost;
}

-(BOOL)haveAccessToHeatmaps
{
    return ![self userToken];
}

-(BOOL)haveFullAccess
{
    return ![self userToken];
}

+(instancetype)tokenFromUserDefaults
{
    NSString * token = [[NSUserDefaults standardUserDefaults] objectForKey:@"bf_access_token"];
    if (token == nil) {
        return nil;
    }
    return [BFModelAccessToken tokenWithString:token];
}

-(void)storeToUserDefaults
{
    [[NSUserDefaults standardUserDefaults] setObject:self.jwtToken forKey:@"bf_access_token"];
}

@end
