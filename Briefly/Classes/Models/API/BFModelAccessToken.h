//
//  BFModelAccessToken.h
//  Briefly
//
//  Created by Artem Bondar on 06/02/2018.
//

#import <Foundation/Foundation.h>

@interface BFModelAccessToken : NSObject

// Constructors
+(instancetype)tokenFromUserDefaults;
+(instancetype)tokenWithString:(NSString*)token;

// Serialization
-(void)storeToUserDefaults;
-(NSString*)jwtAccessToken;

// Access checks
-(BOOL)userToken;
-(BOOL)haveAccessToHeatmaps;
-(BOOL)haveAccessToWidgetGroupWithId:(NSString*)widgetGroupId
                         andObjectId:(NSString*)objectId;

@end
