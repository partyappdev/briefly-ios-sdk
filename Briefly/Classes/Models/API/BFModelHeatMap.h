//
//  BFHeatMap.h
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import <Foundation/Foundation.h>

@interface BFModelHeatMap : NSObject

@property (nonatomic) BOOL base64Encoded;
@property (strong, nonatomic) NSString* viewId;
@property (strong, nonatomic) NSNumber* viewWidth;
@property (strong, nonatomic) NSNumber* viewHeight;
@property (strong, nonatomic) NSNumber* pointsInRow;
@property (strong, nonatomic) NSString* map;

+(instancetype)objectFromJson:(NSDictionary*)json;

@end



@interface BFHeatMap : NSObject

@property (strong, nonatomic) NSString* screenId;
@property (nonatomic) NSInteger size;
@property (nonatomic) NSInteger maxDensityScreenWide;

-(instancetype)initWithHeatMap:(BFModelHeatMap*)heatMap;
-(instancetype)initWithScreenId:(NSString*)screenId size:(NSInteger)size;

-(BOOL)addPointAtX:(CGFloat)x y:(CGFloat)y;
-(NSInteger)maxDensityForDimensions:(CGSize)dimensions;
-(void)heatmapForDimensions:(CGSize)dimensions
                withHandler:(void(^)(UIImage*))handler;

@end
