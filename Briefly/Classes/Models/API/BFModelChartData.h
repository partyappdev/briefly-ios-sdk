//
//  BFModelChartData.h
//  Pods
//
//  Created by Artem Bondar on 08.12.17.
//
//

#import <Foundation/Foundation.h>

@interface BFModelChartDataPoint : NSObject

@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSNumber *x;
@property (nonatomic, strong) NSArray<NSNumber*>* values;

+(instancetype)objectFromJson:(NSDictionary*)json;
+(NSArray<BFModelChartDataPoint*>*)objectsFromJson:(NSArray*)json;

-(NSString*)dateString;
@end

@interface BFModelChartData : NSObject

@property (nonatomic) NSInteger minX;
@property (nonatomic) NSInteger maxX;
@property (nonatomic, strong) NSString *units;
@property (nonatomic, strong) NSArray<BFModelChartDataPoint*> *points;

+(instancetype)objectFromJson:(NSDictionary*)json;

@end
