//
//  BFHeatMap.m
//  Pods
//
//  Created by Artem Bondar on 01.12.17.
//
//

#import "BFModelHeatMap.h"
#import "EasyMapping.h"
#import "BFBrieflyManager.h"
#import <Base64/MF_Base64Additions.h>

// Adjustment variables for weights adjustment
static float bf_weightSensitivity = 0; // Percents from maximum weight
static float bf_weightBoostTo = 80; // Percents to boost least sensible weight to


// Adjustment variables for grouping
static int bf_groupingThreshold = 1;  // Increasing this will improve performance with less accuracy. Negative will disable grouping
static int bf_peaksRemovalThreshold = 5; // Should be greater than groupingThreshold
static float bf_peaksRemovalFactor = 0.3; // Should be from 0 (no peaks removal) to 1 (peaks are completely lowered to zero)

@interface BFHeatMap ()

@property (nonatomic) NSInteger* values;
@property (nonatomic) NSInteger maxDensityOriginal;

@end

@implementation BFHeatMap

-(instancetype)initWithScreenId:(NSString*)screenId size:(NSInteger)size
{
    self = [super init];
    self.size = size;
    self.screenId = screenId;
    self.values = (NSInteger*)calloc(size * size, sizeof(NSInteger));
    if (self.values) {
        memset(self.values, 0, sizeof(NSInteger) * size * size);
    }
    self.maxDensityScreenWide = -1;
    self.maxDensityOriginal = 0;
    return self;
}

-(instancetype)initWithHeatMap:(BFModelHeatMap*)heatMap
{
    self = [super init];
    self.size = heatMap.pointsInRow.integerValue;
    self.values = (NSInteger*)calloc(self.size * self.size, sizeof(NSInteger));
    self.maxDensityScreenWide = 0;
    self.maxDensityOriginal = -1;
    
    if (heatMap.base64Encoded) {
        NSData * dt = [NSData dataWithBase64String:heatMap.map];
        if (self.size * self.size * sizeof(NSInteger) == dt.length) {
            memcpy(self.values, dt.bytes, dt.length);
        } else {
            memset(self.values, 0, sizeof(NSInteger) * self.size * self.size);
            [[BFBrieflyManager sharedManager] logRecord:[NSString stringWithFormat:@"Returned heatmap has unexpected size: %ld (expected %ld)", (unsigned long)dt.length, self.size * self.size * sizeof(NSInteger)] logLevel:BFLogLevelError];
        }
    } else {
        NSArray * intValues = [heatMap.map componentsSeparatedByString:@","];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        [intValues enumerateObjectsWithOptions:NSEnumerationReverse
                                    usingBlock:^(NSString * val, NSUInteger idx, BOOL * _Nonnull stop) {
                                    
                                        NSNumber *numVal = [f numberFromString:val];
                                        [self setValue:numVal.integerValue forLianeralizedIdx:idx];
                                    }];
    }
    return self;
}

-(void)dealloc
{
    if (self.values) {
        free(self.values);
    }
}

-(BOOL)addPointAtX:(CGFloat)x y:(CGFloat)y
{
    if (x < 0 || x >= 1 || y < 0 || y >= 1) {
        return NO;
    }
    NSInteger intX = (NSInteger)floor((CGFloat)self.size * MIN(1, MAX(x, 0)));
    NSInteger intY = (NSInteger)floor((CGFloat)self.size * MIN(1, MAX(y, 0)));
    NSInteger newVal = [self valueAtX:intX y:intY] + 1;
    [self setValue:newVal atX:intX y:intY];
    return YES;
}

-(NSInteger)valueAtX:(NSInteger)x y:(NSInteger)y
{
    if (!self.values) {
        return 0;
    }
    return self.values[x + y * self.size];
}

-(void)setValue:(NSInteger)value atX:(NSInteger)x y:(NSInteger)y
{
    if (!self.values) {
        return;
    }
    self.values[x + y * self.size] = value;
}

//
//
// Build heatmap image helpers
//
//
-(NSInteger)lianeralizedIdxCount
{
    return self.size * self.size;
}

-(NSInteger)xForLianeralizedIdx:(NSInteger)idx
{
    return idx % self.size;
}

-(NSInteger)yForLianeralizedIdx:(NSInteger)idx
{
    return idx / self.size;
}

-(NSInteger)valueForLianeralizedIdx:(NSInteger)idx
{
    return [self valueAtX:[self xForLianeralizedIdx:idx] y:[self yForLianeralizedIdx:idx]];
}

-(void)setValue:(NSInteger)value forLianeralizedIdx:(NSInteger)idx
{
    [self setValue:value atX:[self xForLianeralizedIdx:idx] y:[self yForLianeralizedIdx:idx]];
}


inline static int isqrt(int x)
{
    static const int sqrttable[] = {
        0, 16, 22, 27, 32, 35, 39, 42, 45, 48, 50, 53, 55, 57,
        59, 61, 64, 65, 67, 69, 71, 73, 75, 76, 78, 80, 81, 83,
        84, 86, 87, 89, 90, 91, 93, 94, 96, 97, 98, 99, 101, 102,
        103, 104, 106, 107, 108, 109, 110, 112, 113, 114, 115, 116, 117, 118,
        119, 120, 121, 122, 123, 124, 125, 126, 128, 128, 129, 130, 131, 132,
        133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 144, 145,
        146, 147, 148, 149, 150, 150, 151, 152, 153, 154, 155, 155, 156, 157,
        158, 159, 160, 160, 161, 162, 163, 163, 164, 165, 166, 167, 167, 168,
        169, 170, 170, 171, 172, 173, 173, 174, 175, 176, 176, 177, 178, 178,
        179, 180, 181, 181, 182, 183, 183, 184, 185, 185, 186, 187, 187, 188,
        189, 189, 190, 191, 192, 192, 193, 193, 194, 195, 195, 196, 197, 197,
        198, 199, 199, 200, 201, 201, 202, 203, 203, 204, 204, 205, 206, 206,
        207, 208, 208, 209, 209, 210, 211, 211, 212, 212, 213, 214, 214, 215,
        215, 216, 217, 217, 218, 218, 219, 219, 220, 221, 221, 222, 222, 223,
        224, 224, 225, 225, 226, 226, 227, 227, 228, 229, 229, 230, 230, 231,
        231, 232, 232, 233, 234, 234, 235, 235, 236, 236, 237, 237, 238, 238,
        239, 240, 240, 241, 241, 242, 242, 243, 243, 244, 244, 245, 245, 246,
        246, 247, 247, 248, 248, 249, 249, 250, 250, 251, 251, 252, 252, 253,
        253, 254, 254, 255
    };
    
    int xn;
    
    if (x >= 0x10000)
    {
        if (x >= 0x1000000)
        {
            if (x >= 0x10000000)
            {
                if (x >= 0x40000000)
                {
                    xn = sqrttable[x >> 24] << 8;
                }
                else
                {
                    xn = sqrttable[x >> 22] << 7;
                }
            }
            else
            {
                if (x >= 0x4000000)
                {
                    xn = sqrttable[x >> 20] << 6;
                }
                else
                {
                    xn = sqrttable[x >> 18] << 5;
                }
            }
            
            xn = (xn + 1 + (x / xn)) >> 1;
            xn = (xn + 1 + (x / xn)) >> 1;
            
            return ((xn * xn) > x) ? --xn : xn;
        }
        else
        {
            if (x >= 0x100000)
            {
                if (x >= 0x400000)
                {
                    xn = sqrttable[x >> 16] << 4;
                }
                else
                {
                    xn = sqrttable[x >> 14] << 3;
                }
            }
            else
            {
                if (x >= 0x40000)
                {
                    xn = sqrttable[x >> 12] << 2;
                }
                else
                {
                    xn = sqrttable[x >> 10] << 1;
                }
            }
            
            xn = (xn + 1 + (x / xn)) >> 1;
            
            return ((xn * xn) > x) ? --xn : xn;
        }
    }
    else
    {
        if (x >= 0x100)
        {
            if (x >= 0x1000)
            {
                if (x >= 0x4000)
                {
                    xn = (sqrttable[x >> 8] ) + 1;
                }
                else
                {
                    xn = (sqrttable[x >> 6] >> 1) + 1;
                }
            }
            else
            {
                if (x >= 0x400)
                {
                    xn = (sqrttable[x >> 4] >> 2) + 1;
                }
                else
                {
                    xn = (sqrttable[x >> 2] >> 3) + 1;
                }
            }
            
            return ((xn * xn) > x) ? --xn : xn;
        }
        else
        {
            if (x >= 0)
            {
                return sqrttable[x] >> 4;
            }
            else
            {
                return -1; // negative argument...
            }
        }
    }
}

-(void)heatmapForDimensions:(CGSize)dimensions
                    withHandler:(void(^)(UIImage*))handler
{
    if (self.size == 0) {
        handler(nil);
        return;
    }
    [self heatMapWithRect:CGRectMake(0, 0, dimensions.width, dimensions.height) boost:3.0 onlyUpdateMaxValue:NO weightsAdjustmentEnabled:YES groupingEnabled:YES withHandler:handler];
}

-(NSInteger)maxDensityForDimensions:(CGSize)dimensions
{
    if (self.size == 0) {
        return 0;
    }
    [self heatMapWithRect:CGRectMake(0, 0, dimensions.width, dimensions.height) boost:3.0 onlyUpdateMaxValue:YES weightsAdjustmentEnabled:YES groupingEnabled:YES withHandler:^(UIImage *img) {
        // do nothing
    }];
    return self.maxDensityOriginal;
}

- (void)heatMapWithRect:(CGRect)rect
                       boost:(float)boost
          onlyUpdateMaxValue:(BOOL)onlyUpdateMax
    weightsAdjustmentEnabled:(BOOL)weightsAdjustmentEnabled
             groupingEnabled:(BOOL)groupingEnabled
                 withHandler:(void(^)(UIImage*))handler
{
    // TODO: at checking density we can store most of calculations here
    // to avoid recalculaitons in future (or probably not)
    if (rect.size.width == 0 || rect.size.height == 0) {
        handler(nil);
        return;
    }
    
    int width = rect.size.width;
    int height = rect.size.height;
    int i, j;
    
    // According to heatmap API, boost is heat radius multiplier
    int radius = 5 * boost;
    
    // RGBA array is initialized with 0s
    unsigned char* rgba = (unsigned char*)calloc(width*height*4, sizeof(unsigned char));
    int* density = (int*)calloc(width*height, sizeof(int));
    memset(density, 0, sizeof(int) * width*height);
    
    // Step 1
    // Copy points into plain array (plain array iteration is faster than accessing NSArray objects)
    int points_num = (int)[self lianeralizedIdxCount];
    int *point_x = malloc(sizeof(int) * points_num);
    int *point_y = malloc(sizeof(int) * points_num);
    int *point_weight_percent = malloc(sizeof(int) * points_num);
    float *point_weight = malloc(sizeof(float) * points_num);
    float max_weight = 0;
    i = 0;
    j = 0;
    for (NSInteger idxLen = 0; idxLen < [self lianeralizedIdxCount]; ++ idxLen) {
        if ([self valueForLianeralizedIdx:idxLen] > 0) {
            point_x[i] = rect.size.width * (((CGFloat)[self xForLianeralizedIdx:idxLen]) / (CGFloat)self.size);
            point_y[i] = rect.size.height * (((CGFloat)[self yForLianeralizedIdx:idxLen]) / (CGFloat)self.size);
            point_weight[i] = [self valueForLianeralizedIdx:idxLen];
            if (max_weight < point_weight[i])
                max_weight = point_weight[i];
            i++;
            j++;
        } else {
            points_num--;
        }
    }
    
    // Step 1.5
    // Normalize weights to be 0 .. 100 (like percents)
    // Weights array should be integer for not slowing down calculation by
    // int-float conversion and float multiplication
    float absWeightSensitivity = ( max_weight / 100.0 ) * bf_weightSensitivity;
    float absWeightBoostTo = ( max_weight / 100.0 ) * bf_weightBoostTo;
    for (i = 0; i < points_num; i++)
    {
        if (weightsAdjustmentEnabled)
        {
            if (point_weight[i] <= absWeightSensitivity)
                point_weight[i] *= absWeightBoostTo / absWeightSensitivity;
            else
                point_weight[i] = absWeightBoostTo + ( point_weight[i] - absWeightSensitivity ) * ((max_weight - absWeightBoostTo) / (max_weight - absWeightSensitivity));
        }
        point_weight_percent[i] = 100.0 * (point_weight[i] / max_weight);
    }
    free(point_weight);
    
    // Step 1.75 (optional)
    // Grouping and filtering bunches of points in same location
    int currentDistance;
    int currentDensity;
    
    if (groupingEnabled)
    {
        for (i = 0; i < points_num; i++)
        {
            if (point_weight_percent[i]> 0)
            {
                for (j = i + 1; j < points_num; j++)
                {
                    if (point_weight_percent[j]> 0)
                    {
                        currentDistance = isqrt((point_x[i] - point_x[j])*(point_x[i] - point_x[j]) + (point_y[i] - point_y[j])*(point_y[i] - point_y[j]));
                        
                        if (currentDistance > bf_peaksRemovalThreshold)
                            currentDistance = bf_peaksRemovalThreshold;
                        
                        float K1 = 1 - bf_peaksRemovalFactor;
                        float K2 = bf_peaksRemovalFactor;
                        
                        // Lowering peaks
                        point_weight_percent[i] =
                        K1 * point_weight_percent[i] +
                        K2 * point_weight_percent[i] * (float) ((float)(currentDistance) / (float)bf_peaksRemovalThreshold);
                        
                        // Performing grouping if two points are closer than groupingThreshold
                        if (currentDistance <= bf_groupingThreshold)
                        {
                            // Merge i and j points. Store result in [i], remove [j]
                            point_x[i] = (point_x[i] + point_x[j]) / 2;
                            point_y[i] = (point_y[i] + point_y[j]) / 2;
                            point_weight_percent[i] = point_weight_percent[i] + point_weight_percent[j];
                            
                            // point_weight_percent[j] is set negative to be avoided
                            point_weight_percent[j] = -10;
                            
                            // Repeat again for new point
                            i--;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    // Step 2
    // Fill density info. Density is calculated around each point
    int from_x, from_y, to_x, to_y;
    for (i = 0; i < points_num; i++)
    {
        if (point_weight_percent[i]> 0)
        {
            from_x = point_x[i] - radius;
            from_y = point_y[i] - radius;
            to_x = point_x[i] + radius;
            to_y = point_y[i] + radius;
            
            if (from_x < 0)
                from_x = 0;
            if (from_y < 0)
                from_y = 0;
            if (to_x > width)
                to_x = width;
            if (to_y > height)
                to_y = height;
            
            
            for (int y = from_y; y < to_y; y++)
            {
                for (int x = from_x; x < to_x; x++)
                {
                    currentDistance = (x - point_x[i])*(x - point_x[i]) + (y - point_y[i])*(y - point_y[i]);
                    
                    currentDensity = radius - isqrt(currentDistance);
                    if (currentDensity < 0)
                        currentDensity = 0;
                    
                    density[y*width + x] += currentDensity * point_weight_percent[i];
                }
            }
        }
    }
    
    
    free(point_x);
    free(point_y);
    free(point_weight_percent);

    // Step 2.5
    // Find max density (doing this in step 2 will have less performance)
    int maxDensity = density[0];
    for (i = 1; i < width * height; i++)
    {
        if (maxDensity < density[i])
            maxDensity = density[i];
    }
    
    if (onlyUpdateMax) {
        self.maxDensityOriginal = maxDensity;
        free(density);
        free(rgba);
        handler(nil);
        return;
    } else {
        if (self.maxDensityScreenWide > maxDensity) {
            maxDensity = (int)self.maxDensityScreenWide;
        }
    }

    // Step 3
    // Render density info into raw RGBA pixels
    i = 0;
    float floatDensity;
    uint indexOrigin;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++, i++)
        {
            if (density[i] > 0)
            {
                indexOrigin = 4*i;
                // Normalize density to 0..1
                floatDensity = (float)density[i] / (float)maxDensity;
                
                // Red and alpha component
                rgba[indexOrigin] = floatDensity * 255;
                rgba[indexOrigin+3] = rgba[indexOrigin];
                
                // Green component
                if (floatDensity >= 0.75)
                    rgba[indexOrigin+1] = rgba[indexOrigin];
                else if (floatDensity >= 0.5)
                    rgba[indexOrigin+1] = (floatDensity - 0.5) * 255 * 3;
                
                
                // Blue component
                if (floatDensity >= 0.8)
                    rgba[indexOrigin+2] = (floatDensity - 0.8) * 255 * 5;
            }
        }
    }
    
    free(density);
    
    // Step 4
    // Create image from rendered raw data
    dispatch_async(dispatch_get_main_queue(), ^{
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGContextRef bitmapContext = CGBitmapContextCreate(rgba,
                                                           width,
                                                           height,
                                                           8, // bitsPerComponent
                                                           4 * width, // bytesPerRow
                                                           colorSpace,
                                                           kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
        
        CFRelease(colorSpace);
        
        CGImageRef cgImage = CGBitmapContextCreateImage(bitmapContext);
        
        UIImage * image = [UIImage imageWithCGImage:cgImage];
        
        CFRelease(cgImage);
        CFRelease(bitmapContext);
        
        free(rgba);
        handler(image);
    });
}


@end

@implementation BFModelHeatMap

+(BFEObjectMapping *)objectMapping
{
    return [BFEObjectMapping mappingForClass:self withBlock:^(BFEObjectMapping *mapping) {
        
        [mapping mapPropertiesFromDictionary:@{
                                               @"view_id" : @"viewId",
                                               @"view_width" : @"viewWidth",
                                               @"view_height" : @"viewHeight",
                                               @"points_in_row": @"pointsInRow",
                                               @"map": @"map"
                                               }];
    }];
}

+(instancetype)objectFromJson:(NSDictionary*)json
{
    return [BFEMapper objectFromExternalRepresentation:json withMapping:[BFModelHeatMap objectMapping]];
}

@end
