//
//  BFPublicModels.h
//  Pods
//
//  Created by Artem Bondar on 05.12.17.
//
//

#import <Foundation/Foundation.h>

@interface BFModelCalculateMetricResponseItem : NSObject

@property (readonly, strong, nonatomic) NSString* request;
@property (readonly, strong, nonatomic) NSNumber * value;
@property (readonly, strong, nonatomic) NSString * unit;

+(instancetype)objectFromJson:(NSDictionary*)json;
+(NSArray<BFModelCalculateMetricResponseItem*>*)objectsFromJson:(NSArray*)json byLeavingOnlyRequests:(NSArray*)requests;

@end



@interface BFModelObjectMetricMeta : NSObject

@property (strong, nonatomic) NSString* metricId;
@property (strong, nonatomic) NSString * unit;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * defaultRequest;

+(instancetype)objectFromJson:(NSDictionary*)json;
+(NSArray<BFModelObjectMetricMeta*>*)objectsFromJson:(NSArray*)json;
-(NSString*)nameForUI;

@end



@class BFModelObjectMeta;

@interface BFModelObjectMeta : NSObject

@property (strong, nonatomic) BFModelObjectMeta * groupMeta;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* objectId;
@property (strong, nonatomic) NSArray<BFModelObjectMetricMeta*>* metrics;

-(NSString*)objectNameForUI;

+(instancetype)objectFromJson:(NSDictionary*)json;

@end
