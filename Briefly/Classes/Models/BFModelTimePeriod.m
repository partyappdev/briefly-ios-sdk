//
//  BFModelTimePeriod.m
//  Briefly
//
//  Created by Artem Bondar on 29/01/2018.
//

#import "BFModelTimePeriod.h"
#import "BFFSCalendar.h"
#import "NSString+Utils.h"
#import "NSDate+BfUtils.h"

@interface BFModelTimePeriod ()

+(NSNumber*)periodDay;
+(NSNumber*)periodMonth;
+(NSNumber*)periodWeek;

@end

// Private constructors
@interface BFModelTimePeriodDatesRange ()

+(BOOL)isDatesRangeString:(NSString*)string;
+(instancetype)periodWithSerializedString:(NSString*)string;

@end

@interface BFModelTimePeriodTodayRange ()

+(instancetype)periodWithSerializedString:(NSString*)string;

@end




// Timeranges implementation
@implementation BFModelTimePeriod

+(instancetype)defaultRange
{
    return [BFModelTimePeriodTodayRange periodWithRangeLengthDays:@(30)];
}

+(instancetype)periodWithSerializedString:(NSString*)string
{
    // check if it hase a comma as a separator
    if ([BFModelTimePeriodDatesRange isDatesRangeString:string]) {
        // two arguments: try to build two-dates string
        return [BFModelTimePeriodDatesRange periodWithSerializedString:string];
    } else {
        return [BFModelTimePeriodTodayRange periodWithSerializedString:string];
    }
}

-(NSString*)serializedString
{
    return @"";
}

-(NSArray<NSDate*>*)datesRange
{
    return @[];
}

-(NSDictionary*)timerangeKeysForRequest
{
    return @{};
}

-(NSString*)numanReadable
{
    return nil;
}

-(BFModelTimePeriod*)periodByAddingDaysShift:(NSInteger)daysShift
                             withCurrentDate:(NSDate*)currentDate;
{
    return self;
}

-(BFModelTimePeriod*)previousRangeWithDaysLength:(NSInteger)days
                                         maxDate:(NSDate*)maxDate
{
    return self;
}

-(BFModelTimePeriod*)nextRangeWithDaysLength:(NSInteger)days
                                     maxDate:(NSDate*)maxDate
{
    return self;
}

+(NSNumber*)periodDay
{
    return @(60 * 60 * 24);
}

+(NSNumber*)periodMonth
{
    return @(60 * 60 * 24 * 30);
}

+(NSNumber*)periodWeek
{
    return @(60 * 60 * 24 * 7);
}

@end




// Two dates period
@implementation BFModelTimePeriodDatesRange

+(BOOL)isDatesRangeString:(NSString*)string
{
    // TODO: make a real checking
    if ([string containsString:@","] || [string containsString:@"-"]) {
        return YES;
    }
    return NO;
}

+(instancetype)periodWithSerializedString:(NSString*)string
{
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray * dates = [string componentsSeparatedByString:@","];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    formatter.calendar = [BFFSCalendar utcCalendar];
    formatter.timeZone = formatter.calendar.timeZone;
    BFModelTimePeriodDatesRange * newObj = [BFModelTimePeriodDatesRange new];
    newObj.dateStart = [formatter dateFromString:dates.firstObject];
    newObj.dateEnd = [formatter dateFromString:dates.lastObject];
    return newObj;
}

+(instancetype)periodWithDateStart:(NSDate*)dateStart dateEnd:(NSDate*)dateEnd
{
    BFModelTimePeriodDatesRange * newObj = [BFModelTimePeriodDatesRange new];
    newObj.dateStart = dateStart;
    newObj.dateEnd = dateEnd;
    return newObj;
}

+(instancetype)periodWithLastDateString:(NSString*)dateEndString
{
    BFModelTimePeriodDatesRange * newObj = [BFModelTimePeriodDatesRange new];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    formatter.calendar = [BFFSCalendar utcCalendar];
    formatter.timeZone = formatter.calendar.timeZone;
    NSDate * dateEnd = [formatter dateFromString:dateEndString];
    newObj.dateStart = dateEnd;
    newObj.dateEnd = dateEnd;
    return newObj;
}

-(NSString*)lastDateSerialised
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    formatter.calendar = [BFFSCalendar utcCalendar];
    formatter.timeZone = formatter.calendar.timeZone;
    return [formatter stringFromDate:self.dateEnd];
}

-(NSString *)serializedString
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    formatter.calendar = [BFFSCalendar utcCalendar];
    formatter.timeZone = formatter.calendar.timeZone;
    return [NSString stringWithFormat:@"%@,%@", [formatter stringFromDate:self.dateStart], [formatter stringFromDate:self.dateEnd]];
}

-(BOOL)isEqual:(id)object
{
    BFModelTimePeriodDatesRange * typedObj = (BFModelTimePeriodDatesRange*)object;
    if ([typedObj isKindOfClass:[BFModelTimePeriodDatesRange class]]) {
        return [typedObj.dateStart isEqualToDate:self.dateStart] && [typedObj.dateEnd isEqualToDate:self.dateEnd];
    }
    return NO;
}

-(NSArray<NSDate*>*)datesRange
{
    NSMutableArray * res = [NSMutableArray array];
    NSDate * currentDate = self.dateStart;
    while ([currentDate compare:self.dateEnd] != NSOrderedDescending) {
        [res addObject:currentDate];
        currentDate = [currentDate dateByAddingTimeInterval:86400.0];
    }
    return res;
}

-(NSDictionary*)timerangeKeysForRequest
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    formatter.calendar = [BFFSCalendar utcCalendar];
    formatter.timeZone = formatter.calendar.timeZone;
    return @{@"start_date": [formatter stringFromDate:self.dateStart], @"end_date": [formatter stringFromDate:self.dateEnd]};
}

-(NSString*)numanReadable
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateStyle = NSDateFormatterShortStyle;
    formatter.timeStyle = NSDateFormatterNoStyle;
    if ([self.dateEnd isEqualToDate:self.dateStart]) {
        return [NSString stringWithFormat:@"BfTimeRangeOneDate".bf_localized, [formatter stringFromDate:self.dateEnd]];
    } else {
        return [NSString stringWithFormat:@"BfTimeRangeDatesRange".bf_localized, [formatter stringFromDate:self.dateStart], [formatter stringFromDate:self.dateEnd]];
    }
    return nil;
}

-(BFModelTimePeriod*)periodByAddingDaysShift:(NSInteger)daysShift
                             withCurrentDate:(NSDate*)currentDate
{
    NSDate * dateStart = [self.dateStart bfDateByShiftingForDays:daysShift];
    NSDate * dateEnd = [self.dateEnd bfDateByShiftingForDays:daysShift];
    
    // if the period fully lays in the future - then return NULL
    if ([dateStart compare:currentDate] == NSOrderedDescending) {
        return nil;
    }
    
    NSComparisonResult endDateCompare = [dateEnd compare:currentDate];
    if (endDateCompare == NSOrderedDescending || endDateCompare == NSOrderedSame) {
        return [BFModelTimePeriodTodayRange periodWithRangeLengthDays:@([currentDate bfDaysFromDate:dateStart] + 1)];
    } else {
        return [BFModelTimePeriodDatesRange periodWithDateStart:dateStart dateEnd:dateEnd];
    }
}

-(BFModelTimePeriod*)previousRangeWithDaysLength:(NSInteger)days
                                         maxDate:(NSDate*)maxDate
{
    NSDate * endDate = [self.dateStart bfDateByShiftingForDays:-1];
    NSDate * startDate = [endDate bfDateByShiftingForDays:-1 * days];
    return [BFModelTimePeriodDatesRange periodWithDateStart:startDate dateEnd:endDate];
}

-(BFModelTimePeriod*)nextRangeWithDaysLength:(NSInteger)days
                                     maxDate:(NSDate*)maxDate
{
    NSDate * startDate = [self.dateEnd bfDateByShiftingForDays:1];
    NSDate * endDate = [startDate bfDateByShiftingForDays:days];
    
    if ([startDate compare:maxDate] == NSOrderedDescending) {
        // it's too much
        return nil;
    }

    if ([endDate compare:maxDate] == NSOrderedDescending) {
        endDate = maxDate;
        return [BFModelTimePeriodTodayRange periodWithRangeLengthDays:@([endDate bfDaysFromDate:startDate] + 1)];
    }
    return [BFModelTimePeriodDatesRange periodWithDateStart:startDate dateEnd:endDate];
}

@end



// Two dates period
@implementation BFModelTimePeriodTodayRange

+(instancetype)periodWithSerializedString:(NSString*)string
{
    if ([string isEqualToString:@"month"] || [string isEqualToString:@"week"] || [string isEqualToString:@"day"]) {
        return [BFModelTimePeriodTodayRange periodWithRangeConstant:string];
    }
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    NSNumber * num = [formatter numberFromString:string];
    return [BFModelTimePeriodTodayRange periodWithRangeLength:num];
}

+(instancetype)periodWithRangeConstant:(NSString*)rangeConstant
{
    BFModelTimePeriodTodayRange * newObj = [BFModelTimePeriodTodayRange new];
    newObj.timePeriod = rangeConstant;
    return newObj;
}

+(instancetype)periodWithRangeLength:(NSNumber*)length
{
    BFModelTimePeriodTodayRange * newObj = [BFModelTimePeriodTodayRange new];
    newObj.timePeriod = @(length.integerValue).stringValue;
    return newObj;
}

+(instancetype)periodWithRangeLengthDays:(NSNumber*)lengthDays
{
    BFModelTimePeriodTodayRange * newObj = [BFModelTimePeriodTodayRange new];
    newObj.timePeriod = @(lengthDays.integerValue * [BFModelTimePeriod periodDay].integerValue).stringValue;
    return newObj;
}

-(NSString *)serializedString
{
    return self.timePeriod;
}

-(NSNumber*)timeRangeNumber
{
    if ([self.timePeriod isEqualToString:@"month"]) {
        return [BFModelTimePeriod periodMonth];
    }
    if ([self.timePeriod isEqualToString:@"week"]) {
        return [BFModelTimePeriod periodWeek];
    }
    if ([self.timePeriod isEqualToString:@"day"]) {
        return [BFModelTimePeriod periodDay];
    }
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    NSNumber * num = [formatter numberFromString:self.timePeriod];
    return num;
}

-(BOOL)isEqual:(id)object
{
    BFModelTimePeriodTodayRange * typedObj = (BFModelTimePeriodTodayRange*)object;
    if ([typedObj isKindOfClass:[BFModelTimePeriodTodayRange class]]) {
        return [typedObj.timeRangeNumber isEqualToNumber:self.timeRangeNumber];
    }
    return NO;
}

-(NSDate *)beginningOfToday
{
    NSCalendar *calendar = [BFFSCalendar utcCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    return [calendar dateFromComponents:components];
}

-(NSArray<NSDate*>*)datesRange
{
    NSMutableArray * res = [NSMutableArray array];
    NSDate * endDate = [self beginningOfToday];
    NSDate * currentDate = [endDate dateByAddingTimeInterval:-1 * ([self timeRangeNumber].integerValue - 86400.0)];
    while ([currentDate compare:endDate] != NSOrderedDescending) {
        [res addObject:[[BFFSCalendar utcCalendar] dateBySettingHour:0 minute:0 second:0 ofDate:currentDate options:0]];
        currentDate = [currentDate dateByAddingTimeInterval:86400.0];
    }
    return res;
}

-(NSDictionary*)timerangeKeysForRequest
{
    return @{@"for_days_count": @(self.timeRangeNumber.integerValue / 86400)};
}

-(NSString*)numanReadable
{
    if ([self.timePeriod isEqualToString:@"month"]) {
        return @"BfTimeRangeMonth".bf_localized;
    }
    if ([self.timePeriod isEqualToString:@"week"]) {
        return @"BfTimeRangeWeek".bf_localized;
    }
    if ([self.timePeriod isEqualToString:@"day"]) {
        return @"BfTimeRangeDay".bf_localized;
    }
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    NSNumber * num = [formatter numberFromString:self.timePeriod];
    return [@"BfForLastNDays" bf_localizedWithFormattedValue:num.integerValue / 86400];
}

-(BFModelTimePeriod*)periodByAddingDaysShift:(NSInteger)daysShift
                             withCurrentDate:(NSDate*)currentDate
{
    NSDate * dateStart = [currentDate bfDateByShiftingForDays:-1 * ((self.timeRangeNumber.integerValue/86400) - 1) + daysShift];
    NSDate * dateEnd = [currentDate bfDateByShiftingForDays:daysShift];

    // if the period fully lays in the future - then return NULL
    if ([dateStart compare:currentDate] == NSOrderedDescending) {
        return nil;
    }
    
    NSComparisonResult endDateCompare = [dateEnd compare:currentDate];
    if (endDateCompare == NSOrderedDescending || endDateCompare == NSOrderedSame) {
        return [BFModelTimePeriodTodayRange periodWithRangeLengthDays:@([currentDate bfDaysFromDate:dateStart] + 1)];
    } else {
        return [BFModelTimePeriodDatesRange periodWithDateStart:dateStart dateEnd:dateEnd];
    }
}

-(BFModelTimePeriod*)previousRangeWithDaysLength:(NSInteger)days
                                         maxDate:(NSDate*)maxDate
{
    NSDate * endDate = maxDate;
    NSDate * startDate = [endDate bfDateByShiftingForDays:-1 * days];
    return [BFModelTimePeriodDatesRange periodWithDateStart:startDate dateEnd:endDate];
}

-(BFModelTimePeriod*)nextRangeWithDaysLength:(NSInteger)days
                                     maxDate:(NSDate*)maxDate
{
    // There aren't next period
    return nil;
}

@end
