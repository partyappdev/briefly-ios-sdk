//
//  BFBrieflyManager.h
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import <Foundation/Foundation.h>
#import "BFResposesCache.h"
#import "BFUIManager.h"
#import "BrieflyFacade.h"

typedef enum : NSUInteger {
    BFLogLevelTrace = 0,
    BFLogLevelDebug = 1,
    BFLogLevelWarning = 2,
    BFLogLevelError = 3,
    BFLogLevelFatal = 666
} BFLogLevel;

@class BFModelAccessToken;

@interface BFBrieflyManager : NSObject

#pragma mark PublicApi

// Base configuration
-(void)configureWithApiKey:(NSString*)apiKey;
-(void)configureWithApiKey:(NSString*)apiKey accessToken:(NSString*)accessToken;
-(void)configureWithApiKey:(NSString*)apiKey widgetsOn:(BOOL)widgetsOn;

// Base options
-(void)setClientId:(NSString*)clientId version:(NSString*)version;
-(void)setUserId:(NSString*)userId;
@property (nonatomic) BFStage stage;
@property (nonatomic) NSInteger maxRequestAttempts;
@property (nonatomic) BOOL perScreenTrackingId;

// Debugging
-(void)setHeatmapsDebugMode:(BFHeatmapsMode)mode;
-(void)turnOnDebugLogging;
-(void)turnOffDebugLogging;

// Handle access keys
-(BOOL)handleOpeningUrl:(NSURL*)url withOptions:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;

// Access policy
-(NSString*)currentAccessToken;
-(void)setCurrentAccessToken:(NSString*)accessToken;

#pragma mark InternalApi

@property (strong, nonatomic) BFResposesCache * cache;
@property (strong, nonatomic) NSString * currentUserId;

+(instancetype)sharedManager;

-(BFLogLevel)currentLogLevel;
-(void)logRecord:(NSString*)record;
-(void)logRecord:(NSString*)record logLevel:(BFLogLevel)logLevel;
-(NSInteger)currentTimestamp;
-(BFUIManager*)uiManager;

// Access policy
-(BOOL)accessIsUser;
-(BOOL)accessShowsHeatmaps;
-(BOOL)accessHaveToWidgetGroupWithId:(NSString*)widgetGroupId
                         andObjectId:(NSString*)objectId;

@end
