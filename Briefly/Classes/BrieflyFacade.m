//
//  Briefly.m
//  Pods
//
//  Created by Artem Bondar on 13.11.17.
//
//

#import "BrieflyFacade.h"
#import "BFBrieflyManager.h"
#import "BFBrieflyManager+ApiWrapper.h"

@implementation Briefly

+(void)configureWithApiKey:(NSString* _Nonnull)apiKey
{
    [[BFBrieflyManager sharedManager] configureWithApiKey:apiKey];
}

+(void)configureWithApiKey:(NSString* _Nonnull)apiKey
               accessToken:(NSString* _Nonnull)accessToken
{
    [[BFBrieflyManager sharedManager] configureWithApiKey:apiKey accessToken:accessToken];
}

+(void)configureWithApiKey:(NSString* _Nonnull)apiKey
                 widgetsOn:(BOOL)widgetsOn
{
    [[BFBrieflyManager sharedManager] configureWithApiKey:apiKey widgetsOn:widgetsOn];
}

+(void)setClientId:(NSString* _Nonnull)clientId
           version:(NSString* _Nonnull)version
{
    [[BFBrieflyManager sharedManager] setClientId:clientId version:version];
}

+(void)setUserId:(NSString* _Nullable)userId
{
    [[BFBrieflyManager sharedManager] setUserId:userId];
}

+(void)turnOnDebugLogging
{
    [[BFBrieflyManager sharedManager] turnOnDebugLogging];
}

+(void)turnOffDebugLogging
{
    [[BFBrieflyManager sharedManager] turnOffDebugLogging];
}

+(void)setPerScreenTrackingIdOn:(BOOL)perScreenTrackingId
{
    [BFBrieflyManager sharedManager].perScreenTrackingId = perScreenTrackingId;
}

+(void)setHeatmapsDebugMode:(BFHeatmapsMode)mode
{
    [[BFBrieflyManager sharedManager] setHeatmapsDebugMode:mode];
}

+(void)setStage:(BFStage)stage
{
    [BFBrieflyManager sharedManager].stage = stage;
}

+(BOOL)handleOpeningUrl:(NSURL* _Nullable)url withOptions:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    return [[BFBrieflyManager sharedManager] handleOpeningUrl:url withOptions:options];
}

-(void)addMetricData:(NSNumber* _Nonnull)data
         forObjectId:(NSString* _Nonnull)objectId
         forMetricId:(NSString* _Nonnull)metricId
{
    [self addMetricData:data
                 atTime:[NSDate date]
            forObjectId:objectId
             objectName:nil
            objectGroup:nil
        objectGroupName:nil
            forMetricId:metricId
             metricName:nil
             metricUnit:nil
   defaultClientRequest:nil
      missingDataPolicy:BFMetricMissingDataTreatingPolicyNoData];
}

+(NSString*)currentAccessToken
{
    return [[BFBrieflyManager sharedManager] currentAccessToken];
}

+(void)setAccessToken:(NSString*)accessToken
{
    [[BFBrieflyManager sharedManager] setCurrentAccessToken:accessToken];
}

+(BOOL)widgetsOn
{
    return [[BFBrieflyManager sharedManager] currentAccessToken] != nil;
}

-(void)addMetricData:(NSNumber* _Nonnull)data
              atTime:(NSDate* _Nullable)date
         forObjectId:(NSString* _Nonnull)objectId
          objectName:(NSString* _Nullable)objectName
         objectGroup:(NSString* _Nullable)objectGroup
     objectGroupName:(NSString* _Nullable)objectGroupName
         forMetricId:(NSString* _Nonnull)metricId
          metricName:(NSString* _Nullable)metricName
          metricUnit:(NSString* _Nullable)metricUnit
defaultClientRequest:(NSString* _Nullable)defaultRequest
   missingDataPolicy:(BFMetricMissingDataTreatingPolicy)missingDataPolicy
{
    const NSString * missingDataPolicyStr = missingDataPolicy == BFMetricMissingDataTreatingPolicyNoChanges ? kBFMissingPointsTreatingPolicyNoChanges : kBFMissingPointsTreatingPolicyNoData;
    [[BFBrieflyManager sharedManager] addMetricData:data atTime:date forObjectId:objectId objectName:objectName objectGroup:objectGroup objectGroupName:objectGroupName forMetricId:metricId metricName:metricName metricUnit:metricUnit defaultClientRequest:defaultRequest missingDataPolicy:missingDataPolicyStr withHandler:^(BOOL succeed) {
        // do nothing
    }];
}

@end
