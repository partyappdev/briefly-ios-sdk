//
//  BFBrieflyHacks.m
//  Base64
//
//  Created by Artem Bondar on 15.02.2018.
//

#import "BFBrieflyHacks.h"

@implementation BFBrieflyHacks

+(BOOL)hackIsBlacklistedHeatmapViewId:(NSString*)viewId
{
    return [viewId isEqualToString:@"CatalogViewController"];
}

@end
