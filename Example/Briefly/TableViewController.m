//
//  TableViewController.m
//  Briefly
//
//  Created by Artem Bondar on 14.11.17.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import "TableViewController.h"
#import "TableViewCell.h"
#import "MoreInfoViewController.h"
#import "Visualytics.h"

@interface TableViewController () <BFSortableController, BFSortableDataSource>

@property (strong, nonatomic) BFSorter * sorter;

@end

@implementation TableViewController

-(void)sortableDataSourceEnumerateAllSortableObjects:(void(^)(NSIndexPath *indexPath, NSString *objectId))enumerationHandler
{
    for (NSInteger i = 0; i < self.item_ids.count; ++i) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        NSString * objectId = self.item_ids[i % self.item_ids.count];
        enumerationHandler(indexPath, objectId);
    }
}

-(BFSorter*)sortableControllerSorter
{
    return self.sorter;
}

-(NSString*)sortableDataSourceWidgetGroupId
{
    return @"default";
}
-(void)sortableDataSourceSortOrderChanged
{
    [self.tableView reloadData];
}

-(NSString*)sortableDataSourceAnyObjectId
{
    return self.item_ids.firstObject;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sorter = [BFSorter sorterWithDataSource:self];
    self.item_ids = @[@"11264", @"72", @"10441", @"11913", @"10330", @"133", @"12494", @"10", @"61"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.item_ids.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = (TableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSIndexPath * effectiveIndexPath = [self.sorter sortedIndexPathForItemAtIndexPath:indexPath];
    NSString * itemId = self.item_ids[effectiveIndexPath.item % self.item_ids.count];
    cell.analyticsWidget.analyticsObjectId = itemId;
    cell.analyticsWidget.widgetGroup = @"default";
    cell.analyticsWidget.analyticsDefaultProperty = @"sold.total(month)";
    cell.labelItemDescription.text = [NSString stringWithFormat:@"Information about item_%@", itemId];
    //cell.trackingId = [NSString stringWithFormat:@"bf_cell_%@", itemId];
    [cell setTrackingIdWithClassNameAndObjectId:itemId];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedItemId = self.item_ids[indexPath.item % self.item_ids.count];
    [self performSegueWithIdentifier:@"info" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    [super prepareForSegue:segue sender:sender];
    
    MoreInfoViewController * dest = (MoreInfoViewController*)[segue destinationViewController];
    if ([[segue destinationViewController] isKindOfClass:[MoreInfoViewController class]]) {
        
        dest.itemId = self.selectedItemId;
    }
}

@end
