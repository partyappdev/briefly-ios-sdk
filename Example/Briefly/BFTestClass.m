//
//  BFTestClass.m
//  Briefly_Example
//
//  Created by Artem Bondar on 19.12.2017.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import "BFTestClass.h"

@implementation BFTestClass

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
