//
//  MoreInfoViewController.h
//  Briefly
//
//  Created by Artem Bondar on 14.11.17.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreInfoViewController : UITableViewController

@property (strong, nonatomic) NSString * itemId;

@end
