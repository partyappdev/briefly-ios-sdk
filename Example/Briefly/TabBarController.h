//
//  TabBarController.h
//  Briefly
//
//  Created by Artem Bondar on 14.12.2017.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController

@end
