//
//  TableViewCell.h
//  Briefly
//
//  Created by Artem Bondar on 14.11.17.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Briefly/Briefly.h>

@interface TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel* labelItemDescription;
@property (weak, nonatomic) IBOutlet BFStatisticsLabel* analyticsWidget;

@end
