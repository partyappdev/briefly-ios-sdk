//
//  MoreInfoViewController.m
//  Briefly
//
//  Created by Artem Bondar on 14.11.17.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import "MoreInfoViewController.h"
#import "Visualytics.h"

@interface MoreInfoViewController ()

@property (weak, nonatomic) IBOutlet UILabel* labelItemDescription;
@property (weak, nonatomic) IBOutlet BFStatisticsLabel* analyticsWidget;

@end

@implementation MoreInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setBfTrackingEnabled:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.labelItemDescription.text = [NSString stringWithFormat:@"Information for %@", self.itemId];
    self.analyticsWidget.analyticsObjectId = self.itemId;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)pressDismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
