//
//  BFAppDelegate.m
//  Briefly
//
//  Created by aobondar on 11/13/2017.
//  Copyright (c) 2017 aobondar. All rights reserved.
//

#import "BFAppDelegate.h"
#import "Visualytics.h"

@implementation BFAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Briefly configureWithApiKey:@"pk_live_6b58be5acc284310b3945ebb7b903694" widgetsOn:YES];
    [Briefly setStage:BFStageLive];
    //[Briefly setStage:BFStageTest];
    [Briefly turnOnDebugLogging];
    return YES;
}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    [Briefly handleOpeningUrl:url withOptions:options];
    return YES;
}

@end
