//
//  TableViewController.h
//  Briefly
//
//  Created by Artem Bondar on 14.11.17.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@property (strong, nonatomic) NSString * selectedItemId;
@property (strong, nonatomic) NSArray * item_ids;

@end
