//
//  ImageWithRoundedCorners.m
//  Briefly
//
//  Created by Artem Bondar on 14.11.17.
//  Copyright © 2017 aobondar. All rights reserved.
//

#import "ImageWithRoundedCorners.h"

@implementation ImageWithRoundedCorners

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 12;
    self.clipsToBounds = YES;
}

@end
