//
//  BFAppDelegate.h
//  Briefly
//
//  Created by aobondar on 11/13/2017.
//  Copyright (c) 2017 aobondar. All rights reserved.
//

@import UIKit;

@interface BFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
