//
//  main.m
//  Briefly
//
//  Created by aobondar on 11/13/2017.
//  Copyright (c) 2017 aobondar. All rights reserved.
//

@import UIKit;
#import "BFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BFAppDelegate class]));
    }
}
