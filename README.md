# Briefly ios-sdk

Briefly - is a mobile application analytics platform, which allows you get insights from user data, by composing it into the application's UI.

## Installation

### Using cocoapods

This project in private alpha now, so before start you should get access to the private repo, containing code. To do it mail to techadmin@desq.me. After add to your Podfile:

```
source 'https://desqlabs@bitbucket.org/desqlabs/desqlabs-podspecs.git'

...

pod 'Briefly'
```

## Usage
### Configuration
To start use the library, you firstly should initialize it with your public api key. To do it you can add this code to application's AppDelegate like this:

```objective-c
#import <Briefly/Briefly.h>

...

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[Briefly configureWithApiKey:@"api_key" widgetsOn:NO];
    // Other initialization code
    return YES;
}
```

You can also select a stage, which be used for collecting data: *test* or *live*.
```objective-c
// use a test stage (aka staging)
[Briefly setStage:BFStageTest];

// use a live stage (aka prod)
[Briefly setStage:BFStageLive];
```

### Widgets global configuration
Visibility of all Briefly's widgets are controller by flag in facade class **Briefly**. You set up this flag on configuration call **[Briefly configureWithApiKey: widgetsOn:]**, and you can change it any time later:

```objective-c
BOOL areWidgetsShown = [Briefly widgetsOn];
[Briefly setWidgetsOn:YES];
```


### Add single-value widgets ###
Widgets shows some attribute value of analytics object. The widget's property **analyticsObjectId** should be set to the analytics object id. It can be set in storyboard and/or changed at any time later in runtime.

After setting up binding between object and widget, you can configure the shown on widget value by setting up **analyticsDefaultProperty** value. In runtime the user can change shown value, but by default the user will see the value of property, defined by **analyticsDefaultProperty** . You can set it up with the next values:

* an actual value of object's property: 
``` objective-c
// it will show current residues of the custom
[widgetObj setAnalyticsDefaultProperty:@"residue"]
```
* an agregated value of event's param
``` objective-c
// it will show count of events 'sold' for particular item for last week
[widgetObj setAnalyticsDefaultProperty:@"sold.total(week)"]
```

Optionally you can change appearence by modyfiing **fillColor** and **textColor** properties

To find out more about integration of this widget - you can check the example project.

### Touch heat maps

Not implemented yet

### Sending client statistics

Not implemented yet.


