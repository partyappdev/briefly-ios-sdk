//
//  Visualytics.h
//  Visualytics
//
//  Created by Artem Bondar on 03.04.2018.
//  Copyright © 2018 Artem Bondar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Briefly/Briefly.h>

//! Project version number for Visualytics.
FOUNDATION_EXPORT double VisualyticsVersionNumber;

//! Project version string for Visualytics.
FOUNDATION_EXPORT const unsigned char VisualyticsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Visualytics/PublicHeader.h>


